var queryApp = angular.module("queryApp", ['ngResource']);
queryApp.controller("authCTRL", ['$scope', '$resource', function ($scope, $resource) {
    $scope.login = function () {
        $("#result").hide();
        $("#submit").attr("disabled", true);
        var loginDto = {
            UserId: $("#UserId").val(),
            Password: $("#Password").val()
        };
        var response = $resource(ctx + '/auth/login').save(loginDto);
        response.$promise.then(function (successResponse) {
            $("#submit").removeAttr("disabled");
            $("#result").show();
            $('#result').text(JSON.stringify(successResponse))
        }, function (failResponse) {
            $("#submit").removeAttr("disabled");
            $("#result").show();
            $('#result').text(JSON.stringify(failResponse))
        })
    };
}]);

queryApp.controller("queryCTRL", ['$scope', '$resource', function ($scope, $resource) {
    $scope.getforCSV = function (connectionID, query_id) {
        var response = $resource(ctx + '/exec_csv/' + connectionID + '/' + query_id).get();
        response.$promise.then(function (sucessresponse) {
            console.log(sucessresponse);
        }, function (failresponse) {
            console.log("failure message: " + failresponse);
        })
    };
    $scope.getforJson = function (connectionID, query_id) {
        var response = $resource(ctx + '/exec_json/' + connectionID + '/' + query_id).get();
        response.$promise.then(function (sucessresponse) {
            console.log(sucessresponse.input);
            console.log(sucessresponse.Fields);
        }, function (failresponse) {
            console.log("failure message: " + failresponse);
        })
    };

    $scope.postQuery = function (connectionID) {
        var queryObj = {
            format: "json",
            sql: "SELECT\n\tProd_cd as 'Prod name'\n\t,sum(ORDER_QTY) as 'Prod_Qty'\n\t,sum(SALES_AMT) as 'Prod_Amount'\n\tFROM CRM_SL_ORDER_DETAIL\n\tgroup by prod_cd"
        };
        var response = $resource(ctx + '/exec_sql/' + connectionID).save(queryObj);
        response.$promise.then(function (sucessresponse) {
            console.log(sucessresponse.input);
            console.log(sucessresponse.Fields);
        }, function (failresponse) {
            console.log("failure message: " + failresponse);
        })
    };
}]);

