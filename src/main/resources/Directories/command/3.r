options(scipen=999)
library(ggplot2)
theme_set(theme_bw())  
data('midwest', package = "ggplot2") 
gg <- ggplot(midwest, aes(x=area, y=poptotal)) +   geom_point(aes(col=state, size=popdensity)) +   geom_smooth(method='loess', se=F) +   xlim(c(0, 0.1)) +   ylim(c(0, 500000)) +   labs(subtitle='Area Vs Population',        y='Population',         x='Area',        title='Scatterplot',        caption = 'Source: midwest') 
library(RCurl)
png(tf1 <- tempfile(fileext = '.png'))
plot(gg)  
dev.off() 
txt <- base64Encode(readBin(tf1, 'raw', file.info(tf1)[1, 'size']), 'txt') 
html <- sprintf("<div class=col-lg-6><img    src=data:image/png;base64,%s></div>", txt)   

library(ggplot2)
library(ggExtra)
data(mpg, package="ggplot2")
# mpg <- read.csv("http://goo.gl/uEeRGu")

# Scatterplot
theme_set(theme_bw())  # pre-set the bw theme.
mpg_select <- mpg[mpg$hwy >= 35 & mpg$cty > 27, ]

png(tf2 <- tempfile(fileext = '.png'))
g <- ggplot(mpg, aes(cty, hwy)) + 
  geom_count() + 
  geom_smooth(method="lm", se=F)
gg<-ggMarginal(g, type = "histogram", fill="transparent")
ggMarginal(g, type = "boxplot", fill="transparent") 
plot(gg)
dev.off() 
txt <- base64Encode(readBin(tf2, 'raw', file.info(tf2)[1, 'size']), 'txt') 
html2 <- sprintf("<div class=col-lg-6><img    src=data:image/png;base64,%s></div>", txt)   