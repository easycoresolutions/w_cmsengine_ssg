#!/bin/bash

while getopts ":ls" opt; do
	case $opt in

		s)
			top -b -c -o +%MEM | head -5 | awk -v date_time=`date +"%F,%T"` '
			BEGIN {
				ORS = "";
				print "date,time,up_duration,user,load_average_1min,load_average_5min,load_average_15min,";
				print "tasks,running,sleeping,stopped,zombie,cpu_us,cpu_sy,cpu_ni,cpu_id,cpu_wa,cpu_hi,cpu_si,cpu_st,";
				print "Kib_Mem,Kib_Mem_free,Kib_Mem_used,Kib_Mem_buffer,Kib_Swap,Kib_Swap_used,Kib_Swap_free,Kib_Swap_Avail\n";
			}
			NR==1 {													# first line format different from others
				print date_time ","									# date/time from date command
				for (i=5; i<=NF-7; i++){							# uptime is a variable number of fields 
					gsub(/\,/, "", $i);								# remove comma, use space as seperator
					print $i;
					if (i!=NF-7) print " ";
				}				
				print "," $(NF-6) "," $(NF-2) $(NF-1) $NF ",";		# user,load_average_1min,load_average_5min,load_average_15min
			}
			NR!=1{													# print only numeric fields for other lines
				for (i=1; i<=NF; i++)
					if ($i ~ /^[0-9.]+$/)
						print $i ",";
	  		}'
		;;

		l)
			top -b -o +%CPU | head -100 | tail -94 | awk '
				BEGIN {
					OFS=",";										# force reformat of output to csv
				}
				{
					$1=$1
				}1'
     	;;
		
		\?)
			echo "invalid option: -$OPTARG" >&2
      	;;

	esac
done
