#!/bin/bash

control_file=$1

[ -z $control_file ] && { echo "control file required"; exit 1; }
[ ! -f $control_file ] && { echo "control file not found"; exit 1; }

echo "proc_name,pid,%CPU,status,%MEM,TIME,CMD"

ps_ef=`ps -ef`

IFS=","; while read proc_name search_string
do
	search_string=$(echo "$search_string" | awk '{$1=$1};1')
	output=$(grep -- "$search_string" <<< "$ps_ef")
	if [[ -n $output ]]; then
	    while IFS= read -r line; do
	    	pid=$(IFS=" "; arr=($line); echo ${arr[1]})
	    	ps --no-headers -p $pid -o pid,%cpu,%mem,time,cmd |\
	     	awk -v proc_name="$proc_name" 'BEGIN {OFS=","}{print proc_name, $1, $2, "live", $3, $4, $5}'
	    done <<< "$output"
	else
	    echo "$proc_name,NA,NA,NA,NA,NA,NA"
	fi

done < $control_file