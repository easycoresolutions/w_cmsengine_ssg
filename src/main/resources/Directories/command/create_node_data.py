#!python3.6
# -*- coding: UTF-8 -*-
import codecs
import datetime
import jaydebeapi
import json
import os
import pandas as pd
import sys

virtual_id = "V_" + datetime.datetime.today().strftime('%Y%m%d%H%M%S')
os.chdir('/root/apache-tomcat-8.0.47/webapps/Directories/command/') 
os.environ['CLASSPATH'] = "/root/apache-tomcat-8.0.47/webapps/Directories/command/jdbc/mariadb-java-client-2.4.1.jar:/root/apache-tomcat-8.0.47/webapps/Directories/command/jdbc//mariadb-java-client-2.4.1.jar:/root/apache-tomcat-8.0.47/webapps/Directories/command/jdbc//ojdbc6.jar:/root/apache-tomcat-8.0.47/webapps/Directories/command/jdbc/mssql-jdbc-7.2.2.jre8.jar";

#logging
log_location = os.path.dirname((os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
log_location = os.path.join(log_location, "log")
logger = codecs.open(os.path.join(log_location, "create_node_data_log" + '.log'), "a", "utf-8")

usage = "Usage: " + 'python create_node_data.py "CAMP_ID: C0001; NODE_ID: N0000001; NAME: targeting generation(oracle -> mssql) ;'\
                        + 'USER: mss; IN_CONNECTIONID: oracle1; IN_DBMS_TYPE: ORACLE; '\
                        + 'IN_SQL: select CUSTCODE_NEW from MW_TRAN where ROWNUM < 100; '\
                        + 'OUT_CONNECTIONID: mssql24; OUT_DBMS_TYPE: MSSQL; OUT_TABLE_NAME: C0001_N000001; '\
                        + 'MBR_NO_TYPE:VARCHAR(24);'\
                        + 'LOG_TABLE: Z_LOG"'

 

ideal_params = ["CAMP_ID", "NODE_ID", "NAME", "USER", "IN_CONNECTIONID", "IN_DBMS_TYPE", "IN_SQL", \
                "OUT_CONNECTIONID", "OUT_DBMS_TYPE", "OUT_TABLE_NAME", "LOG_TABLE", "MBR_NO_TYPE"]

try:
   parameters = sys.argv[1]
   parameters = parameters.split(";")
   parameters = {x.split(":")[0].strip(): x.split(":")[1].strip() for x in parameters if x.strip()}
except:
   print(usage)
   sys.exit(1)

for i in ideal_params:
   if i not in parameters.keys():
      print("parameter - %s is not provided!" % (i))
      logger.write("parameter - %s is not provided!\n" % (i))
      sys.exit(1)
       
#getting in connection info
connection_id_location = os.path.dirname((os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
connection_id_location = os.path.join(connection_id_location, "connection_info")
in_conn_info = open(os.path.join(connection_id_location, parameters["IN_CONNECTIONID"] + ".txt"), "r").read().strip()
in_conn_info = json.loads(in_conn_info)

if parameters["IN_DBMS_TYPE"] == "MSSQL":
   in_con = jaydebeapi.connect('com.microsoft.sqlserver.jdbc.SQLServerDriver', in_conn_info["URL"].strip(), {'user': in_conn_info["ID"].strip(), 'password': in_conn_info["PW"].strip(), 'CHARSET': 'UTF8'})
elif parameters["IN_DBMS_TYPE"] == "ORACLE":
   in_con = jaydebeapi.connect('oracle.jdbc.driver.OracleDriver', in_conn_info["URL"].strip(), {'user': in_conn_info["ID"].strip(), 'password': in_conn_info["PW"].strip(), 'CHARSET': 'UTF8'})
elif parameters["IN_DBMS_TYPE"] == "MARIADB":
   in_con = jaydebeapi.connect('org.mariadb.jdbc.Driver', in_conn_info["URL"].strip(), {'user': in_conn_info["ID"].strip(), 'password': in_conn_info["PW"].strip(), 'CHARSET': 'UTF8'})

# getting out connection info
connection_id_location = os.path.dirname((os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
connection_id_location = os.path.join(connection_id_location, "connection_info")
out_conn_info = open(os.path.join(connection_id_location, parameters["OUT_CONNECTIONID"] + ".txt"), "r").read().strip()
out_conn_info = json.loads(out_conn_info)
  
if parameters["OUT_DBMS_TYPE"] == "MSSQL":
   out_con = jaydebeapi.connect('com.microsoft.sqlserver.jdbc.SQLServerDriver', out_conn_info["URL"].strip(), {'user': out_conn_info["ID"].strip(), 'password': out_conn_info["PW"].strip(), 'CHARSET': 'UTF8'})
elif parameters["OUT_DBMS_TYPE"] == "ORACLE":
   out_con = jaydebeapi.connect('oracle.jdbc.driver.OracleDriver', out_conn_info["URL"].strip(), {'user': out_conn_info["ID"].strip(), 'password': out_conn_info["PW"].strip(), 'CHARSET': 'UTF8'})
elif parameters["OUT_DBMS_TYPE"] == "MARIADB":
   out_con = jaydebeapi.connect('org.mariadb.jdbc.Driver', out_conn_info["URL"].strip(), {'user': out_conn_info["ID"].strip(), 'password': out_conn_info["PW"].strip(), 'CHARSET': 'UTF8'})
 
def write_to_log_table(LOG_KIND, LOG_CD, LOG_MSG, LOG_DATA, LID):
   PROGRAM_ID = "'%s'" % (parameters["CAMP_ID"] + parameters["NODE_ID"] + virtual_id)
   LOG_SEQ = "''"
   LOG_KIND = "'%s'" % (LOG_KIND)
   LOG_CD = "'%s'" % (LOG_CD)
   LOG_MSG = "'%s'" % (LOG_MSG.replace("'", ''))
   LOG_DATA = "'%s'" % (LOG_DATA)
   CAMP_ID = "'%s'" % (parameters["CAMP_ID"])
   LID = LID
   LOAD_DTTM = "'%s'" % (datetime.datetime.today().strftime('%Y%m%d%H%M%S'))
   
   #print to console
   # print("%s %s %s %s %s %s %s %s" % (PROGRAM_ID, LOG_KIND, LOG_CD, LOG_MSG, LOG_DATA, CAMP_ID, LID, LOAD_DTTM))
   logger.write("%s, %s, %s, %s, %s, %s, %s, %s, %s" % (PROGRAM_ID.replace("'", ""), LOG_KIND.replace("'", ""),\
                                                        LOG_CD.replace("'", ""), LOG_MSG.replace("'", ""), LOG_DATA.replace("'", ""),\
                                                         CAMP_ID.replace("'", ""), LID, LOAD_DTTM.replace("'", ""), os.linesep))
   #insert into LOG_TABLE
   try:
      cur = out_con.cursor()
      insert_sql = "INSERT INTO %s (PROGRAM_ID, LOG_KIND, LOG_CD, LOG_MSG, \
                  LOG_DATA, CAMP_ID, LID, LOAD_DTTM) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)" \
                  % (parameters["LOG_TABLE"], PROGRAM_ID, LOG_KIND, LOG_CD, LOG_MSG, LOG_DATA, CAMP_ID, LID, LOAD_DTTM)
      # print(insert_sql)
      res = cur.execute(insert_sql)
      cur.close()
      # print("LOG INSERTION success")
   except Exception as e:
      pass
      # f = codecs.open("error.txt", "w", "utf-8")
      # f.write(str(e))
      # f.close()
      # print("failed: ", str(e))

write_to_log_table(LOG_KIND="S", LOG_CD="", LOG_MSG="", LOG_DATA="", LID=0)

#check out table name, drop create or create -- basically create new everytime
#drop table
sql = "DROP TABLE %s" % (parameters["OUT_TABLE_NAME"])
cur = out_con.cursor()
try:
   res = cur.execute(sql)
   # print("drop table success")
except Exception as e:
   #ignore because most likely the error is for table doesn't exist
   pass

#create out table
sql = "CREATE TABLE %s (MBR_NO %s NOT NULL)" % (parameters["OUT_TABLE_NAME"], parameters["MBR_NO_TYPE"])
 
cur = out_con.cursor()
try:
   res = cur.execute(sql)
   # print("create table success")
except Exception as e:
   pass
   # f = codecs.open("error.txt", "w", "utf-8")
   # f.write(str(e))
   # f.close()
   # print("create table failed", str(e))

DONE = False
#get in table records
try:
   df = pd.read_sql(parameters["IN_SQL"], in_con)
except Exception as e:
   sys.stdout.write("failed, %s" % (str(e)))
   write_to_log_table(LOG_KIND="X", LOG_CD="fail", LOG_MSG=str(e), LOG_DATA=parameters["IN_SQL"], LID=0)
   DONE = True
   
if DONE == False:
   in_records = df.to_csv(encoding='utf-8', header = True, doublequote = True, sep=',', index=False)
   # print(in_records)
   in_records = [x for x in in_records.split(os.linesep) if x]
   in_records = in_records[1:]
   in_records =[x.split(",") for x in in_records]
   in_records =[",".join(["'%s'" % y for y in x]) for x in in_records]
   # print(in_records)

   #insert into OUT_TABLE
   cur = out_con.cursor()
   out_con.jconn.setAutoCommit(False)

   try:
      for in_record in in_records:
         insert_sql = "INSERT INTO %s (MBR_NO) VALUES (%s)" % (parameters["OUT_TABLE_NAME"], in_record)
         res = cur.execute(insert_sql)
         # print("inserted %s" % (in_record))
      cur.close()
      #sys.stdout.write("success, %s" % (len(in_records)))
      print "success,",len(in_records)
      write_to_log_table(LOG_KIND="X", LOG_CD="success", LOG_MSG="", LOG_DATA=parameters["IN_SQL"], LID=len(in_records))
   except Exception as e:
      try:
         out_con.rollback()
      except:
         pass
      # f = codecs.open("error.txt", "w", "utf-8")
      # f.write(str(e))
      # f.close()
      sys.stdout.write("failed, %s" % (str(e)))
      # print("failed: ", str(e))
      write_to_log_table(LOG_KIND="X", LOG_CD="fail", LOG_MSG=str(e), LOG_DATA=parameters["IN_SQL"], LID=0)
   else:
      out_con.commit()
   finally:
       cur.close()
       out_con.jconn.setAutoCommit(True)
   
#verify if inserted
# df = pd.read_sql("select * from %s" % (parameters["OUT_TABLE_NAME"]), out_con)
# print("verification: OUT_TABLE records")
# sys.stdout.write(df.to_csv(encoding='utf-8', header = True,\
#          doublequote = True, sep=',', index=False))
# 

write_to_log_table(LOG_KIND="E", LOG_CD="XXX", LOG_MSG="", LOG_DATA="", LID=0)

#verify log
# sql = "SELECT * FROM %s" % (parameters["LOG_TABLE"])
# df = pd.read_sql(sql, out_con)
# print("verification: LOG records")
# sys.stdout.write(df.to_csv(encoding='utf-8', header = True,\
#          doublequote = True, sep=',', index=False))