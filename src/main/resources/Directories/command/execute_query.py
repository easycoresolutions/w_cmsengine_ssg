import json
import sys
import cx_Oracle
import os
import datetime

print(len (sys.argv))
print(sys.argv[0])
print(sys.argv[1])
print(sys.argv[2])
if len (sys.argv) != 3:
    print("Usage: " + "python python_query_executor.py id: 123au3e; owner: mss; connectionid: oracle1; " + \
          "name: my_sql_script; sql: select * from mw_tran where ROWNUM < 100")
    sys.exit(1)
    
parameters = sys.argv[1]
parameters = parameters.split(";")
parameters = {x.split(":")[0].strip(): x.split(":")[1].strip() for x in parameters}

#getting connection info
connection_id_location = os.path.dirname((os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
connection_id_location = os.path.join(connection_id_location, "connection_info")
conn_info = open(os.path.join(connection_id_location, parameters["connectionid"] + ".txt"), "r").read().strip()
conn_info = json.loads(conn_info)
 
#logging
log_location = os.path.dirname((os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
log_location = os.path.join(log_location, "log")
logger = open(os.path.join(log_location, parameters["id"] + '.log'), "a")

con = cx_Oracle.connect(conn_info["ID"].strip()+'/'+conn_info["PW"]+'@'+conn_info["URL"])

logger.write("%s, %s, %s, %s, %s, %s" % ("-", parameters["id"], datetime.datetime.now(), 'STARTED', parameters["owner"], "-") + os.linesep)

cur = con.cursor()

try:
   cur.execute(parameters["sql"])
   for result in cur:
       print(str(result).replace("(", "").replace(")", ""))
   logger.write("%s, %s, %s, %s, %s, %s" % ("success", parameters["id"], datetime.datetime.now(), 'EXECUTION', parameters["owner"], str(cur.rowcount)) + os.linesep)
except Exception as e:
   print(e)
   logger.write("%s, %s, %s, %s, %s, %s" % ("error: " + str(e), parameters["id"], datetime.datetime.now(), 'EXECUTION:', parameters["owner"], "-") + os.linesep)

cur.close()
con.close()

logger.write("%s, %s, %s, %s, %s, %s" % ("-", parameters["id"], datetime.datetime.now(), 'ENDED', parameters["owner"], "-") + os.linesep)
logger.close()
