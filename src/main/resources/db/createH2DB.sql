CREATE TABLE Tokens_BackListing (
  id     INTEGER PRIMARY KEY auto_increment,
  token  VARCHAR(1024),
  userId VARCHAR(16)
);