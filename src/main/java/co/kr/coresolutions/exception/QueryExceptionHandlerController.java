package co.kr.coresolutions.exception;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

//@ControllerAdvice
public class QueryExceptionHandlerController {

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public HttpEntity<String> handleException2(Exception exception) {
        return new HttpEntity<String>(exception.getMessage());
    }

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public HttpEntity<String> handleException(MethodArgumentNotValidException exception) {
        return new HttpEntity<String>(exception.getMessage());
    }
}
