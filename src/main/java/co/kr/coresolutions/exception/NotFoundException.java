package co.kr.coresolutions.exception;

public class NotFoundException extends RuntimeException {
    public NotFoundException(String entityName, String id) {
        super(entityName + " with id = " + id + " not found.");
    }

    public NotFoundException(String message) {
        super(message);
    }
}
