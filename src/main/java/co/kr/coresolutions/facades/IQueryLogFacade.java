package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.dtos.LogDto;

public interface IQueryLogFacade {
    void send(LogDto logDto);
}
