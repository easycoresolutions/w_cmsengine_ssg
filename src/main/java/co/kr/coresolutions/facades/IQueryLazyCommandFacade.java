package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.dtos.LazyCommandDto;
import org.json.JSONArray;

public interface IQueryLazyCommandFacade {
    CustomResponseDto send(LazyCommandDto lazyCommandDto);

    JSONArray getAll(String queueName);

    CustomResponseDto remove(String queueName, String commandID);
}
