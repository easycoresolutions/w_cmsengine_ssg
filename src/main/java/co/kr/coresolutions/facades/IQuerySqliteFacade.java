package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.TableLoad;
import co.kr.coresolutions.model.dtos.SqliteFileRepoDto;

public interface IQuerySqliteFacade {
    Object createDBFile(SqliteFileRepoDto sqliteFileRepoDto);

    Object deleteDbFile(String userID, String dbFileName);

    Object deleteTableFromDBfile(String userID, String tableName);

    Object tableLoad(TableLoad tableLoad);

    Object getDBFiles(String userID);

    Object activate(String userID, String dbFile);

    Object getActiveDbms(String userID);
}
