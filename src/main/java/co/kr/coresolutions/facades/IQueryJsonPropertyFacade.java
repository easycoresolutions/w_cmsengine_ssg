package co.kr.coresolutions.facades;

import com.fasterxml.jackson.databind.JsonNode;

public interface IQueryJsonPropertyFacade {
    Object post(String user, String subsystem, String keyName, JsonNode objectNode);

    Object delete(String user, String subsystem, String keyName, int seqNum);

    Object get();

    Object findBy(String user, String subsystem, String keyName);
}
