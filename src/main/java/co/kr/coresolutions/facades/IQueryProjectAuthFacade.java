package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.dtos.ProjectAuthDto;
import com.fasterxml.jackson.databind.JsonNode;

public interface IQueryProjectAuthFacade {
    CustomResponseDto saveProject(JsonNode jsonObject, String connectionID, String projectID);

    CustomResponseDto deleteRow(ProjectAuthDto jsonNode, String connectionID, String projectID);

    CustomResponseDto getDetails(JsonNode jsonNode, String connectionID, String projectID);
}
