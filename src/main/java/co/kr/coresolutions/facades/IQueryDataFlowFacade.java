package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.model.dtos.FileBatchDto;

import java.io.IOException;

public interface IQueryDataFlowFacade {

    ResponseDto fileBatchUpload(String userId, FileBatchDto fileBatchDto);

    String findOne(String userId, String fileName) throws IOException;

    String findOneLog(String userId, String fileName);

    ResponseDto fileDelete(String userId, String fileName);

    ResponseDto submit(String userId, String fileName);
}
