package co.kr.coresolutions.facades;

import co.kr.coresolutions.enums.QueuePushLevel;

public interface IQueryQueuePushedFacade {
    Object getQueues(QueuePushLevel queuePushLevel);

    Object deleteQueueQueryID(QueuePushLevel queuePushLevel, String queryID);
}
