package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.model.Command;
import co.kr.coresolutions.model.dtos.CommandDto;
import co.kr.coresolutions.model.dtos.CommandTemplate;
import co.kr.coresolutions.model.dtos.FileDto;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface IQueryCommandFacade {

    ResponseDto fileUpload(final FileDto fileDto) throws IOException;

    ResponseDto fileDelete(String userId, String fileNameExt) throws IOException;

    String findAll(String userId) throws IOException;

    ResponseDto execute(CommandDto commandDto, String userId, HttpServletResponse response) throws IOException;

    List<Command.CommandDetails> monitor();

    List monitorWaiting();

    ResponseDto deleteCommand(String commandid);

    String getOwner();

    CustomResponseDto executeETL(String directory, String xml, CommandTemplate commandTemplate, HttpServletResponse response);

    CustomResponseDto executeETLFromDBMS(String connectionID, String projectID, String scriptID, CommandTemplate commandTemplate, HttpServletResponse response);
}
