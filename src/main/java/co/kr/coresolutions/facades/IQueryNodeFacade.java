package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.dtos.NodeDto;

@FunctionalInterface
public interface IQueryNodeFacade {
    CustomResponseDto execNodes(String campId, String stepId, NodeDto nodeDto, String urlPath);
}
