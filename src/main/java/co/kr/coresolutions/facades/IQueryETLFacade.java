package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.CustomResponseDto;

import java.io.IOException;

public interface IQueryETLFacade {
    CustomResponseDto exchange(String connectionID, String projectID, String scriptID, String userID, String commandID) throws IOException;

    void logToDBTriggered(String startDttm, String query, int querySeq, String partMessage, String connectionID, String internalConnectionId, String inputSCHEMA, String projectID,
                          String scriptID, String status, String logMessage, long duration);
}
