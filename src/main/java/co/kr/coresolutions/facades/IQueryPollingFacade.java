package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.dtos.PollingDto;

@FunctionalInterface
public interface IQueryPollingFacade {
    CustomResponseDto pooling(PollingDto pollingDto);
}
