package co.kr.coresolutions.facades;

public interface IQueryChartMonitorFacade {
    Object listTemplates();
    Object deleteTemplate(String queryID);
}
