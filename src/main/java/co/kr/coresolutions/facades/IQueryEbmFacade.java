package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.CustomResponseDto;
import com.fasterxml.jackson.databind.JsonNode;

@FunctionalInterface
public interface IQueryEbmFacade {
    CustomResponseDto BizCommon(String connectionID, JsonNode jsonObject, boolean isEbm);
}
