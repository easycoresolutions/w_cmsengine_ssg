package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.model.dtos.ContainerProjectDto;

public interface IQueryContainerProjectFacade {
    ResponseDto saveContainerProject(ContainerProjectDto containerProjectDto, String userID, String projectID);

    ResponseDto deleteContainerProject(String userID, String projectID);

    ResponseDto getContainerProject(String userID, String projectID);

    ResponseDto getContainerProjects(String userID);
}
