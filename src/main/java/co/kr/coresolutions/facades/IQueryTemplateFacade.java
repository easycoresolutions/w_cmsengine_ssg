package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.dtos.Chart3Dto;
import co.kr.coresolutions.model.dtos.Chart4Dto;
import co.kr.coresolutions.model.dtos.CommandTemplateLiquid;

import javax.servlet.http.HttpServletRequest;

public interface IQueryTemplateFacade {
    Object execChartInternal(Chart3Dto chartDto, HttpServletRequest httpServletRequest);

    Object execCommandTemplate(HttpServletRequest httpServletRequest, CommandTemplateLiquid commandTemplateLiquid);

    Object execChartInternalSelectJson(Chart4Dto chartDto, String connectionId);
}
