package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.model.dtos.JsonDataDto;

public interface IQueryRabbitMQFacade {

    ResponseDto publish(JsonDataDto jsonDataDto, String connectionId, String queueName);

    ResponseDto findAllQueues(String connectionId);

    ResponseDto findAllExchanges(String connectionId, String queueName);
}
