package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.dtos.CommentDto;

public interface IQueryCommentFacade {
    Object addComment(String connectionID, String projectID, CommentDto commentDto);

    Object listComments(String connectionID, String projectID);

    Object deleteComment(String connectionID, String projectID);
}
