package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.CustomResponseDto;
import com.fasterxml.jackson.databind.JsonNode;

public interface IQueryProjectFacade {
    CustomResponseDto saveProject(JsonNode jsonObject, String connectionID, String tableName, String projectID);

    CustomResponseDto deleteRow(JsonNode jsonNode, String connectionID, String tableName, String projectID);

    CustomResponseDto getRow(String connectionID, String tableName, String projectID);

    CustomResponseDto getRows(String connectionID, String tableName, String userID);
}
