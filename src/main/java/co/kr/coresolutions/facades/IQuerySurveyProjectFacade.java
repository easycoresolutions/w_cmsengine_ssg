package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.model.dtos.SurveyProjectDto;

public interface IQuerySurveyProjectFacade {
    ResponseDto saveSurveyProject(SurveyProjectDto surveyProjectDto, String userID, String projectID);

    ResponseDto deleteSurveyProject(String userID, String projectID);

    ResponseDto getSurveyProject(String userID, String projectID);

    ResponseDto getSurveyProjects(String userID);
}
