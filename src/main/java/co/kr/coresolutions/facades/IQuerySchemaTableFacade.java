package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.dtos.SchemaDto;

public interface IQuerySchemaTableFacade {
    Object saveSchema(SchemaDto schemaDto, String connectionID, String schemaID);

    Object deleteRow(String connectionID, String schemaID);

    CustomResponseDto getSchema(String connectionID, String schemaID);
}
