package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.dtos.LiquidTemplateDTO;
import co.kr.coresolutions.model.dtos.LiquidTemplateQueryDTO;
import co.kr.coresolutions.model.dtos.MustacheTemplateDTO;

public interface IQueryLiquidFacade {
    Object convertTemplate(MustacheTemplateDTO mustacheTemplateDTO);

    Object convertLiquidTemplate(LiquidTemplateDTO liquidTemplateDTO);

    Object convertLiquidTemplateQuery(LiquidTemplateQueryDTO liquidTemplateQueryDTO);
}
