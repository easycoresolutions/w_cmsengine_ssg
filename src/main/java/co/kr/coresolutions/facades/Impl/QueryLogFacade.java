package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.facades.IQueryLogFacade;
import co.kr.coresolutions.model.dtos.LogDto;
import co.kr.coresolutions.service.QueryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryLogFacade implements IQueryLogFacade {
    private final QueryService queryService;

    @Override
    public void send(LogDto logDto) {
        queryService.addLogging("", logDto.getMessageKey() + "\t" + logDto.getUserID() + "\t" + logDto.getMessage() + "\n");
    }
}

