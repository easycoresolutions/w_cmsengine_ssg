package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.enums.QueuePushLevel;
import co.kr.coresolutions.facades.IQueryPythonFacade;
import co.kr.coresolutions.facades.IQueryQueuePushedFacadeWorker;
import co.kr.coresolutions.model.dtos.SocketDto;
import co.kr.coresolutions.model.dtos.SocketLoginDto;
import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServicePythonSocketClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryPythonFacade implements IQueryPythonFacade, IQueryPythonFacade.IAdmin, IQueryPythonFacade.IUser, IQueryPythonFacade.IQueue {
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd:HHmmss");
    private final QueryService queryService;
    private final Constants constants;
    private final QueryServicePythonSocketClient queryServicePythonSocketClient;
    private static final Map<String, String> sessionQueueConnection = new HashMap<>();
    private final IQueryQueuePushedFacadeWorker queryQueuePushedFacadeWorker;

    @Override
    public Object open(String owner, String connectionID) {
        String partMessage = "/python/{" + connectionID + "}/open/{" + owner + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }
        return queryServicePythonSocketClient
                .requestResponse("openSession", queryService.getInfoConnection(connectionID), SocketDto.builder().owner(owner).build());
    }

    @Override
    public Object close(String owner, String connectionID) {
        String partMessage = "/python/{" + connectionID + "}/close/{" + owner + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }
        return queryServicePythonSocketClient.requestResponse("closeSession"
                , queryService.getInfoConnection(connectionID), SocketDto.builder().owner(owner).build());
    }

    @Override
    public Object evaluate(SocketDto socketDto, String connectionID) {
        String url = "python/{" + connectionID + "}/evaluate";
        String partMessage = url + " triggered, ";
        if (socketDto.getQueuePushLevel() != null) {
            String queryID = socketDto.getQueryID();
            QueuePushLevel queuePushLevel = socketDto.getQueuePushLevel();
            if (queryID == null || queryID.isEmpty()) {
                queryID = UUID.randomUUID().toString();
                socketDto.setQueryID(queryID);
            }
            sessionQueueConnection.put(queryID, connectionID);
            queryQueuePushedFacadeWorker.addToQueue(queuePushLevel, queryID, url, socketDto);
            queryService.addLogging("POST",
                    partMessage + "{" + queryID + "} is pushed into {" + queuePushLevel.getName() + "}");
            return CustomResponseDto
                    .builder()
                    .Success(true)
                    .message("{" + queryID + "} is pushed into {" + queuePushLevel.getName() + "}")
                    .build();
        } else {
            return executeEvaluatePython(url, socketDto, connectionID);
        }
    }

    @Override
    public Object executeEvaluatePython(String url, SocketDto socketDto, String connectionID) {
        String partMessage = url + " triggered, ";
        if (connectionID == null) {
            connectionID = sessionQueueConnection.get(socketDto.getQueryID());
            sessionQueueConnection.remove(socketDto.getQueryID());
        }
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }
        QueryServicePythonSocketClient.sessionPythonSocketRunningUsers.put(socketDto.getOwner(), simpleDateFormat.format(new Date()));
        Object requestResponse = queryServicePythonSocketClient.requestResponse("evaluate", queryService.getInfoConnection(connectionID), socketDto);
        SocketDto.OutputInfo outputInfo = socketDto.getOutputInfo();
        if (socketDto.isOutputEnabled() && outputInfo != null) {
            String outputContent = null;
            if (requestResponse instanceof CustomResponseDto) {
                CustomResponseDto customResponseDto = ((CustomResponseDto) requestResponse);
                if (customResponseDto.getSuccess()) {
                    outputContent = customResponseDto.getMessage();
                }
            } else {
                outputContent = requestResponse.toString();
            }
            //means success
            if (outputContent != null) {
                try {
                    String dir = constants.getSpawnDir()
                            + socketDto.getUserID() + File.separator
                            + outputInfo.getDir().replaceAll("TODTODAY", simpleDateFormat.format(new Date()))
                            + File.separator;
                    Files.createDirectories(Paths.get(dir));
                    Files.write(Paths.get(dir + outputInfo.getFilename() + "." + outputInfo.getExt()),
                            outputContent.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("title", outputInfo.getTitle());
                    jsonObject.put("desc", outputInfo.getDesc());
                    jsonObject.put("filename", outputInfo.getFilename() + "." + outputInfo.getExt());
                    jsonObject.put("seq", outputInfo.getSeq());
                    Files.write(Paths.get(dir + outputInfo.getFilename() + ".desc"),
                            jsonObject.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                    return CustomResponseDto.ok();
                } catch (IOException e) {
                    return CustomResponseDto.builder().message(e.getMessage()).build();
                }
            }
        }
        return requestResponse;
    }

    @Override
    public Object listUsers() {
        String partMessage = "/python/users triggered, ";
        queryService.addLogging("GET", partMessage + "success");
        return QueryServicePythonSocketClient.sessionPythonSocketRequester.keySet();
    }

    @Override
    public Object listUsersRunning(String connectionID) {
        String partMessage = "/python/{" + connectionID + "}/running triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("GET", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }

        queryService.addLogging("GET", partMessage + "success");
        return queryServicePythonSocketClient.requestResponse("listUsersRunning", queryService.getInfoConnection(connectionID), SocketDto.builder().build());
    }

    @Override
    public Object deleteUser(SocketLoginDto socketLoginDto, String connectionID, String userID) {
        String partMessage = "/python/{" + connectionID + "}/user/{" + userID + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("DELETE", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }

        queryService.addLogging("DELETE", partMessage + "success");
        return queryServicePythonSocketClient.fireAndForget("deleteRunningUser", queryService.getInfoConnection(connectionID), socketLoginDto, userID);
    }
}
