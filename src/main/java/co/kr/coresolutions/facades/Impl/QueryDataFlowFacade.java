package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.HelperUtil;
import co.kr.coresolutions.commons.ResponseCodes;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.converters.QueryDataFlowConverter;
import co.kr.coresolutions.enums.Extension;
import co.kr.coresolutions.exception.ValidationException;
import co.kr.coresolutions.facades.IQueryDataFlowFacade;
import co.kr.coresolutions.model.dtos.FileBatchDto;
import co.kr.coresolutions.model.dtos.HttpDto;
import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryDataFlowHttp;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceDataFlow;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryDataFlowFacade implements IQueryDataFlowFacade {
    private final Constants constants;
    private String dataFlowDir;
    private static String pattern1 = "SUBMIT";
    private static String pattern2 = "SUBMIT_END";
    private static String pattern1_1 = "BODY";
    private static String pattern2_1 = "BODY_END";
    private static String pattern1_2 = "API";
    private static String pattern1_3 = "METHOD";
    private final QueryDataFlowConverter queryDataFlowConverter;
    private final QueryDataFlowHttp queryDataFlowHttp;
    private final QueryServiceDataFlow queryServiceDataFlow;
    private final QueryService queryService;

    @PostConstruct
    public void init() {
        dataFlowDir = constants.getDataFlowDir();
    }

    @Override
    public ResponseDto fileBatchUpload(String userId, FileBatchDto fileBatchDto) {

        try {
            if (!Files.isDirectory(Paths.get(dataFlowDir + userId)))
                Files.createDirectory(Paths.get(dataFlowDir + userId));
            Files.write(Paths
                            .get(dataFlowDir + userId + File.separator + fileBatchDto.getName() + Extension.SCR.getName()),
                    fileBatchDto.getFile().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE);
            queryService.addLogging("POST", "file Upload successfully");

        } catch (Exception e) {
            e.printStackTrace();
            queryService.addLogging("POST", "error upload file");
            return ResponseDto.builder().errorCode(HttpStatus.BAD_REQUEST.value()).message(e.getMessage()).build();
        }

        return ResponseDto.ok();
    }

    @Override
    public String findOne(String userId, String fileName) {
        try {
            queryService.addLogging("GET", "read file successfully from data flow dir");
            return new String(Files.readAllBytes(Paths.get(dataFlowDir + userId + File.separator + fileName + Extension.SCR.getName())), StandardCharsets.UTF_8);
        } catch (IOException e) {
            return "file read error\t" + e.getMessage();
        }
    }

    @Override
    public String findOneLog(String userId, String fileName) {
        try {
            queryService.addLogging("GET", "read file log successfully from data flow dir");
            return new String(Files.readAllBytes(Paths.get(dataFlowDir + userId + File.separator + fileName + Extension.LOG.getName())), StandardCharsets.UTF_8);
        } catch (IOException e) {
            return "file log read error\t" + e.getMessage();
        }
    }

    @Override
    @SneakyThrows
    public ResponseDto fileDelete(String userId, String fileName) {
        try {
            Files.delete(Paths.get(dataFlowDir + userId + File.separator + fileName + Extension.SCR.getName()));
            Files.delete(Paths.get(dataFlowDir + userId + File.separator + fileName + Extension.LOG.getName()));
            queryService.addLogging("DELETE", "file and log deleted successfully with name\t" + fileName);
        } catch (IOException e) {
            queryService.addLogging("DELETE", "file and log trying deleted coz error with name\t" + fileName);
            return ResponseDto.builder().errorCode(HttpStatus.NOT_FOUND.value()).build();
        }

        return ResponseDto.ok();
    }

    @Override
    public ResponseDto submit(String userId, String fileName) {
        String content = findOne(userId, fileName);
        if (content.startsWith("file read error")) return ResponseDto.NOT_EXISTS;


        try {
            Arrays.asList(StringUtils.substringsBetween(content, pattern1, pattern2)).forEach(s -> {

                String oneBatch = pattern1.concat(s.concat(pattern2));
                HttpDto httpDto;

                try {
                    httpDto = queryDataFlowConverter
                            .convertToDto(StringUtils.substringBetween(oneBatch, pattern1_2, System.lineSeparator()).replaceFirst(":", "").trim(),
                                    StringUtils.substringBetween(oneBatch, pattern1_3, System.lineSeparator()).replaceFirst(":", "").trim(),
                                    StringUtils.substringBetween(oneBatch, pattern1_1, pattern2_1).replaceFirst(":", "")
                            );
                } catch (Exception e) {
                    throw new ValidationException();
                }

                String logName = StringUtils.substringBetween(oneBatch, pattern1, System.lineSeparator()).replaceFirst(":", "").trim();

                String startDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern(HelperUtil.dataFlowLogDateFormat));

                try {
                    queryServiceDataFlow.log(userId, fileName, logName, startDate, queryDataFlowHttp.execute(httpDto));
                } catch (Exception e) {
                    queryServiceDataFlow.log(userId, fileName, logName, startDate, ResponseDto.builder().errorCode(ResponseCodes.GENERAL_ERROR).message(e.getMessage()).build());
                }

            });
        } catch (ValidationException e) {
            return ResponseDto.ERROR_PARSE;
        }


        return ResponseDto.ok();
    }
}
