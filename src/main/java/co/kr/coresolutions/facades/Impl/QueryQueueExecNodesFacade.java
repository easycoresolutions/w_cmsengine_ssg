package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryNodeFacade;
import co.kr.coresolutions.facades.IQueryQueueExecNodesFacade;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.dtos.NodeDto;
import co.kr.coresolutions.model.dtos.QueueExecNodesDto;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONObject;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryQueueExecNodesFacade implements IQueryQueueExecNodesFacade {
    //Queues implement FIFO algo except others sub-interfaces
    private static volatile Queue<List<Object>> queue = new LinkedList<>();
    private static String keySocket = "QUEUE_EXECNODES";
    private static Connection infoConnection;
    private static volatile CompletableFuture<CustomResponseDto> completableFuture;
    private final QueryService queryService;
    private final IQueryNodeFacade queryNodeFacade;
    private final IQuerySocketFacade querySocketFacade;
    private final ObjectMapper objectMapper;

    @PostConstruct
    public void init() {
        infoConnection = queryService.getInfoConnection(Constants.quadMaxConnectionID);
        new ConcurrentTaskScheduler().scheduleAtFixedRate(this::runQueue, 1000);
    }

    private void runQueue() {
        if (completableFuture != null) {
            try {
                completableFuture.get();
            } catch (Exception ignored) {
            }
        }
        Optional.ofNullable(queue.poll()).filter(objects -> objects.size() > 7).ifPresent(this::accept);
    }

    @Override
    public Object send(String commandID, QueueExecNodesDto execNodesDto, String urlPath) {
        String partMessage = "/queue_execNodes/{" + commandID + "} triggered, ";
        String[] strings = Arrays.stream(execNodesDto.getCommand().split("/")).skip(2).toArray(String[]::new);
        if (strings.length > 1) {
            String startDate = Constants.simpleDateFormat.format(new Date());
            Future<String> future = CompletableFuture.supplyAsync(() -> {
                try {
                    Callable callable = () -> queryService.checkValidityUpdateClauses("DELETE FROM " +
                            infoConnection.getSCHEME() + "T_CAMP_EXECNODES WHERE COMMANDID = '" + commandID + "'", Constants.quadMaxConnectionID);
                    callable.call();
                    return queryService.checkValidityUpdateClauses("INSERT INTO " + infoConnection.getSCHEME()
                            + "T_CAMP_EXECNODES (COMMANDID, COMMAND, OWNER, EXECNODES_JSON, LOAD_DTTM) VALUES ('"
                            + commandID + "', '"
                            + execNodesDto.getCommand() + "', '"
                            + execNodesDto.getOwner() + "', '"
                            + StringEscapeUtils.escapeJson(objectMapper.writeValueAsString(execNodesDto.getExecnodes())) + "', '"
                            + Constants.simpleDateFormat.format(new Date()) + "')", Constants.quadMaxConnectionID);
                } catch (Exception e) {
                    querySocketFacade.send(SocketBodyDto.builder().userId(execNodesDto.getOwner()).key(keySocket).message(partMessage + e.getMessage()).build());
                    queryService.addLogging("POST", partMessage + e.getMessage());
                    return "error " + e.getMessage();
                }
            });

            try {
                String result = future.get();
                if (!result.startsWith("success")) {
                    queryService.addLogging("POST", partMessage + result);
                    querySocketFacade.send(SocketBodyDto.builder().userId(execNodesDto.getOwner()).key(keySocket).message(partMessage + result).build());
                    return CustomResponseDto.builder().message(result).build();
                }
                queue.add(Lists.newArrayList(strings[0], strings[1], execNodesDto.getExecnodes(), urlPath, commandID, execNodesDto.getOwner(), execNodesDto.getCommand(), startDate));
            } catch (Exception e) {
                querySocketFacade.send(SocketBodyDto.builder().userId(execNodesDto.getOwner()).key(keySocket).message(partMessage + e.getMessage()).build());
                queryService.addLogging("POST", partMessage + e.getMessage());
                return CustomResponseDto.builder().message(e.getMessage()).build();
            }
            querySocketFacade.send(SocketBodyDto.builder().userId(execNodesDto.getOwner()).key(keySocket).message(partMessage + "success").build());
            queryService.addLogging("POST", partMessage + "success");
            return CustomResponseDto.ok();
        }
        queryService.addLogging("POST", partMessage + "fail, not valid url from command");
        return CustomResponseDto.builder().message("not valid url from command").build();
    }

    @Override
    public Object findWaiting() {
        String partMessage = "/queue_execNodes/waiting triggered, ";
        return queue.stream().filter(objects -> objects.size() > 7).map(objects -> {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("execnodes", new JSONObject(objectMapper.writeValueAsString(objects.get(2))));
            } catch (IOException e) {
                querySocketFacade.send(SocketBodyDto.builder().userId((String) objects.get(5)).key(keySocket).message(partMessage + e.getMessage()).build());
                queryService.addLogging("GET", partMessage + e.getMessage());
            }
            jsonObject.put("commandid", objects.get(4));
            jsonObject.put("owner", objects.get(5));
            jsonObject.put("command", objects.get(6));
            return jsonObject;
        }).collect(Collectors.toList()).toString();
    }

    @Override
    public Object remove(String commandID) {
        String partMessage = "/queue_execNodes/{" + commandID + "} triggered, ";
        String result = queryService.checkValidityUpdateClauses("DELETE FROM " +
                infoConnection.getSCHEME() + "T_CAMP_EXECNODES WHERE COMMANDID = '" + commandID + "'", Constants.quadMaxConnectionID);
        queue.removeIf(objects -> objects.get(4).equals(commandID));
        if (!result.startsWith("success")) {
            queryService.addLogging("DELETE", partMessage + result);
            return CustomResponseDto.builder().message(result).build();
        }
        queryService.addLogging("DELETE", partMessage + "success");
        return CustomResponseDto.ok();
    }

    private void accept(List<Object> objects) {
        String commandID = (String) objects.get(4);
        String owner = (String) objects.get(5);
        String command = (String) objects.get(6);
        String urlPath = (String) objects.get(3);

        completableFuture = CompletableFuture.supplyAsync(()
                -> queryService.checkValidityUpdateClauses("INSERT INTO " + infoConnection.getSCHEME()
                + "T_CAMP_EXECNODES_RUNNING (COMMANDID, COMMAND, OWNER, STARTED_DTTM) VALUES ('"
                + commandID + "', '"
                + command + "', '"
                + owner + "', '"
                + Constants.simpleDateFormat.format(new Date()) + "')", Constants.quadMaxConnectionID))
                .thenCompose(result -> CompletableFuture.supplyAsync(()
                        -> queryNodeFacade.execNodes((String) objects.get(0), (String) objects.get(1), (NodeDto) objects.get(2), urlPath)));


        CompletableFuture
                .supplyAsync(() -> queryService.checkValidityUpdateClauses("DELETE FROM " +
                        infoConnection.getSCHEME() + "T_CAMP_EXECNODES WHERE COMMANDID = '" + commandID + "'", Constants.quadMaxConnectionID))
                .thenApply(result -> {
                    try {
                        return completableFuture.get();
                    } catch (Exception e) {
                        return CustomResponseDto.builder().message(e.getMessage()).build();
                    }
                })
                .thenCompose(customResponseDto -> CompletableFuture.supplyAsync(() ->
                {
                    queryService.checkValidityUpdateClauses("DELETE FROM " +
                            infoConnection.getSCHEME() + "T_CAMP_EXECNODES_RUNNING WHERE COMMANDID = '" + commandID + "'", Constants.quadMaxConnectionID);
                    return customResponseDto;
                }))
                .thenApplyAsync(customResponseDto ->
                        queryService.checkValidityUpdateClauses("INSERT INTO " + infoConnection.getSCHEME()
                                + "T_CAMP_EXECNODES_COMPLETED (COMMANDID, COMMAND, EXEC_SEQ, OWNER, STATUS, ERROR_NODEID, LOG_MESSAGE, STARTED_DTTM, ENDED_DTTM) VALUES ('"
                                + commandID + "', '"
                                + command + "', "
                                + customResponseDto.getCount() + ", '"
                                + owner + "', '"
                                + (customResponseDto.getSuccess() ? "success" : "fail") + "', "
                                + (customResponseDto.getSuccess() || customResponseDto.getNodes().isEmpty() ? null : "'" + customResponseDto.getNodes() + "'") + ", '"
                                + Optional.ofNullable(customResponseDto.getMessage()).orElse("").replace("'", "") + "', '"
                                + objects.get(7) + "', '"
                                + Constants.simpleDateFormat.format(new Date()) + "')", Constants.quadMaxConnectionID)
                );
    }
}
