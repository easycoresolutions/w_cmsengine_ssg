package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.enums.EmailDeliveryType;
import co.kr.coresolutions.enums.EmailType;
import co.kr.coresolutions.facades.IQueryEmailFacade;
import co.kr.coresolutions.model.dtos.EmailDto;
import co.kr.coresolutions.model.dtos.EmailSpawnDto;
import co.kr.coresolutions.model.dtos.SubEmailDto;
import co.kr.coresolutions.service.Constants;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.annotation.PostConstruct;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
@RequiredArgsConstructor
public class QueryEmailFacade implements IQueryEmailFacade {
    private final Constants constants;
    private String CONNECTION_DIR;
    private final ObjectMapper objectMapper;

    private SubEmailDto readFile(String connectionId) throws IOException {
        return objectMapper.configure(JsonParser.Feature.ALLOW_MISSING_VALUES, false)
                .readValue(new String(Files.readAllBytes(Paths.get(CONNECTION_DIR + connectionId + ".txt")), StandardCharsets.UTF_8), SubEmailDto.class);
    }

    @PostConstruct
    public void init() {
        CONNECTION_DIR = constants.getConnectionDir();
    }

    @Override
    public ResponseDto sendSimpleEmail(@Valid EmailDto emailDto) {
        try {
            SubEmailDto subEmailDto = readFile(emailDto.getConnectionId());

            MultiPartEmail email;

            String type = emailDto.getEmailType();

            if (type.equalsIgnoreCase(EmailType.html.getName())) {
                email = new HtmlEmail();
                email.setCharset("utf-8");
                ((HtmlEmail) email).setHtmlMsg(emailDto.getData());
            } else {
                email = new MultiPartEmail();
                email.setCharset("utf-8");
                email.setMsg(emailDto.getData());
            }

            emailDto.getAttachment().forEach(oneAttachmentUrl -> {
                EmailAttachment attachment = new EmailAttachment();
                try {
                    if (StringUtils.startsWithIgnoreCase(oneAttachmentUrl, "http")) {
                        attachment.setURL(new URL(oneAttachmentUrl));
                    } else {
                        attachment.setPath(oneAttachmentUrl);
                    }
                    attachment.setDisposition(EmailAttachment.ATTACHMENT);
                    email.attach(attachment);
                } catch (MalformedURLException | EmailException ignored) {
                }
            });

            if (!emailDto.getTo().isEmpty()) {
                email.addTo(StringUtils.join(emailDto.getTo(), ",").split(","));
            }

            if (!emailDto.getCc().isEmpty()) {
                email.addCc(StringUtils.join(emailDto.getCc(), ",").split(","));
            }

            email.setFrom(emailDto.getFrom(), "CoreSolution");
            email.setSubject(emailDto.getSubject());
            email.setHostName(subEmailDto.getSmtp());
            email.setAuthentication(subEmailDto.getId(), subEmailDto.getPassword());
            email.setSmtpPort(subEmailDto.getPort());
            email.send();

            return ResponseDto.ok();

        } catch (EmailException | IOException e) {
            return ResponseDto.builder().errorCode(HttpStatus.BAD_REQUEST.value()).message(e.getMessage()).build();
        }

    }

    @Override
    public ResponseDto sendSpawnEmail(EmailSpawnDto emailSpawnDto) {
        try {
            SubEmailDto subEmailDto = readFile(emailSpawnDto.getConnectionId());

            Path pathDesc = Paths.get(constants.getSpawnDir() + emailSpawnDto.getUserid() +
                    File.separator + emailSpawnDto.getDir() + File.separator + emailSpawnDto.getDescFile() + ".desc");
            JsonNode jsonNode = objectMapper.readTree(Files.readAllBytes(pathDesc));
            MultiPartEmail email;

            String type = emailSpawnDto.getDeliveryType().getName();
            email = new MultiPartEmail();
            MimeMultipart mimeMultipart = new MimeMultipart();
            BodyPart bodyPart = new MimeBodyPart();
            bodyPart.setContent(emailSpawnDto.getData(), "text/html");
            mimeMultipart.addBodyPart(bodyPart);

            if (type.equalsIgnoreCase(EmailDeliveryType.attached.getName())) {
                if (jsonNode.has("filename") && jsonNode.get("filename").isTextual()) {
                    bodyPart = new MimeBodyPart();
                    bodyPart.setDataHandler(new DataHandler(new FileDataSource(Paths.get(constants.getSpawnDir() + emailSpawnDto.getUserid() +
                            File.separator + emailSpawnDto.getDir() + File.separator + jsonNode.get("filename").asText()).toFile())));
                    bodyPart.setFileName(jsonNode.get("filename").asText());
                    mimeMultipart.addBodyPart(bodyPart);
                }
            } else {
                if (jsonNode.has("filename") && jsonNode.get("filename").isTextual()) {
                    bodyPart = new MimeBodyPart();
                    bodyPart.setDataHandler(new DataHandler(new FileDataSource(Paths.get(constants.getSpawnDir() + emailSpawnDto.getUserid() +
                            File.separator + emailSpawnDto.getDir() + File.separator + jsonNode.get("filename").asText()).toFile())));
                    mimeMultipart.addBodyPart(bodyPart);
                }
            }
            email.setContent(mimeMultipart);


            email.setCharset("utf-8");

            if (!emailSpawnDto.getTo().isEmpty()) {
                email.addTo(StringUtils.join(emailSpawnDto.getTo(), ",").split(","));
            }

            if (!emailSpawnDto.getCc().isEmpty()) {
                email.addCc(StringUtils.join(emailSpawnDto.getCc(), ",").split(","));
            }

            email.setFrom(emailSpawnDto.getFrom(), "CoreSolution");
            if (jsonNode.has("title") && jsonNode.get("title").isTextual()) {
                email.setSubject(jsonNode.get("title").asText());
            }
            email.setHostName(subEmailDto.getSmtp());
            email.setAuthentication(subEmailDto.getId(), subEmailDto.getPassword());
            email.setSmtpPort(subEmailDto.getPort());
            email.send();

            return ResponseDto.ok();

        } catch (EmailException | IOException | MessagingException e) {
            return ResponseDto.builder().errorCode(HttpStatus.BAD_REQUEST.value()).message(e.getMessage()).build();
        }

    }
}
