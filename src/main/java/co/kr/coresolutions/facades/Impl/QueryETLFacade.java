package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryETLFacade;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.Command;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.ETLTemplate;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceCommand;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryETLFacade implements IQueryETLFacade {
    private final QueryService queryService;
    private final Constants constants;
    private String rootDir;
    @Autowired
    @Qualifier("XmlMapper")
    private ObjectMapper xmlMapper;
    private final ObjectMapper objectMapper;
    private final IQuerySocketFacade querySocketFacade;
    private final QueryServiceCommand queryServiceCommand;

    @PostConstruct
    public void init() {
        rootDir = constants.getLogDir();
    }

    @Override
    public CustomResponseDto exchange(String connectionID, String projectID, String scriptID, String userID, String commandID) {
        AtomicInteger querySeq = new AtomicInteger();
        String partErrorMessage = "project_id(" + projectID + ") or script_id({" + scriptID + "}) not defined.";
        String startDttm = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        Command.CommandDetails commandDetails = Command
                .CommandDetails
                .builder()
                .commadName("etl_sql")
                .CommandID(commandID)
                .owner(userID)
                .startdatetime(startDttm)
                .build();
        queryServiceCommand.saveCacheCommand(commandID, commandDetails);
        long startTime = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
        String partMessage = "etl_sql/{" + connectionID + "}/{" + projectID + "}/{" + scriptID + "} triggered, ";
        Connection infoConnectionInput = queryService.getInfoConnection(connectionID);
        String inputSCHEMA = infoConnectionInput.getSCHEME();
        final String internalFetchQuery = "SELECT SCRIPT_CONTENT FROM " + inputSCHEMA + "T_ETL_SCRIPT WHERE ETL_ID='" + projectID + "' AND SCRIPT_ID='" + scriptID + "'";
        final CustomResponseDto[] customResponseDto = {queryService.getResultQuery(connectionID, internalFetchQuery)};
        if (customResponseDto[0].getSuccess()) {
            String resultValidity = customResponseDto[0].getMessage();
            Path path = null;
            try {
                JSONArray jsonArray = new JSONArray(resultValidity);
                path = Files.write(Files.createTempFile(Paths.get(rootDir), String.valueOf(startTime), ".xml"),
                        jsonArray.getJSONObject(0).get("SCRIPT_CONTENT").toString().getBytes());
                ETLTemplate etlTemplate = xmlMapper.readValue(path.toFile(), ETLTemplate.class);
                ETLTemplate.ETLQuery etlQuery = etlTemplate.getQuery();
                ETLTemplate.ETLQuery.ETLScript etlScript = etlQuery.getScript();
                String fetchSql = queryService.convertGlobalDateTime(etlQuery.getFetchSql());

                if (!fetchSql.isEmpty()) {
                    customResponseDto[0] = queryService.getResultQuery(etlQuery.getConnectionID(), fetchSql);
                    String logMessage = (customResponseDto[0].getSuccess() ? "success" :
                            (customResponseDto[0].getErrorMessage()
                                    .equalsIgnoreCase("fail due []") ? partErrorMessage : customResponseDto[0].getErrorMessage()));

                    queryService.addLogging("POST", partMessage + "query is\t" + fetchSql + "\t" + logMessage);
                    querySocketFacade.send(SocketBodyDto.builder().userId(userID).key("ETL_SQL").message(fetchSql).build());
                    logToDBTriggered(startDttm, fetchSql, querySeq.incrementAndGet(), partMessage, connectionID, etlQuery.getConnectionID(),
                            inputSCHEMA, projectID, scriptID, (customResponseDto[0].getSuccess() ? "success" : "fail"),
                            (customResponseDto[0].getSuccess() ? "success" : customResponseDto[0].getErrorMessage()),
                            LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) - startTime);
                }

                if ((customResponseDto[0].getSuccess() && !etlScript.getInsertSql().isEmpty()) || fetchSql.isEmpty()) {
                    JsonNode jsonNode = null;
                    if (!fetchSql.isEmpty()) {
                        jsonNode = objectMapper.readTree(customResponseDto[0].getMessage());
                    }
                    final boolean[] firstFail = {false};
                    JsonNode node = jsonNode;
                    Arrays.stream(etlScript.getInsertSql().split(";"))
                            .filter(s -> !s.trim().isEmpty())
                            .map(s -> queryService.convertGlobalDateTime(s.replace("{PROJECT_ID}", projectID)
                                    .replace("{USERID}", userID).replace("{SCRIPT_ID}", scriptID).trim()))
                            .forEach(insertSql -> {
                                if (!firstFail[0]) {
                                    CustomResponseDto customResponseDtoResult = queryService.getResultQueryCUD(etlScript.getConnectionID(), insertSql, node,
                                            (etlScript.getCount() == null ? 0 : etlScript.getCount()));
                                    String logMessage = (customResponseDtoResult.getSuccess() ? "success" :
                                            (customResponseDtoResult.getErrorMessage()
                                                    .equalsIgnoreCase("fail due []") ? partErrorMessage : customResponseDtoResult.getErrorMessage()));
                                    queryService.addLogging("POST", partMessage + "query is\t" + insertSql + "\t" + logMessage);
                                    String status = (customResponseDtoResult.getSuccess() ? "success" : "fail");
                                    querySocketFacade.send(SocketBodyDto.builder().userId(userID).key("ETL_SQL").message(insertSql).build());
                                    logToDBTriggered(startDttm, insertSql, querySeq.incrementAndGet(),
                                            partMessage, connectionID, etlScript.getConnectionID(), inputSCHEMA, projectID, scriptID, status,
                                            logMessage, LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) - startTime);
                                    if (!customResponseDtoResult.getSuccess() && customResponseDto[0].getErrorCode() == null) {
                                        firstFail[0] = true;
                                        customResponseDto[0] = customResponseDtoResult;
                                        customResponseDto[0].setErrorCode(1);
                                    }
                                }
                            });
                }
            } catch (IOException e) {
                customResponseDto[0] = CustomResponseDto.builder().ErrorMessage(e.getMessage()).build();
                queryService.addLogging("POST", partMessage + e.getMessage());
            } finally {
                queryService.addLogging("POST", partMessage + (customResponseDto[0].getSuccess() ? "success" : "fail"));
                try {
                    Files.deleteIfExists(path);
                } catch (IOException ignored) {
                }
            }
        } else {
            if (customResponseDto[0].getErrorMessage().equalsIgnoreCase("fail due []")) {
                queryService.addLogging("POST", partMessage + "query is\t" + internalFetchQuery + "\t" + partErrorMessage);
                customResponseDto[0].setErrorMessage(partErrorMessage);
            } else {
                queryService.addLogging("POST", partMessage + "query is\t" + internalFetchQuery + "\t" + customResponseDto[0].getErrorMessage());
            }
        }

        customResponseDto[0].setErrorCode(null);
        if (customResponseDto[0].getSuccess()) {
            customResponseDto[0].setMessage(null);
        }
        queryServiceCommand.removeCacheCommand(commandID);
        return customResponseDto[0];
    }

    @Override
    public void logToDBTriggered(String startDttm, String query, int querySeq, String partMessage, String connectionID, String internalConnectionId, String inputSCHEMA, String projectID,
                                 String scriptID, String status, String logMessage, long duration) {
        Thread thread = new Thread(() -> {
            try {
                queryService.checkValidityUpdateClauses("INSERT INTO " + inputSCHEMA +
                        "T_ETL_TRIGGERED (ETL_ID, SCRIPT_ID, STATUS, LOG_TEXT, DURATION, START_DTTM, QUERY_SEQ, QUERY, CONNECTIONID, LOAD_DTTM) VALUES ("
                        + (projectID != null ? "'" + projectID + "'" : null) + ", "
                        + (scriptID != null ? "'" + scriptID + "'" : null) + ", '"
                        + status + "', '"
                        + logMessage.replace("'", "") + "', "
                        + duration + ", '"
                        + startDttm + "', "
                        + querySeq + ", "
                        + (query != null ? "'" + StringEscapeUtils.escapeSql(query) + "'" : null) + ", "
                        + (internalConnectionId != null ? "'" + internalConnectionId + "'" : null) + ", '"
                        + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "')", connectionID);
            } catch (Exception e) {
                queryService.addLogging("POST", partMessage + "fail when inserting data to T_ETL_TRIGGERED:\t" + e.getMessage());
            }
        });
        thread.setDaemon(true);
        thread.start();
    }
}

