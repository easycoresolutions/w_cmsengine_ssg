package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.ResponseCodes;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQueryDateFacade;
import co.kr.coresolutions.facades.IQueryNodeFacade;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.dtos.DateCriteriaDto;
import co.kr.coresolutions.model.dtos.HttpDto;
import co.kr.coresolutions.model.dtos.NodeCommand;
import co.kr.coresolutions.model.dtos.NodeDto;
import co.kr.coresolutions.model.dtos.NodeStructureDto;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryDataFlowHttp;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceNode;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Joiner;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@RequiredArgsConstructor
public class QueryNodeFacade implements IQueryNodeFacade {
    private final QueryService queryService;
    public static String validateDateMessage = "";
    private static List<String> requestPathKeys = new ArrayList<>();
    public static String[] message = {""};
    private final ObjectMapper objectMapper;
    private final QueryDataFlowHttp queryDataFlowHttp;
    private final IQueryDateFacade queryDateFacade;
    private final QueryServiceNode queryServiceNode;
    private final IQuerySocketFacade querySocketFacade;
    private final Constants constants;

    @Override
    public CustomResponseDto execNodes(String campId, String stepId, NodeDto nodeDto, String urlPath) {
        if (QueryNodeFacade.requestPathKeys.contains(campId + "," + stepId)) {
            return CustomResponseDto
                    .builder()
                    .message("Same {" + campId + "/{" + stepId + "} is already running. Request is canceled!")
                    .build();
        }

        QueryNodeFacade.requestPathKeys.add(campId + "," + stepId);

        String source = nodeDto.getSource();
        if (source != null && !(source.equals("body") || source.equals("batch_body"))) {
            QueryNodeFacade.requestPathKeys.remove(campId + "," + stepId);
            return CustomResponseDto
                    .builder()
                    .message("valid value is one of  body  or batch_body")
                    .build();
        }

        QueryNodeFacade.validateDateMessage = "";
        String connectionID = nodeDto.getConnectionID();
        String partMessage = "execNodes triggered (campId = " + campId + " , stepId = " + stepId + "), ";
        String tableName = nodeDto.getTable();
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            QueryNodeFacade.message[0] = "fail due to connection file with name\t" + connectionID + "\tdoesn't exist";
            querySocketFacade.send(SocketBodyDto.builder().userId(nodeDto.getUser()).key("EXECNODES").message(partMessage + QueryNodeFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + QueryNodeFacade.message[0]);
            QueryNodeFacade.requestPathKeys.remove(campId + "," + stepId);
            return CustomResponseDto.builder().nodes("").message(QueryNodeFacade.message[0]).build();
        }

        String[] connectionIDZLog = {""};
        Optional.ofNullable(constants.getConfigFileAsJson())
                .ifPresent(jsonNode -> {
                    if (jsonNode.hasNonNull("LogonConnectionId") && jsonNode.get("LogonConnectionId").isTextual()) {
                        connectionIDZLog[0] = jsonNode.get("LogonConnectionId").asText();
                    }
                });

        if (connectionIDZLog[0].isEmpty()) {
            QueryNodeFacade.message[0] = "LogonConnectionId didn't found in config file or empty value";
            querySocketFacade.send(SocketBodyDto.builder().userId(nodeDto.getUser()).key("EXECNODES").message(partMessage + QueryNodeFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + QueryNodeFacade.message[0]);
            QueryNodeFacade.requestPathKeys.remove(campId + "," + stepId);
            return CustomResponseDto.builder().nodes("").message(QueryNodeFacade.message[0]).build();
        }

        checkTodayAllowed(nodeDto.getDateCriteria(), campId, connectionIDZLog[0]);
        if (!QueryNodeFacade.validateDateMessage.isEmpty()) {
            querySocketFacade.send(SocketBodyDto.builder().userId(nodeDto.getUser()).key("EXECNODES").message(partMessage + QueryNodeFacade.validateDateMessage).build());
            queryService.addLogging("POST", partMessage + QueryNodeFacade.message[0]);
            QueryNodeFacade.requestPathKeys.remove(campId + "," + stepId);
            return CustomResponseDto.builder().nodes("").message(QueryNodeFacade.validateDateMessage).build();
        }

        String sql;
        Connection infoConnection = queryService.getInfoConnection(connectionID);
        sql = "SELECT NODE_COMMAND FROM " + infoConnection.getSCHEME() + tableName + " WHERE CAMP_ID='" + campId + "' AND STEP_ID = '" + stepId + "' ORDER BY SEQ";

        String resultValidity = queryService.checkValidity(sql, connectionID, "", "cache", false);
        if (resultValidity.startsWith("Error") || resultValidity.startsWith("error") || resultValidity.startsWith("maxRunningTimeOut") || resultValidity.startsWith("MaxRows")) {
            QueryNodeFacade.message[0] = "sql\t" + sql + "\t, fail due\t" + resultValidity;
            querySocketFacade.send(SocketBodyDto.builder().userId(nodeDto.getUser()).key("EXECNODES").message(partMessage + QueryNodeFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + QueryNodeFacade.message[0]);
            QueryNodeFacade.requestPathKeys.remove(campId + "," + stepId);
            return CustomResponseDto.builder().nodes("").message(QueryNodeFacade.message[0]).build();
        }

        if (resultValidity.equalsIgnoreCase("[]")) {
            QueryNodeFacade.message[0] = "sql\t" + sql + "\t, fail due Empty value returned";
            querySocketFacade.send(SocketBodyDto.builder().userId(nodeDto.getUser()).key("EXECNODES").message(partMessage + QueryNodeFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + QueryNodeFacade.message[0]);
            QueryNodeFacade.requestPathKeys.remove(campId + "," + stepId);
            return CustomResponseDto.builder().nodes("").message(QueryNodeFacade.message[0]).build();
        }

        final String[] lastSuccessMessage = new String[1];
        List<String> nodeIds = new ArrayList<>();
        String seq = getSeq(campId, stepId, connectionIDZLog[0]);
        try {
            List<NodeStructureDto> nodeStructureDtos = Arrays.asList(objectMapper.readValue(resultValidity, NodeStructureDto[].class));
            List<NodeCommand> nodeCommands = nodeStructureDtos.stream().map(nodeStructureDto -> {
                try {
                    return objectMapper.readValue(nodeStructureDto.getNodeCommand(), NodeCommand.class);
                } catch (IOException e) {
                    return null;
                }
            }).filter(Objects::nonNull).collect(Collectors.toList());
            String finalUrlPath = urlPath.contains("query") ? urlPath.substring(0, urlPath.indexOf("query")) : urlPath;
            final String[] responseErrorMessage = new String[1];
            final String[] nodeIssue = new String[1];
            boolean anyMatchIssueExecution = nodeCommands.stream()
                    .filter(nodeCommand -> {
                        //from request
                        NodeDto.Conditions conditions = nodeDto.getConditions();

                        //in case both existing in request and node_command
                        if (conditions.getBatchsubmit() != null && conditions.getRetargeting() != null) {
                            if (conditions.getSubmit() != null) {
                                return (conditions.getBatchsubmit().equals(nodeCommand.getBatchSubmit())) &&
                                        (conditions.getRetargeting().equals(nodeCommand.getRetargeting())) &&
                                        (nodeCommand.getSubmit().equals(conditions.getSubmit()));
                            }
                            return (conditions.getBatchsubmit().equals(nodeCommand.getBatchSubmit())) &&
                                    (conditions.getRetargeting().equals(nodeCommand.getRetargeting()));
                        }

                        //in case only batchsubmit existing in request and node_command, and retargeting missing from request
                        if (conditions.getBatchsubmit() != null && conditions.getRetargeting() == null) {
                            if (conditions.getSubmit() != null) {
                                return (conditions.getBatchsubmit().equals(nodeCommand.getBatchSubmit())) &&
                                        (nodeCommand.getSubmit().equals(conditions.getSubmit()));
                            }
                            return conditions.getBatchsubmit().equals(nodeCommand.getBatchSubmit());
                        }

                        //in case only retargeting existing in request and node_command, and batchsubmit missing from request
                        if (conditions.getRetargeting() != null && conditions.getBatchsubmit() == null) {
                            if (conditions.getSubmit() != null) {
                                return (conditions.getRetargeting().equals(nodeCommand.getRetargeting())) &&
                                        (nodeCommand.getSubmit().equals(conditions.getSubmit()));
                            }
                            return conditions.getRetargeting().equals(nodeCommand.getRetargeting());
                        }

                        //in case both missing from request
                        if (conditions.getRetargeting() == null && conditions.getBatchsubmit() == null) {
                            if (conditions.getSubmit() != null) {
                                return (nodeCommand.getBatchSubmit()) &&
                                        (nodeCommand.getSubmit().equals(conditions.getSubmit()));
                            }
                            return nodeCommand.getBatchSubmit();
                        }

                        return false;
                    })
                    .anyMatch(nodeCommand -> {
//                        String url = replaceUrl(nodeCommand.getUrl(), finalUrlPath, infoConnection, connectionID);
                        String url = replaceUrl(nodeCommand.getUrl(), finalUrlPath, infoConnection, connectionID).replaceAll("@@SEQ@@", seq);
                        JsonNode jsonBody = nodeCommand.getBatch_body();

                        if (source != null && source.equals("body")) {
                            jsonBody = nodeCommand.getBody();
                        }

                        if (jsonBody.isNull()) {
                            return false;
                        }

                        if (jsonBody.isArray()) {
                            return StreamSupport.stream(jsonBody.spliterator(), false)
                                    .anyMatch(jsonNode -> {
                                        ResponseDto responseDto = queryDataFlowHttp.execute(HttpDto
                                                .builder()
                                                .api(url)
                                                .body(jsonNode.toString().replaceAll("\\{QUERY_SERVER\\}", finalUrlPath.substring(0, finalUrlPath.length() - 1))
                                                        .replaceAll("@@SEQ@@", String.valueOf(Integer.valueOf(seq) + 1)).replaceAll("@@SEQ0@@", seq))
                                                .method(HttpMethod.POST)
                                                .build());
                                        lastSuccessMessage[0] = responseDto.getMessage().toLowerCase().trim();
                                        return isSuccessExecution(nodeIds, responseErrorMessage, nodeIssue, nodeCommand, responseDto);
                                    });
                        } else {
                            String body = jsonBody.toString()
                                    .replaceAll("\\{QUERY_SERVER\\}", finalUrlPath.substring(0, finalUrlPath.length() - 1))
                                    .replaceAll("@@SEQ@@", String.valueOf(Integer.valueOf(seq) + 1)).replaceAll("@@SEQ0@@", seq);
                            ResponseDto responseDto = queryDataFlowHttp.execute(HttpDto
                                    .builder()
                                    .api(url)
                                    .body(body)
                                    .method(HttpMethod.POST)
                                    .build());
                            lastSuccessMessage[0] = responseDto.getMessage().toLowerCase().trim();
                            return isSuccessExecution(nodeIds, responseErrorMessage, nodeIssue, nodeCommand, responseDto);
                        }
                    });
            if (anyMatchIssueExecution) {
                queryServiceNode.logToDB(campId, responseErrorMessage[0], false, connectionIDZLog[0], null);
                querySocketFacade.send(SocketBodyDto.builder().userId(nodeDto.getUser()).key("EXECNODES").message(partMessage + responseErrorMessage[0]).build());
                queryService.addLogging("POST", partMessage + responseErrorMessage[0]);
                QueryNodeFacade.requestPathKeys.remove(campId + "," + stepId);
                return CustomResponseDto.builder().count(Integer.valueOf(seq)).nodes(nodeIssue[0]).message(responseErrorMessage[0]).build();
            }
        } catch (IOException e) {
            QueryNodeFacade.message[0] = e.getMessage();
            querySocketFacade.send(SocketBodyDto.builder().userId(nodeDto.getUser()).key("EXECNODES").message(partMessage + QueryNodeFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + QueryNodeFacade.message[0]);
            queryServiceNode.logToDB(campId, partMessage + QueryNodeFacade.message[0], false, connectionIDZLog[0], null);
            QueryNodeFacade.requestPathKeys.remove(campId + "," + stepId);
            return CustomResponseDto.builder().count(Integer.valueOf(seq)).message(QueryNodeFacade.message[0]).nodes("").build();
        }

        QueryNodeFacade.message[0] = "execNodes success";
        querySocketFacade.send(SocketBodyDto.builder().userId(nodeDto.getUser()).key("EXECNODES").message(partMessage + QueryNodeFacade.message[0]).build());
        queryService.addLogging("POST", partMessage + QueryNodeFacade.message[0]);
        queryServiceNode.logToDB(campId, QueryNodeFacade.message[0] + ", nodes\t" + Joiner.on(",").join(nodeIds), true, connectionIDZLog[0], null);
        QueryNodeFacade.requestPathKeys.remove(campId + "," + stepId);
        return CustomResponseDto.builder().count(Integer.valueOf(seq)).nodes(Joiner.on(",").join(nodeIds)).Success(true)
                .records(lastSuccessMessage[0].startsWith("success,") ? lastSuccessMessage[0].substring(lastSuccessMessage[0].indexOf("success,") + 8) : lastSuccessMessage[0])
                .build();
    }

    private String replaceUrl(String url, String urlPath, Connection infoConnection, String connectionID) {
        if (url.startsWith("{SCHEDULER}")) {
            ResponseDto responseDto = queryDataFlowHttp.execute(HttpDto
                    .builder()
                    .api(urlPath + "scheduler/schedule/status")
                    .method(HttpMethod.GET)
                    .build());
            if (responseDto.getStatusCode() == ResponseCodes.OK_RESPONSE) {
                JSONObject jsonObject = new JSONObject(responseDto.getMessage());
                if (jsonObject.has("master")) {
                    return url.replaceAll("\\{SCHEDULER\\}", jsonObject.getString("master"));
                }
            }
        } else if (url.toLowerCase().contains("{{SCHEDULE_MASTER}}".toLowerCase())) {
            String resultValidity = queryService.checkValidity("SELECT SC_IP FROM " + infoConnection.getSCHEME() + "t_schedule_master", connectionID, "", "cache", false);
            try {
                JsonNode jsonNode = objectMapper.readTree(resultValidity);
                if (jsonNode.get(0).has("SC_IP")) {
                    return url.replace("{{SCHEDULE_MASTER}}", jsonNode.get(0).get("SC_IP").asText());
                }
            } catch (IOException e) {
            }
        }
        return urlPath + url;
    }

    private boolean isSuccessExecution(List<String> nodeIds, String[] responseErrorMessage, String[] nodeIssue, NodeCommand nodeCommand, ResponseDto responseDto) {
        if (responseDto.getStatusCode() != ResponseCodes.OK_RESPONSE) {
            nodeIssue[0] = nodeCommand.getNodeId();
            responseErrorMessage[0] = responseDto.getMessage();
            queryService.addLogging("POST", "execNodes triggered, fail due\t" + responseErrorMessage[0]);
            return true;
        } else {
            nodeIds.add(nodeCommand.getNodeId());
            return false;
        }
    }

    private String getSeq(String campId, String stepID, String connectionID) {
        final int[] sequence = {0};
        String query;
        Connection infoConnection = queryService.getInfoConnection(connectionID);
        /* SELECT MAX(exec_seq) from t_camp_nodeinfo where camp_id={camp_id} and step_id = {stepid}*/
        query = "SELECT MAX(EXEC_SEQ) FROM " + infoConnection.getSCHEME() + "T_CAMP_NODEINFO WHERE CAMP_ID ='" + campId + "' AND STEP_ID = '" + stepID + "'";

        String resultValidity = queryService.checkValidity(query, connectionID, "", "cache", false);
        if (resultValidity.startsWith("Error") || resultValidity.startsWith("error") || resultValidity.startsWith("maxRunningTimeOut") || resultValidity.startsWith("MaxRows")) {
            queryService.addLogging("POST", "/execNodes triggered, error executing seq query is\t" + resultValidity);
        }

        if (!resultValidity.equalsIgnoreCase("[]")) {
            JSONArray jsonArray = new JSONArray(resultValidity);
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                if (jsonObject.has("MAX(EXEC_SEQ)")) {
                    sequence[0] = Integer.parseInt(jsonObject.get("MAX(EXEC_SEQ)").toString());
                }
            } catch (Exception e) {
                queryService.addLogging("POST", "/execNodes triggered, error catching sequence is\t" + e.getMessage());
            }
        }
        return String.valueOf(sequence[0]);
    }

    private void checkTodayAllowed(DateCriteriaDto dateCriteria, String campId, String connectionIDZLog) {
        DateCriteriaDto.Month month = dateCriteria.getMonth();
        DateCriteriaDto.Week week = dateCriteria.getWeek();
        Boolean validateHoliday = dateCriteria.getValidateHoliday();
        Date date = new Date();

        if (month != null) {
            String monthInLowerCaseFormat = new SimpleDateFormat("MMM", Locale.US).format(date).toLowerCase();
            if (!queryDateFacade.isValidWeekOrMonth(monthInLowerCaseFormat, month)) {
                queryService.addLogging("POST", "execNodes triggered, today Date\t" + date + "\t is not allowed for month");
                queryServiceNode.logToDB(campId, "A given date(" + date + ") is not allowed in " + monthInLowerCaseFormat, false, connectionIDZLog, null);
                return;
            }
            if (week != null) {
                String weekInLowerCaseFormat = new SimpleDateFormat("EEE", Locale.US).format(date).toLowerCase();
                if (!queryDateFacade.isValidWeekOrMonth(weekInLowerCaseFormat, week)) {
                    queryService.addLogging("POST", "execNodes triggered, today Date\t" + date + "\t is not allowed for week");
                    queryServiceNode
                            .logToDB(campId, "A given date(" + date + ") is not allowed in " + weekInLowerCaseFormat, false, connectionIDZLog, null);
                    return;
                }
                if (validateHoliday != null && validateHoliday) {
                    queryServiceNode.checkHoliday(campId, date, connectionIDZLog, "t_holiday");
                }
            }
        }
    }

}
