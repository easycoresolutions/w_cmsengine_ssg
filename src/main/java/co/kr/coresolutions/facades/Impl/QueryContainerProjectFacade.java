package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQueryContainerProjectFacade;
import co.kr.coresolutions.model.dtos.ContainerProjectDto;
import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.GONE;
import static org.springframework.http.HttpStatus.OK;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryContainerProjectFacade implements IQueryContainerProjectFacade {
    private final QueryService queryService;
    private final Constants constants;
    private String containerProjectQueryDir;
    private final ObjectMapper objectMapper;

    @PostConstruct
    public void init() {
        containerProjectQueryDir = constants.getContainerProjectQueryDir();
    }

    @Override
    @SneakyThrows
    public ResponseDto saveContainerProject(ContainerProjectDto containerProjectDto, String userID, String projectID) {
        final boolean[] indexExists = {false};
        final boolean[] userIDExists = {false};
        if (!Paths.get(containerProjectQueryDir).toFile().isDirectory())
            Files.createDirectory(Paths.get(containerProjectQueryDir));

        QuerySurveyResultFacade.isIndexExist(userID, indexExists, userIDExists, containerProjectQueryDir);

        Path userIDPath = Paths.get(containerProjectQueryDir + File.separator + userID);
        if (!userIDExists[0]) {
            userIDExists[0] = true;
            if (!userIDPath.toFile().isDirectory()) {
                userIDPath = Files.createDirectory(userIDPath);
            }
        }

        JSONArray jsonArray;
        byte[] containerProject = objectMapper.writeValueAsBytes(containerProjectDto);
        JSONObject jsonObject1 = new JSONObject(new String(containerProject));
        JSONObject containerProjectWithoutJsonField = new JSONObject(jsonObject1, Arrays.stream(JSONObject.getNames(jsonObject1)).filter(s ->
                !s.equals("containerprojectjson")
        ).collect(Collectors.toList()).toArray(new String[JSONObject.getNames(jsonObject1).length]));

        Files.write(Paths.get(userIDPath + File.separator + projectID + ".txt"), containerProject,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

        if (!indexExists[0]) {
            jsonArray = new JSONArray();
            jsonArray.put(containerProjectWithoutJsonField);
            Files.write(Paths.get(userIDPath + File.separator + "index.txt"),
                    jsonArray.toString().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE);
        } else {
            JsonParser parser = new JsonParser();
            JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(userIDPath + File.separator + "index.txt"))));
            JsonArray jsonArrayTemp = jsonElement.getAsJsonArray();
            JSONArray returnArray = new JSONArray();

            jsonArrayTemp.iterator().forEachRemaining(e -> {
                JSONObject temp = new JSONObject(e.toString());
                if (temp.has("containerprojectID") && !temp.get("containerprojectID").toString().equalsIgnoreCase(projectID)
                        && containerProjectDto.getContainerProjectID().equalsIgnoreCase(projectID)) {
                    returnArray.put(temp);
                }
            });

            returnArray.put(containerProjectWithoutJsonField);
            Files.write(Paths.get(userIDPath + File.separator + "index.txt"), returnArray.toString().getBytes(StandardCharsets.UTF_8),
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        }

        queryService.addLogging("POST", projectID + "\tcontainer project uploaded successfully\n");
        return ResponseDto.builder().successCode(OK.value()).message(projectID + "\tcontainer project uploaded successfully").build();
    }

    @Override
    @SneakyThrows
    public ResponseDto deleteContainerProject(String userID, String projectID) {
        if (!Paths.get(containerProjectQueryDir).toFile().isDirectory())
            Files.createDirectory(Paths.get(containerProjectQueryDir));
        if (!Paths.get(containerProjectQueryDir + File.separator + userID).toFile().isDirectory()) {
            queryService.addLogging("DELETE", userID + "\tdoesn't exist.\n");
            return ResponseDto.builder().errorCode(CONFLICT.value()).message(userID + "\tdoesn't exist.").build();
        } else {
            try {
                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(containerProjectQueryDir + File.separator + userID + File.separator + "index.txt"))));
                JsonArray jsonArrayTemp = jsonElement.getAsJsonArray();

                JSONArray returnArray = new JSONArray();
                jsonArrayTemp.iterator().forEachRemaining(e -> {
                    JSONObject temp = new JSONObject(e.toString());
                    if (!temp.get("containerprojectID").toString().equalsIgnoreCase(projectID))
                        returnArray.put(temp);
                });

                Files.write(Paths.get(containerProjectQueryDir + File.separator + userID + File.separator + "index.txt"),
                        returnArray.toString().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                boolean deleted = Files.deleteIfExists(Paths.get(containerProjectQueryDir + File.separator + userID + File.separator + projectID + ".txt"));
                if (!deleted) {
                    queryService.addLogging("DELETE", projectID + "\tdoesn't exist.\n");
                    return ResponseDto.builder().errorCode(CONFLICT.value()).message(projectID + "\tdoesn't exist.").build();
                }
            } catch (IOException e) {
                return ResponseDto.NotValidRequest();
            }
        }
        queryService.addLogging("DELETE", "deleteContainerProject: " + projectID + "\n");
        return ResponseDto.builder().successCode(OK.value()).message(projectID + "\tDeleted successfully").build();
    }

    @Override
    @SneakyThrows
    public ResponseDto getContainerProject(String userID, String projectID) {
        if (!Paths.get(containerProjectQueryDir).toFile().isDirectory())
            Files.createDirectory(Paths.get(containerProjectQueryDir));

        if (!Paths.get(containerProjectQueryDir + File.separator + userID).toFile().isDirectory()) {
            queryService.addLogging("GET", userID + "\tdirectory doesn't exist.\n");
            return ResponseDto.builder().errorCode(GONE.value()).message(projectID + "\tdirectory doesn't exist.").build();
        } else {
            try {
                ResponseDto responseDto = ResponseDto.builder().successCode(OK.value())
                        .message(new String(Files.readAllBytes(Paths.get(containerProjectQueryDir + File.separator + userID + File.separator + projectID + ".txt")),
                                StandardCharsets.UTF_8)).build();
                queryService.addLogging("GET", " getContainerProject called userID " + userID + " , projectID " + projectID + "\n");
                return responseDto;
            } catch (IOException e) {
                queryService.addLogging("GET", projectID + "\tdoesn't exist.\n");
                return ResponseDto.builder().errorCode(CONFLICT.value()).message(projectID + "\tdoesn't exist.").build();
            }
        }
    }

    @Override
    @SneakyThrows
    public ResponseDto getContainerProjects(String userID) {
        if (!Paths.get(containerProjectQueryDir).toFile().isDirectory())
            Files.createDirectory(Paths.get(containerProjectQueryDir));
        try {
            ResponseDto responseDto = ResponseDto.builder().successCode(OK.value())
                    .message(new String(Files.readAllBytes(Paths.get(containerProjectQueryDir + File.separator + userID + File.separator + "index.txt")),
                            StandardCharsets.UTF_8)).build();
            queryService.addLogging("GET", "getContainerProjects called " + userID + "\n");
            return responseDto;
        } catch (IOException e1) {
            queryService.addLogging("GET", userID + "\tdoesn't exist.\n");
            return ResponseDto.builder().errorCode(GONE.value()).message(userID + "\tdoesn't exist.").build();
        }
    }
}

