package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryProjectAuthFacade;
import co.kr.coresolutions.model.dtos.ProjectAuthDto;
import co.kr.coresolutions.model.dtos.ProjectDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceProjectAuth;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiFunction;

@Component
@RequiredArgsConstructor
public class QueryProjectAuthFacade implements IQueryProjectAuthFacade {
    private final QueryService queryService;
    private final QueryServiceProjectAuth queryServiceProjectAuth;
    private final ObjectMapper objectMapper;

    @Override
    public CustomResponseDto saveProject(JsonNode node, String connectionID, String projectID) {
        String partMessage = "projectauth/{" + connectionID + "}/{" + projectID + "} triggered, ";
        final CustomResponseDto[] customResponseDto = {queryServiceProjectAuth.doUsualValidation(node, connectionID, projectID, false)};
        if (customResponseDto[0].getSuccess()) {
            String mainTable = customResponseDto[0].getDetailedMessage();
            JSONObject jsonObject = new JSONObject(customResponseDto[0].getMessage());
            List<String> listRandomIds = new LinkedList<>();

            customResponseDto[0] = queryServiceProjectAuth.insertForMain(listRandomIds, connectionID, mainTable, projectID, new JSONObject(jsonObject.get(mainTable).toString()));
            if (!customResponseDto[0].getSuccess()) {
                queryService.addLogging("POST", partMessage + customResponseDto[0].getDetailedMessage());
                queryService.rollBack(listRandomIds);
                return customResponseDto[0];
            }

            final boolean[] success = {true};
            long countAux = jsonObject.keySet().stream().filter(s -> !s.equals(mainTable) && !s.equals("project_table")).count();
            if (countAux < 1) {
                queryService.addLogging("POST", partMessage + customResponseDto[0].getDetailedMessage());
                queryService.rollBack(listRandomIds);
                return customResponseDto[0];
            }
            int[] countArray = {0};
            jsonObject.keySet().stream().filter(s -> !s.equals(mainTable) && !s.equals("project_table"))
                    .forEach(tableName -> {
                        if (!success[0]) {
                            return;
                        }
                        JSONArray jsonArray = new JSONArray();
                        try {
                            jsonArray = new JSONArray(jsonObject.get(tableName).toString());
                            countArray[0]++;
                        } catch (Exception e) {
                        }

                        //delete if exist and insert in auxiliary tables
                        String randomDeleteAuxiliary = UUID.randomUUID().toString();
                        String resultDelete = queryService
                                .updateClausesWithRollback(
                                        Lists.newArrayList("DELETE FROM " + tableName + " where projectID = '" + projectID + "'"), connectionID, randomDeleteAuxiliary);
                        if (!resultDelete.startsWith("success")) {
                            success[0] = false;
                            queryService.rollBack(listRandomIds);
                            customResponseDto[0].setDetailedMessage(resultDelete);
                            customResponseDto[0].setSuccess(success[0]);
                            return;
                        }
                        listRandomIds.add(randomDeleteAuxiliary);

                        String randomInsertAuxiliary = UUID.randomUUID().toString();
                        customResponseDto[0] = queryServiceProjectAuth
                                .insertAuxiliaryTable(tableName, jsonArray, randomDeleteAuxiliary + "same" + randomInsertAuxiliary, projectID, connectionID);
                        if (!customResponseDto[0].getSuccess()) {
                            success[0] = false;
                            queryService.rollBack(listRandomIds);
                            return;
                        }
                        listRandomIds.add(randomInsertAuxiliary);
                    });
            if (countArray[0] == 0) {
                return CustomResponseDto.builder().DetailedMessage("fail due missing at least one jsonArray in the body as auxiliary table").build();
            }


            if (customResponseDto[0].getSuccess()) {
                queryService.addLogging("POST", partMessage + "Success insert");
                customResponseDto[0].setDetailedMessage("project/{" + connectionID + "}/{" + mainTable + "}/{" + projectID + "} triggered, Success insert");
                queryService.commit(listRandomIds);
            }
        }
        queryService.addLogging("POST", partMessage + customResponseDto[0].getDetailedMessage());
        return customResponseDto[0];
    }

    @Override
    public CustomResponseDto deleteRow(ProjectAuthDto projectAuthDto, String connectionID, String projectID) {
        String partMessage = "projectauth/{" + connectionID + "}/{" + projectID + "} triggered, ";
        final CustomResponseDto[] customResponseDto = {queryServiceProjectAuth.doUsualValidation(null, connectionID, projectID, false)};
        if (customResponseDto[0].getSuccess()) {
            List<String> listRandomIds = new LinkedList<>();
            String randomDeleteMain = UUID.randomUUID().toString();
            String resultDeleteMain = queryService
                    .updateClausesWithRollback(
                            Lists.newArrayList("DELETE FROM " + projectAuthDto.getMainTable() + " where projectID = '" + projectID + "'"), connectionID, randomDeleteMain);
            if (!resultDeleteMain.startsWith("success")) {
                queryService.addLogging("DELETE", partMessage + customResponseDto[0].getDetailedMessage());
                return CustomResponseDto.builder().DetailedMessage("fail due error is\t" + resultDeleteMain).build();
            }
            listRandomIds.add(randomDeleteMain);
            final boolean[] success = {true};
            projectAuthDto.getAuxiliaryTables().stream().filter(s -> !s.equalsIgnoreCase(projectAuthDto.getMainTable())).forEach(auxiliaryTable -> {
                if (!success[0]) {
                    return;
                }
                String randomDeleteAuxiliary = UUID.randomUUID().toString();
                String resultDeleteAuxiliary = queryService
                        .updateClausesWithRollback(
                                Lists.newArrayList("DELETE FROM " + auxiliaryTable + " where projectID = '" + projectID + "'"), connectionID, randomDeleteAuxiliary);
                if (!resultDeleteAuxiliary.startsWith("success")) {
                    customResponseDto[0].setDetailedMessage(resultDeleteAuxiliary);
                    success[0] = false;
                    queryService.rollBack(listRandomIds);
                    return;
                }
                listRandomIds.add(randomDeleteAuxiliary);
            });
            customResponseDto[0].setSuccess(success[0]);
            if (success[0]) {
                customResponseDto[0].setDetailedMessage("success");
                queryService.commit(listRandomIds);
            }
        }
        queryService.addLogging("DELETE", partMessage + customResponseDto[0].getDetailedMessage());
        return customResponseDto[0];
    }

    @Override
    public CustomResponseDto getDetails(JsonNode jsonNode, String connectionID, String projectID) {
        String partMessage = "projectauth/getdetails/{" + connectionID + "}/{" + projectID + "} triggered, ";
        final CustomResponseDto[] customResponseDto = {queryServiceProjectAuth.doUsualValidation(null, connectionID, projectID, true)};
        if (customResponseDto[0].getSuccess()) {
            BiFunction<CustomResponseDto, CustomResponseDto, CustomResponseDto> combineBIFunction = (customResponseDtoMain, customResponseDtoAux) -> {
                if (customResponseDtoMain.getSuccess() && customResponseDtoAux.getSuccess()) {
                    JSONArray jsonArray = new JSONArray(customResponseDtoMain.getDetailedMessage());
                    try {
                        JSONObject jsonObjectMain = new JSONObject(jsonArray.get(0).toString());
                        jsonObjectMain.put("projectjson", objectMapper.readValue(jsonObjectMain.toString(), ProjectDto.class).getProjectJson());
                        JSONObject jsonObjectAux = new JSONObject(customResponseDtoAux.getDetailedMessage());
                        jsonObjectAux.keySet().forEach(keyAux -> jsonObjectMain.put(keyAux, new JSONArray(jsonObjectAux.get(keyAux).toString())));
                        customResponseDtoMain.setDetailedMessage(jsonObjectMain.toString());
                    } catch (Exception e) {
                        customResponseDtoMain.setDetailedMessage("fail due " + e.getMessage());
                        customResponseDtoMain.setSuccess(false);
                    }
                } else {
                    if (customResponseDtoAux.getMessage() != null) {
                        customResponseDtoMain.setDetailedMessage(customResponseDtoAux.getDetailedMessage());
                        customResponseDtoMain.setSuccess(false);
                    }
                }
                customResponseDtoMain.setMessage(null);
                return customResponseDtoMain;
            };
            CompletableFuture<CustomResponseDto> completableFutureMain =
                    CompletableFuture.supplyAsync(() -> queryServiceProjectAuth.getForMainOrAuxTable(jsonNode, connectionID, projectID, true));

            CompletableFuture<CustomResponseDto> completableFutureAux =
                    CompletableFuture.supplyAsync(() -> queryServiceProjectAuth.getForMainOrAuxTable(jsonNode, connectionID, projectID, false));

            customResponseDto[0] = completableFutureMain.thenCombine(completableFutureAux, combineBIFunction)
                    .exceptionally(throwable -> {
                        customResponseDto[0].setDetailedMessage(throwable.getMessage());
                        customResponseDto[0].setSuccess(false);
                        customResponseDto[0].setMessage(null);
                        return customResponseDto[0];
                    }).join();
        }

        queryService.addLogging("GET", partMessage + (!customResponseDto[0].getSuccess() ? customResponseDto[0].getDetailedMessage() : "Success"));
        return customResponseDto[0];
    }
}
