package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.ErrorCode;
import co.kr.coresolutions.facades.IQueryEbmFacade;
import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceJSON;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.StreamSupport;

@Component
@RequiredArgsConstructor
public class QueryEbmFacade implements IQueryEbmFacade {
    private final QueryService queryService;
    private final ObjectMapper objectMapper;
    private final QueryServiceJSON queryServiceJSON;
    private final Constants constants;
    private String businessQueryDir;

    @PostConstruct
    public void init() {
        businessQueryDir = constants.getBusinessQueryDir();
    }

    @Override
    @SneakyThrows
    public CustomResponseDto BizCommon(String connectionID, JsonNode jsonNode, boolean isEbm) {
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", (isEbm ? "/ebm_rulecondition/" : "/biz_rulecondition/") + connectionID + " triggered , fail due to connection file with name\t" + connectionID + "\tdoesn't exist");
            return CustomResponseDto.builder().ErrorCode(130)
                    .ErrorMessage((isEbm ? "/ebm_rulecondition/" : "/biz_rulecondition/") + connectionID + " triggered , fail due to connection file with name\t" + connectionID + "\tdoesn't exist").build();
        }
        String sql = new String(Files.readAllBytes(Paths.get(businessQueryDir + (isEbm ? "ebm_rulecondition.txt" : "biz_rulecondition.txt"))));
        if (sql.isEmpty()) {
            queryService.addLogging("POST", (isEbm ? "/ebm_rulecondition/" : "/biz_rulecondition/") + connectionID + " triggered , fail due to "
                    + (isEbm ? "ebm_rulecondition.txt" : "biz_rulecondition.txt") + " is empty");
            return CustomResponseDto.builder().ErrorCode(1)
                    .ErrorMessage((isEbm ? "/ebm_rulecondition/" : "/biz_rulecondition/") + connectionID + " triggered , fail due to "
                            + (isEbm ? "ebm_rulecondition.txt" : "biz_rulecondition.txt") + " is empty").build();
        }

        String fullQuery = new String(formatCommand(sql, new JSONObject(objectMapper.writeValueAsString(jsonNode))).getBytes(), StandardCharsets.UTF_8);
        String resultValidity = queryService.checkValidity(fullQuery, connectionID, "", "cache", false);
        if (resultValidity.startsWith("Error") || resultValidity.startsWith("error") || resultValidity.startsWith("maxRunningTimeOut") || resultValidity.startsWith("MaxRows")) {
            queryService.addLogging("POST", (isEbm ? "/ebm_rulecondition/" : "/biz_rulecondition/") + "triggered with sql\t" + fullQuery + "\t, fail due\t" + resultValidity);
            return CustomResponseDto.builder().ErrorCode(131).ErrorMessage(resultValidity).build();
        }
        if (resultValidity.equalsIgnoreCase("[]")) {
            queryService.addLogging("POST", (isEbm ? "/ebm_rulecondition/" : "/biz_rulecondition/") + "triggered with sql\t" + fullQuery + "\t, fail due Empty value returned");
            return CustomResponseDto.builder().Success(true).DetailedMessage("{}").build();
        }

        String structuredJson = new String(queryServiceJSON.structureJson(resultValidity, false, queryService.getQueryDataTypes(connectionID, fullQuery, false), "cache")
                .getBytes(), StandardCharsets.UTF_8);

        if (structuredJson.startsWith("Error") || structuredJson.startsWith("error")) {
            queryService.addLogging("POST", (isEbm ? "/ebm_rulecondition/" : "/biz_rulecondition/") + "triggered with sql\t" + fullQuery + "\t, fail due\t" + structuredJson);
            return CustomResponseDto.builder().ErrorCode(ErrorCode.FOUR.getCode()).ErrorMessage(structuredJson).build();
        }


        JSONObject jsonReturnObject = new JSONObject();
        JSONArray subGroups = new JSONArray();
        JSONObject subGroupsObject = new JSONObject();
        String goID = "GO_ID";
        String goName = "GO_NAME";
        subGroupsObject.put(goID, 1);
        subGroupsObject.put(goName, "UNION");
        subGroups.put(subGroupsObject);
        subGroupsObject = new JSONObject();
        subGroupsObject.put(goID, 2);
        subGroupsObject.put(goName, "DIFFERENCE");
        subGroups.put(subGroupsObject);
        subGroupsObject = new JSONObject();
        subGroupsObject.put(goID, 3);
        subGroupsObject.put(goName, "INTERSECTION");
        subGroups.put(subGroupsObject);
        jsonReturnObject.put("Groups", subGroups);

        List<String> bizIDs = new ArrayList<>();
        JSONObject jsonObjectGlobal = new JSONObject(structuredJson);
        if (jsonObjectGlobal.has("Input")) {
            JSONArray jsonArray = jsonObjectGlobal.getJSONArray("Input");
            JSONArray bizQuery = new JSONArray();
            JSONObject bizQueryFilters = new JSONObject();
            StreamSupport.stream(jsonArray.spliterator(), false).forEach(o -> {
                JSONObject jsonObject = new JSONObject(o.toString());
                JSONObject oneBizQueryObject = new JSONObject();
                String bizIdName = (isEbm ? "fact_id" : "query_id").toUpperCase();
                String jsonBizName = (isEbm ? "field_json_val" : "prompt_json_val").toUpperCase();
                String bizNameName = (isEbm ? "fact_name" : "query_name").toUpperCase();
                String connectionIdName = (isEbm ? "dbms_id" : "query_dbmsid").toUpperCase();
                String factSqlName = (isEbm ? "fact_sql" : "query_meta").toUpperCase();

                String notDefine = "NotDefine";

                boolean canAdd = false;
                if (jsonObject.has(bizIdName)) {
                    String bizID = jsonObject.get(bizIdName).toString();
                    if (!bizIDs.contains(bizID.equals(notDefine) ? "" : bizID)) {
                        canAdd = true;
                    }
                    if (canAdd) {
                        oneBizQueryObject.put("BIZID", bizID.equals(notDefine) ? "" : bizID);
                    }
                    if (jsonObject.has(jsonBizName)) {
                        String jsonBiz = jsonObject.get(jsonBizName).toString();
                        if (bizQueryFilters.has(bizID)) {
                            JSONArray oneBizQueryFilters = bizQueryFilters.getJSONArray(bizID);
                            oneBizQueryFilters.put(new JSONObject(jsonBiz));
                            bizQueryFilters.put(bizID, oneBizQueryFilters);
                        } else {
                            JSONArray oneBizQueryFilters = new JSONArray();
                            oneBizQueryFilters.put(new JSONObject(jsonBiz));
                            bizQueryFilters.put(bizID, oneBizQueryFilters);
                        }
                    }
                    bizIDs.add(bizID.equals(notDefine) ? "" : bizID);
                }

                if (canAdd) {
                    if (jsonObject.has(bizNameName)) {
                        String bizName = jsonObject.get(bizNameName).toString();
                        oneBizQueryObject.put("BIZNAME", bizName.equals(notDefine) ? "" : bizName);
                    }

                    if (jsonObject.has(connectionIdName)) {
                        String connectionId = jsonObject.get(connectionIdName).toString();
                        oneBizQueryObject.put("CONNECTIONID", connectionId.equals(notDefine) ? "" : connectionId);
                    }

                    if (jsonObject.has(factSqlName)) {
                        String factSql = jsonObject.get(factSqlName).toString();
                        oneBizQueryObject.put("factSql", factSql.equals(notDefine) ? "" : factSql);
                    }
                    oneBizQueryObject.put("BIZ_CATEGORY", "");
                    bizQuery.put(oneBizQueryObject);
                }
            });
            jsonReturnObject.put("BIZQUERY", bizQuery);
            jsonReturnObject.put("BIZQUERY_FILTERS", bizQueryFilters);
        }

        queryService.addLogging("POST", (isEbm ? "/ebm_rulecondition/" : "/biz_rulecondition/") + "triggered with sql\t" + fullQuery + "\t, success");
        return CustomResponseDto.builder().Success(true).DetailedMessage(jsonReturnObject.toString()).build();
    }

    public String formatCommand(String sql, JSONObject jsonObject) {
        StringBuilder fullySql = new StringBuilder();
        fullySql.append(sql).append("\t");
        jsonObject.keySet().forEach(key -> {
            fullySql.append(" " + key + " ").append("\t");
            JSONObject insideJsonObject;
            try {
                insideJsonObject = jsonObject.getJSONObject(key);
                insideJsonObject.keySet().forEach(keyChild -> fullySql.append(" " + insideJsonObject.get(keyChild).toString()).append("\t"));
            } catch (JSONException e) {
                fullySql.append(" " + jsonObject.get(key).toString()).append("\t");
            }
        });
        return fullySql.toString();
    }
}
