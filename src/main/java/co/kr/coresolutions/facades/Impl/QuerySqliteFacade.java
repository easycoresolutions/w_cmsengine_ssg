package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQuerySqliteFacade;
import co.kr.coresolutions.model.TableLoad;
import co.kr.coresolutions.model.dtos.SqliteFileRepoDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceSQLite;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Component
@RequiredArgsConstructor
public class QuerySqliteFacade implements IQuerySqliteFacade {
    private final QueryService queryService;
    private final QueryServiceSQLite queryServiceSQLite;

    @Override
    public Object createDBFile(SqliteFileRepoDto sqliteFileRepoDto) {
        String partMessage = "createdb_file/sqlite triggered, ";
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();

        String userID = sqliteFileRepoDto.getUserID();
        String dbFileName = sqliteFileRepoDto.getDbFile();

        boolean isDBFileName = queryServiceSQLite.isDBFileNameExists(userID, dbFileName);
        if (isDBFileName) {
            customResponseDto.setMessage("db-file " + dbFileName + " already exist!");
            queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
            return customResponseDto;
        }

        String resultCUFR = queryServiceSQLite.createOrUpdateFileRepo(sqliteFileRepoDto);
        if (!resultCUFR.startsWith("success")) {
            customResponseDto.setMessage(resultCUFR);
            queryService.addLogging("POST", partMessage + resultCUFR);
            return customResponseDto;
        }

        String result = queryServiceSQLite.createDBFile("mysqlite", userID, dbFileName);
        if (result.startsWith("error")) {
            customResponseDto.setMessage(new String(result.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
            return customResponseDto;
        }
        customResponseDto.setMessage("success");
        customResponseDto.setSuccess(true);
        queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
        return customResponseDto;
    }

    @Override
    public Object deleteDbFile(String userID, String dbFileName) {
        String partMessage = "sqlite/{" + userID + "}/{" + dbFileName + "} triggered, ";
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();
        String resultDFR = queryServiceSQLite.deleteFileRepo(userID, dbFileName);
        if (!resultDFR.startsWith("success")) {
            customResponseDto.setMessage(resultDFR);
            queryService.addLogging("POST", partMessage + resultDFR);
            return customResponseDto;
        }

        boolean resultDelete = queryServiceSQLite.deleteDbFile(userID, dbFileName);
        if (!resultDelete) {
            customResponseDto.setMessage(dbFileName + " doesn't exist!");
            queryService.addLogging("DELETE", partMessage + customResponseDto.getMessage());
            return customResponseDto;
        }

        customResponseDto.setMessage("success");
        customResponseDto.setSuccess(true);
        queryService.addLogging("DELETE", partMessage + customResponseDto.getMessage());
        return customResponseDto;
    }

    @Override
    public Object deleteTableFromDBfile(String userID, String tableName) {
        String partMessage = "sqlite/{" + userID + "}/{" + tableName + "} triggered, ";
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();
        String dbFile = queryServiceSQLite.getDBFileRepo(userID);
        if (dbFile.equals(QueryServiceSQLite.DBFILE + " doesn't exist!") || dbFile.equals("There is no active dbfile defined!") || dbFile.startsWith("error : {")) {
            customResponseDto.setMessage(dbFile);
            queryService.addLogging("POST", partMessage + dbFile);
            return customResponseDto;
        }

        String resultDTFR = queryServiceSQLite.deleteTableFileRepo(userID);
        if (!resultDTFR.startsWith("success")) {
            customResponseDto.setMessage(resultDTFR);
            queryService.addLogging("POST", partMessage + resultDTFR);
            return customResponseDto;
        }

        boolean resultDelete = queryServiceSQLite.deleteTableFromDBfile("mysqlite", userID, dbFile, tableName);

        if (!resultDelete) {
            customResponseDto.setMessage(tableName + " doesn't exist!");
            queryService.addLogging("DELETE", partMessage + customResponseDto.getMessage());
            return customResponseDto;
        }

        customResponseDto.setMessage("success");
        customResponseDto.setSuccess(true);
        queryService.addLogging("DELETE", partMessage + customResponseDto.getMessage());
        return customResponseDto;
    }

    @Override
    public Object tableLoad(TableLoad tableLoad) {
        String partMessage = "sqlite/tableload triggered, ";
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();
        String dbFile = queryServiceSQLite.getDBFileRepo(tableLoad.getUserid());
        if (dbFile.equals(QueryServiceSQLite.DBFILE + " doesn't exist!") || dbFile.equals("There is no active dbfile defined!") || dbFile.startsWith("error : {")) {
            customResponseDto.setMessage(dbFile);
            queryService.addLogging("POST", partMessage + dbFile);
            return customResponseDto;
        }

        tableLoad.setDb_file(dbFile);
        boolean isDBFileName = queryServiceSQLite.isDBFileNameExists(tableLoad.getUserid(), tableLoad.getDb_file());
        if (!isDBFileName) {
            customResponseDto.setMessage("db-file " + tableLoad.getDb_file() + " doesn't exist!");
            queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
            return customResponseDto;
        }

        String contentFile = queryServiceSQLite.isFileNameExists(tableLoad.getUserid(), tableLoad.getInput_directory(), tableLoad.getFile_name());
        if (contentFile.startsWith("error") || contentFile.length() == 0) {
            customResponseDto.setMessage("file_name " + tableLoad.getFile_name() + " doesn't exist!");
            queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
            return customResponseDto;
        }

        String resultUnload = queryServiceSQLite.tableLoad(tableLoad);
        if (resultUnload.startsWith("Error") || resultUnload.startsWith("sqlite3: Error")) {
            customResponseDto.setMessage(new String(resultUnload.getBytes(StandardCharsets.UTF_8)));
            queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
            return customResponseDto;
        }

        customResponseDto.setMessage("success");
        customResponseDto.setSuccess(true);
        queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
        return customResponseDto;
    }

    @Override
    public Object getDBFiles(String userID) {
        String partMessage = "sqlite/{" + userID + "}/db_files triggered, ";
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();

        String dbFiles = queryServiceSQLite.getDBFiles(userID);

        if (dbFiles.startsWith("error : {") || dbFiles.equalsIgnoreCase(QueryServiceSQLite.fileRepo + " doesn't exist!")) {
            customResponseDto.setMessage(dbFiles);
            queryService.addLogging("GET", partMessage + customResponseDto.getMessage());
            return customResponseDto;
        }

        queryService.addLogging("GET", partMessage + "success");
        return dbFiles;
    }

    @Override
    public Object activate(String userID, String dbFile) {
        String partMessage = "sqlite/{" + userID + "}/active/{" + dbFile + "} triggered, ";
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();

        String isActivated = queryServiceSQLite.activate(userID, dbFile);

        if (!isActivated.equals("success")) {
            customResponseDto.setMessage(isActivated);
            queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
            return customResponseDto;
        }

        customResponseDto.setMessage(isActivated);
        customResponseDto.setSuccess(true);
        queryService.addLogging("POST", partMessage + isActivated);
        return customResponseDto;
    }

    @Override
    public Object getActiveDbms(String userID) {
        String partMessage = "sqlite/{" + userID + "}/activefile triggered, ";
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();
        String activeDBMS = queryServiceSQLite.getActiveDBMS(userID);

        customResponseDto.setMessage(activeDBMS);
        if (activeDBMS.startsWith("error")) {
            queryService.addLogging("GET", partMessage + customResponseDto.getMessage());
            return customResponseDto;
        }

        customResponseDto.setSuccess(true);
        queryService.addLogging("GET", partMessage + "success");
        return customResponseDto;
    }
}
