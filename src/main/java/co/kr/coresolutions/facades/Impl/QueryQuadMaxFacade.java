package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryQuadMaxWorkFacade;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.dtos.QuadMaxWorkDto;
import co.kr.coresolutions.service.QueryService;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.protobuf.StringValue;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryQuadMaxFacade implements IQueryQuadMaxWorkFacade {
    private final QueryService queryService;

    @Override
    public CustomResponseDto countRows(String connectionID, String tableName) {
        String partMessage = "/{" + connectionID + "}/{" + tableName + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("GET", partMessage + "fail due to connection file with name\t" + connectionID + "\tdoesn't exist");
            return CustomResponseDto.builder().message("fail due to connection file with name\t" + connectionID + "\tdoesn't exist").build();
        }
        Connection connectionInfo = queryService.getInfoConnection(connectionID);
        if (!connectionInfo.getDBMS().isEmpty() && !connectionInfo.getID().isEmpty() && !connectionInfo.getPW().isEmpty()
                && !connectionInfo.getDRIVER().isEmpty() && !connectionInfo.getURL().isEmpty()) {
            String count = queryService.checkValidity(("select count(*) CNT from " + connectionInfo.getSCHEME() + tableName).toUpperCase(), connectionID, "", "cache", false);
            if (!count.equals("") && !count.toLowerCase().contains("error")) {
                queryService.setRunStop("cache");
                queryService.addLogging("GET", partMessage + "success");
                return CustomResponseDto.builder().Success(true).message(String.valueOf(new JSONArray(count).getJSONObject(0).getInt("CNT"))).build();
            } else {
                queryService.addLogging("GET", partMessage + "fail due to an error\t" + count);
                return CustomResponseDto.builder().message("fail due to an error\t" + count).build();
            }
        } else {
            queryService.addLogging("GET", partMessage + "fail due missing some infos from connection " + connectionID + " file");
            return CustomResponseDto.builder().message("missing some infos from connection " + connectionID + " file").build();
        }
    }

    @Override
    public Object countRows(String connectionID, QuadMaxWorkDto quadMaxWorkDto) {
        String partMessage = "/{" + connectionID + "}/getcount triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name\t" + connectionID + "\tdoesn't exist");
            return CustomResponseDto.builder().message("fail due to connection file with name\t" + connectionID + "\tdoesn't exist").build();
        }
        JSONObject response = new JSONObject();
        quadMaxWorkDto.getNodes().fields().forEachRemaining(stringJsonNodeEntry -> {
            String key = stringJsonNodeEntry.getKey();	// node_id
            JsonNode entryValue = stringJsonNodeEntry.getValue();
            if (entryValue.isTextual()) {
                String value = entryValue.asText();
                CustomResponseDto customResponseDto = null;
                
                // work테이블 존재하는지 검사
                customResponseDto = queryService.getResultQuery(connectionID,
                		"SELECT COUNT(*) COUNT FROM ALL_TABLES WHERE TABLE_NAME = '"+ quadMaxWorkDto.getProjectID() + "_" + key + "' AND OWNER = 'US_CMPN_TEMP' ");
                
                if (customResponseDto.getSuccess()) {
                	int tableCount = new JSONArray(customResponseDto.getMessage()).getJSONObject(0).getInt("COUNT");
                	if(tableCount == 1) {
                		customResponseDto = queryService.getResultQuery(connectionID,
                				("select count(*) CNT from " + queryService.getInfoConnection(connectionID).getSCHEME() +
                						quadMaxWorkDto.getProjectID() + "_" + key + " ")
                				.toUpperCase().concat(value.isEmpty() ? "" : value));
                		if (customResponseDto.getSuccess()) {
                			response.put(key, new JSONArray(customResponseDto.getMessage()).getJSONObject(0).getInt("CNT"));
                		} else {
                			response.put(key, customResponseDto.getErrorMessage());
                		}                		
                	} else {
                		response.put(key, "table not exists!");
                	}
                } else {
                	response.put(key, "table not exists?");
                }
//                CustomResponseDto customResponseDto = queryService.getResultQuery
//                        (connectionID,
//                                ("select count(*) CNT from " + queryService.getInfoConnection(connectionID).getSCHEME() +
//                                        quadMaxWorkDto.getProjectID() + "_" + key + " ")
//                                        .toUpperCase().concat(value.isEmpty() ? "" : value));
//                if (customResponseDto.getSuccess()) {
//                    response.put(key, new JSONArray(customResponseDto.getMessage()).getJSONObject(0).getInt("CNT"));
//                } else {
//                    response.put(key, customResponseDto.getErrorMessage());
//                }
            } else {
                response.put(key, "not valid json value!");
            }
        });
        queryService.addLogging("POST", partMessage + "success");
        return CustomResponseDto.builder().Success(true).message(response.toString()).build();
    }

    @Override
    public CustomResponseDto dropTable(String connectionID, String tableName) {
        String partMessage = "/{" + connectionID + "}/{" + tableName + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("DELETE", partMessage + "fail due to connection file with name\t" + connectionID + "\tdoesn't exist");
            return CustomResponseDto.builder().message("fail due to connection file with name\t" + connectionID + "\tdoesn't exist")
                    .build();
        }
        String resultValidity = "";
        co.kr.coresolutions.model.Connection connection = queryService.getInfoConnection(connectionID);
        
        if (connection.getDBMS().equalsIgnoreCase("ORACLE")) {
        	String cntResult = queryService.checkValidity("SELECT COUNT(1) AS CNT FROM ALL_OBJECTS WHERE OBJECT_TYPE IN ('TABLE') AND OBJECT_NAME = '"+tableName+"'", connectionID, "", "", false);
        	if (!cntResult.equals("") && !cntResult.toLowerCase().contains("error")) {
            	JSONArray successArray = new JSONArray(cntResult);
            	JSONObject cntRslt = (JSONObject)successArray.get(0);
            	if (cntRslt.getInt("CNT") > 0) {
	        		resultValidity = queryService.checkValidityUpdateClauses("DROP TABLE " + tableName, connectionID);
	        		if (!resultValidity.startsWith("success")) {
	        			queryService.addLogging("DELETE", "/quadmax_work/" + tableName + " triggered, fail due " + resultValidity);
	        			return CustomResponseDto.builder().ErrorCode(3).ErrorMessage("/quadmax_work/" + tableName + " triggered , fail due\t" + resultValidity).build();
	        		}
            	}
        	} 
        } else {
        	resultValidity = queryService.checkValidityUpdateClauses("DROP TABLE IF EXISTS " + tableName, connectionID);
            if (!resultValidity.startsWith("success")) {
                queryService.addLogging("DELETE", "/quadmax_work/" + tableName + " triggered, fail due " + resultValidity);
                return CustomResponseDto.builder().ErrorCode(3)
                        .ErrorMessage("/quadmax_work/" + tableName + " triggered , fail due\t" + resultValidity).build();
            }
        }     
//        String resultValidity = queryService.checkValidityUpdateClauses("DROP TABLE IF EXISTS " + tableName, connectionID);
//        if (!resultValidity.startsWith("success")) {
//            queryService.addLogging("DELETE", partMessage + "fail due " + resultValidity);
//            return CustomResponseDto.builder().message("fail due\t" + resultValidity).build();
//        }
        queryService.addLogging("DELETE", partMessage + "success");
        return CustomResponseDto.builder().Success(true).message("success").build();
    }
}

