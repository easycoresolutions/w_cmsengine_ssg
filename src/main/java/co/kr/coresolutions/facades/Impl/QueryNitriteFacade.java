package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryNitriteFacade;
import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.dizitart.no2.Cursor;
import org.dizitart.no2.Document;
import org.dizitart.no2.Filter;
import org.dizitart.no2.IndexOptions;
import org.dizitart.no2.IndexType;
import org.dizitart.no2.KeyValuePair;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.NitriteCollection;
import org.dizitart.no2.NitriteId;
import org.dizitart.no2.filters.Filters;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryNitriteFacade implements IQueryNitriteFacade {
    private static final Map<String, Long> docIdNitriteIdMap = new HashMap<>();
    private static String NITRITE_DIR;
    private final QueryService queryService;
    private final Constants constants;
    private final ObjectMapper objectMapper;

    @PostConstruct
    public void init() throws IOException {
        NITRITE_DIR = constants.getNitriteDir();
        if (!Files.exists(Paths.get(NITRITE_DIR))) {
            Files.createDirectory(Paths.get(NITRITE_DIR));
        }
    }

    @Override
    public Object save(String fileName, String collectionName, String docID, ObjectNode json) {
        String partMessage = "nitrite/{" + fileName + "}/{" + collectionName + "}/{" + docID + "} triggered, ";
        try (Nitrite nitriteDb = NitriteInitializer.getNitriteDb(fileName)) {
            NitriteCollection collection = nitriteDb.getCollection(collectionName);
            Document document = new Document();
            document.putAll(objectMapper.convertValue(json, Map.class));
            if (!QueryNitriteFacade.docIdNitriteIdMap.containsKey(docID)) {
                collection.createIndex(docID, IndexOptions.indexOptions(IndexType.NonUnique, true));
                QueryNitriteFacade.docIdNitriteIdMap.put(docID, document.getId().getIdValue());
                queryService.addLogging("POST", partMessage + "success");
                return CustomResponseDto.builder().message(collection.insert(document).getAffectedCount() + " record has been inserted").Success(true).build();
            }
            queryService.addLogging("POST", partMessage + "Requested doc-id{" + docID + "}) already exist!");
            return CustomResponseDto.builder().message("Requested doc-id{" + docID + "}) already exist!").build();
        } catch (Exception e) {
            queryService.addLogging("POST", partMessage + "fail due\t" + e.getMessage());
            return CustomResponseDto.builder().message("fail due\t" + e.getMessage()).build();
        }
    }

    @Override
    public Object delete(String fileName, String collectionName, String docID) {
        String partMessage = "nitrite/{" + fileName + "}/{" + collectionName + "}/{" + docID + "} triggered, ";
        if (!Paths.get(NITRITE_DIR + fileName + ".db").toFile().exists()) {
            queryService.addLogging("DELETE", partMessage + "Requested file name{" + fileName + "}) doesn't exist!");
            return CustomResponseDto.builder().message("Requested file name{" + fileName + "}) doesn't exist!").build();
        }
        try (Nitrite nitriteDb = NitriteInitializer.getNitriteDb(fileName)) {
            if (!nitriteDb.hasCollection(collectionName)) {
                queryService.addLogging("DELETE", partMessage + "Requested collection- name{" + collectionName + "}) doesn't exist!");
                return CustomResponseDto.builder().message("Requested collection- name{" + collectionName + "}) doesn't exist!").build();
            }
            NitriteCollection collection = nitriteDb.getCollection(collectionName);
            if (QueryNitriteFacade.docIdNitriteIdMap.containsKey(docID)) {
                collection.remove(collection.getById(NitriteId.createId(QueryNitriteFacade.docIdNitriteIdMap.get(docID))));
                QueryNitriteFacade.docIdNitriteIdMap.remove(docID);
                collection.dropIndex(docID);
                queryService.addLogging("DELETE", partMessage + "success");
                return CustomResponseDto.builder().message(docID + " deleted").Success(true).build();
            } else {
                queryService.addLogging("DELETE", partMessage + "Requested doc-id{" + docID + "}) doesn't exist!");
                return CustomResponseDto.builder().message("Requested doc-id{" + docID + "}) doesn't exist!").build();
            }
        } catch (Exception e) {
            queryService.addLogging("DELETE", partMessage + "fail due\t" + e.getMessage());
            return CustomResponseDto.builder().message("fail due\t" + e.getMessage()).build();
        }
    }

    @Override
    public Object update(String fileName, String collectionName, String docID, ObjectNode json) {
        String partMessage = "nitrite/{" + fileName + "}/{" + collectionName + "}/{" + docID + "} triggered, ";
        try (Nitrite nitriteDb = NitriteInitializer.getNitriteDb(fileName)) {
            NitriteCollection collection = nitriteDb.getCollection(collectionName);
            Document document = collection.getById(NitriteId.createId(QueryNitriteFacade.docIdNitriteIdMap.get(docID)));
            int affectedCount;
            if (document == null) {
                document = new Document();
                document.putAll(objectMapper.convertValue(json, Map.class));
                affectedCount = collection.insert(document).getAffectedCount();
            } else {
                document.putAll(objectMapper.convertValue(json, Map.class));
                affectedCount = collection.update(document).getAffectedCount();
            }
            QueryNitriteFacade.docIdNitriteIdMap.put(docID, document.getId().getIdValue());
            queryService.addLogging("PUT", partMessage + "success");
            return CustomResponseDto.builder().message(affectedCount + " record has been updated").Success(true).build();
        } catch (Exception e) {
            queryService.addLogging("PUT", partMessage + "fail due\t" + e.getMessage());
            return CustomResponseDto.builder().message("fail due\t" + e.getMessage()).build();
        }
    }

    @Override
    public Object get(String fileName) {
        String partMessage = "nitrite/{" + fileName + "}/collections triggered, ";
        try (Nitrite nitriteDb = NitriteInitializer.getNitriteDb(fileName)) {
            queryService.addLogging("GET", partMessage + "success");
            return nitriteDb.listCollectionNames();
        } catch (Exception e) {
            queryService.addLogging("GET", partMessage + "fail due\t" + e.getMessage());
            return CustomResponseDto.builder().message("fail due\t" + e.getMessage()).build();
        }
    }

    @Override
    public Object getIf(String fileName, String collectionName, ObjectNode json) {
        String partMessage = "nitrite/find/{" + fileName + "}/{" + collectionName + "}/docs triggered, ";
        try (Nitrite nitriteDb = NitriteInitializer.getNitriteDb(fileName)) {
            JSONObject jsonObject = new JSONObject();
            NitriteCollection nitriteCollection = nitriteDb.getCollection(collectionName);
            Cursor cursor = json.size() > 0 ? nitriteCollection.find(Filters.and(getFilters(json))) : nitriteCollection.find();
            getCustomJsonResponse(collectionName, jsonObject, cursor);
            return jsonObject.toString();
        } catch (Exception e) {
            queryService.addLogging("GET", partMessage + "fail due\t" + e.getMessage());
            return CustomResponseDto.builder().message("fail due\t" + e.getMessage()).build();
        }
    }

    @Override
    public Object getByDocID(String fileName, String collectionName, String docID) {
        String partMessage = "nitrite/{" + fileName + "}/{" + collectionName + "}/{" + docID + "} triggered, ";
        if (!Paths.get(NITRITE_DIR + fileName + ".db").toFile().exists()) {
            queryService.addLogging("GET", partMessage + "db file({" + fileName + "}) doesn't exist!");
            return CustomResponseDto.builder().message("db file({" + fileName + "}) doesn't exist!").build();
        }
        try (Nitrite nitriteDb = NitriteInitializer.getNitriteDb(fileName)) {
            if (!nitriteDb.hasCollection(collectionName)) {
                queryService.addLogging("GET", partMessage + "collection name ({" + collectionName + "}) doesn't exist!");
                return CustomResponseDto.builder().message("collection name ({" + collectionName + "}) doesn't exist!").build();
            }
            NitriteCollection collection = nitriteDb.getCollection(collectionName);
            if (QueryNitriteFacade.docIdNitriteIdMap.containsKey(docID)) {
                Document document = collection.getById(NitriteId.createId(QueryNitriteFacade.docIdNitriteIdMap.get(docID)));
                queryService.addLogging("GET", partMessage + "success");
                return objectMapper.writeValueAsString(StreamSupport.stream(document.spliterator(), false)
                        .filter(keyValuePair -> !keyValuePair.getKey().equalsIgnoreCase("_revision") &&
                                !keyValuePair.getKey().equalsIgnoreCase("_modified") &&
                                !keyValuePair.getKey().equalsIgnoreCase("_id"))
                        .collect(Collectors.toMap(KeyValuePair::getKey, KeyValuePair::getValue)));
            } else {
                queryService.addLogging("GET", partMessage + "docid({" + docID + "}) doesn't exist!");
                return CustomResponseDto.builder().message("docid({" + docID + "}) doesn't exist!").build();
            }
        } catch (Exception e) {
            queryService.addLogging("GET", partMessage + "fail due\t" + e.getMessage());
            return CustomResponseDto.builder().message("fail due\t" + e.getMessage()).build();
        }
    }

    @Override
    public Object saveIndexes(String fileName, String collectionName, ObjectNode json) {
        String partMessage = "index/{" + fileName + "}/{" + collectionName + "} triggered, ";
        try (Nitrite nitriteDb = NitriteInitializer.getNitriteDb(fileName)) {
            if (!nitriteDb.hasCollection(collectionName)) {
                queryService.addLogging("POST", partMessage + "collection name ({" + collectionName + "}) doesn't exist!");
                return CustomResponseDto.builder().message("collection name ({" + collectionName + "}) doesn't exist!").build();
            }
            NitriteCollection collection = nitriteDb.getCollection(collectionName);
            json.fields().forEachRemaining(jsonNode -> collection.createIndex(jsonNode.getKey(),
                    IndexOptions.indexOptions(Optional.of(IndexType.valueOf(jsonNode.getValue().asText())).orElse(IndexType.NonUnique), true)));
            queryService.addLogging("POST", partMessage + "success");
            return CustomResponseDto.builder().message("success").Success(true).build();
        } catch (Exception e) {
            queryService.addLogging("POST", partMessage + "fail due\t" + e.getMessage());
            return CustomResponseDto.builder().message("fail due\t" + e.getMessage()).build();
        }
    }

    @Override
    public Object deleteIndex(String fileName, String collectionName, String fieldName) {
        String partMessage = "index/{" + fileName + "}/{" + collectionName + "}/{" + fieldName + "} triggered, ";
        if (!Paths.get(NITRITE_DIR + fileName + ".db").toFile().exists()) {
            queryService.addLogging("DELETE", partMessage + "Requested file name{" + fileName + "}) doesn't exist!");
            return CustomResponseDto.builder().message("Requested file name{" + fileName + "}) doesn't exist!").build();
        }
        try (Nitrite nitriteDb = NitriteInitializer.getNitriteDb(fileName)) {
            if (!nitriteDb.hasCollection(collectionName)) {
                queryService.addLogging("DELETE", partMessage + "Requested collection- name{" + collectionName + "}) doesn't exist!");
                return CustomResponseDto.builder().message("Requested collection- name{" + collectionName + "}) doesn't exist!").build();
            }
            NitriteCollection collection = nitriteDb.getCollection(collectionName);
            if (collection.hasIndex(fieldName)) {
                collection.dropIndex(fieldName);
                queryService.addLogging("DELETE", partMessage + "success");
                return CustomResponseDto.builder().message(fieldName + " index deleted").Success(true).build();
            } else {
                queryService.addLogging("DELETE", partMessage + "Requested fieldName index{" + fieldName + "}) doesn't exist!");
                return CustomResponseDto.builder().message("Requested fieldName index{" + fieldName + "}) doesn't exist!").build();
            }
        } catch (Exception e) {
            queryService.addLogging("DELETE", partMessage + "fail due\t" + e.getMessage());
            return CustomResponseDto.builder().message("fail due\t" + e.getMessage()).build();
        }
    }

    @Override
    public Object rebuildIndex(String fileName, String collectionName, String fieldName) {
        String partMessage = "indexrebuild/{" + fileName + "}/{" + collectionName + "}/{" + fieldName + "} triggered, ";
        if (!Paths.get(NITRITE_DIR + fileName + ".db").toFile().exists()) {
            queryService.addLogging("POST", partMessage + "Requested file name{" + fileName + "}) doesn't exist!");
            return CustomResponseDto.builder().message("Requested file name{" + fileName + "}) doesn't exist!").build();
        }
        try (Nitrite nitriteDb = NitriteInitializer.getNitriteDb(fileName)) {
            if (!nitriteDb.hasCollection(collectionName)) {
                queryService.addLogging("POST", partMessage + "Requested collection- name{" + collectionName + "}) doesn't exist!");
                return CustomResponseDto.builder().message("Requested collection- name{" + collectionName + "}) doesn't exist!").build();
            }
            NitriteCollection collection = nitriteDb.getCollection(collectionName);
            if (collection.hasIndex(fieldName)) {
                collection.rebuildIndex(fieldName, true);
                queryService.addLogging("POST", partMessage + "success");
                return CustomResponseDto.builder().message(fieldName + " index rebuilt").Success(true).build();
            } else {
                queryService.addLogging("POST", partMessage + "Requested fieldName index{" + fieldName + "}) doesn't exist!");
                return CustomResponseDto.builder().message("Requested fieldName index{" + fieldName + "}) doesn't exist!").build();
            }
        } catch (Exception e) {
            queryService.addLogging("POST", partMessage + "fail due\t" + e.getMessage());
            return CustomResponseDto.builder().message("fail due\t" + e.getMessage()).build();
        }
    }

    private Filter[] getFilters(JsonNode node) {
        List<Filter> filters = new ArrayList<>();
        node.fields().forEachRemaining(jsonNode -> {
            Filter filter = getCommonFilter(jsonNode.getKey(), jsonNode.getValue());
            if (filter != null) {
                filters.add(filter);
            }
        });
        return filters.toArray(new Filter[0]);
    }

    private Filter getCommonFilter(String operation, JsonNode jsonNodeValues) {
        if (operation.equalsIgnoreCase("and")) {
            return Filters.and(getFilters(jsonNodeValues));
        } else if (operation.equalsIgnoreCase("or")) {
            return Filters.or(getFilters(jsonNodeValues));
        } else if (operation.equalsIgnoreCase("not")) {
            return Filters.not(getFilters(jsonNodeValues)[0]);
        } else if (jsonNodeValues.size() > 1) {
            String key = jsonNodeValues.get(0).asText();
            if (operation.equalsIgnoreCase("eq")) {
                JsonNode value = jsonNodeValues.get(1);
                return Filters.eq(key, value.isNumber() ? value.numberValue() : (value.isObject() ? value : value.asText()));
            } else if (operation.equalsIgnoreCase("gt")) {
                return Filters.gt(key, jsonNodeValues.get(1).numberValue());
            } else if (operation.equalsIgnoreCase("gte")) {
                return Filters.gte(key, jsonNodeValues.get(1).numberValue());
            } else if (operation.equalsIgnoreCase("lt")) {
                return Filters.lt(key, jsonNodeValues.get(1).numberValue());
            } else if (operation.equalsIgnoreCase("lte")) {
                return Filters.lte(key, jsonNodeValues.get(1).numberValue());
            } else if (operation.equalsIgnoreCase("text")) {
                return Filters.text(key, jsonNodeValues.get(1).asText());
            } else if (operation.equalsIgnoreCase("regex")) {
                return Filters.regex(key, jsonNodeValues.get(1).asText());
            } else if (operation.equalsIgnoreCase("in") || operation.equalsIgnoreCase("notIn")) {
                List values = new ArrayList();
                IntStream.range(1, jsonNodeValues.size())
                        .forEach(index -> {
                            JsonNode value = jsonNodeValues.get(index);
                            values.add(value.isNumber() ? value.numberValue() : (value.isObject() ? value : value.asText()));
                        });
                return Filters.notIn(key, values.toArray());
            } else if (operation.equalsIgnoreCase("elemMatch")) {
                return Filters.elemMatch(key, getFilters(jsonNodeValues.get(1))[0]);
            }
        }
        return null;
    }

    private void getCustomJsonResponse(String collectionName, JSONObject jsonObject, Cursor cursor) {
        try {
            JSONArray jsonArrayCustom = new JSONArray();
            JSONArray jsonArray = new JSONArray(objectMapper.writeValueAsString(cursor.iterator()));
            jsonArray.forEach(o -> {
                JSONObject jsonObject1 = new JSONObject(o.toString());
                jsonObject1.remove("_revision");
                jsonObject1.remove("_id");
                jsonObject1.remove("_modified");
                jsonArrayCustom.put(jsonObject1);
            });
            jsonObject.put(collectionName, jsonArrayCustom);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private interface NitriteInitializer {
        static Nitrite getNitriteDb(String fileName) {
            return Nitrite.builder().compressed().filePath(NITRITE_DIR + fileName + ".db").autoCommitBufferSize(1024 * 10)
                    .openOrCreate("mss", "++Core2018");
        }
    }
}
