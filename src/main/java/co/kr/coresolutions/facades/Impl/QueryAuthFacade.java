package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.ErrorCode;
import co.kr.coresolutions.configuration.security.JwtTokenProvider;
import co.kr.coresolutions.dao.OrderedJSONObject;
import co.kr.coresolutions.facades.IQueryAuthFacade;
import co.kr.coresolutions.jpa.domain.User;
import co.kr.coresolutions.model.Role;
import co.kr.coresolutions.model.dtos.LoginDto;
import co.kr.coresolutions.service.QueryServiceAuth;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryAuthFacade implements IQueryAuthFacade {
    private final QueryServiceAuth queryServiceAuth;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;
    private final JdbcTemplate jdbcTemplate;

    @Override
    public CustomResponseDto login(LoginDto loginDto) {
        CustomResponseDto customResponseDto;
        User user = queryServiceAuth.findUserById(loginDto.getUserId());
        if (user == null) {
            return CustomResponseDto.builder().ErrorCode(ErrorCode.ONE.getCode()).ErrorMessage("there is no emp_id defined in T_EMP").build();
        } else {
            try {
                Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUserId(), loginDto.getPassword()));
                List<Role> roles = authentication.getAuthorities().stream().map(o -> Role.valueOf(o.getAuthority())).collect(Collectors.toList());
                String token = jwtTokenProvider
                        .generateToken(loginDto.getUserId());
                customResponseDto = queryServiceAuth.getDetails(loginDto.getUserId());
                if (customResponseDto == null) {
//                    queryServiceAuth.saveUserHistory(loginDto.getUserId(), Optional.ofNullable(user.getUserkey()).orElse(""), 3, null, null);
                    return CustomResponseDto.builder().ErrorCode(ErrorCode.THREE.getCode()).ErrorMessage("internal math_id is not defined").build();
                }
/*
                if (Optional.ofNullable(customResponseDto.getErrorCode()).orElse(0).equals(4)) {
                    queryServiceAuth.saveUserHistory(loginDto.getUserId(), Optional.ofNullable(user.getUserkey()).orElse(""), 3, null, null);
                    return CustomResponseDto.builder().ErrorCode(ErrorCode.FOUR.getCode()).ErrorMessage("last pw changed-date exceed 3 month").build();
                }
                if (Optional.ofNullable(customResponseDto.getErrorCode()).orElse(0).equals(5)) {
                    queryServiceAuth.saveUserHistory(loginDto.getUserId(), Optional.ofNullable(user.getUserkey()).orElse(""), 3, null, null);
                    return CustomResponseDto.builder().ErrorCode(ErrorCode.FIVE.getCode()).ErrorMessage("Number of login action exceed a given threadhold. ").build();
                }
*/
                customResponseDto.setServiceConnectionKey(token);
                customResponseDto.setInterAccessLevel(roles.get(0).getCodeAuthority());
                customResponseDto.setSuccess(true);
//                queryServiceAuth.saveUserHistory(loginDto.getUserId(), Optional.ofNullable(user.getUserkey()).orElse(""), 1, null, null);
            } catch (AuthenticationException | IOException e) {
//                queryServiceAuth.saveUserHistory(loginDto.getUserId(), Optional.ofNullable(user.getUserkey()).orElse(""), 3, null, null);
                return CustomResponseDto.builder().ErrorCode(ErrorCode.TWO.getCode()).ErrorMessage("given pw is not matched with saved-pw.").build();
            }
        }

        return customResponseDto;
    }

    @Override
    public CustomResponseDto logout(HttpServletRequest request) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String usernameToken;
        if (principal instanceof UserDetails) {
            usernameToken = ((UserDetails) principal).getUsername();
        } else {
            usernameToken = principal.toString();
        }
        final String requestHeader = request.getHeader("Authorization");
        String authToken = null;
        if (requestHeader != null && requestHeader.startsWith("Bearer ")) {
            authToken = requestHeader.substring(7);
        }
        if (authToken != null) {
            jdbcTemplate.execute("INSERT INTO Tokens_BackListing(token, userId) VALUES ('" + authToken + "', '" + usernameToken + "')");
        }
        return CustomResponseDto.builder().Success(true).build();
    }

/*
    @Override
    public CustomResponseDto addUser(LoginDto loginDto) {
        if (loginDto.getInternalAuths().isEmpty()) {
            return CustomResponseDto.builder().ErrorCode(6).ErrorMessage("InternalAuths should be defined").build();
        }
        if (loginDto.getAccessLevel() == null) {
            return CustomResponseDto.builder().ErrorCode(6).ErrorMessage("Access level should be defined").build();
        } else if (loginDto.getAccessLevel() > 1 || loginDto.getAccessLevel() < 0) {
            return CustomResponseDto.builder().ErrorCode(6).ErrorMessage("Access level should be valid 0 or 1").build();
        }
        if (queryServiceAuth.findUserById(loginDto.getUserId()) == null) {
            Map<String, String> map = queryServiceAuth.validateAndOrChangePassword(loginDto.getUserId(), loginDto.getPassword(), "", "", "", false);
            if (map.containsKey("ErrorMessage")) {
                if (map.get("ErrorMessage").length() > 0) {
                    return CustomResponseDto.builder().ErrorCode(ErrorCode.valueOf(map.get("ErrorCode")).getCode()).ErrorMessage(map.get("ErrorMessage")).build();
                }
            }
            map = queryServiceAuth.addUser(loginDto);
            if (map.containsKey("ErrorMessage")) {
                if (map.get("ErrorMessage").length() > 0) {
                    return CustomResponseDto.builder().ErrorCode(ErrorCode.valueOf(map.get("ErrorCode")).getCode()).ErrorMessage(map.get("ErrorMessage")).build();
                } else {
                    return CustomResponseDto.builder().Success(true).InterAccessLevel(loginDto.getAccessLevel()).build();
                }
            }
        }
        return CustomResponseDto.builder().ErrorCode(ErrorCode.ONE.getCode()).ErrorMessage("USERID already existing.").build();
    }

    @Override
    public CustomResponseDto removeUser(LoginDto loginDto) {
        if (queryServiceAuth.findUserById(loginDto.getUserId()) != null) {
            Map<String, String> map = queryServiceAuth.removeUser(loginDto);
            if (map.containsKey("ErrorMessage")) {
                if (map.get("ErrorMessage").length() > 0) {
                    return CustomResponseDto.builder().ErrorCode(ErrorCode.valueOf(map.get("ErrorCode")).getCode()).ErrorMessage(map.get("ErrorMessage")).build();
                } else {
                    return CustomResponseDto.builder().Success(true).build();
                }
            }
        }
        return CustomResponseDto.builder().ErrorCode(ErrorCode.ONE.getCode()).ErrorMessage("USERID doesn't exist").build();
    }


    @Override
    public CustomResponseDto changePwUser(String userId, String password, String newPassword) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String usernameToken = null;
        String passwordToken = null;

        if (principal instanceof UserDetails) {
            usernameToken = ((UserDetails) principal).getUsername();
            passwordToken = ((UserDetails) principal).getPassword();
        }

        if (usernameToken == null || !usernameToken.equals(userId)) {
            return CustomResponseDto.builder().ErrorCode(ErrorCode.SIX.getCode()).ErrorMessage("you don't have permission to change another user").build();
        }
        User user = queryServiceAuth.findUserById(userId);
        if (user != null) {
            Map<String, String> map = queryServiceAuth.validateAndOrChangePassword(userId, password, newPassword, passwordToken, user.getUserkey(), true);
            if (map.containsKey("ErrorMessage")) {
                if (map.get("ErrorMessage").length() > 0) {
                    return CustomResponseDto.builder().ErrorCode(ErrorCode.valueOf(map.get("ErrorCode")).getCode()).ErrorMessage(map.get("ErrorMessage")).build();
                }
            }
            return CustomResponseDto.builder().Success(true).InterAccessLevel(Integer.parseInt(user.getAccessLevel())).build();
        }
        return CustomResponseDto.builder().ErrorCode(ErrorCode.ONE.getCode()).ErrorMessage("USERID doesn't exist").build();
    }

        @Override
    public CustomResponseDto initPassword(JsonNode jsonNode) {
        if (!jsonNode.has("UserId") || !jsonNode.get("UserId").isTextual()) {
            return CustomResponseDto.builder().ErrorCode(ErrorCode.ONE.getCode()).ErrorMessage("UserId required And should be text").build();
        }
        String userID = jsonNode.get("UserId").asText();
        User user = queryServiceAuth.findUserById(userID);
        if (user != null) {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String usernameToken = null;

            if (principal instanceof UserDetails) {
                usernameToken = ((UserDetails) principal).getUsername();
            }

            final boolean[] isAdmin = {false};
            SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream().findFirst().ifPresent(o -> {
                if (o.getAuthority().equalsIgnoreCase("ROLE_ADMIN")) {
                    isAdmin[0] = true;
                }
            });

            if (!isAdmin[0]) {
                if (usernameToken == null || !usernameToken.equals(userID)) {
                    return CustomResponseDto
                            .builder().ErrorCode(ErrorCode.SIX.getCode()).ErrorMessage("you don't have permission to intipw another user, your userName doesn't match UserId").build();
                }
            }

            String newPassword = generatePassword();
            queryServiceAuth.commonUpdatePw(userID, user.getUserkey(), newPassword, new HashMap<>(), 5, "");
            return CustomResponseDto.builder().Success(true).plain_pw(newPassword).build();
        }
        return CustomResponseDto.builder().ErrorCode(140).ErrorMessage("USERID doesn't exist").build();
    }

        private String generatePassword() {
        int max = 4;
        int min = 2;
        Random r = new Random();
        String upperCaseLetters = RandomStringUtils.random(r.nextInt((max - min) + 1) + min, 65, 90, true, true);
        String lowerCaseLetters = RandomStringUtils.random(r.nextInt((max - min) + 1) + min, 97, 122, true, true);
        String numbers = RandomStringUtils.randomNumeric(r.nextInt((max - min) + 1) + min);
        String specialChar = RandomStringUtils.random(2, 33, 47, false, false);
        String totalChars = RandomStringUtils.randomAlphanumeric(2);
        String combinedChars = upperCaseLetters.concat(lowerCaseLetters)
                .concat(numbers)
                .concat(specialChar)
                .concat(totalChars);
        List<Character> pwdChars = combinedChars.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
        Collections.shuffle(pwdChars);
        return pwdChars.stream().collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString();
    }


*/

    @Override
    public CustomResponseDto getBlackList() {
        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet("SELECT * FROM Tokens_BackListing");
        SqlRowSetMetaData sqlRowSetMetaData = sqlRowSet.getMetaData();
        JSONArray jsonArray = new JSONArray();
        while (sqlRowSet.next()) {
            OrderedJSONObject jsonObject = new OrderedJSONObject();
            for (int i = 0; i < sqlRowSetMetaData.getColumnCount(); i++) {
                String value = sqlRowSet.getString(i + 1);
                jsonObject.put(sqlRowSetMetaData.getColumnName(i + 1), value);
            }
            jsonArray.put(jsonObject);
        }
        return CustomResponseDto.builder().Success(true).DetailedMessage(jsonArray.toString()).build();
    }


}
