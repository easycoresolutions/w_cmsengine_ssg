package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryClickHouseFacade;
import co.kr.coresolutions.model.TableLoadClickHouse;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceClickHouse;
import co.kr.coresolutions.service.QueryServiceJSON;
import co.kr.coresolutions.service.QueryServiceSQLite;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class QueryClickHouseFacade implements IQueryClickHouseFacade {
    private final QueryService queryService;
    private final QueryServiceSQLite queryServiceSQLite;
    private final QueryServiceClickHouse queryServiceClickHouse;
    private final QueryServiceJSON queryServiceJSON;

    @Override
    public Object tableLoad(TableLoadClickHouse tableLoadClickHouse) {
        String queryID = UUID.randomUUID().toString();
        String owner = tableLoadClickHouse.getUserID();
        String partMessage = "clickhouse_import triggered, query_id : " + queryID + " , ";
        TableLoadClickHouse.ClickHouseDetails clickHouseDetails =
                TableLoadClickHouse.ClickHouseDetails.builder().queryID(queryID).fileName(tableLoadClickHouse.getFileName()).owner(owner)
                        .startdatetime(QueryServiceSQLite.SIMPLE_DATE_FORMAT.format(new Date())).build();

        tableLoadClickHouse.setClickHouseDetails(clickHouseDetails);
        QueryLoadFacade.sessionClickHouseImport.put(queryID, clickHouseDetails);
        String contentFile = queryServiceSQLite.isFileNameExists(tableLoadClickHouse.getUserID(), tableLoadClickHouse.getInputDirectory(), tableLoadClickHouse.getFileName());
        if (contentFile.startsWith("error") || contentFile.length() == 0) {
            queryService.addLogging("POST", partMessage + "file_name " + tableLoadClickHouse.getFileName() + " doesn't exist.");
            QueryLoadFacade.sessionClickHouseImport.remove(queryID);
            return CustomResponseDto.builder().message("file_name " + tableLoadClickHouse.getFileName() + " doesn't exist.").build();
        }

        CustomResponseDto customResponseDto = queryServiceClickHouse.tableLoad(tableLoadClickHouse);
        if (!customResponseDto.getSuccess()) {
            QueryLoadFacade.sessionClickHouseImport.remove(queryID);
            queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
            return customResponseDto;
        }

        String log = "clickhouse_import tableload successfully";
        queryService.addLogging("POST", log);
        QueryLoadFacade.sessionClickHouseImport.remove(queryID);
        return CustomResponseDto.builder().Success(true).message(log).build();
    }

    @Override
    public Object monitorProcess(String connectionID) {
        String partMessage = "clickhouse_process/{" + connectionID + "} triggered, ";
        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExist) {
            queryService.addLogging("GET", partMessage + connectionID + " (information) doesn't exist.");
            return CustomResponseDto.builder().message(connectionID + " (information) doesn't exist.").build();
        }
        CustomResponseDto customResponseDto = queryServiceClickHouse.getListProcess(connectionID);
        if (!customResponseDto.getSuccess()) {
            queryService.addLogging("GET", partMessage + customResponseDto.getMessage());
            return customResponseDto;
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("success", true);
        jsonObject.put("message", "success");
        jsonObject.put("data", new JSONObject(new String(queryServiceJSON.structureJson(customResponseDto.getMessage(),
                false, new HashMap<>(), "cache").getBytes(), StandardCharsets.UTF_8)));
        queryService.addLogging("GET", partMessage + "success");

        return jsonObject.toString();
    }

    @Override
    public Object deleteProcess(String connectionID, String queryID) {
        String partMessage = "clickhouse_process/{" + connectionID + "}/{" + queryID + "} triggered, ";
        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExist) {
            queryService.addLogging("DELETE", partMessage + connectionID + " (information) doesn't exist.");
            return CustomResponseDto.builder().message(connectionID + " (information) doesn't exist.").build();
        }
        CustomResponseDto customResponseDto = queryServiceClickHouse.removeProcess(connectionID, queryID);
        if (!customResponseDto.getSuccess()) {
            queryService.addLogging("DELETE", partMessage + customResponseDto.getMessage());
            return customResponseDto;
        }
        queryService.addLogging("DELETE", partMessage + "success");
        return CustomResponseDto.ok();
    }

    @Override
    public Object monitorImport() {
        String partMessage = "clickhouse_import/monitor triggered, ";
        queryService.addLogging("GET", partMessage);
        return queryServiceClickHouse.getCacheImport();
    }

    @Override
    public Object deleteImport(String queryID) {
        String partMessage = "clickhouse_import/monitor/{" + queryID + "} triggered, ";
        if (queryServiceClickHouse.removeCacheClickHouseImport(queryID)) {
            queryService.addLogging("DELETE", partMessage + "success");
            return CustomResponseDto.builder().message("clickhouse_import with query_id " + queryID + " deleted success").Success(true).build();
        }
        queryService.addLogging("DELETE", partMessage + "fail queryID does not exist");
        return CustomResponseDto.builder().message("queryID does not exist!").build();
    }

    @Override
    public Object monitorTables(String connectionID, String dbName) {
        String partMessage = "clickhouse_tables/{" + connectionID + "}/{" + dbName + "} triggered, ";
        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExist) {
            queryService.addLogging("GET", partMessage + connectionID + " (information) doesn't exist.");
            return CustomResponseDto.builder().message(connectionID + " (information) doesn't exist.").build();
        }
        CustomResponseDto customResponseDto = queryServiceClickHouse.listTables(connectionID, dbName);
        if (!customResponseDto.getSuccess()) {
            queryService.addLogging("GET", partMessage + customResponseDto.getMessage());
            return customResponseDto;
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("success", true);
        jsonObject.put("message", "success");
        jsonObject.put("data", new JSONObject(new String(queryServiceJSON.structureJson(customResponseDto.getMessage(),
                false, new HashMap<>(), "cache").getBytes(), StandardCharsets.UTF_8)));
        queryService.addLogging("GET", partMessage + "success");

        return jsonObject.toString();
    }

}
