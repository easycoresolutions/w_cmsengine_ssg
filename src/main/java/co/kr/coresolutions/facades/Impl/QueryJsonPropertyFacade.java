package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryJsonPropertyFacade;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.service.QueryService;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class QueryJsonPropertyFacade implements IQueryJsonPropertyFacade {
    static final String connectionID = "quadmax";
    static Connection infoConnectionInput;
    static String inputSCHEMA;
    private final QueryService queryService;

    @PostConstruct
    public void init() {
        infoConnectionInput = queryService.getInfoConnection(connectionID);
        inputSCHEMA = infoConnectionInput.getSCHEME();
    }

    @Override
    public Object post(String user, String subsystem, String keyName, JsonNode objectNode) {
        String partMessage = "jsonproperty/{" + user + "}/{" + subsystem + "}/{" + keyName + "} triggered, ";
        if (!queryService.isConnectionIdFileExists(connectionID)) {
            queryService.addLogging("POST", partMessage + "fail due " + connectionID + " (information) doesn't exist.");
            return CustomResponseDto.builder().message(connectionID + " (information) doesn't exist.").build();
        }

        String queryGetMaxSeq = "SELECT MAX(SEQ) FROM " + inputSCHEMA + "T_JSON_PROPERTIES WHERE SUBSYS = '" + subsystem + "' AND KEYNAME = '" + keyName + "'";
        CustomResponseDto customResponseDto = queryService.getResultQuery(connectionID, queryGetMaxSeq);

        if (!customResponseDto.getSuccess()) {
            queryService.addLogging("POST", partMessage + customResponseDto.getErrorMessage());
            return CustomResponseDto.builder().message(customResponseDto.getErrorMessage()).build();
        }

        int maxSeq = 0;

        JSONArray jsonArray = new JSONArray(customResponseDto.getMessage());
        if (!jsonArray.isEmpty()) {
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            if (jsonObject.has("MAX(SEQ)")) {
                try {
                    maxSeq = Integer.parseInt(jsonObject.get("MAX(SEQ)").toString());
                } catch (NumberFormatException e) {
                    queryService.addLogging("POST", partMessage + e.getMessage());
                }
            }
        }

        String resultValidity = queryService.checkValidityUpdateClauses("INSERT INTO " + inputSCHEMA +
                "T_JSON_PROPERTIES(SUBSYS, KEYNAME, SEQ, OWNER, JSONVALUE, LOAD_DTTM) VALUES (" +
                "'" + subsystem + "', '" + keyName + "', " + (maxSeq + 1) + ", '" + user + "', '" + objectNode.toString()
                .replace("'", "''") + "', '" +
                new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "')", connectionID);
        if (!resultValidity.isEmpty() && !resultValidity.startsWith("success")) {
            queryService.addLogging("POST", partMessage + "fail due " + resultValidity);
            return CustomResponseDto.builder().message("fail due " + resultValidity).build();
        }
        queryService.addLogging("POST", partMessage + "success");
        return CustomResponseDto.ok();
    }

    @Override
    public Object delete(String user, String subsystem, String keyName, int seqNum) {
        String partMessage = "jsonproperty/{" + user + "}/{" + subsystem + "}/{" + keyName + "}/{" + seqNum + "} triggered, ";
        if (!queryService.isConnectionIdFileExists(connectionID)) {
            queryService.addLogging("DELETE", partMessage + "fail due " + connectionID + " (information) doesn't exist.");
            return CustomResponseDto.builder().message(connectionID + " (information) doesn't exist.").build();
        }

        String resultValidity = queryService.checkValidityUpdateClauses("DELETE FROM " + inputSCHEMA + "T_JSON_PROPERTIES WHERE SUBSYS = '" + subsystem + "' AND " +
                " OWNER = '" + user + "' AND KEYNAME = '" + keyName + "' AND SEQ = " + seqNum, connectionID);

        if (!resultValidity.startsWith("success")) {
            queryService.addLogging("DELETE", partMessage + "fail due " + resultValidity);
            return CustomResponseDto.builder().message("fail due " + resultValidity).build();
        }
        queryService.addLogging("DELETE", partMessage + "success");
        return CustomResponseDto.ok();
    }

    @Override
    public Object get() {
        String partMessage = "jsonproperty triggered, ";
        if (!queryService.isConnectionIdFileExists(connectionID)) {
            queryService.addLogging("GET", partMessage + "fail due " + connectionID + " (information) doesn't exist.");
            return CustomResponseDto.builder().message(connectionID + " (information) doesn't exist.").build();
        }

        String querySelect = "SELECT SUBSYS,KEYNAME FROM " + inputSCHEMA + "T_JSON_PROPERTIES GROUP BY SUBSYS,KEYNAME";
        CustomResponseDto customResponseDto = queryService.getResultQuery(connectionID, querySelect);

        if (!customResponseDto.getSuccess()) {
            if (customResponseDto.getErrorMessage().equalsIgnoreCase("fail due []")) {
                queryService.addLogging("GET", partMessage + "success");
                return new JSONArray().toString();
            }
            queryService.addLogging("GET", partMessage + customResponseDto.getErrorMessage());
            return CustomResponseDto.builder().message(customResponseDto.getErrorMessage()).build();
        }

        queryService.addLogging("GET", partMessage + "success");
        return new JSONArray(customResponseDto.getMessage()).toString();
    }

    @Override
    public Object findBy(String user, String subsystem, String keyName) {
        String partMessage = "jsonproperty/{" + user + "}/{" + subsystem + "}/{" + keyName + "} triggered, ";
        if (!queryService.isConnectionIdFileExists(connectionID)) {
            queryService.addLogging("GET", partMessage + "fail due " + connectionID + " (information) doesn't exist.");
            return CustomResponseDto.builder().message(connectionID + " (information) doesn't exist.").build();
        }

        String querySelect = "SELECT T.JSONVALUE FROM (SELECT MAX(SEQ) AS MAX_SEQ " +
                "FROM " + inputSCHEMA + "T_JSON_PROPERTIES WHERE SUBSYS = '" + subsystem + "' and KEYNAME = '" + keyName + "' and OWNER = '" + user + "') AS MAX " +
                "INNER JOIN " + inputSCHEMA + "T_JSON_PROPERTIES AS T ON T.SEQ = MAX.MAX_SEQ AND T.SUBSYS = '" + subsystem + "' AND T.KEYNAME = '" + keyName
                + "' AND T.OWNER = '" + user + "'";
        CustomResponseDto customResponseDto = queryService.getResultQuery(connectionID, querySelect);

        if (!customResponseDto.getSuccess()) {
            if (customResponseDto.getErrorMessage().equalsIgnoreCase("fail due []")) {
                queryService.addLogging("GET", partMessage + "success");
                return new JSONObject().toString();
            }
            queryService.addLogging("GET", partMessage + customResponseDto.getErrorMessage());
            return CustomResponseDto.builder().message(customResponseDto.getErrorMessage()).build();
        }

        queryService.addLogging("GET", partMessage + "success");
        return new JSONArray(customResponseDto.getMessage()).getJSONObject(0).get("JSONVALUE").toString();
    }
}
