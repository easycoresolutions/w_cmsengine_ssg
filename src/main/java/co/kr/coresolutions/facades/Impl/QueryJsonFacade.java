package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryJsonFacade;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.dtos.QueryDto;
import co.kr.coresolutions.model.dtos.QueryTemplateDto;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceFiles;
import co.kr.coresolutions.service.QueryServiceJSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
@RequiredArgsConstructor
public class QueryJsonFacade implements IQueryJsonFacade {
    private final QueryService queryService;
    private final QueryServiceJSON queryServiceJSON;
    private final IQuerySocketFacade querySocketFacade;
    private final QueryServiceFiles queryServiceFile;
    private final ObjectMapper objectMapper;

    @Override
    public Object postForAll(List<QueryDto> queryDtoList) {
        StringBuilder stringBuilder = new StringBuilder();
        String partMessage = "multiple/select_json triggered, ";
        final String[] failConnectionID = new String[1];
        final String[] errorQueryID = new String[1];
        final String[] errorMessage = new String[1];
        boolean isErrorOccurred = queryDtoList
                .stream()
                .peek(queryDto -> queryDto.setSql(queryService.convertGlobalVariable(queryService.convertGlobalDateTime(queryService.getFormattedString(queryDto.getSql())))))
                .anyMatch(queryDto -> {
                    failConnectionID[0] = queryDto.getConnectionID();
                    errorQueryID[0] = queryDto.getQueryID();
                    if (!queryService.isConnectionIdFileExists(failConnectionID[0])) {
                        errorMessage[0] = failConnectionID[0] + " (information) doesn't exist.";
                        return true;
                    }

                    String resultValidity = queryService.checkValidityWithLabel(
                            queryDto.getSql(), failConnectionID[0], "", errorQueryID[0], "", false);
                    if (!(resultValidity.toLowerCase().startsWith("error") || resultValidity.equalsIgnoreCase("[]")
                            || resultValidity.startsWith("maxRunningTimeOut") || resultValidity.startsWith("MaxRows"))) {
                        String response = queryServiceJSON.structureJson(resultValidity, false,
                                queryService.getQueryDataTypes(failConnectionID[0], queryDto.getSql(), false), errorQueryID[0]);
                        if (response.startsWith("Error") || response.startsWith("error")) {
                            errorMessage[0] = response;
                            return true;
                        }
                        stringBuilder.append("\"" + errorQueryID[0] + "\" : " + response + ",");
                        return false;
                    }
                    errorMessage[0] = resultValidity;
                    return true;
                });

        if (isErrorOccurred) {
            queryService.addLogging("POST", partMessage + "connectionid " + failConnectionID[0] + ", " + errorMessage[0]);
            return CustomResponseDto.builder().message(errorMessage[0]).errorID(errorQueryID[0]).build();
        }
        queryService.addLogging("POST", partMessage + "success");
        return "{" + stringBuilder.deleteCharAt(stringBuilder.length() - 1).toString() + "}";
    }

    @Override
    public Object postForAllExecJson(List<QueryTemplateDto> queryTemplateDtoList) {
        String partMessage = "multiple/exec_json triggered, ";
        StringBuilder stringBuilder = new StringBuilder();
        final String[] failConnectionID = new String[1];
        final String[] errorQueryID = new String[1];
        final String[] errorMessage = new String[1];
        final String[] userId = new String[1];

        boolean isErrorOccurred = queryTemplateDtoList.stream().anyMatch(queryDto -> {
            failConnectionID[0] = queryDto.getConnectionID();
            errorQueryID[0] = queryDto.getQueryID();
            userId[0] = queryDto.getUserID();
            String fileName = queryDto.getFile();
            try {
                if (!queryService.isConnectionIdFileExists(failConnectionID[0])) {
                    errorMessage[0] = failConnectionID[0] + " (information) doesn't exist.";
                    return true;
                }

                if (!queryServiceFile.isQueryFileExist(fileName)) {
                    errorMessage[0] = "queryFile " + fileName + " doesn't exist.";
                    return true;
                }
                ObjectNode jsonNodes = JsonNodeFactory.instance.objectNode();
                jsonNodes.putAll(queryDto.getReplace());
                jsonNodes.putPOJO("TEMPLATE", queryDto.getTemplate().toString());
                String query = queryService
                        .convertGlobalDateTime(queryService.convertGlobalVariable(queryService.getFormattedQuery(queryServiceFile.fetchQueryFromFile(fileName),
                                (ObjectNode) objectMapper.readTree(jsonNodes.toString()))));
                String resultValidity = queryService.checkValidityWithLabel(query, failConnectionID[0], "", errorQueryID[0], "", false);
                if (!(resultValidity.toLowerCase().startsWith("error") || resultValidity.equalsIgnoreCase("[]")
                        || resultValidity.startsWith("maxRunningTimeOut") || resultValidity.startsWith("MaxRows"))) {
                    String response = queryServiceJSON.structureJson(resultValidity, false,
                            queryService.getQueryDataTypes(failConnectionID[0], query, false), errorQueryID[0]);
                    if (response.startsWith("Error") || response.startsWith("error")) {
                        errorMessage[0] = response;
                        return true;
                    }
                    stringBuilder.append("\"" + errorQueryID[0] + "\" : " + response + ",");
                    return false;
                }
                errorMessage[0] = resultValidity;
                return true;
            } catch (IOException e) {
                errorMessage[0] = e.getMessage();
                return true;
            }
        });

        if (isErrorOccurred) {
            querySocketFacade.send(SocketBodyDto.builder().userId(userId[0]).key("EXEC_JSON").message(partMessage + "connectionid " + failConnectionID[0] + ", " + errorMessage[0]).build());
            queryService.addLogging("POST", partMessage + "connectionid " + failConnectionID[0] + ", " + errorMessage[0]);
            return CustomResponseDto.builder().message(errorMessage[0]).errorID(errorQueryID[0]).build();
        }

        querySocketFacade.send(SocketBodyDto.builder().userId(userId[0]).key("EXEC_JSON").message(partMessage + "success").build());
        queryService.addLogging("POST", partMessage + "success");
        return "{" + stringBuilder.deleteCharAt(stringBuilder.length() - 1).toString() + "}";
    }
}
