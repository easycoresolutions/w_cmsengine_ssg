package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.service.QueryService;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class QuerySocketFacade implements IQuerySocketFacade {
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final QueryService queryService;

    public SocketBodyDto send(SocketBodyDto socketBodyDto) {
        Thread thread = new Thread(() -> {
            simpMessagingTemplate.convertAndSend("/topic/messages", socketBodyDto);
            queryService.addLogging("POST", "/socket/send api triggered, send message:\t" +
                    new JSONObject(socketBodyDto).toString().replaceAll("\\s+", "   ") + "\tsocket push success\n");
        });
        thread.setDaemon(true);
        thread.start();
        return socketBodyDto;
    }
}
