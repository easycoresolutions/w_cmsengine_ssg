package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQuerySchemaTableFacade;
import co.kr.coresolutions.model.dtos.SchemaDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceSchemaTable;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class QuerySchemaTableFacade implements IQuerySchemaTableFacade {
    private static final String mainTable = "t_ssbi_schema_index";
    private static final String auxiTable = "t_ssbi_schema_index_auth";
    private final QueryService queryService;
    private final QueryServiceSchemaTable queryServiceSchemaTable;

    @Override
    public Object saveSchema(SchemaDto schemaDto, String connectionID, String schemaID) {
        String partMessage = "schematable/{" + connectionID + "}/{" + schemaID + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name\t" + connectionID + "\t doesn't exist");
            return CustomResponseDto.builder().message("fail due to connection file with name\t" + connectionID + "\t doesn't exist").build();
        }
        if (!schemaDto.getSchemaID().equals(schemaID)) {
            return CustomResponseDto.builder().message("fail due mismatch schemaID from url and body!").build();
        }

        List<String> listRandomIds = new LinkedList<>();
        CustomResponseDto customResponseDto = queryServiceSchemaTable.insertForMain(listRandomIds, connectionID, mainTable, schemaID, schemaDto);
        if (!customResponseDto.getSuccess()) {
            queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
            queryService.rollBack(listRandomIds);
            return customResponseDto;
        }

        customResponseDto = queryServiceSchemaTable
                .deleteAndInsertAuxiliaryTable(auxiTable,
                        schemaDto.getAuth().stream().map(SchemaDto.SchemaAuth::getAuthID).collect(Collectors.toList()), listRandomIds, schemaID, connectionID);
        if (!customResponseDto.getSuccess()) {
            queryService.addLogging("POST", partMessage + customResponseDto.getMessage());
            queryService.rollBack(listRandomIds);
            return customResponseDto;
        }

        queryService.addLogging("POST", partMessage + "Success insert");
        customResponseDto.setMessage("Success insert");
        queryService.commit(listRandomIds);

        return customResponseDto;
    }

    @Override
    public Object deleteRow(String connectionID, String schemaID) {
        String partMessage = "schematable/{" + connectionID + "}/{" + schemaID + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("DELETE", partMessage + "fail due to connection file with name\t" + connectionID + "\t doesn't exist");
            return CustomResponseDto.builder().message("fail due to connection file with name\t" + connectionID + "\t doesn't exist").build();
        }

        List<String> listRandomIds = new LinkedList<>();
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();

        String randomDeleteMain = UUID.randomUUID().toString();
        String resultDeleteMain = queryService
                .updateClausesWithRollback(
                        Lists.newArrayList("UPDATE " + mainTable + " SET DEL_F = 'Y' WHERE SCHEMA_ID = '" + schemaID + "'"), connectionID, randomDeleteMain);
        if (!resultDeleteMain.startsWith("success")) {
            queryService.addLogging("DELETE", partMessage + resultDeleteMain);
            customResponseDto.setMessage(resultDeleteMain);
            return customResponseDto;
        }
        listRandomIds.add(randomDeleteMain);

        String randomDeleteAuxiliary = UUID.randomUUID().toString();
        String resultDeleteAuxiliary = queryService
                .updateClausesWithRollback(
                        Lists.newArrayList("DELETE FROM " + auxiTable + " WHERE SCHEMA_ID = '" + schemaID + "'"), connectionID, randomDeleteAuxiliary);
        if (!resultDeleteAuxiliary.startsWith("success")) {
            queryService.rollBack(listRandomIds);
            queryService.addLogging("DELETE", partMessage + resultDeleteAuxiliary);
            customResponseDto.setMessage(resultDeleteAuxiliary);
            return customResponseDto;
        }
        listRandomIds.add(randomDeleteAuxiliary);

        queryService.commit(listRandomIds);
        queryService.addLogging("DELETE", partMessage + "Success delete");
        customResponseDto.setMessage("Success delete");
        customResponseDto.setSuccess(true);
        return customResponseDto;
    }

    @Override
    public CustomResponseDto getSchema(String connectionID, String schemaID) {
        String partMessage = "schematable/{" + connectionID + "}/{" + schemaID + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("GET", partMessage + "fail due to connection file with name\t" + connectionID + "\t doesn't exist");
            return CustomResponseDto.builder().message("fail due to connection file with name\t" + connectionID + "\t doesn't exist").build();
        }

        String resultValidity = queryService
                .checkValidity("SELECT SCHEMA_JSON FROM " + mainTable + " WHERE SCHEMA_ID = '" + schemaID + "'",
                        connectionID, "", "", false);
        if (resultValidity.equalsIgnoreCase(connectionID) || resultValidity.startsWith("Error") || resultValidity.startsWith("error")) {
            queryService.addLogging("GET", partMessage + "fail due error is " + resultValidity);
            return CustomResponseDto.builder().message(resultValidity).build();
        }

        JSONArray jsonArray = new JSONArray(resultValidity);
        if (jsonArray.isEmpty()) {
            queryService.addLogging("GET", partMessage + "fail due SCHEMA_ID " + schemaID + " doesn't exist");
            return CustomResponseDto.builder().message("SCHEMA_ID " + schemaID + " doesn't exist").build();
        }
        queryService.addLogging("GET", partMessage + "success");
        return CustomResponseDto.builder().Success(true).message(jsonArray.getJSONObject(0).get("SCHEMA_JSON").toString()).build();
    }

}
