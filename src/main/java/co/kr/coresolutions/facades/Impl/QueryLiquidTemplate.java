package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryLiquidFacade;
import co.kr.coresolutions.model.dtos.LiquidTemplateDTO;
import co.kr.coresolutions.model.dtos.LiquidTemplateQueryDTO;
import co.kr.coresolutions.model.dtos.MustacheTemplateDTO;
import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceFiles;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Lists;
import liqp.Template;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class QueryLiquidTemplate implements IQueryLiquidFacade {
    private final QueryService queryService;
    private final QueryServiceFiles queryServiceFile;
    private final ObjectMapper objectMapper;
    private final Constants constants;
    private String liquidDir;
    private String businessQueryDir;

    @PostConstruct
    public void init() {
        liquidDir = constants.getLiquidDir();
        businessQueryDir = constants.getBusinessQueryDir();
    }

    @Override
    public Object convertTemplate(MustacheTemplateDTO mustacheTemplateDTO) {
        String separator = mustacheTemplateDTO.getSeparator();
        List<String> missingFiles = mustacheTemplateDTO.getIds().stream().filter(id -> Files.notExists(Paths.get(liquidDir + id + ".txt"))).collect(Collectors.toList());
        if (!missingFiles.isEmpty()) {
            return CustomResponseDto.builder().message("file(s) " + missingFiles + " doesn't exist").build();
        }
        final String[] result = {""};
        boolean allSuccess = mustacheTemplateDTO.getIds().stream().allMatch(id -> {
            Template template;
            try {
                template = Template.parse(new String(Files.readAllBytes(Paths.get(liquidDir + id + ".txt"))));
                String actualResult = template.render(mustacheTemplateDTO.getParameters().toString());
                result[0] = result[0].isEmpty() ? actualResult : result[0] + (separator.equals("line") ? System.lineSeparator() : separator) + actualResult;
                return true;
            } catch (Exception e) {
                result[0] = "fail due\t" + e.getMessage();
                return false;
            }
        });
        if (!allSuccess) {
            return CustomResponseDto.builder().message(result[0]).build();
        }
        return CustomResponseDto.builder().Success(true).message(result[0]).build();
    }

    @Override
    public Object convertLiquidTemplate(LiquidTemplateDTO liquidTemplateDTO) {
        String separator = liquidTemplateDTO.getSeparator();
        List<String> missingFiles = liquidTemplateDTO.getIds().stream().filter(id -> Files.notExists(Paths.get(liquidDir + id + ".txt"))).collect(Collectors.toList());
        if (!missingFiles.isEmpty()) {
            return CustomResponseDto.builder().message("file(s) " + missingFiles + " doesn't exist").build();
        }
        String connectionId = liquidTemplateDTO.getConnectionID();
        boolean isFileExist = queryService.isConnectionIdFileExists(connectionId);
        if (!isFileExist) {
            return CustomResponseDto.builder().message("Connection file with name\t" + connectionId + "\tdoesn't exist").build();
        }
        if (Files.notExists(Paths.get(businessQueryDir + liquidTemplateDTO.getProfile() + ".txt"))
                || Files.notExists(Paths.get(businessQueryDir + liquidTemplateDTO.getRecommendations() + ".txt"))) {
            return CustomResponseDto.builder().message("profile or recommendations file(s) missing").build();
        }

        AtomicReference<String> failMessage = new AtomicReference<>("");
        ObjectNode replace = JsonNodeFactory.instance.objectNode();
        replace.putAll(liquidTemplateDTO.getReplace());
        JSONObject parameters = new JSONObject();
        parameters.put("header", "Colors");
        parameters.put("empty", false);
        parameters.put("items", Lists.newArrayList("{\"name\": \"red\", \"first\": true, \"url\": \"#Red\"}",
                "{\"name\": \"green\", \"link\": true, \"url\": \"#Green\"}",
                "{\"name\": \"blue\", \"link\": true, \"url\": \"#Blue\"}"
        ));

        try {
            Thread threadProfile = new Thread(() -> {
                String query = null;
                try {
                    query = queryService
                            .convertGlobalDateTime(queryService.convertGlobalVariable(queryService.getFormattedQuery(queryServiceFile.fetchQueryFromFile(liquidTemplateDTO.getProfile()),
                                    (ObjectNode) objectMapper.readTree(replace.toString()))));
                } catch (IOException e) {
                    failMessage.set(e.getMessage());
                }
                CustomResponseDto customResponseDto = queryService.getResultQuery(connectionId, query);
                if (customResponseDto.getSuccess()) {
                    JSONArray jsonArray = new JSONArray(customResponseDto.getMessage());
                    if (!jsonArray.isEmpty()) {
                        parameters.put("profile", jsonArray.getJSONObject(0));
                    }
                } else {
                    failMessage.set(customResponseDto.getErrorMessage());
                }
            });
            threadProfile.setDaemon(true);
            threadProfile.start();

            Thread threadRecommendation = new Thread(() -> {
                String query = null;
                try {
                    query = queryService
                            .convertGlobalDateTime(queryService.convertGlobalVariable(queryService.getFormattedQuery(queryServiceFile.fetchQueryFromFile(liquidTemplateDTO.getRecommendations()),
                                    (ObjectNode) objectMapper.readTree(replace.toString()))));
                } catch (IOException e) {
                    failMessage.set(e.getMessage());
                }
                CustomResponseDto customResponseDto = queryService.getResultQuery(connectionId, query);
                if (customResponseDto.getSuccess()) {
                    JSONArray jsonArray = new JSONArray(customResponseDto.getMessage());
                    if (!jsonArray.isEmpty()) {
                        parameters.put("recommendations", jsonArray);
                    }
                } else {
                    failMessage.set(customResponseDto.getErrorMessage());
                }
            });
            threadRecommendation.setDaemon(true);
            threadRecommendation.start();

            while (true) {
                threadProfile.join(10);
                threadRecommendation.join(10);
                if ((threadProfile.isInterrupted() || threadRecommendation.isInterrupted()) ||
                        (threadProfile.getState() == Thread.State.TERMINATED
                                && threadRecommendation.getState() == Thread.State.TERMINATED)) {
                    break;
                }
            }

        } catch (InterruptedException e) {
            return CustomResponseDto.builder().message(e.getMessage()).build();
        }

        if (!failMessage.get().isEmpty()) {
            return CustomResponseDto.builder().message(failMessage.get()).build();
        }

        return convertTemplate(MustacheTemplateDTO.builder().ids(liquidTemplateDTO.getIds()).owner(liquidTemplateDTO.getOwner())
                .separator(separator).parameters(objectMapper.valueToTree(parameters.toMap())).build());
    }

    @Override
    public Object convertLiquidTemplateQuery(LiquidTemplateQueryDTO liquidTemplateQueryDTO) {
        String separator = liquidTemplateQueryDTO.getSeparator();
        List<String> missingFiles = liquidTemplateQueryDTO.getIds().stream().filter(id -> Files.notExists(Paths.get(liquidDir + id + ".txt"))).collect(Collectors.toList());
        if (!missingFiles.isEmpty()) {
            return CustomResponseDto.builder().message("file(s) " + missingFiles + " doesn't exist").build();
        }
        String connectionId = liquidTemplateQueryDTO.getConnectionID();
        boolean isFileExist = queryService.isConnectionIdFileExists(connectionId);
        if (!isFileExist) {
            return CustomResponseDto.builder().message("Connection file with name\t" + connectionId + "\tdoesn't exist").build();
        }
        CustomResponseDto customResponseDto = queryService.getResultQuery(connectionId, liquidTemplateQueryDTO.getSql());
        JSONObject parameters = new JSONObject();
        if (customResponseDto.getSuccess()) {
            JSONArray jsonArray = new JSONArray(customResponseDto.getMessage());
            if (!jsonArray.isEmpty()) {
                parameters.put("sql", jsonArray);
                return convertTemplate(MustacheTemplateDTO.builder().ids(liquidTemplateQueryDTO.getIds()).owner(liquidTemplateQueryDTO.getOwner())
                        .separator(separator).parameters(objectMapper.valueToTree(parameters.toMap())).build());
            }
        }
        return customResponseDto;
    }

}
