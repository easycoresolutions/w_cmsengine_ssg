package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQuerySurveyResultFacade;
import co.kr.coresolutions.model.dtos.SurveyResultDto;
import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.GONE;
import static org.springframework.http.HttpStatus.OK;

@Component
@RequiredArgsConstructor
@Slf4j
public class QuerySurveyResultFacade implements IQuerySurveyResultFacade {
    private final QueryService queryService;
    private final Constants constants;
    private String surveyResultQueryDir;

    static void isIndexExist(String userID, boolean[] indexExists, boolean[] userIDExists, String queryDir) {
        try (Stream<Path> paths = Files.walk(Paths.get(queryDir))) {
            paths
                    .filter(Files::isDirectory)
                    .forEach(path -> {
                        try (Stream<Path> path1 = Files.walk(Paths.get(queryDir + File.separator + userID))) {
                            userIDExists[0] = true;
                            path1
                                    .filter(Files::isRegularFile)
                                    .forEach(path2 -> {
                                        if (path2.getFileName().toString().equalsIgnoreCase("index.txt")) {
                                            indexExists[0] = true;
                                        }
                                    });
                        } catch (IOException e) {
                        }
                    });
        } catch (Exception e) {
        }
    }

    @PostConstruct
    public void init() {
        surveyResultQueryDir = constants.getSurveyResultQueryDir();
    }

    @Override
    @SneakyThrows
    public ResponseDto saveSurveyResult(SurveyResultDto surveyResultDto, String userID, String projectID) {
        final boolean[] indexExists = {false};
        final boolean[] userIDExists = {false};
        if (!Paths.get(surveyResultQueryDir).toFile().isDirectory())
            Files.createDirectory(Paths.get(surveyResultQueryDir));

        isIndexExist(userID, indexExists, userIDExists, surveyResultQueryDir);

        Path userIDPath = Paths.get(surveyResultQueryDir + File.separator + userID);
        if (!userIDExists[0]) {
            userIDExists[0] = true;
            if (!userIDPath.toFile().isDirectory()) {
                userIDPath = Files.createDirectory(userIDPath);
            }
        }

        JSONArray jsonArray;
        byte[] surveyProject = new Gson().toJson(surveyResultDto).getBytes(StandardCharsets.UTF_8);
        JSONObject jsonObject1 = new JSONObject(new String(surveyProject));
        JSONObject surveyProjectWithoutJsonField = new JSONObject(jsonObject1, Arrays.stream(JSONObject.getNames(jsonObject1)).filter(s ->
                !s.equals("surveyresultjson")
        ).collect(Collectors.toList()).toArray(new String[JSONObject.getNames(jsonObject1).length]));

        Files.write(Paths.get(userIDPath + File.separator + projectID + ".txt"), surveyProject,
                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

        if (!indexExists[0]) {
            jsonArray = new JSONArray();
            jsonArray.put(surveyProjectWithoutJsonField);
            Files.write(Paths.get(userIDPath + File.separator + "index.txt"),
                    jsonArray.toString().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE);
        } else {
            JsonParser parser = new JsonParser();
            JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(userIDPath + File.separator + "index.txt"))));
            JsonArray jsonArrayTemp = jsonElement.getAsJsonArray();
            JSONArray returnArray = new JSONArray();

            jsonArrayTemp.iterator().forEachRemaining(e -> {
                JSONObject temp = new JSONObject(e.toString());
                if (temp.has("surveyresultID") && !temp.get("surveyresultID").toString().equalsIgnoreCase(projectID)
                        && surveyResultDto.getSurveyProjectID().equalsIgnoreCase(projectID)) {
                    returnArray.put(temp);
                }
            });

            returnArray.put(surveyProjectWithoutJsonField);
            Files.write(Paths.get(userIDPath + File.separator + "index.txt"), returnArray.toString().getBytes(StandardCharsets.UTF_8),
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

        }

        queryService.addLogging("POST", projectID + "\tsurvey result uploaded successfully\n");
        return ResponseDto.builder().successCode(OK.value()).message(projectID + "\tsurvey result uploaded successfully").build();
    }

    @Override
    @SneakyThrows
    public ResponseDto deleteSurveyResult(String userID, String projectID) {
        if (!Paths.get(surveyResultQueryDir).toFile().isDirectory())
            Files.createDirectory(Paths.get(surveyResultQueryDir));
        if (!Paths.get(surveyResultQueryDir + File.separator + userID).toFile().isDirectory()) {
            queryService.addLogging("DELETE", userID + "\tdoesn't exist.\n");
            return ResponseDto.builder().errorCode(CONFLICT.value()).message(userID + "\tdoesn't exist.").build();
        } else {
            try {
                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(surveyResultQueryDir + File.separator + userID + File.separator + "index.txt"))));
                JsonArray jsonArrayTemp = jsonElement.getAsJsonArray();

                JSONArray returnArray = new JSONArray();
                jsonArrayTemp.iterator().forEachRemaining(e -> {
                    JSONObject temp = new JSONObject(e.toString());
                    if (!temp.get("surveyresultID").toString().equalsIgnoreCase(projectID))
                        returnArray.put(temp);
                });

                Files.write(Paths.get(surveyResultQueryDir + File.separator + userID + File.separator + "index.txt"),
                        returnArray.toString().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                boolean deleted = Files.deleteIfExists(Paths.get(surveyResultQueryDir + File.separator + userID + File.separator + projectID + ".txt"));
                if (!deleted) {
                    queryService.addLogging("DELETE", projectID + "\tdoesn't exist.\n");
                    return ResponseDto.builder().errorCode(CONFLICT.value()).message(projectID + "\tdoesn't exist.").build();
                }
            } catch (IOException e) {
                return ResponseDto.NotValidRequest();
            }
        }
        queryService.addLogging("DELETE", "deleteSurveyResult: " + projectID + "\n");
        return ResponseDto.builder().successCode(OK.value()).message(projectID + "\tDeleted successfully").build();
    }

    @Override
    @SneakyThrows
    public ResponseDto getSurveyResult(String userID, String projectID) {
        if (!Paths.get(surveyResultQueryDir).toFile().isDirectory())
            Files.createDirectory(Paths.get(surveyResultQueryDir));

        if (!Paths.get(surveyResultQueryDir + File.separator + userID).toFile().isDirectory()) {
            queryService.addLogging("GET", userID + "\tdirectory doesn't exist.\n");
            return ResponseDto.builder().errorCode(GONE.value()).message(projectID + "\tdirectory doesn't exist.").build();
        } else {
            try {
                ResponseDto responseDto = ResponseDto.builder().successCode(OK.value())
                        .message(new String(Files.readAllBytes(Paths.get(surveyResultQueryDir + File.separator + userID + File.separator + projectID + ".txt")),
                                StandardCharsets.UTF_8)).build();
                queryService.addLogging("GET", " getSurveyResult called userID " + userID + " , projectID " + projectID + "\n");
                return responseDto;
            } catch (IOException e) {
                queryService.addLogging("GET", projectID + "\tdoesn't exist.\n");
                return ResponseDto.builder().errorCode(CONFLICT.value()).message(projectID + "\tdoesn't exist.").build();
            }
        }
    }

    @Override
    @SneakyThrows
    public ResponseDto getSurveyResults(String userID) {
        if (!Paths.get(surveyResultQueryDir).toFile().isDirectory())
            Files.createDirectory(Paths.get(surveyResultQueryDir));
        try {
            ResponseDto responseDto = ResponseDto.builder().successCode(OK.value())
                    .message(new String(Files.readAllBytes(Paths.get(surveyResultQueryDir + File.separator + userID + File.separator + "index.txt")),
                            StandardCharsets.UTF_8)).build();
            queryService.addLogging("GET", "getSurveyResults called " + userID + "\n");
            return responseDto;
        } catch (IOException e1) {
            queryService.addLogging("GET", userID + "\tdoesn't exist.\n");
            return ResponseDto.builder().errorCode(GONE.value()).message(userID + "\tdoesn't exist.").build();
        }
    }
}

