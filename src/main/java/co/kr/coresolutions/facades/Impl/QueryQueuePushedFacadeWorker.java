package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.enums.QueuePushLevel;
import co.kr.coresolutions.facades.IQueryQueuePushedFacadeWorker;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.model.dtos.SocketDto;
import co.kr.coresolutions.model.dtos.TargetDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.util.CoreEntry;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;

import static co.kr.coresolutions.facades.Impl.QueryQueuePushedFacade.SIMPLE_DATE_FORMAT;
import static co.kr.coresolutions.facades.Impl.QueryQueuePushedFacade.connectionIDQueue;
import static co.kr.coresolutions.facades.Impl.QueryQueuePushedFacade.coreEntriesHIGH;
import static co.kr.coresolutions.facades.Impl.QueryQueuePushedFacade.coreEntriesLOW;
import static co.kr.coresolutions.facades.Impl.QueryQueuePushedFacade.coreEntriesMIDDLE;
import static co.kr.coresolutions.facades.Impl.QueryQueuePushedFacade.tableQueue;

@Component
@RequiredArgsConstructor
public class QueryQueuePushedFacadeWorker implements IQueryQueuePushedFacadeWorker {
    private final QueryService queryService;
    private final ObjectMapper objectMapper;
    private final IQuerySocketFacade querySocketFacade;

    @Override
    public void addToQueue(QueuePushLevel queuePushLevel, String queryID, String url, Object object) {
        Connection connection = queryService.getInfoConnection(connectionIDQueue);
        String scheme = connection.getSCHEME();
        String owner;
        String userID;
        if (object instanceof TargetDto) {
            TargetDto targetDto = (TargetDto) object;
            owner = targetDto.getOwner();
            userID = targetDto.getUserID();
        } else {
            SocketDto socketDto = (SocketDto) object;
            owner = socketDto.getOwner();
            userID = socketDto.getUserID();
        }
        try {
            String key = "{query/" + url + "}_{" + queuePushLevel.getName() + "}_started";
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(key).message(SIMPLE_DATE_FORMAT.format(new Date())).build());
            String queryInsert = "INSERT INTO " + scheme +
                    tableQueue + " (QUERYID, OWNER, STEPS, DATETIME, QUEUENAME, REQUEST_URL, REQUEST_BODY, STATUS_MESSAGE) VALUES ('" + queryID + "', '" + owner
                    + "', 'S', '" + SIMPLE_DATE_FORMAT.format(new Date()) + "','" + queuePushLevel.getName() + "', '" + url + "', '" + objectMapper.writeValueAsString(object).replace("'", "''")
                    + "', 'Request')";
            queryService.checkValidityUpdateClauses(queryInsert, connectionIDQueue);
        } catch (JsonProcessingException e) {
            queryService.addLogging("addToQueueLogTable", e.getMessage());
        }
        CoreEntry coreEntry = CoreEntry.builder().key(CoreEntry.builder().key(queuePushLevel).value(queryID).build())
                .value(CoreEntry.builder().key(url).value(object).build()).build();
        switch (queuePushLevel) {
            case HIGH:
                coreEntriesHIGH.add(coreEntry);
                break;
            case MEDIUM:
                coreEntriesMIDDLE.add(coreEntry);
                break;
            case LOW:
                coreEntriesLOW.add(coreEntry);
                break;
        }
    }

}
