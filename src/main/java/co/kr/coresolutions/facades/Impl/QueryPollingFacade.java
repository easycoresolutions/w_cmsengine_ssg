package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.dao.NativeQuery;
import co.kr.coresolutions.facades.IQueryPollingFacade;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.dtos.DBPollingDto;
import co.kr.coresolutions.model.dtos.PollingDto;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceCSV;
import co.kr.coresolutions.service.QueryServicePolling;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class QueryPollingFacade implements IQueryPollingFacade {
    public static Boolean[] success = {false};
    public static String[] message = {""};
    public static Set<Integer> dpOutSeq = new HashSet<>();
    private static final Integer[] records = {0};
    private final QueryService queryService;
    private final ObjectMapper objectMapper;
    private final QueryServiceCSV queryServiceCSV;
    private final QueryServicePolling queryServicePolling;
    private final NativeQuery nativeQuery;
    private final IQuerySocketFacade querySocketFacade;
    public static volatile Set<String> dpIDS = new HashSet<>();

    @Override
    public CustomResponseDto pooling(PollingDto pollingDto) {
        String dpID = pollingDto.getDpID();
        String connectionID = pollingDto.getConnectionID();
        if (dpIDS.contains(dpID)) {
            queryServicePolling.logToDBTriggered("-1", "0", 0, false, "Same DP_ID {" + dpID + "} is running", connectionID, dpID, queryService.getInfoConnection(connectionID));
            queryServicePolling.logToDBTriggered("-1", "0", 0, false, QueryPollingFacade.message[0], connectionID, dpID, queryService.getInfoConnection(connectionID));
            return CustomResponseDto.builder().message("dpID " + dpID + " still running, please try again later!").build();
        }
        dpIDS.add(dpID);
        
        String user = pollingDto.getUser();
        QueryPollingFacade.success[0] = false;
        QueryPollingFacade.message[0] = "";
        QueryPollingFacade.records[0] = 0;
        QueryPollingFacade.dpOutSeq = new HashSet<>();
        String partMessage = "dbpolling triggered (ID = " + dpID + " ), ";
        final boolean[] isConnectionFileExists = {queryService.isConnectionIdFileExists(connectionID)};
        if (!isConnectionFileExists[0]) {
        	dpIDS.remove(dpID);
            QueryPollingFacade.message[0] = "fail due to connection file with name\t" + connectionID + "\tdoesn't exist";
            querySocketFacade.send(SocketBodyDto.builder().userId(user).key("DBPOLLING").message(partMessage + QueryPollingFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + QueryPollingFacade.message[0]);
            return CustomResponseDto.builder().message(QueryPollingFacade.message[0]).records("").build();
        }

        String query = " SELECT DP.DP_ID, DP_QUERY, DP_IN_CONNID, DP_SEQ_FIELD, DP_LAST_SEQ,DP_MAX_RECORDS,DP_OUT_SEQ,DP_OUTTYPE,DP_TARGET," +
                "DP_TABLENAME,DP_OUTMODE,DP_TABLE_KEY,DP_FILE_SEP,DP_FILE_REPEAT,COMM.VAL1 AS OUT_CONNID , COMM.VAL3 AS DML_SQL  FROM T_DBPOLLING DP \n" +
                " INNER JOIN T_LOOKUP_VALUES COMM ON DP.DP_ID = '" + dpID + "' AND COMM.LANG='ko_KR' AND COMM.DEL_F='N' AND LOOKUP_TYPE='I_DBPOLLING_TARGET'\n" +
                " INNER JOIN T_DBPOLLING_TARGETS  TRGT ON TRGT.DP_ID = DP.DP_ID AND TRGT.DP_TARGET = COMM.LOOKUP_CODE";

        String queryID = UUID.randomUUID().toString();
        String resultValidity = queryService.checkValidityWithLabel(query, connectionID, "", queryID, "", false);
        queryService.clearResources(queryID);
        Connection infoConnection = queryService.getInfoConnection(connectionID);

        if (resultValidity.startsWith("Error") || resultValidity.startsWith("error") || resultValidity.startsWith("maxRunningTimeOut") || resultValidity.startsWith("MaxRows")) {
        	dpIDS.remove(dpID);
            querySocketFacade.send(SocketBodyDto.builder().userId(user).key("DBPOLLING").message(partMessage + "sql\t" + query + "\t, fail due\t" + resultValidity).build());
            queryService.addLogging("POST", partMessage + "sql\t" + query + "\t, fail due\t" + resultValidity);
            queryServicePolling.logToDB(connectionID, infoConnection, dpID, "fail", "sql\t" + query + "\t, fail due\t" + resultValidity, null);
            queryServicePolling.logToDBTriggered("-1", "0", 0, false, resultValidity, connectionID, dpID, infoConnection);
            return CustomResponseDto.builder().message(resultValidity).records("").build();
        }

        if (resultValidity.equalsIgnoreCase("[]")) {
        	dpIDS.remove(dpID);
            querySocketFacade.send(SocketBodyDto.builder().userId(user).key("DBPOLLING").message(partMessage + "sql\t" + query + "\t, fail due Empty value returned").build());
            queryService.addLogging("POST", partMessage + "sql\t" + query + "\t, fail due Empty value returned");
            queryServicePolling.logToDB(connectionID, infoConnection, dpID, "fail", "fail due Empty value returned", null);
            queryServicePolling.logToDBTriggered("-1", "0", 0, false, "fail due Empty value returned from T_DBPOLLING", connectionID, dpID, infoConnection);
            return CustomResponseDto.builder().message("fail due Empty value returned from T_DBPOLLING").records("").build();
        }
        queryService.addLogging(dpID+" STEP[0]", "SELECT DB POLLING JOB LIST");
        try {
            List<DBPollingDto> dbPollingDtos = Arrays.asList(objectMapper.readValue(resultValidity, DBPollingDto[].class));
            dbPollingDtos.stream().filter(dbPollingDto -> {
            	 queryService.addLogging(dpID+" STEP[1]", "DpOutSeq=" + dbPollingDto.getDpOutSeq());
                if (pollingDto.getDpOutSeq() != null) {
                    return dbPollingDto.getDpOutSeq().equals(pollingDto.getDpOutSeq());
                } else {
                    return true;
                }
            }).forEach(dbPollingDto -> {
            	queryService.addLogging(dpID+" STEP[2]", "START POLLING");	
                String dbLastSeq = dbPollingDto.getDpLastSeq();
                String lastSeqProcessed = dbLastSeq != null ? dbLastSeq : "0";
                String dpSqlField = dbPollingDto.getDpSqlField();
                String dbOutMode = dbPollingDto.getDpOutMode();
                final String[] dbTableName = {dbPollingDto.getDpTableName()};
                String inConnId = dbPollingDto.getDpInConnId();
                String outConnId = dbPollingDto.getOutConnId();
                String dpTarget = dbPollingDto.getDpTarget();
                Integer dpMaxRecord = dbPollingDto.getDpMaxRecords();
                Integer dtoDpOutSeq = dbPollingDto.getDpOutSeq();
                final String[] dmSql = {dbPollingDto.getDmlSql()};
                JsonNode dpReplace = pollingDto.getReplaceTableName();
                dpReplace.fields().forEachRemaining(stringJsonNodeEntry -> {
                    dmSql[0] = dmSql[0].replace(stringJsonNodeEntry.getKey(), stringJsonNodeEntry.getValue().asText());
                    if (dbTableName[0].equalsIgnoreCase(stringJsonNodeEntry.getKey())) {
                        dbTableName[0] = stringJsonNodeEntry.getValue().asText();
                    }
                });
                //json or csv
                String dpOutType = dbPollingDto.getDpOutType();
                queryService.addLogging(dpID+" STEP[2-1]", "lastSeqProcessed:" + lastSeqProcessed);	
                String lastSeqProPlusMax = String.valueOf(Long.parseLong(lastSeqProcessed) + dpMaxRecord);
                queryService.addLogging(dpID+" STEP[2-2]", dpOutType + "," + lastSeqProPlusMax);	
                String dbQuery = dbPollingDto.getDpQuery().replace("{LAST_SEQ_PROCESSED}", lastSeqProcessed)
                        .replace("{LAST_SEQ_PROCESSED_PLUS_MAX}", lastSeqProPlusMax).replace("{LAST_SEQ_PROCESSED_ROW_COUNT}", String.valueOf(dpMaxRecord));
                queryService.addLogging(dpID+" STEP[2-3]", dbQuery);	
                
                isConnectionFileExists[0] = queryService.isConnectionIdFileExists(inConnId);
                if (!isConnectionFileExists[0]) {
                    QueryPollingFacade.message[0] = "DP_OUT_SEQ is\t " + dtoDpOutSeq + "\tinConnection sql\t" + dbQuery + "\t, fail due to inConnection file with name\t" + inConnId + "\tdoesn't exist";
                    QueryPollingFacade.dpOutSeq.add(dtoDpOutSeq);
                    querySocketFacade.send(SocketBodyDto.builder().userId(user).key("DBPOLLING").message(partMessage + QueryPollingFacade.message[0]).build());
                    queryService.addLogging("POST", partMessage + QueryPollingFacade.message[0]);
                    queryServicePolling
                            .logToDB(connectionID, infoConnection, dpID, "fail", QueryPollingFacade.message[0], null);
                    queryServicePolling.logToDBTriggered("-1", "0", dtoDpOutSeq, false, QueryPollingFacade.message[0], connectionID, dpID, infoConnection);
                    return;
                }
                queryService.addLogging(dpID+" STEP[3]", "MAKE QUERY FOR SELECTING SOURCE DATA : " + dbQuery);	
                
                String resultValiditySecond = queryService.loadWithLimits(dbQuery, inConnId, "", queryID, true, dpMaxRecord);
                queryService.clearResources(queryID);
                queryService.addLogging(dpID+" STEP[4]", "COMPLETE SELECTING SOURCE DATA");
                
                if (resultValiditySecond.startsWith("Error") || resultValiditySecond.startsWith("error")
                        || resultValiditySecond.startsWith("maxRunningTimeOut") || resultValiditySecond.startsWith("MaxRows")) {
                    QueryPollingFacade.message[0] = "DP_OUT_SEQ is\t " + dtoDpOutSeq + "\tinConnection sql\t" + dbQuery + "\t, fail due\t" + resultValiditySecond;
                    QueryPollingFacade.dpOutSeq.add(dtoDpOutSeq);
                    querySocketFacade
                            .send(SocketBodyDto.builder().userId(user).key("DBPOLLING").message(partMessage + QueryPollingFacade.message[0]).build());
                    queryService.addLogging("POST", partMessage + QueryPollingFacade.message[0]);
                    queryServicePolling.logToDB(connectionID, infoConnection, dpID, "fail", QueryPollingFacade.message[0], null);
                    queryServicePolling.logToDBTriggered("-1", "0", dtoDpOutSeq, false, QueryPollingFacade.message[0], connectionID, dpID, infoConnection);
                    return;
                }

                if (resultValiditySecond.equalsIgnoreCase("[]")) {
                    QueryPollingFacade.message[0] = "DP_OUT_SEQ is\t " + dtoDpOutSeq + "\tinConnection sql\t" + dbQuery + "\t, Empty resultset returned";
                    QueryPollingFacade.success[0] = false;
                    querySocketFacade
                            .send(SocketBodyDto.builder()
                                    .userId(user)
                                    .key("DBPOLLING").message(partMessage + QueryPollingFacade.message[0]).build());
                    queryService.addLogging("POST", partMessage + QueryPollingFacade.message[0]);
                    queryServicePolling.logToDB(connectionID, infoConnection, dpID, "fail", QueryPollingFacade.message[0], null);
                    queryServicePolling.logToDBTriggered("-1", "0", dtoDpOutSeq, false, QueryPollingFacade.message[0], connectionID, dpID, infoConnection);
                    return;
                }

                JsonNode jsonNode;
                try {
                    jsonNode = objectMapper.readTree(resultValiditySecond);
                    QueryPollingFacade.records[0] = jsonNode.size();
                } catch (IOException e) {
                    QueryPollingFacade.message[0] = e.getMessage();
                    queryServicePolling.logToDBTriggered("-1", "0", dtoDpOutSeq, false, QueryPollingFacade.message[0], connectionID, dpID, infoConnection);
                    return;
                }

                BigInteger maxSeqNumber = queryServicePolling.getMaxSeqNumber(jsonNode, dpSqlField);

                List<String> list = null;
                if (dpOutType.equalsIgnoreCase("csv") || dpOutType.equalsIgnoreCase("file")) {
                    list = Arrays.asList(queryServiceCSV.structureCSV(resultValiditySecond).split("\n"));
                    if (dpOutType.equalsIgnoreCase("csv")) {
                        list = list.subList(1, list.size());
                        QueryPollingFacade.records[0] = list.size();
                    }
                }
                
                queryService.addLogging(dpID+" STEP[5]", "data size:"+jsonNode.size() + ", dpTarget:" + dpTarget + ", dbOutMode:" + dbOutMode);	
                
                //kafka or table
                if (!dpTarget.equalsIgnoreCase("FILE")) {
                    isConnectionFileExists[0] = queryService.isConnectionIdFileExists(outConnId);
                    if (!isConnectionFileExists[0]) {
                        QueryPollingFacade.message[0] = "DP_OUT_SEQ is\t " + dtoDpOutSeq + "fail due to outConnection file with name\t" + outConnId + "\tdoesn't exist";
                        QueryPollingFacade.dpOutSeq.add(dtoDpOutSeq);
                        querySocketFacade
                                .send(SocketBodyDto.builder()
                                        .userId(user)
                                        .key("DBPOLLING").message(partMessage + QueryPollingFacade.message[0]).build());
                        queryService.addLogging("POST", partMessage + QueryPollingFacade.message[0]);
                        queryServicePolling.logToDB(connectionID, infoConnection, dpID, "fail", QueryPollingFacade.message[0], null);
                        queryServicePolling.logToDBTriggered("-1", "0", dtoDpOutSeq, false, QueryPollingFacade.message[0], connectionID, dpID, infoConnection);
                        return;
                    }
                }

                List<String> finalList = list;
                JsonNode finalJsonNode = jsonNode;
                if (dpTarget.equalsIgnoreCase("kafka") && ((list != null && !list.isEmpty()) || !jsonNode.isNull())) {
                    try {
                        queryServicePolling.push(dpOutType.equalsIgnoreCase("csv") ? finalList : finalJsonNode, outConnId, dpOutType);
                    } catch (Exception e) {
                        logErrorConnectionKafkaOrJms(connectionID, dpID, partMessage, infoConnection, e);
                        QueryPollingFacade.dpOutSeq.add(dtoDpOutSeq);
                        queryServicePolling.logToDBTriggered("-1", "0", dtoDpOutSeq, false, e.getMessage(), connectionID, dpID, infoConnection);
                        return;
                    }
                } else if (dpTarget.equalsIgnoreCase("table")) {
                    if (dbOutMode.equalsIgnoreCase("U")) {
                        //update the table  dbTableName where dbTableKey
                        Connection infoConnectionOutput = queryService.getInfoConnection(outConnId);
                        QueryPollingFacade.success[0] = queryServicePolling.update(dpSqlField, jsonNode, outConnId, dbTableName[0], dbPollingDto.getDpTableKey().toUpperCase(), infoConnectionOutput.getDBMS());
                    } else if (dbOutMode.equalsIgnoreCase("I")) {
                        Connection infoConnectionOutput = queryService.getInfoConnection(outConnId);
                        //insert a record in  dbTableName
                        QueryPollingFacade.success[0] = queryServicePolling.insert(jsonNode, outConnId, dbTableName[0], null, false, infoConnectionOutput.getDBMS());
                    } else if (dbOutMode.equalsIgnoreCase("D")) {	
                    	// "T"타입 대체용. db접속 was계정(us_cmpn)에 truncate권한이 없어 추가함. 
                    	Connection infoConnectionOutput = queryService.getInfoConnection(outConnId);
                    	// delete every row in dbTableName 
                    	QueryPollingFacade.success[0] = queryServicePolling.deleteInsert(jsonNode, outConnId, dbTableName[0], null, false, infoConnectionOutput.getDBMS());
                    } else if (dbOutMode.equalsIgnoreCase("T")) {
                        Connection infoConnectionOutput = queryService.getInfoConnection(outConnId);
                        //truncateInsert a record in  dbTableName
                        QueryPollingFacade.success[0] = queryServicePolling.truncateInsert(jsonNode, outConnId, dbTableName[0], null, false, infoConnectionOutput.getDBMS());
                    } else if (dbOutMode.equalsIgnoreCase("N")) {
                        Connection infoConnectionOutput = queryService.getInfoConnection(outConnId);
                        queryServicePolling.recreateTable(outConnId, dbTableName[0], dmSql[0]);
                        QueryPollingFacade.success[0] = queryServicePolling.insert(jsonNode, outConnId, dbTableName[0], null, false, infoConnectionOutput.getDBMS());
                    } else {
                        QueryPollingFacade.message[0] = "DP_OUT_SEQ is\t " + dtoDpOutSeq + "\tinConnection sql\t" + dbQuery + "\t, fail due invalid DB_OUTMODE\t" + dbOutMode;
                        QueryPollingFacade.dpOutSeq.add(dtoDpOutSeq);
                        querySocketFacade.send(SocketBodyDto.builder().userId(user).key("DBPOLLING").message(partMessage + QueryPollingFacade.message[0]).build());
                        queryService.addLogging("POST", partMessage + QueryPollingFacade.message[0]);
                        queryServicePolling
                                .logToDB(connectionID, infoConnection, dpID, "fail", QueryPollingFacade.message[0], null);
                        queryServicePolling.logToDBTriggered("-1", "0", dtoDpOutSeq, false, QueryPollingFacade.message[0], connectionID, dpID, infoConnection);
                        return;
                    }
                } else if (dpTarget.equalsIgnoreCase("TIBCO_EMS") || dpTarget.equalsIgnoreCase("RABBITMQ") || dpTarget.equalsIgnoreCase("ACTIVEMQ")) {
                    try {
                    	if (dpTarget.equalsIgnoreCase("TIBCO_EMS")) {
                    		queryServicePolling.pushWithTibco(dpOutType.equalsIgnoreCase("csv") ? finalList : finalJsonNode, outConnId, dpOutType);
                    	} else if (dpTarget.equalsIgnoreCase("RABBITMQ")) {
                    		queryServicePolling.rabbitmqSend(dpOutType.equalsIgnoreCase("csv") ? finalList : finalJsonNode, outConnId, dpOutType);
                    	} else if (dpTarget.equalsIgnoreCase("ACTIVEMQ")) {
                    		queryServicePolling.activemqSend(dpOutType.equalsIgnoreCase("csv") ? finalList : finalJsonNode, outConnId, dpOutType);
                    	}
                    } catch (Exception e) {
                        logErrorConnectionKafkaOrJms(connectionID, dpID, partMessage, infoConnection, e);
                        QueryPollingFacade.dpOutSeq.add(dtoDpOutSeq);
                        queryServicePolling.logToDBTriggered("-1", "0", dtoDpOutSeq, false, QueryPollingFacade.message[0], connectionID, dpID, infoConnection);
                        return;
                    }
                } else if (dpTarget.equalsIgnoreCase("FILE") && (list != null && !list.isEmpty())) {
                    String dpFileRepeat = dbPollingDto.getDpFileRepeat();
                    String dpFileSEP = dbPollingDto.getDpFileSEP();
                    if (!dpFileRepeat.isEmpty() && !dpFileSEP.isEmpty()) {
                        queryServicePolling.saveToFile(dtoDpOutSeq, dpID, dbTableName[0], dpFileRepeat, list.stream()
                                .map(s -> s.replaceAll("\\\\t", " ").replaceAll(",", dpFileSEP)).collect(Collectors.toList()));
                    }
                }

                if (!QueryPollingFacade.success[0]) {
                    QueryPollingFacade.dpOutSeq.add(dtoDpOutSeq);
                }
                queryServicePolling.logToDBTriggered(QueryPollingFacade.success[0] ? lastSeqProcessed : "-1",
                        QueryPollingFacade.success[0] ? String.valueOf(maxSeqNumber) : "0", dtoDpOutSeq, QueryPollingFacade.success[0], QueryPollingFacade.message[0], connectionID, dpID, infoConnection);

                querySocketFacade
                        .send(SocketBodyDto.builder()
                                .userId(user)
                                .key("DBPOLLING")
                                .message(partMessage + "DP_OUT_SEQ is " + dtoDpOutSeq + QueryPollingFacade.message[0])
                                .build());
                if (QueryPollingFacade.success[0]) {
                    nativeQuery.executeQuery(connectionID, "UPDATE "
                            + infoConnection.getSCHEME() + "T_DBPOLLING SET DP_LAST_SEQ = " + maxSeqNumber + " WHERE DP_ID = '" + dpID + "'");
                }
                queryService.addLogging("POST", partMessage + "DP_OUT_SEQ is\t " + dtoDpOutSeq + "\t" + (QueryPollingFacade.success[0] ? "success" : "fail") + ",\t" + QueryPollingFacade.message[0]);
                queryServicePolling
                        .logToDB(connectionID, infoConnection, dpID, (QueryPollingFacade.success[0] ? "success" : "fail"), QueryPollingFacade.message[0], String.valueOf(maxSeqNumber));
            });
        } catch (Exception e) {
        	dpIDS.remove(dpID);
            queryServicePolling.logToDBTriggered("-1", "0", 0, false, e.getMessage(), connectionID, dpID, infoConnection);
            return CustomResponseDto.builder().dpOutSeq(QueryPollingFacade.dpOutSeq).message(e.getMessage()).records(String.valueOf(QueryPollingFacade.records[0])).build();
        }
        dpIDS.remove(dpID);
        if (!QueryPollingFacade.success[0]) {
            QueryPollingFacade.records[0] = 0;
        }
        if (!QueryPollingFacade.dpOutSeq.isEmpty()) {
            querySocketFacade
                    .send(SocketBodyDto.builder()
                            .userId(user)
                            .key("DBPOLLING").message(partMessage + "fail").build());
            queryService.addLogging("POST", partMessage + "fail");
            return CustomResponseDto.builder().Success(false).records(String.valueOf(QueryPollingFacade.records[0]))
                    .message(QueryPollingFacade.message[0]).dpOutSeq(QueryPollingFacade.dpOutSeq).build();
        }
        querySocketFacade
                .send(SocketBodyDto.builder()
                        .userId(user)
                        .key("DBPOLLING").message(partMessage + "success").build());
        queryService.addLogging("POST", partMessage + "success");
        return CustomResponseDto.builder().Success(QueryPollingFacade.success[0]).records(String.valueOf(QueryPollingFacade.records[0]))
                .message(QueryPollingFacade.message[0]).build();
    }

    private void logErrorConnectionKafkaOrJms(String connectionID, String dpID, String partMessage, Connection infoConnection, Exception e) {
        QueryPollingFacade.message[0] = e.getMessage();
        QueryPollingFacade.success[0] = false;
        queryService.addLogging("POST", partMessage + QueryPollingFacade.message[0]);
        queryServicePolling.logToDB(connectionID, infoConnection, dpID, "fail", QueryPollingFacade.message[0], null);
    }


}
