package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.ResponseCodes;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQuerySessionFacade;
import co.kr.coresolutions.model.dtos.SessionDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class QuerySessionFacade implements IQuerySessionFacade {
    private final ObjectMapper objectMapper;
    private final CacheManager cacheManager;

    @Override
    @CachePut(value = "cacheSession", cacheManager = "cacheManager")
    public SessionDto saveOrUpdateSession(String ownerId) {
        String localDateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd:HH:mm:ss"));
        return SessionDto.builder()
                .ownerId(ownerId)
                .sessionId(Base64.getEncoder().encodeToString(localDateTime.concat(ownerId).getBytes()))
                .openedTime(localDateTime)
                .lastUpdatedTime(localDateTime)
                .build();
    }

    @Override
    public ResponseDto listSessions() {
        Cache cache = cacheManager.getCache("cacheSession");
        Object nativeCache = cache.getNativeCache();
        List<SessionDto> sessionList = new ArrayList<>();
        if (nativeCache instanceof Ehcache) {
            Ehcache ehcache = (Ehcache) nativeCache;
            ehcache.getKeys().forEach(oneKey -> {
                Element element = ehcache.get(oneKey);
                sessionList.add((SessionDto) element.getObjectValue());
            });
        }

        return ResponseDto.builder()
                .successCode(ResponseCodes.OK_RESPONSE)
                .message(String.valueOf(objectMapper.valueToTree(sessionList)))
                .build();
    }

    @Override
    @CacheEvict(value = "cacheSession", cacheManager = "cacheManager")
    public ResponseDto deleteSession(String ownerId) {
        return ResponseDto.ok();
    }
}
