package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.enums.QueuePushLevel;
import co.kr.coresolutions.facades.IQueryQueuePushedFacadeWorker;
import co.kr.coresolutions.facades.IQueryRFacade;
import co.kr.coresolutions.model.dtos.SocketDto;
import co.kr.coresolutions.model.dtos.SocketLoginDto;
import co.kr.coresolutions.model.dtos.SpawnOutputDto;
import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceRSocketClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryRFacade implements IQueryRFacade {
    private static final Map<String, String> sessionQueueConnection = new HashMap<>();
    private final QueryService queryService;
    private static final SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("yyyyMMdd:HHmmss");
    private final QueryServiceRSocketClient queryServiceRSocketClient;
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
    private final Constants constants;
    private final ObjectMapper objectMapper;
    private final IQueryQueuePushedFacadeWorker queryQueuePushedFacadeWorker;

    @Override
    public Object evaluate(SocketDto socketDto, String connectionID) {
        String url = "R/{" + connectionID + "}/evaluate2";
        String partMessage = url + " triggered, ";
        if (socketDto.getQueuePushLevel() != null) {
            String queryID = socketDto.getQueryID();
            QueuePushLevel queuePushLevel = socketDto.getQueuePushLevel();
            if (queryID == null || queryID.isEmpty()) {
                queryID = UUID.randomUUID().toString();
                socketDto.setQueryID(queryID);
            }
            sessionQueueConnection.put(queryID, connectionID);
            queryQueuePushedFacadeWorker.addToQueue(queuePushLevel, queryID, url, socketDto);
            queryService.addLogging("POST",
                    partMessage + "{" + queryID + "} is pushed into {" + queuePushLevel.getName() + "}");
            return CustomResponseDto
                    .builder()
                    .Success(true)
                    .message("{" + queryID + "} is pushed into {" + queuePushLevel.getName() + "}")
                    .build();
        } else {
            return executeEvaluateR(url, socketDto, connectionID);
        }
    }


    @Override
    public Object executeEvaluateR(String url, SocketDto socketDto, String connectionID) {
        if (connectionID == null) {
            connectionID = sessionQueueConnection.get(socketDto.getQueryID());
            sessionQueueConnection.remove(socketDto.getQueryID());
        }
        String partMessage = url + " triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }
        QueryServiceRSocketClient.sessionRSocketRunningUsers.put(socketDto.getOwner(), simpleDateTimeFormat.format(new Date()));
        Object requestResponse = queryServiceRSocketClient.requestResponse("evaluate", queryService.getInfoConnection(connectionID), socketDto);
        SocketDto.OutputInfo outputInfo = socketDto.getOutputInfo();
        if (socketDto.isOutputEnabled() && outputInfo != null) {
            String outputContent = null;
            if (requestResponse instanceof CustomResponseDto) {
                CustomResponseDto customResponseDto = ((CustomResponseDto) requestResponse);
                if (customResponseDto.getSuccess()) {
                    outputContent = customResponseDto.getMessage();
                }
            } else {
                outputContent = requestResponse.toString();
            }
            //means success
            if (outputContent != null) {
                try {
                    String dir = constants.getSpawnDir()
                            + socketDto.getUserID() + File.separator
                            + outputInfo.getDir().replaceAll("TODAY", simpleDateFormat.format(new Date()))
                            + File.separator;
                    Files.createDirectories(Paths.get(dir));
                    Files.write(Paths.get(dir + outputInfo.getFilename() + "." + outputInfo.getExt()),
                            outputContent.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("title", outputInfo.getTitle());
                    jsonObject.put("desc", outputInfo.getDesc());
                    jsonObject.put("filename", outputInfo.getFilename() + "." + outputInfo.getExt());
                    jsonObject.put("seq", outputInfo.getSeq());
                    Files.write(Paths.get(dir + outputInfo.getFilename() + ".desc"),
                            jsonObject.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                    return CustomResponseDto.ok();
                } catch (IOException e) {
                    return CustomResponseDto.builder().message(e.getMessage()).build();
                }
            }
        }
        return requestResponse;
    }

    @Override
    public Object open(String owner, String connectionID) {
        String partMessage = "/R/{" + connectionID + "}/ropen/{" + owner + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }
        return queryServiceRSocketClient.requestResponse("openSession", queryService.getInfoConnection(connectionID), SocketDto.builder().owner(owner).build());
    }

    @Override
    public Object close(String owner, String connectionID) {
        String partMessage = "/R/{" + connectionID + "}/rclose/{" + owner + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("POST", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }
        return queryServiceRSocketClient.requestResponse("closeSession", queryService.getInfoConnection(connectionID), SocketDto.builder().owner(owner).build());
    }

    @Override
    public Object listUsers() {
        String partMessage = "/R/rusers triggered, ";
        queryService.addLogging("GET", partMessage + "success");
        return QueryServiceRSocketClient.sessionRSocketRequester.keySet();
    }

    @Override
    public Object listUsersRunning(String connectionID) {
        String partMessage = "R/{" + connectionID + "}/running triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("GET", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }

        queryService.addLogging("GET", partMessage + "success");
        return queryServiceRSocketClient.requestResponse("listUsersRunning", queryService.getInfoConnection(connectionID), SocketDto.builder().build());
    }

    @Override
    public Object deleteUser(SocketLoginDto socketLoginDto, String connectionID, String userID) {
        String partMessage = "R/{" + connectionID + "}/ruser/{" + userID + "} triggered, ";
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            queryService.addLogging("DELETE", partMessage + "fail due to connection file with name " + connectionID + " doesn't exist");
            return CustomResponseDto.builder().message("information (" + connectionID + ") doesn't exist").build();
        }

        queryService.addLogging("DELETE", partMessage + "success");
        return queryServiceRSocketClient.fireAndForget("deleteRunningUser", queryService.getInfoConnection(connectionID), socketLoginDto, userID);
    }

    @Override
    public Object listSpawnOutputs(SpawnOutputDto spawnOutputDto) {
        String partMessage = "spawnoutputs triggered, ";
        String dir = constants.getSpawnDir()
                + spawnOutputDto.getUserID() + File.separator
                + spawnOutputDto.getDir().replaceAll("TODAY", simpleDateFormat.format(new Date()))
                + File.separator;
        List<SpawnOutputDto.OutputInfo> outputInfoList = new ArrayList<>();
        try (Stream<Path> paths = Files.walk(Paths.get(dir))) {
            paths
                    .filter(Files::isRegularFile)
                    .filter(Files::isReadable)
                    .filter(path -> path.toString().endsWith(".desc"))
                    .forEach(path -> {
                        try {
                            outputInfoList.add(objectMapper.readValue(Files.readAllBytes(path), SpawnOutputDto.OutputInfo.class));
                        } catch (IOException e) {
                            log.error(e.getMessage());
                        }
                    });
            queryService.addLogging("POST", partMessage + "success");
            return outputInfoList.stream().sorted(Comparator.comparingLong(SpawnOutputDto.OutputInfo::getSeq)).collect(Collectors.toList());
        } catch (Exception e) {
            queryService.addLogging("POST", partMessage + e.getMessage());
            return CustomResponseDto.builder().message(e.getMessage()).build();
        }
    }
}
