package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.dao.NativeQuery;
import co.kr.coresolutions.enums.QueuePushLevel;
import co.kr.coresolutions.facades.IQueryPythonFacade;
import co.kr.coresolutions.facades.IQueryQueuePushedFacade;
import co.kr.coresolutions.facades.IQueryRFacade;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.facades.IQueryTargetFacade;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.model.dtos.SocketDto;
import co.kr.coresolutions.model.dtos.TargetDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServicePythonSocketClient;
import co.kr.coresolutions.service.QueryServiceRSocketClient;
import co.kr.coresolutions.util.CoreEntry;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;
import java.util.stream.Collectors;

import static co.kr.coresolutions.enums.QueuePushLevel.HIGH;
import static co.kr.coresolutions.enums.QueuePushLevel.LOW;
import static co.kr.coresolutions.enums.QueuePushLevel.MEDIUM;

@Component
@RequiredArgsConstructor
public class QueryQueuePushedFacade implements IQueryQueuePushedFacade {
    public static final Queue<CoreEntry> coreEntriesHIGH = new LinkedList<>();
    public static final Queue<CoreEntry> coreEntriesMIDDLE = new LinkedList<>();
    public static final Queue<CoreEntry> coreEntriesLOW = new LinkedList<>();
    protected static final String connectionIDQueue = "quadmax";
    protected static final String tableQueue = "t_ssbi_queuelog";
    protected static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyyMMdd:HHmmss");
    private final QueryService queryService;
    private final ObjectMapper objectMapper;
    private final NativeQuery nativeQuery;
    private final IQueryTargetFacade queryTargetFacade;
    private final IQueryRFacade queryRFacade;
    private final IQuerySocketFacade querySocketFacade;
    private final IQueryPythonFacade.IQueue queryPythonFacade;

    @PostConstruct
    @SneakyThrows
    public void init() {
        TaskScheduler threadPoolTaskExecutorLONGQ = new ConcurrentTaskScheduler();
        threadPoolTaskExecutorLONGQ.scheduleAtFixedRate(() -> runForEveryQueue(coreEntriesHIGH), 1000);
        TaskScheduler threadPoolTaskExecutorLONGMIDDLEQ = new ConcurrentTaskScheduler();
        threadPoolTaskExecutorLONGMIDDLEQ.scheduleAtFixedRate(() -> runForEveryQueue(coreEntriesMIDDLE), 1000);
        TaskScheduler threadPoolTaskExecutorMIDDLEQ = new ConcurrentTaskScheduler();
        threadPoolTaskExecutorMIDDLEQ.scheduleAtFixedRate(() -> runForEveryQueue(coreEntriesLOW), 1000);
    }

    public void runForEveryQueue(Queue<CoreEntry> coreEntries) {
        CoreEntry coreEntry = coreEntries.poll();
        if (coreEntry != null) {
            CoreEntry coreEntryKey = (CoreEntry) coreEntry.getKey();
            CoreEntry coreEntryValue = (CoreEntry) coreEntry.getValue();
            if (coreEntryKey != null && coreEntryValue != null) {
                QueuePushLevel pushLevel = (QueuePushLevel) coreEntryKey.getKey();
                String url = (String) coreEntryValue.getKey();
                Object object = coreEntryValue.getValue();
                if (object != null) {
                    if (object instanceof TargetDto) {
                        TargetDto targetDto = (TargetDto) object;
                        logToDBEnd(targetDto, pushLevel, queryTargetFacade.executeTarget(url, targetDto), url);
                    }
                    //evaluate r or py
                    else if (object instanceof SocketDto) {
                        SocketDto socketDto = (SocketDto) object;
                        if (url.startsWith("R/")) {
                            logToDBEnd(socketDto, pushLevel, queryRFacade.executeEvaluateR(url, socketDto, null), url);
                        } else if (url.startsWith("python")) {
                            logToDBEnd(socketDto, pushLevel, queryPythonFacade.executeEvaluatePython(url, socketDto, null), url);
                        }
                    }
                }
            }
        }
    }

    private void logToDBEnd(Object request, QueuePushLevel queuePushLevel, Object response, String url) {
        Connection connection = queryService.getInfoConnection(connectionIDQueue);
        String scheme = connection.getSCHEME();
        String owner;
        String queryID;
        String isSuccess = "TRUE";
        String userID;
        if (request instanceof TargetDto) {
            TargetDto targetDto = (TargetDto) request;
            owner = targetDto.getOwner();
            queryID = targetDto.getQueryID();
            userID = targetDto.getUserID();
        } else {
            SocketDto socketDto = (SocketDto) request;
            owner = socketDto.getOwner();
            queryID = socketDto.getQueryID();
            String idRequest = socketDto.getIdRequest();
            userID = socketDto.getUserID();
            if (idRequest != null && !idRequest.isEmpty()) {
                if (url.startsWith("R/")) {
                    if (QueryServiceRSocketClient.sessionRSocketIDErrors.contains(idRequest)) {
                        isSuccess = "FALSE";
                        QueryServiceRSocketClient.sessionRSocketIDErrors.remove(idRequest);
                    }
                } else if (url.startsWith("python")) {
                    if (QueryServicePythonSocketClient.sessionPythonSocketIDErrors.contains(idRequest)) {
                        isSuccess = "FALSE";
                        QueryServicePythonSocketClient.sessionPythonSocketIDErrors.remove(idRequest);
                    }
                }
            }
        }
        String responseString = null;
        if (response instanceof CustomResponseDto) {
            CustomResponseDto customResponseDto = (CustomResponseDto) response;
            if (customResponseDto.getMessage() != null) {
                responseString = customResponseDto.getMessage();
            } else if (customResponseDto.getErrorMessage() != null) {
                responseString = customResponseDto.getErrorMessage();
            } else {
                if (customResponseDto.getCount() != null) {
                    responseString = String.valueOf(customResponseDto.getCount());
                }
            }
            if (responseString == null) {
                if (customResponseDto.getSuccess()) {
                    responseString = "SUCCESS";
                } else {
                    responseString = "FAIL";
                }
            }
            if (!customResponseDto.getSuccess()) {
                isSuccess = "FALSE";
            }
        } else {
            responseString = String.valueOf(response);
        }

        String key = "{query/" + url + "}_{" + queuePushLevel.getName() + "}_{" + isSuccess + "}_ended";
        querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(key).message(SIMPLE_DATE_FORMAT.format(new Date())).build());

        try {
            String queryInsert = "INSERT INTO " + scheme +
                    tableQueue + " (QUERYID, OWNER, STEPS, DATETIME, QUEUENAME, REQUEST_URL, REQUEST_BODY, REQUEST_SUCCESS, STATUS_MESSAGE) VALUES ('" + queryID + "', '" + owner
                    + "', 'E', '" + SIMPLE_DATE_FORMAT.format(new Date()) + "', '" + queuePushLevel.getName() + "', '" + url + "', '" + objectMapper.writeValueAsString(request).replace("'", "''")
                    + "', '" + isSuccess + "', '" + StringEscapeUtils.escapeSql(responseString) + "')";
            nativeQuery.executeQuery(connectionIDQueue, queryInsert);
        } catch (JsonProcessingException e) {
            queryService.addLogging("addToQueueLogTable", e.getMessage());
        }
    }

    @Override
    public Object getQueues(QueuePushLevel queuePushLevel) {
        String partMessage = "queuepushed/{" + queuePushLevel.getName() + "} triggered, ";
        JSONArray jsonArray = new JSONArray();
        switch (queuePushLevel) {
            case HIGH:
                jsonArray = getForQueueName(coreEntriesHIGH, HIGH);
                break;
            case MEDIUM:
                jsonArray = getForQueueName(coreEntriesMIDDLE, MEDIUM);
                break;
            case LOW:
                jsonArray = getForQueueName(coreEntriesLOW, LOW);
                break;
        }
        queryService.addLogging("POST", partMessage + "success");
        return jsonArray;
    }

    @Override
    public Object deleteQueueQueryID(QueuePushLevel queuePushLevel, String queryID) {
        String partMessage = "queuepushed/{" + queuePushLevel.getName() + "}/{" + queryID + "} triggered, ";
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();
        switch (queuePushLevel) {
            case HIGH:
                customResponseDto = removeForQueueName(coreEntriesHIGH, HIGH, queryID);
                break;
            case MEDIUM:
                customResponseDto = removeForQueueName(coreEntriesMIDDLE, MEDIUM, queryID);
                break;
            case LOW:
                customResponseDto = removeForQueueName(coreEntriesLOW, LOW, queryID);
                break;
        }
        if (customResponseDto.getSuccess()) {
            queryService.addLogging("DELETE", partMessage + "success");
            customResponseDto.setMessage("{" + queryID + "} is removed");
            return customResponseDto;
        }
        queryService.addLogging("DELETE", partMessage + "fail");
        customResponseDto.setMessage("{" + queryID + "} doesn't exist!");
        return customResponseDto;
    }

    private CustomResponseDto removeForQueueName(Queue<CoreEntry> coreEntries, QueuePushLevel queuePushLevel, String queryID) {
        final boolean[] removed = {false};
        coreEntries.stream().filter(coreEntry -> {
            CoreEntry coreEntryKey = (CoreEntry) coreEntry.getKey();
            QueuePushLevel pushLevel = (QueuePushLevel) coreEntryKey.getKey();
            return pushLevel.getName().equals(queuePushLevel.getName());
        }).filter(coreEntry -> {
            CoreEntry coreEntryKey = (CoreEntry) coreEntry.getKey();
            String queryIDFound = (String) coreEntryKey.getValue();
            return queryID.equals(queryIDFound);
        }).collect(Collectors.toList()).forEach(coreEntry -> {
            if (coreEntries.remove(coreEntry)) {
                removed[0] = true;
            }
        });
        return CustomResponseDto.builder().Success(removed[0]).build();
    }

    private JSONArray getForQueueName(Queue<CoreEntry> coreEntries, QueuePushLevel queuePushLevel) {
        JSONArray jsonArray = new JSONArray();
        coreEntries.stream().filter(coreEntry -> {
            CoreEntry coreEntryKey = (CoreEntry) coreEntry.getKey();
            QueuePushLevel pushLevel = (QueuePushLevel) coreEntryKey.getKey();
            return pushLevel.getName().equals(queuePushLevel.getName());
        }).forEach(coreEntry -> {
            CoreEntry coreEntryKey = (CoreEntry) coreEntry.getKey();
            CoreEntry coreEntryValue = (CoreEntry) coreEntry.getValue();
            String queryID = (String) coreEntryKey.getValue();
            String url = (String) coreEntryValue.getKey();
            Object object = coreEntryValue.getValue();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("queryID", queryID);
            jsonObject.put("url", url);
            try {
                jsonObject.put("body", objectMapper.writeValueAsString(object));
            } catch (JsonProcessingException e) {
                jsonObject.put("body", e.getMessage());
            }
            jsonArray.put(jsonObject);
        });
        return jsonArray;
    }


}
