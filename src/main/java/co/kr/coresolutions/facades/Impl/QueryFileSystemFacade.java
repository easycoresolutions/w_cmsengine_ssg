package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryFileSystemFacade;
import co.kr.coresolutions.model.dtos.FileSystemDto;
import co.kr.coresolutions.service.QueryServiceFileSystem;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class QueryFileSystemFacade implements IQueryFileSystemFacade {
    private final QueryServiceFileSystem queryServiceFileSystem;

    @Override
    public CustomResponseDto getFiles(FileSystemDto fileSystemDto) {
        return queryServiceFileSystem.getAllFilesAndDirectories(fileSystemDto.getDirectory());
    }

    @Override
    public CustomResponseDto getContents(FileSystemDto fileSystemDto) {
        return queryServiceFileSystem.getContentFile(fileSystemDto.getDirectory(), fileSystemDto.getFilename());
    }

    @Override
    public CustomResponseDto write(FileSystemDto fileSystemDto) {
        return queryServiceFileSystem.write(fileSystemDto);
    }

    @Override
    public CustomResponseDto delete(FileSystemDto fileSystemDto) {
        return queryServiceFileSystem.delete(fileSystemDto);
    }
}
