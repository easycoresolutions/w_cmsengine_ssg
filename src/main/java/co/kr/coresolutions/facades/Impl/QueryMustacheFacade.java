package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryMustacheFacade;
import co.kr.coresolutions.model.dtos.MustacheTemplateDTO;
import co.kr.coresolutions.service.Constants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryMustacheFacade implements IQueryMustacheFacade {
    private final MustacheFactory mustacheFactory;
    private final ObjectMapper objectMapper;
    @Autowired
    private Constants constants;
    private String mustacheDir;

    @PostConstruct
    public void init() {
        mustacheDir = constants.getMustacheDir();
    }

    @Override
    public Object convert(MustacheTemplateDTO mustacheTemplateDTO) {
        String separator = mustacheTemplateDTO.getSeparator();
        List<String> missingFiles = mustacheTemplateDTO.getIds().stream().filter(id -> Files.notExists(Paths.get(mustacheDir + id + ".txt"))).collect(Collectors.toList());
        if (!missingFiles.isEmpty()) {
            return CustomResponseDto.builder().message("file(s) " + missingFiles + " doesn't exist").build();
        }

        if (separator.equals("line") || separator.isEmpty()) {
            Map map = objectMapper.convertValue(mustacheTemplateDTO.getParameters(), Map.class);
            Writer writer = new StringWriter();
            final String[] result = {""};
            boolean allSuccess = mustacheTemplateDTO.getIds().stream().allMatch(id -> {
                Mustache mustache;
                try {
                    mustache = mustacheFactory.compile(new StringReader(new String(Files.readAllBytes(Paths.get(mustacheDir + id + ".txt")))), "coreSolutionTemplate");
                    String actualResult = mustache.execute(writer, map).toString();
                    result[0] = result[0].isEmpty() ? actualResult : result[0] + (separator.equals("line") ? System.lineSeparator() : "") + actualResult;
                    return true;
                } catch (IOException e) {
                    result[0] = "fail due\t" + e.getMessage();
                    return false;
                }
            });
            if (!allSuccess) {
                return CustomResponseDto.builder().message(result[0]).build();
            }
            return CustomResponseDto.builder().Success(true).message(result[0]).build();
        }
        return CustomResponseDto.builder().message("separator should be line or \"\"").build();
    }
}

