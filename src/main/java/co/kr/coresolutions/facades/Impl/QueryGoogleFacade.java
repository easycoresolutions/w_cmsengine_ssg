package co.kr.coresolutions.facades.Impl;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryGoogleFacade;
import co.kr.coresolutions.model.dtos.GoogleTemplateDTO;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceJSON;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

@Component
@RequiredArgsConstructor
@Slf4j
public class QueryGoogleFacade implements IQueryGoogleFacade {
    private final QueryService queryService;
    private final ObjectMapper objectMapper;
    private final QueryServiceJSON queryServiceJSON;
    private final RestTemplate restTemplate;

    @Override
    public Object getAndConvertSheet(String connectionID, GoogleTemplateDTO googleTemplateDTO) {
        boolean isFileExist = queryService.isConnectionIdFileExists(connectionID);
        if (!isFileExist) {
            return CustomResponseDto.builder().message("Connection file with name\t" + connectionID + "\tdoesn't exist").build();
        }
        try {
            String url = objectMapper.readTree(queryService.getConnectionFile(connectionID)).get("URL").asText().replace("{sheetId}", googleTemplateDTO.getSheetId())
                    .replace("{sheetRange}", googleTemplateDTO.getSheetRange()).replace("{sheetKey}", googleTemplateDTO.getSheetKey());
            ResponseEntity responseEntity = restTemplate
                    .exchange(UriComponentsBuilder.fromHttpUrl(url).build().toUri(), HttpMethod.GET, new HttpEntity<>(new HttpHeaders()), String.class);
            Object response = responseEntity.getBody();
            if (response == null) {
                return CustomResponseDto.builder().message("error from google null returned").build();
            }
            if (responseEntity.getStatusCodeValue() != 200) {
                return CustomResponseDto.builder().message("error from google\t" + response.toString()).build();
            }
            JsonNode jsonNode = objectMapper.readTree(response.toString());
            if (jsonNode.isNull() || !jsonNode.has("values")) {
                return CustomResponseDto.builder().message("connection file contains invalid structure").build();
            }
            JSONArray jsonArray = new JSONArray(objectMapper.readTree(responseEntity.getBody().toString()).get("values").toString());
            JSONArray jsonArrayToBuild = new JSONArray();
            JSONArray fieldsArray = jsonArray.getJSONArray(0);
            StreamSupport.stream(jsonArray.spliterator(), false).skip(1).forEach(o -> {
                JSONObject jsonObject = new JSONObject();
                JSONArray jsonArray1 = new JSONArray(o.toString());
                IntStream.range(0, fieldsArray.length())
                        .forEach(index -> jsonObject.put(fieldsArray.get(index).toString(), jsonArray1.get(index).toString()));
                jsonArrayToBuild.put(jsonObject);
            });
            return queryServiceJSON.structureJson(jsonArrayToBuild.toString(), false, new HashMap<>(), "cache");
        } catch (Exception e) {
            return CustomResponseDto.builder().message(e.getMessage()).build();
        }
    }
}
