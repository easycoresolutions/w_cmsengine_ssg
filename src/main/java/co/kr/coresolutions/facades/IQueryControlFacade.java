package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.model.dtos.ExcelTemplateDto;
import co.kr.coresolutions.model.dtos.FileControlDto;

import javax.servlet.http.HttpServletResponse;

public interface IQueryControlFacade {
    ResponseDto createFile(FileControlDto fileControlDto);

    ResponseDto deleteDir(FileControlDto fileControlDto);

    ResponseDto reportSubmit(FileControlDto fileControlDto, HttpServletResponse response);

    CustomResponseDto reportExcel(ExcelTemplateDto excelTemplateDto);
}
