package co.kr.coresolutions.facades;

import com.fasterxml.jackson.databind.node.ObjectNode;

public interface IQueryNitriteFacade {
    Object save(String fileName, String collectionName, String docID, ObjectNode json);

    Object delete(String fileName, String collectionName, String docID);

    Object update(String fileName, String collectionName, String docID, ObjectNode json);

    Object get(String fileName);

    Object getIf(String fileName, String collectionName, ObjectNode json);

    Object getByDocID(String fileName, String collectionName, String docID);

    Object saveIndexes(String fileName, String collectionName, ObjectNode json);

    Object deleteIndex(String fileName, String collectionName, String fieldName);

    Object rebuildIndex(String fileName, String collectionName, String fieldName);
}
