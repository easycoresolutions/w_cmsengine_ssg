package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.dtos.Kafka2Dto;
import co.kr.coresolutions.model.dtos.Kafka3Dto;

public interface IQueryKafkaFacade {
    Object send(String connectionID, Kafka3Dto kafka3Dto);

    Object sendRecords(String connectionID, Kafka2Dto kafka2Dto);
}
