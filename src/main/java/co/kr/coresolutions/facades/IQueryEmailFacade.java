package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.model.dtos.EmailDto;
import co.kr.coresolutions.model.dtos.EmailSpawnDto;

public interface IQueryEmailFacade {
    ResponseDto sendSimpleEmail(EmailDto emailDto);

    ResponseDto sendSpawnEmail(EmailSpawnDto emailSpawnDto);
}
