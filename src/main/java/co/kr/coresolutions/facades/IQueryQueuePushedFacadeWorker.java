package co.kr.coresolutions.facades;

import co.kr.coresolutions.enums.QueuePushLevel;

@FunctionalInterface
public interface IQueryQueuePushedFacadeWorker {
    void addToQueue(QueuePushLevel queuePushLevel, String queryID, String url, Object object);
}
