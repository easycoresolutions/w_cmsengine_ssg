package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.TableLoadClickHouse;

public interface IQueryClickHouseFacade {
    Object tableLoad(TableLoadClickHouse tableLoadClickHouse);

    Object monitorProcess(String connectionID);

    Object deleteProcess(String connectionID, String queryID);

    Object monitorImport();

    Object deleteImport(String queryID);

    Object monitorTables(String connectionID, String dbName);
}
