package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.dtos.MustacheTemplateDTO;

@FunctionalInterface
public interface IQueryMustacheFacade {
    Object convert(MustacheTemplateDTO mustacheTemplateDTO);
}
