package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.dtos.QueueExecNodesDto;

public interface IQueryQueueExecNodesFacade {
    Object send(String commandID, QueueExecNodesDto execNodesDto, String urlPath);

    Object findWaiting();

    Object remove(String commandID);
}
