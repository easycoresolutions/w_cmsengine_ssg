package co.kr.coresolutions.facades;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.dtos.LoginDto;

import javax.servlet.http.HttpServletRequest;

public interface IQueryAuthFacade {
    CustomResponseDto login(LoginDto loginDto);

    CustomResponseDto logout(HttpServletRequest request);

//    CustomResponseDto addUser(LoginDto loginDto) throws IOException;

//    CustomResponseDto removeUser(LoginDto loginDto) throws IOException;

//    CustomResponseDto changePwUser(String userId, String password, String newPassword) throws IOException;

    CustomResponseDto getBlackList();

//    CustomResponseDto initPassword(JsonNode jsonNode);
}
