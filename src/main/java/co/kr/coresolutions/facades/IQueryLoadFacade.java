package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.UnLoad;

public interface IQueryLoadFacade {
    Object unloadClickHouse(UnLoad unLoad, String connectionID);

    Object unload(UnLoad unLoad, String connectionID);

    Object monitor();

    Object deleteUnload(String unloadID);
}
