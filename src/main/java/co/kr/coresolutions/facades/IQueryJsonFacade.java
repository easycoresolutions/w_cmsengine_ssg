package co.kr.coresolutions.facades;

import co.kr.coresolutions.model.dtos.QueryDto;
import co.kr.coresolutions.model.dtos.QueryTemplateDto;

import java.util.List;

public interface IQueryJsonFacade {
    Object postForAll(List<QueryDto> queryDtoList);

    Object postForAllExecJson(List<QueryTemplateDto> queryTemplateDtoList);
}
