package co.kr.coresolutions.jpa.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

@AllArgsConstructor
@Getter
public enum Role implements GrantedAuthority {
    ROLE_ADMIN("ADMIN"), ROLE_USER("USER");
    private String name;

    public String getAuthority() {
        return name();
    }
}
