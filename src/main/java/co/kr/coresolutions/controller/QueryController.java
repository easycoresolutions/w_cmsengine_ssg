package co.kr.coresolutions.controller;

import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryService;
import com.ibatis.common.resources.Resources;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

@Controller
@Slf4j
@RequiredArgsConstructor
public class QueryController {
    private final QueryService queryService;
    private final Constants constants;
    private String _referenceQueryDir;
    @PostConstruct
    public void init() throws IOException {
        _referenceQueryDir = constants.getReferenceDir();
        String resource = "config" + File.separator + "config.properties";

        Properties properties = new Properties();
        Reader reader = Resources.getResourceAsReader(resource);

        properties.load(reader);

        String isIgnite = properties.getProperty("IS_CACHE");
        log.info("isIgnite enabled=\t" + isIgnite);

        if (isIgnite != null && isIgnite.equalsIgnoreCase("true")) {
            CacheManager cm = CacheManager.getInstance();

            if (!cm.cacheExists("cacheReference"))
                cm.addCache("cacheReference");

            if (!cm.cacheExists("Rcache")) {
                Cache cache = cm.getCache("Rcache");
                cache.removeAll();
            }

            Cache cache = cm.getCache("cacheReference");

            AtomicBoolean failCaching = new AtomicBoolean(false);

            try (Stream<Path> paths = Files.walk(Paths.get(_referenceQueryDir))) {
                paths
                        .filter(Files::isRegularFile)
                        .forEach(path1 -> {
                                    failCaching.set(false);
                                    try {
                                        cache.putIfAbsent(new Element(path1.getFileName().toString(), new String(Files.readAllBytes(path1), StandardCharsets.UTF_8)));
                                    } catch (IOException e1) {
                                        failCaching.set(true);
                                    }
                                    try {
                                        if (!failCaching.get())
                                            queryService.addLogging("EhCache - cacheReference", "REFERENCE_NAME : " + path1.getFileName().toString() + " (" + (Files.readAllLines(path1).size() - 1) + ") is cached successfully." + "\n");
                                        else
                                            queryService.addLogging("EhCache - cacheReference", "REFERENCE_NAME : " + path1.getFileName().toString() + " has an error, not cached." + "\n");

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                }
                        );
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Description : return the home.jsp page.
     *
     * @param model : model.
     */
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(Model model) {
        return "home.jsp";
    }

    /**
     * Description : return the Message.jsp page.
     *
     * @param model : model.
     */
    @RequestMapping(value = "/message", method = RequestMethod.GET)
    public String message(Model model) {
        return "Message.jsp";
    }

    /**
     * Description : return the login.jsp page.
     *
     * @param model : model.
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        return "login.jsp";
    }

}

