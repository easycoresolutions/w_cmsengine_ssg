package co.kr.coresolutions.dao;

import co.kr.coresolutions.service.QueryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class NativeQuery {
    private final QueryService queryService;
    private final DataBase dataBase;

/*
    public Map<String, Object> getCreateTableSchema(String sourceConnectionId, String sourceTable, String targetTable, JSONObject jsonObject) {
        Map<String, Object> map = new HashMap<>();
        Connection conn = null;
        ResultSet columns = null;
        ResultSet PK = null;
        try {
            Map<String, String> infoConnection = queryService.getInfoConnection(sourceConnectionId);
            conn = dataBase.makeConnection(sourceConnectionId);
            DatabaseMetaData meta = conn.getMetaData();
            columns = meta.getColumns(null, null, sourceTable, null);

            List<String> pkNames = new LinkedList<>();
            PK = meta.getPrimaryKeys(null, null, sourceTable);
            while (PK.next()) {
                pkNames.add(PK.getString("COLUMN_NAME"));
            }
            Set<String> columnsdefinitions = new HashSet<>();
            while (columns.next()) {
                String columnName = columns.getString("COLUMN_NAME");
                String TYPE_NAME = columns.getString("TYPE_NAME");
                if (TYPE_NAME.toLowerCase().contains("varchar2")) TYPE_NAME = "VARCHAR";
                String columnsize = columns.getString("COLUMN_SIZE");
                if (TYPE_NAME.equalsIgnoreCase("char")) TYPE_NAME = "VARCHAR";
                String isNullable = columns.getString("IS_NULLABLE");
                String is_autoIncrment = !infoConnection.get("DBMS").equalsIgnoreCase("oracle") ? columns.getString("IS_AUTOINCREMENT") : "";
                columnsdefinitions.add(columnName + " " + TYPE_NAME + (TYPE_NAME.toLowerCase().contains("char") ? "(" + columnsize + ")" : "")
                        + (isNullable.equalsIgnoreCase("no") ? " NOT NULL " : " ")
                        + (is_autoIncrment.equalsIgnoreCase("yes") ? "AUTO_INCREMENT " : " ") +
                        (pkNames.contains(columnName) ? " PRIMARY KEY " : " "));
            }

            List<String> stringList = new LinkedList<>();
            StringBuilder stringBuilder = new StringBuilder();
            columnsdefinitions.stream().forEach(s -> {
                if (jsonObject.has(s.split("\\s+")[0])) {
                    stringBuilder.append(s);
                    stringList.add(s);
                    stringBuilder.append(",");
                }
            });

            map.put("sql", "Create Table " + targetTable + "(" + stringBuilder.substring(0, stringBuilder.length() - 1) + ")");
            map.put("keys", stringList);

            columns.close();
            columns = null;
            PK.close();
            PK = null;
            conn.close();
            conn = null;
        } catch (SQLException | ClassNotFoundException | IOException e) {
            e.printStackTrace();
            map.put("error", "error : {" + e.getMessage() + "}");
        } finally {
            if (PK != null) {
                try {
                    PK.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            if (columns != null) {
                try {
                    columns.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return map;

    }
*/

    public String executeBatchQuery(String connectionId, List<String> queries) {
        try (Connection conn = dataBase.makeConnection(connectionId); Statement stmt = conn.createStatement()) {
//        	queryService.addLogging("executeBatchQuery", queries.toString());
            conn.setAutoCommit(false);
            queries.forEach(query -> {
                try {
                    stmt.addBatch(query);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            stmt.executeBatch();
            conn.commit();
        } catch (Exception e) {
        	queryService.addLogging("error", e.getMessage());	
        	for (StackTraceElement el : e.getStackTrace()) {
        		queryService.addLogging("error", el.toString());				
			}
            e.printStackTrace();
            return "error : {" + e.getMessage() + "}";
        }
        return "";
    }


    public String executeQuery(String connectionId, String query) {
        if (query.startsWith("error")) {
            return query;
        }

        try (Connection conn = dataBase.makeConnection(connectionId); Statement stmt = conn.createStatement()) {
            boolean success = stmt.execute(query);
            if (success) {
                return "";
            }
        } catch (SQLException e) {
            return "error : {" + e.getMessage() + "}";
        } catch (ClassNotFoundException e) {
            return "error : binary class doesn't found at classpath for connectionId " + connectionId;
        }
        return "";
    }

    public String isTableExist(String targetConnectionId, String targetTable) {
        Connection conn = null;
        ResultSet resultSet = null;
        try {
            conn = dataBase.makeConnection(targetConnectionId);
            DatabaseMetaData meta = conn.getMetaData();
            resultSet = meta.getTables(null, null, targetTable.toUpperCase(), new String[]{"TABLE"});
            while (resultSet.next()) {
                if (resultSet.getString("TABLE_NAME").equalsIgnoreCase(targetTable)) return "yes";
            }
            resultSet.close();
            resultSet = null;
            conn.close();
            conn = null;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return "error : {" + e.getMessage() + "}";
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return "no";
    }


    public String getCreateTableAuditSchema(String audit_table) {
        return "CREATE TABLE " + audit_table + "(tablename varchar(256),create_date varchar(8) ,created_time varchar(8),created_rows int)";
    }

    public String CreateTable(String targetTable, String targetId, org.json.JSONObject output_schema) {
        List<String> names = new LinkedList<>(), types = new LinkedList<>();
        List<Integer> lengths = new LinkedList<>();
        StringBuilder stringBuilder = new StringBuilder();
        try {
            names = (List<String>) output_schema.get("name");
            types = (List<String>) output_schema.get("type");
            lengths = (List<Integer>) output_schema.get("length");
        } catch (Exception e) {
            e.printStackTrace();
            return "Not valid output_scheme syntax";
        }

        if (names.size() == types.size() && types.size() == lengths.size() && types.size() > 0) {
            Iterator<String> it1 = names.iterator();
            Iterator<String> it2 = types.iterator();
            Iterator<Integer> it3 = lengths.iterator();
            while (it1.hasNext() && it2.hasNext() && it3.hasNext()) {
                stringBuilder.append(it1.next() + " " + it2.next() + " (" + it3.next() + ") ,");
            }
            String resultQuery = executeQuery(targetId, "CREATE TABLE " + targetTable + " ( " + stringBuilder.substring(0, stringBuilder.length() - 1) + " );");
            if (resultQuery.length() > 0) return resultQuery;
        } else {
            return "Not valid output_scheme syntax";
        }
        return "";
    }

    public Map<String, Object> getColumns(String targetId, String targetTable) {
        Map<String, Object> map = new HashMap<>();
        Connection conn = null;
        ResultSet columns = null;
        try {
            if (targetTable.contains("."))
                targetTable = targetTable.substring(targetTable.lastIndexOf(".") + 1);

            conn = dataBase.makeConnection(targetId);
            DatabaseMetaData meta = conn.getMetaData();
            columns = meta.getColumns(null, null, targetTable.toUpperCase(), null);
            Set<String> columnDefinitions = new HashSet<>();
            Map<String, String> dataTypes = new HashMap<>();
            while (columns.next()) {
                String column_name = columns.getString("COLUMN_NAME");
                columnDefinitions.add(column_name);
                dataTypes.put(column_name.toLowerCase(), columns.getString("TYPE_NAME").toLowerCase());
            }
            map.put("keys", columnDefinitions);
            map.put("datatypes", dataTypes);
            conn.close();
            conn = null;
            columns.close();
            columns = null;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            map.put("error", "error : {" + e.getMessage() + "}");
        } finally {
            if (columns != null) {
                try {
                    columns.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return map;
    }

    public void createSQLiteDB(String connectionId, String sqliteUrl, String userID, String dbFileName) throws ClassNotFoundException, SQLException {
        co.kr.coresolutions.model.Connection infoConnection = queryService.getInfoConnection(connectionId);
        Class.forName(infoConnection.getDRIVER());
        String url = infoConnection.getURL();
        if (!sqliteUrl.isEmpty()) {
            url = sqliteUrl;
        }
        DriverManager.getConnection(url + userID + dbFileName, infoConnection.getID(), infoConnection.getPW());
    }

    public boolean deleteTableSQLite(String connectionId, String sqliteUrl, String directoryToDB, String tableName) {
        try {
            co.kr.coresolutions.model.Connection infoConnection = queryService.getInfoConnection(connectionId);
            Class.forName(infoConnection.getDRIVER());
            String url = infoConnection.getURL();
            if (!sqliteUrl.isEmpty()) {
                url = sqliteUrl;
            }
            Connection connection = DriverManager.getConnection(url + directoryToDB, infoConnection.getID(), infoConnection.getPW());
            Statement statement = connection.createStatement();
            return statement.executeUpdate("DROP TABLE " + tableName) == 0;
        } catch (Exception e) {
            return false;
        }
    }

    public Map<String, Object> getQueryColumns(String connectionId, String query, boolean isLabel) {
        Map<String, Object> map = new HashMap<>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = dataBase.makeConnection(connectionId);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            ResultSetMetaData meta = rs.getMetaData();
            Set<String> columnsdefinitions = new HashSet<>();
            Map<String, String> datatypes = new HashMap<>();
            for (int i = 0; i < meta.getColumnCount(); ++i) {
                columnsdefinitions.add(isLabel ? meta.getColumnLabel(i + 1) : meta.getColumnName(i + 1));
                datatypes.put((isLabel ? meta.getColumnLabel(i + 1) : meta.getColumnName(i + 1)).toLowerCase(), meta.getColumnTypeName(i + 1).toLowerCase());
            }
            map.put("keys", columnsdefinitions);
            map.put("datatypes", datatypes);

            rs.close();
            rs = null;
            stmt.close();
            stmt = null;
            conn.close();
            conn = null;
        } catch (SQLException | ClassNotFoundException e) {
            map.put("error", "error : {" + e.getMessage() + "}");
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return map;
    }
}

