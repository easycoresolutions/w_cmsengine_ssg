package co.kr.coresolutions.dao;

import co.kr.coresolutions.model.Connection;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public abstract class AbstractDao {

    protected SessionFactory createSessionFactory(Connection infoConnection) {
        String dbms = infoConnection.getDBMS();
        Configuration cfg = new Configuration()
                .setProperty("hibernate.dialect", dbms.equalsIgnoreCase("MSSQL") ? "org.hibernate.dialect.SQLServerDialect" :
                        dbms.equalsIgnoreCase("ORACLE") ? "org.hibernate.dialect.Oracle10gDialect" :
                                dbms.equalsIgnoreCase("DRUID") ? "org.hibernate.dialect.MySQLDialect" :
                                        dbms.equalsIgnoreCase("MYSQL") ? "org.hibernate.dialect.MySQLDialect" :
                                                dbms.equalsIgnoreCase("IGNITE") ? "org.hibernate.dialect.MySQLDialect" :
                                                        dbms.equalsIgnoreCase("ClickHouse") ? "org.hibernate.dialect.MySQLDialect" :
                                                                dbms.equalsIgnoreCase("SQLITE") ? "org.hibernate.dialect.MySQLDialect" :
                                                                        dbms.equalsIgnoreCase("DB2") ? "org.hibernate.dialect.DB2Dialect" :
                                                                                dbms.equalsIgnoreCase("HANA") ? "org.hibernate.dialect.AbstractHANADialect" : "")
                .setProperty("hibernate.connection.driver_class", infoConnection.getDRIVER())
                .setProperty("hibernate.connection.url", infoConnection.getURL())
                .setProperty("hibernate.connection.password", infoConnection.getPW())
                .setProperty("hibernate.connection.username", infoConnection.getID())
                .setProperty("hibernate.order_inserts", "true")
                .setProperty("hibernate.connection.CharSet", "utf8")
                .setProperty("hibernate.jdbc.batch_size", "50")
                .setProperty("hibernate.connection.characterEncoding", "utf8")
                .setProperty("hibernate.connection.useUnicode", "true")
                .setProperty("hibernate.jdbc.batch_versioned_data", "true")
                .setProperty("hibernate.generate_statistics", "true")
                .setProperty("hibernate.current_session_context_class", "org.hibernate.context.internal.ThreadLocalSessionContext")
                .setProperty("javax.persistence.query.timeout", "10");
        return cfg.buildSessionFactory();
    }

}
