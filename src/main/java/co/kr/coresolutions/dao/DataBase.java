package co.kr.coresolutions.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface DataBase {

    String executeQuery(co.kr.coresolutions.model.Connection infoConnection, String formattedString, int maxRunningTime, int maxRows);

    String executeBatchQueryForLater(co.kr.coresolutions.model.Connection infoConnection, List<String> queries, int maxRunningTime, int maxRows, String randomId);

    String executeQuerySelectJDBC(co.kr.coresolutions.model.Connection infoConnection, String connectionID, String sql,
                                  String queryid, boolean isLabel, boolean isColumnType, long liquidLimit);

    String executeQuerySelectJDBCLimits(co.kr.coresolutions.model.Connection infoConnection, String connectionID, String sql, String queryid, boolean isLabel, int limits);

    String getQueryCountJDBC(String connectionId, String query, String queryID) throws IOException;

    Connection makeConnection(String connectionId) throws SQLException, ClassNotFoundException;

    void doCommit(List<String> randomIds);

    void rollBack(List<String> randomIds);

    int MAX_FETCH_SIZE = 10000;

    String unloadQuerySelect(co.kr.coresolutions.model.Connection infoConnection, String connectionID, String unLoadID, String query, int maxRowsForSingleCommit);
}
