package co.kr.coresolutions.dao;

import co.kr.coresolutions.commons.Chunk;
import co.kr.coresolutions.commons.HelperUtil;
import co.kr.coresolutions.commons.QueryChunkPublisher;
import co.kr.coresolutions.facades.Impl.QueryLoadFacade;
import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceSQLite;
import co.kr.coresolutions.util.Util;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import oracle.jdbc.internal.OracleTypes;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

@Repository
@RequiredArgsConstructor
public class DataBaseImpl extends AbstractDao implements DataBase {
    private final QueryService queryService;
    private final Constants constants;
    private final QueryChunkPublisher queryChunkPublisher;


    @Override
    public String getQueryCountJDBC(String connectionID, String query, String queryID) {
        JSONArray jsonArray = new JSONArray();
        String cntQuery;
        try (Connection connection = makeConnection(connectionID); Statement stmt = connection.createStatement()) {
            cntQuery = "SELECT COUNT(1) cnt FROM (" + query + ") T ";
            String startTime = Util.getCurrentTime();
            JSONObject jsonObject = QueryService.sessionMap.get(queryID);
            if (jsonObject != null) {
                jsonObject.put("con", stmt);
                jsonObject.put("query", query);
                jsonObject.put("startTime", startTime);
                QueryService.sessionMap.put(queryID, jsonObject);
            }

            ResultSet results = stmt.executeQuery(cntQuery);
            while (results.next()) {
                OrderedJSONObject orderedJSONObject = new OrderedJSONObject();
                String value = results.getString(1);
                if (queryService.isRunStop(queryID)) {
                    HelperUtil.queryIDObjectMultiMap.remove(queryID);
                    throw new InterruptedException();
                }
                orderedJSONObject.put("cnt", new BigDecimal(value));
                jsonArray.put(orderedJSONObject);
            }
            results.close();
        } catch (SQLException | ClassNotFoundException throwables) {
            queryService.addLogging("Query Select count JDBC ", "SQL Query is\t" + query + "\n");
            return "Error : {" + throwables.toString() + "}";
        } catch (InterruptedException e) {
            queryService.addLogging("Query Select count JDBC ", "SQL Query is\t" + query + "\n interrupted by user request");
            return "Error : { query  " + queryID + " killed by user request }";
        }
        return jsonArray.toString();
    }

    @Override
    public String executeQuery(co.kr.coresolutions.model.Connection infoConnection, String formattedString, int maxRunningTime, int maxRows) {
        int numberOfRows = 0;
        SessionFactory _tempSessionFactory = null;
        Session session = null;
        @SuppressWarnings("rawtypes")
        NativeQuery nQuery;
        try {
            _tempSessionFactory = createSessionFactory(infoConnection);
            session = _tempSessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            if (maxRunningTime > 0) {
                tx.setTimeout(maxRunningTime);
            }

            nQuery = session.createNativeQuery(formattedString);
            numberOfRows = nQuery.executeUpdate();

            tx.commit();
            session.close();
            session.close();
            session = null;
            _tempSessionFactory.close();
        } catch (Exception ex) {
            queryService.addLogging("Query Update", "SQL Query is\t" + formattedString + "failed " + ex.getMessage() + "\n");
            if (ex.getCause() != null) {
                queryService.addLogging("Query Update", "SQL Query is\t" + formattedString + "\n");
                return ("Error : {" + ex.getCause().getMessage() + "}");
            }
        } finally {
            if (session != null) {
                session.clear();
                session.close();
            }
            if (_tempSessionFactory != null) {
                _tempSessionFactory.close();
            }
        }
        return "success count : " + numberOfRows;
    }

    @Override
    public void doCommit(List<String> randomIds) {
        randomIds.forEach(randomId -> {
            List<Object> objects = QueryService.transactionMap.get(randomId);
            if (objects != null && !objects.isEmpty()) {
                Transaction transaction = (Transaction) objects.get(0);
                SessionFactory sessionFactory = (SessionFactory) objects.get(1);
                Session session = (Session) objects.get(2);
                if (transaction != null && transaction.isActive()) {
                    transaction.commit();
                }
                if (session.isOpen()) {
                    session.close();
                }
                if (sessionFactory != null && sessionFactory.isOpen()) {
                    sessionFactory.close();
                }
                QueryService.sessionFutureMap.remove(randomId);
                QueryService.transactionMap.remove(randomId);
            }
        });
    }

    @Override
    public void rollBack(List<String> randomIds) {
        randomIds.forEach(randomId -> {
            List<Object> objects = QueryService.transactionMap.get(randomId);
            if (objects != null && !objects.isEmpty()) {
                Transaction transaction = (Transaction) objects.get(0);
                SessionFactory sessionFactory = (SessionFactory) objects.get(1);
                Session session = (Session) objects.get(2);
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                }
                if (session != null) {
                    session.close();
                }
                if (sessionFactory != null && sessionFactory.isOpen()) {
                    sessionFactory.close();
                }
                QueryService.sessionFutureMap.remove(randomId);
                QueryService.transactionMap.remove(randomId);
            }
        });
    }

    @Override
    public String executeBatchQueryForLater(co.kr.coresolutions.model.Connection infoConnection, List<String> queries, int maxRunningTime, int maxRows, String randomId) {
        int numberOfRows = 0;
        SessionFactory sessionFactory;
        Session session;
        Transaction transaction;
        try {
            String firstRandomID = "";
            String secondRandomID = "";
            if (randomId.contains("same")) {
                firstRandomID = randomId.substring(0, randomId.indexOf("same"));
                secondRandomID = randomId.substring(randomId.indexOf("same") + 4);
            }
            if (!firstRandomID.isEmpty() && QueryService.sessionFutureMap.containsKey(firstRandomID)) {
                session = (Session) QueryService.sessionFutureMap.get(firstRandomID);
                sessionFactory = session.getSessionFactory();
                transaction = session.getTransaction();
            } else {
                sessionFactory = createSessionFactory(infoConnection);
                session = sessionFactory.openSession();
                transaction = session.beginTransaction();
                QueryService.sessionFutureMap.put(randomId, session);
            }
            if (maxRunningTime > 0) {
                transaction.setTimeout(maxRunningTime);
            }
            for (int i = 0; i < queries.size(); i++) {
                Query query = session.createNativeQuery(new String(queries.get(i).getBytes(StandardCharsets.UTF_8)));
                query.executeUpdate();
                if (i % 50 == 0) {
                    session.flush();
                    session.clear();
                }
            }
            session.flush();
            session.clear();

            QueryService.transactionMap.put(secondRandomID.isEmpty() ? randomId : secondRandomID, Lists.newArrayList(transaction, sessionFactory, session));

        } catch (Exception ex) {
            queryService.addLogging("Query Update", "SQL Query is\t" + queries + " failed " + ex.getMessage() + "\n");
            if (ex.getCause() != null) {
                queryService.addLogging("Query Update", "SQL Query is\t" + queries + "\n");
                return ("Error : {" + ex.getCause().getMessage() + "}");
            }
        }
        return "success count : " + numberOfRows;
    }

    @Override
    public String executeQuerySelectJDBC(co.kr.coresolutions.model.Connection infoConnection, String connectionID, String sql,
                                         String queryid, boolean isLabel, boolean isColumnType, long liquidLimit) {
        Connection connection = null;
        JSONArray jsonArray = new JSONArray();
        CallableStatement callableStatement = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            connection = makeConnection(connectionID);
            if (connection != null) {
                String loSql = sql.toLowerCase();
                if (infoConnection.getDRIVER().contains("oracle") &&
                        (loSql.startsWith("call ") || loSql.startsWith("exec ") || loSql.startsWith("execute "))) {
                    callableStatement = connection.prepareCall(sql);

                    int paraCount = StringUtils.countMatches(sql, "?");
                    for (int i = 0; i < paraCount; ++i) {
                        callableStatement.registerOutParameter(i + 1, OracleTypes.VARCHAR);
                    }
                    callableStatement.execute();

                    OrderedJSONObject jsonObject = new OrderedJSONObject();
                    for (int i = 0; i < paraCount; ++i) {
                        if (queryService.isRunStop(queryid)) {
                            HelperUtil.queryIDObjectMultiMap.remove(queryid);
                            throw new InterruptedException();
                        }
                        jsonObject.put("OUT" + (i + 1), String.valueOf(callableStatement.getObject(i + 1)));
                    }
                    jsonArray.put(jsonObject);
                } else {

                    stmt = connection.createStatement();
                    String startTime = Util.getCurrentTime();
                    JSONObject jsonObject = QueryService.sessionMap.get(queryid);
                    if (jsonObject != null) {
                        jsonObject.put("con", stmt);
                        jsonObject.put("query", sql);
                        jsonObject.put("startTime", startTime);
                        QueryService.sessionMap.put(queryid, jsonObject);
                    }

                    rs = stmt.executeQuery(sql);

                    ResultSetMetaData meta = rs.getMetaData();

                    if (isColumnType) {
                        JSONObject jsonObjectInfo = new JSONObject();
                        for (int i = 0; i < meta.getColumnCount(); i++) {
                            if (queryService.isRunStop(queryid)) {
                                HelperUtil.queryIDObjectMultiMap.remove(queryid);
                                throw new InterruptedException();
                            }
                            jsonObjectInfo.put(meta.getColumnLabel(i + 1), meta.getColumnTypeName(i + 1));
                        }
                        jsonArray.put(0, jsonObjectInfo);
                    }

                    JSONObject dataTypeObject = HelperUtil.queryIDColumnDataTypeMap.get(queryid);
                    if (dataTypeObject == null) {
                        dataTypeObject = new JSONObject();
                    }

                    int counter = 0;
                    while (rs.next()) {
                        if (queryService.isRunStop(queryid)) {
                            HelperUtil.queryIDObjectMultiMap.remove(queryid);
                            throw new InterruptedException();
                        }
                        if (liquidLimit > -1 && counter >= liquidLimit) {
                            return "Error : {Too many records for retrieved, limit is {" + liquidLimit
                                    + "} and retrieved-records are greater than liquid limit}";
                        }

                        OrderedJSONObject orderedJSONObject = new OrderedJSONObject();
                        for (int i = 0; i < meta.getColumnCount(); i++) {
                            String value = rs.getString(i + 1);
                            String columnName = meta.getColumnName(i + 1);
                            String columnNameLabel = meta.getColumnLabel(i + 1);
                            dataTypeObject.put(isLabel ? columnNameLabel : columnName, meta.getColumnTypeName(i + 1).toLowerCase());
                            if (value == null) {
                                orderedJSONObject.put((isLabel ? columnNameLabel : columnName), "null");
                            } else if (isNumericType(meta.getColumnTypeName(i + 1))) {
                                orderedJSONObject.put((isLabel ? columnNameLabel : columnName), new BigDecimal(value));
                            } else {
                                orderedJSONObject.put((isLabel ? columnNameLabel : columnName), value);
                            }
                        }
                        jsonArray.put(orderedJSONObject);
                        counter++;
                    }
                    HelperUtil.queryIDColumnDataTypeMap.put(queryid, dataTypeObject);
                }
            }
            return jsonArray.toString();
        } catch (SQLException | ClassNotFoundException throwables) {
            queryService.addLogging("Query Select", "SQL Query is\t" + queryid + "\n");
            return "Error : {" + throwables.toString() + "}";
        } catch (InterruptedException e) {
            queryService.addLogging("Query Select", "SQL Query is\t" + queryid + " interrupted by user request\n");
            return "Error : { query  " + queryid + " killed by user request }";
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e2) {
                }
            }
            if (callableStatement != null) {
                try {
                    callableStatement.close();
                } catch (Exception e2) {
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e2) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e1) {
                }
            }
        }
    }

    @Override
    public String executeQuerySelectJDBCLimits(co.kr.coresolutions.model.Connection infoConnection, String connectionID, String sql, String queryid, boolean isLabel, int limits) {
        Connection connection = null;
        JSONArray jsonArray = new JSONArray();
        CallableStatement callableStatement = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            connection = makeConnection(connectionID);
            if (connection != null) {
                String loSql = sql.toLowerCase();
                if (infoConnection.getDRIVER().contains("oracle") && (loSql.startsWith("call ")
                        || loSql.startsWith("exec ") || loSql.startsWith("execute "))) {
                    callableStatement = connection.prepareCall(sql);
                    int paraCount = StringUtils.countMatches(sql, "?");
                    for (int i = 0; i < paraCount; ++i) {
                        callableStatement.registerOutParameter(i + 1, OracleTypes.VARCHAR);
                    }
                    callableStatement.execute();

                    OrderedJSONObject jsonObject = new OrderedJSONObject();
                    for (int i = 0; i < paraCount; ++i) {
                        if (queryService.isRunStop(queryid)) {
                            HelperUtil.queryIDObjectMultiMap.remove(queryid);
                            throw new InterruptedException();
                        }
                        jsonObject.put("OUT" + (i + 1), String.valueOf(callableStatement.getObject(i + 1)));
                    }
                    jsonArray.put(jsonObject);
                } else {
                    stmt = connection.createStatement();
                    String startTime = Util.getCurrentTime();
                    JSONObject jsonObject = QueryService.sessionMap.get(queryid);
                    if (jsonObject != null) {
                        jsonObject.put("con", stmt);
                        jsonObject.put("query", sql);
                        jsonObject.put("startTime", startTime);
                        QueryService.sessionMap.put(queryid, jsonObject);
                    }

                    rs = stmt.executeQuery(sql);

                    ResultSetMetaData meta = rs.getMetaData();

                    while (rs.next() && jsonArray.length() < limits) {
                        if (queryService.isRunStop(queryid)) {
                            HelperUtil.queryIDObjectMultiMap.remove(queryid);
                            throw new InterruptedException();
                        }
                        OrderedJSONObject orderedJSONObject = new OrderedJSONObject();
                        for (int i = 0; i < meta.getColumnCount(); i++) {
                            String value = rs.getString(i + 1);
                            if (value == null) {
                                orderedJSONObject.put((isLabel ? meta.getColumnLabel(i + 1) : meta.getColumnName(i + 1)), "null");
                            } else if (isNumericType(meta.getColumnTypeName(i + 1))) {
                                orderedJSONObject.put((isLabel ? meta.getColumnLabel(i + 1) : meta.getColumnName(i + 1)), new BigDecimal(value));
                            } else {
                                orderedJSONObject.put((isLabel ? meta.getColumnLabel(i + 1) : meta.getColumnName(i + 1)), value);
                            }
                        }
                        jsonArray.put(orderedJSONObject);
                    }
                }
            }
            return jsonArray.toString();
        } catch (SQLException | ClassNotFoundException throwables) {
            queryService.addLogging("Query Select", "SQL Query is\t" + queryid + "\n");
            return "Error : {" + throwables.toString() + "}";
        } catch (InterruptedException e) {
            queryService.addLogging("Query Select", "SQL Query is\t" + queryid + " interrupted by user request\n");
            return "Error : { query  " + queryid + " killed by user request }";
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e2) {
                }
            }
            if (callableStatement != null) {
                try {
                    callableStatement.close();
                } catch (Exception e2) {
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e2) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e1) {
                }
            }
        }
    }

    private boolean isNumericType(String columnTypeName) {
        return QueryService.numList.contains(columnTypeName.toLowerCase());
    }


    @Override
    public String unloadQuerySelect(co.kr.coresolutions.model.Connection infoConnection, String connectionID, String unLoadID, String sql,
                                    int maxRowsForSingleCommit) {
        JSONArray jsonArray = new JSONArray();
        try (Connection connection = makeConnection(connectionID); Statement stmt = connection.createStatement(); ResultSet rs = stmt.executeQuery(sql)) {
            ResultSetMetaData meta = rs.getMetaData();
            AtomicLong atomicLong = new AtomicLong(0);
            AtomicBoolean atomicBoolean = new AtomicBoolean(false);
            Map<String, List<Object>> metadata = new LinkedHashMap<>();

            while (rs.next()) {
                //skip result set and exit
                if (!unLoadID.isEmpty() && !QueryLoadFacade.sessionUnload.containsKey(unLoadID)) {
                    return "Error : " + QueryLoadFacade.sessionUnloadError.get(unLoadID);
                }
                OrderedJSONObject jsonObject = new OrderedJSONObject();
                for (int i = 0; i < meta.getColumnCount(); i++) {
                    if (atomicLong.get() == 0) {
                        metadata.put(meta.getColumnLabel(i + 1), Lists.newArrayList(meta.getColumnTypeName(i + 1), meta.getColumnDisplaySize(i + 1)));
                    }
                    String value = rs.getString(i + 1);
                    if (value == null) {
                        jsonObject.put(meta.getColumnLabel(i + 1), "null");
                    } else if (isNumericType(meta.getColumnTypeName(i + 1))) {
                        jsonObject.put(meta.getColumnLabel(i + 1), new BigDecimal(value));
                    } else {
                        jsonObject.put(meta.getColumnLabel(i + 1), value);
                    }
                }
                jsonArray.put(jsonObject);

                if (atomicLong.getAndIncrement() > 0 && (atomicLong.get() % (maxRowsForSingleCommit <= 0 ? DataBase.MAX_FETCH_SIZE : maxRowsForSingleCommit) == 0)) {
                    if (!unLoadID.isEmpty() && !QueryLoadFacade.sessionUnload.containsKey(unLoadID)) {
                        return "Error : " + QueryLoadFacade.sessionUnloadError.get(unLoadID);
                    }
                    atomicLong.set(0);
                    queryChunkPublisher.update(Chunk.builder().metadata(metadata).content(jsonArray.toString())
                            .sameOutput(!atomicBoolean.get() ? atomicBoolean.getAndSet(true) : atomicBoolean.get()).build(), null);
                    jsonArray = new JSONArray();
                }
            }
            if (!jsonArray.isEmpty()) {
                if (!unLoadID.isEmpty() && !QueryLoadFacade.sessionUnload.containsKey(unLoadID)) {
                    return "Error : " + QueryLoadFacade.sessionUnloadError.get(unLoadID);
                }
                queryChunkPublisher.update(Chunk.builder().metadata(metadata).content(jsonArray.toString())
                        .sameOutput(!atomicBoolean.get() ? atomicBoolean.getAndSet(true) : atomicBoolean.get()).build(), null);
            }
            return "success";
        } catch (Exception e) {
            queryService.addLogging("Query Select ", "SQL Query is\t" + sql + "\n");
            return "Error : {" + e.toString() + "}";
        }
    }

    @Override
    public Connection makeConnection(String connectionId) throws SQLException, ClassNotFoundException {
        co.kr.coresolutions.model.Connection connection = queryService.getInfoConnection(connectionId);
        Class.forName(connection.getDRIVER());
        String url = connection.getURL();
        if (connection.getDRIVER().equalsIgnoreCase("org.sqlite.JDBC") && url.trim().isEmpty()) {
            url = "jdbc:sqlite:" + constants.getSQLiteQueryDir() + QueryServiceSQLite.SQLITE_USERID + File.separator + QueryServiceSQLite.DBFILE;
        }
        return DriverManager.getConnection(url, connection.getID(), connection.getPW());
    }
}

