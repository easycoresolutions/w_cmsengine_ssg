package co.kr.coresolutions.commons;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Monitor {
    @JsonProperty
    private String api;
    private Monitor.ID ID;

    @Data
    @Builder(builderMethodName = "IDBuilder")
    @NoArgsConstructor
    public static class ID implements Serializable {
        @JsonProperty("queryid")
        String queryID;
        @JsonProperty(value = "starttime")
        String stDT;
        String owner;

        @JsonCreator
        public ID(@JsonProperty String queryID, @JsonProperty String stDT, @JsonProperty String owner) {
            this.queryID = queryID;
            this.stDT = HelperUtil.DT_FORMAT_YYYYMMDDHHMMSS.format(new Date());
            this.owner = owner;
        }
    }
}
