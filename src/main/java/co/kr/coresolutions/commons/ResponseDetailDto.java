package co.kr.coresolutions.commons;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseDetailDto {
    private String field;
    private String code;
    private String message;
    private List<Object> arguments;

    public ResponseDetailDto(String field, String code, String message) {
        this.field = field;
        this.code = code;
        this.message = message;
    }
}
