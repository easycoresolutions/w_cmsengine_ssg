package co.kr.coresolutions.commons;

import java.util.Observer;

public interface QueryObservable extends Observer {
    void attach(Observer observer, Object... args);

    void detach(Observer observer);
}