package co.kr.coresolutions.commons;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

public class ConditionalOneExistValidator implements ConstraintValidator<ConditionalOneExist, Object> {
    private ConditionalOneExist conditional;

    @Override
    public void initialize(ConditionalOneExist conditional) {
        this.conditional = conditional;
    }


    @Override
    public boolean isValid(Object objectSource, ConstraintValidatorContext context) {

        AtomicBoolean valid = new AtomicBoolean(true);

        BeanWrapperImpl wrapper = new BeanWrapperImpl(objectSource);

        String objectFirst = (String) Optional.ofNullable(wrapper.getPropertyValue(conditional.firstSelected().trim())).orElse("");
        String objectSecond = (String) Optional.ofNullable(wrapper.getPropertyValue(conditional.secondSelected().trim())).orElse("");

        if ((objectFirst.isEmpty() && objectSecond.isEmpty()) || (!objectFirst.isEmpty() && !objectSecond.isEmpty())) {
            valid.set(false);
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(conditional.message()).addConstraintViolation();
        }

        return valid.get();
    }

}
