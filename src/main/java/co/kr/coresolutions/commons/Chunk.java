package co.kr.coresolutions.commons;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Observable;

@Getter
@RequiredArgsConstructor
@Builder
public class Chunk extends Observable {
    final String content;
    final boolean sameOutput;
    final Map<String, List<Object>> metadata;
}
