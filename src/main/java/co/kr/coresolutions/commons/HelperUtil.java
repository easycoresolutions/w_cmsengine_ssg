package co.kr.coresolutions.commons;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public interface HelperUtil {

    String FORMAT_UTF_8 = "UTF-8";

    String formatUSASCII = "US-ASCII";

    String dataFlowLogDateFormat = "yyyyMMdd:HH:mm:ss";
    //should be concurrent, as it's fail-safe iterator
    Map<Object, Object> queryIDObjectMultiMap = new ConcurrentHashMap<>();
    Map<String, JSONObject> queryIDColumnDataTypeMap = new ConcurrentHashMap<>();
    SimpleDateFormat DT_FORMAT_YYYYMMDDHHMMSS = new SimpleDateFormat("yyyyMMddHHmmss");
}
