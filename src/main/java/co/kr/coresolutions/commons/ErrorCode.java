package co.kr.coresolutions.commons;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum ErrorCode {
    ZERO("0"), ONE("1"), TWO("2"), THREE("3"), FOUR("4"), FIVE("5"), SIX("6");
    String errorCode;

    public int getCode() {
        return Integer.parseInt(errorCode);
    }

}
