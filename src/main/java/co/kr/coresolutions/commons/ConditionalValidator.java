package co.kr.coresolutions.commons;

import co.kr.coresolutions.enums.EmailType;
import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

public class ConditionalValidator implements ConstraintValidator<Conditional, Object> {
    private Conditional conditional;

    @Override
    public void initialize(Conditional conditional) {
        this.conditional = conditional;
    }


    @Override
    public boolean isValid(Object objectSource, ConstraintValidatorContext context) {

        AtomicBoolean valid = new AtomicBoolean(true);

        BeanWrapperImpl wrapper = new BeanWrapperImpl(objectSource);

        String objectValue = (String) Optional.ofNullable(wrapper.getPropertyValue(conditional.selected())).orElse("");

        if (!(objectValue.equalsIgnoreCase(EmailType.html.getName()) || objectValue.equalsIgnoreCase(EmailType.text.getName()))) {
            valid.set(false);
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(conditional.message())
                    .addConstraintViolation();
        }

        return valid.get();
    }

}
