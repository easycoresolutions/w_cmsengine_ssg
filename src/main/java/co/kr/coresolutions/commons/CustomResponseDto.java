package co.kr.coresolutions.commons;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class CustomResponseDto {
    private String ServiceConnectionKey;
    @Builder.Default
    private Boolean Success = false;
    private String plain_pw;
    private String records;
    private Integer ErrorCode;
    private String ErrorMessage;
    @JsonProperty(value = "error_id")
    private String errorID;
    private List<String> InternalAuths;
    private int InterAccessLevel;
    private String DetailedMessage;
    private String TransactionId;
    private String format;
    private String nodes;
    @JsonProperty(value = "DP_OUT_SEQ")
    private Set<Integer> dpOutSeq;
    private String message;
    private Integer count;

    public static CustomResponseDto ok() {
        return CustomResponseDto.builder().Success(true).build();
    }
}
