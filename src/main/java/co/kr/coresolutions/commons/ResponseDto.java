package co.kr.coresolutions.commons;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ResponseDto {

    public static final ResponseDto NOT_EXISTS = ResponseDto.builder()
            .errorCode(HttpStatus.NOT_FOUND.value())
            .message("target not found.")
            .build();
    public static final ResponseDto ERROR_PARSE = ResponseDto.builder()
            .errorCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
            .message("bad result returned, can't parse properly")
            .build();

    @Builder.Default
    private int errorCode = 0;
    @Builder.Default
    private int successCode = 0;
    private int statusCode;
    private String message;
    private List<ResponseDetailDto> details;
    @Builder.Default
    private String endDateExecution = LocalDateTime.now().format(DateTimeFormatter.ofPattern(HelperUtil.dataFlowLogDateFormat));

    public static ResponseDto ok() {
        return ResponseDto.builder().message("Ok").successCode(ResponseCodes.OK_RESPONSE).build();
    }

    public static ResponseDto NotValidRequest() {
        return ResponseDto.builder().message("Not Valid Request Input").errorCode(HttpStatus.BAD_REQUEST.value()).build();
    }
}
