package co.kr.coresolutions.commons;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

public class ConditionalValidatorPassword implements ConstraintValidator<ConditionalPassword, Object> {
    private ConditionalPassword conditional;

    @Override
    public void initialize(ConditionalPassword conditional) {
        this.conditional = conditional;
    }


    @Override
    public boolean isValid(Object objectSource, ConstraintValidatorContext context) {

        AtomicBoolean valid = new AtomicBoolean(true);
        BeanWrapperImpl wrapper = new BeanWrapperImpl(objectSource);
        String objectValueSelected = (String) Optional.ofNullable(wrapper.getPropertyValue(conditional.selected())).orElse("");
        String objectValueTarget = (String) Optional.ofNullable(wrapper.getPropertyValue(conditional.target())).orElse("");

        if (objectValueSelected.equalsIgnoreCase(objectValueTarget)) {
            valid.set(false);
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(conditional.message())
                    .addConstraintViolation();
        }

        return valid.get();
    }

}
