package co.kr.coresolutions.caches;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

@Component
public class QueryRabbitMQCache {

//    @Cacheable(value = "cacheAMQPConnection", key = "#url", cacheManager = "cacheManagerCaffeine")
    public Connection getConnection(String url) throws NoSuchAlgorithmException, KeyManagementException, URISyntaxException, IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(url);
        return factory.newConnection();
    }
}
