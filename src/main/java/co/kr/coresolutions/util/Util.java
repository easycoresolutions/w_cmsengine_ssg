package co.kr.coresolutions.util;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;

public class Util {
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");

    public static String getBaseUrl(HttpServletRequest request) {
        String scheme = request.getScheme();
        int port = request.getServerPort();
        return scheme + "://" + request.getServerName()
                + ((("http".equals(scheme) && port == 80) || ("https".equals(scheme) && port == 443)) ? "" : ":" + port) + request.getContextPath() + "/";
    }

    private static long uniqueId = 0;

    /**
     * 주어진 날짜의 iNextDay  일 후의 날짜를 리턴한다.<BR>
     * strCurDate : 20021215
     * iNextDay : 3
     * -> return : 20021218
     * 윤년 계산됨
     */
    public static String afterNDay(String strCurDate, int iNextDay) {

        // 현재 날짜의 Calendar 객체를 가져온다.
        Calendar calCurDate = Util.convertStringToCal(strCurDate);

        // N일을 더한다.
        int iDate = calCurDate.get(Calendar.DATE) + iNextDay;

        //일을 다시 설정한다.
        calCurDate.set(Calendar.DATE, iDate);
        String strResult = Util.convertCalToString(calCurDate);

        if (strCurDate.length() == 8) {
            strResult = strResult.substring(0, 8);
        }

        return strResult;
    }

    public static String nowDateTime() {
        Calendar cal = Calendar.getInstance();
        return String.format("%04d%02d%02d%02d%02d%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
    }

    public static String nowDate() {
        Calendar cal = Calendar.getInstance();
        return String.format("%04d%02d%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DATE));
    }

    public static String nowTime() {
        Calendar cal = Calendar.getInstance();
        return String.format("%02d%02d%02d", cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
    }

    /**
     * 주어진 날짜의 iNextMonth  일 후의 날짜를 리턴한다.<BR>
     * strCurDate : 20020915
     * iNextMonth : 3
     * -> return : 20021215
     * 윤년 계산됨
     */
    public static String afterNMonth(String strCurDate, int iNextMonth) {
        // 현재 날짜의 Calendar 객체를 가져온다.
        Calendar calCurDate = Util.convertStringToCal(strCurDate);

        // N개월을 더한다.
        int iMonth = calCurDate.get(Calendar.MONTH) + iNextMonth;

        // 달을 설정한다.
        calCurDate.set(Calendar.MONTH, iMonth);
        String strResult = Util.convertCalToString(calCurDate);

        if (strCurDate.length() == 8) {
            strResult = strResult.substring(0, 8);
        }

        return strResult;
    }

    /**
     * 주어진 날짜의 3개월 후의 날짜를 리턴한다.<BR>
     * "20011031" -> "20020131"<BR>
     * "20011031103040" -> "20020131103040"<BR>
     * 윤년 계산됨
     *
     * @param strCurDate 날짜
     * @return 3개월 후의 날짜
     */
    public static String beforeNDay(String strCurDate, int iNextDay) {

        // 현재 날짜의 Calendar 객체를 가져온다.
        Calendar calCurDate = Util.convertStringToCal(strCurDate);

        // N일을  뺀다.
        int iDate = calCurDate.get(Calendar.DATE) - iNextDay;

        //일을  다시 설정한다.
        calCurDate.set(Calendar.DATE, iDate);

        String strResult = Util.convertCalToString(calCurDate);

        if (strCurDate.length() == 8) {
            strResult = strResult.substring(0, 8);
        }

        return strResult;
    }

    /**
     * 주어진 날짜의 3개월 후의 날짜를 리턴한다.<BR>
     * "20011031" -> "20020131"<BR>
     * "20011031103040" -> "20020131103040"<BR>
     * 윤년 계산됨
     *
     * @param strCurDate 날짜
     * @return 3개월 후의 날짜
     */
    public static String beforeNMonth(String strCurDate, int iNextMonth) {

        // 현재 날짜의 Calendar 객체를 가져온다.
        Calendar calCurDate = Util.convertStringToCal(strCurDate);

        // N개월을 뺀다
        int iMonth = calCurDate.get(Calendar.MONTH) - iNextMonth;

        // 달을  다시 설정한다.
        calCurDate.set(Calendar.MONTH, iMonth);

        String strResult = Util.convertCalToString(calCurDate);
        if (strCurDate.length() == 8) {
            strResult = strResult.substring(0, 8);
        }

        return strResult;
    }

    /**
     * 주어진 날짜의 3개월 후의 날짜를 리턴한다.<BR>
     * "20011031" -> "20020131"<BR>
     * "20011031103040" -> "20020131103040"<BR>
     * 윤년 계산됨
     *
     * @param strCurDate 날짜
     * @return 3개월 후의 날짜
     */
    public static String beforeNYear(String strCurDate, int iNextYear) {

        // 현재 날짜의 Calendar 객체를 가져온다.
        Calendar calCurDate = Util.convertStringToCal(strCurDate);
        // N년을 뺀다
        int iYear = calCurDate.get(Calendar.YEAR) - iNextYear;

        //년을 다시 설정한다.
        calCurDate.set(Calendar.DATE, iYear);

        String strResult = Util.convertCalToString(calCurDate);

        if (strCurDate.length() == 8) {
            strResult = strResult.substring(0, 8);
        }

        return strResult;
    }

    /**
     * 날짜를 데이터베이스에 입력하기위한 스트링으로 변환하는 함수(년도월일시분초까지)<BR>
     * 날짜가 null일 경우 공백문자열("")을 리턴시킴
     *
     * @param calDay Calendar 변환 시킬 날짜
     * @return 변환된 문자열
     */
    public static String convertCalToString(Calendar calDay) {

        if (calDay == null) {
            return "";
        }

        StringBuffer sbufferDay = new StringBuffer();
        // 날짜를 가져옴
        String strYear = Util.fixTextSize(Integer.toString(calDay.get(Calendar.YEAR)), '0', 4);
        String strMonth = Util.fixTextSize(Integer.toString(calDay.get(Calendar.MONTH) + 1), '0', 2);
        String strDay = Util.fixTextSize(Integer.toString(calDay.get(Calendar.DAY_OF_MONTH)), '0', 2);

        sbufferDay.append(strYear);
        sbufferDay.append(strMonth);
        sbufferDay.append(strDay);

        // 시간을 가져옴
        String strHour = Util.fixTextSize(Integer.toString(calDay.get(Calendar.HOUR_OF_DAY)), '0', 2);
        String strMin = Util.fixTextSize(Integer.toString(calDay.get(Calendar.MINUTE)), '0', 2);
        String strSecond = Util.fixTextSize(Integer.toString(calDay.get(Calendar.SECOND)), '0', 2);

        sbufferDay.append(strHour);
        sbufferDay.append(strMin);
        sbufferDay.append(strSecond);

        return sbufferDay.toString();
    }

    /**
     * 문자열을 날짜로 변환<BR>
     * 길이가 맞지 않거나 null일때 오늘을 리턴함<BR>
     *
     * @param strDay String 변환 시킬 문자열
     * @return 변환된 문자열
     */
    public static Calendar convertStringToCal(String strDay) {
        if (strDay == null || !(strDay.length() == 8 || strDay.length() == 14)) {
            return new GregorianCalendar();
        }

        // 날짜를 가져온다.
        int iYear = Integer.parseInt(strDay.substring(0, 4));
        int iMonth = Integer.parseInt(strDay.substring(4, 6)) - 1;  // 달은 0부터 시작 한다.
        int iDay = Integer.parseInt(strDay.substring(6, 8));

        // 시간을 가져온다.
        int iHour = 0;
        int iMin = 0;
        int iSecond = 0;
        if (strDay.length() == 14) {
            iHour = Integer.parseInt(strDay.substring(8, 10));
            iMin = Integer.parseInt(strDay.substring(10, 12));
            iSecond = Integer.parseInt(strDay.substring(12, 14));
        }

        // Calendar객체를 생성하고 날짜와 시간을 설정한다.
        Calendar calDay = new GregorianCalendar();
        calDay.set(iYear, iMonth, iDay, iHour, iMin, iSecond);

        return calDay;
    }

    /**
     * 문자열을 특정 크기로 만듬, 만약 남는 공간이 있으면 왼쪽에서부터 특정문자(cSpace)를 채움<BR>
     * null이 입력되더라도 크기 만큼 특정문자를 채움
     *
     * @param strText    String 문자열
     * @param cSpace     char 빈공란에 채울 특정문자
     * @param iTotalSize int 특정 크기
     * @return 변경된 문자열
     */
    public static String fixTextSize(String strText, char cSpace, int iTotalSize) {

        if (strText == null) {
            strText = "";
        }

        if (strText.length() < iTotalSize) {

            // 문자열의 크기가 특정크기보다 작을 때는 특정문자로 채움
            char[] carraySpace = new char[iTotalSize - strText.length()];
            Arrays.fill(carraySpace, cSpace);

            String strSpace = new String(carraySpace);

            return strSpace + strText;

        } else {

            // 문자열의 크기가 특정크기보다 클때는 앞쪽의 문자열 잘라냄
            return strText.substring(strText.length() - iTotalSize);

        }

    }

    /**
     * 시간 문자열을 날짜만을 가져와서 웹상에서 표시할 형식으로 변환<BR>
     * 길이가 맞지 않거나 null일때 원래 스트링을 리턴함<BR>
     * Ex) "20011014123020" --> "2001.10.14"
     *
     * @param strDate 시간 문자열
     * @return 날짜만의 문자열
     */
    public static String getCalendar2String(java.util.Calendar cal) {

        int iYear = cal.get(java.util.Calendar.YEAR);
        int iMonth = cal.get(java.util.Calendar.MONTH) + 1;
        int iDay = cal.get(java.util.Calendar.DATE);


        return iYear + fixTextSize(String.valueOf(iMonth), '0', 2) + fixTextSize(String.valueOf(iDay), '0', 2);

    }

    /**
     * SQL 의 from 절에서 고객 TABLE이라고 할 수 있는 것들을 찾아온다.
     * 작성 날짜: (2002-08-08 오후 4:07:15)
     *
     * @param strFrom java.lang.String
     * @return java.lang.String
     */
    public static String getCustomerTable(String strFrom) {

        if (strFrom == null) return "";
        strFrom = strFrom.toUpperCase();


        if (strFrom.equals("TS_CUSTOMER")) {//고객 TABLE
            return "TS_CUSTOMER";
        } else if (strFrom.equals("TS_OPPORTUNITY")) {//기회 MASTER TABLE
            return "TS_OPPORTUNITY";
        } else if (strFrom.equals("TS_NUMERIC")) {//NUMERIC MASTER TABLE
            return "TS_NUMERIC";
        } else if (strFrom.equals("TS_CUSTOMER_JU")) {//고객 TABLE
            return "TS_CUSTOMER_JU";
        } else {

            StringTokenizer token = new StringTokenizer(strFrom, ",");
            String strOneToken = null;

            while (token.hasMoreTokens()) {
                strOneToken = token.nextToken();
                strOneToken = strOneToken.toUpperCase().trim();
                if (strOneToken.equals("TS_CUSTOMER")) {//고객 TABLE
                    return "TS_CUSTOMER";
                } else if (strOneToken.equals("TS_OPPORTUNITY")) {//기회 MASTER TABLE
                    return "TS_OPPORTUNITY";
                } else if (strOneToken.equals("TS_NUMERIC")) {//NUMERIC MASTER TABLE
                    return "TS_NUMERIC";
                } else if (strOneToken.equals("TS_CUSTOMER_JU")) {//고객 TABLE
                    return "TS_CUSTOMER_JU";
                }
            }

            return "";

        }
    }

    /**
     * 현재 시간을 가져오는 함수
     *
     * @return hh:dd:ss 형식
     */
    public static int getDayOfMonth(String strDate) {

        java.util.Calendar now = java.util.Calendar.getInstance();

        int iYear = Integer.parseInt(strDate.substring(0, 4));
        int iMonth = Integer.parseInt(strDate.substring(4, 6));
        int iDay = Integer.parseInt(strDate.substring(6, 8));

        now.set(iYear, iMonth - 1, iDay);

        return now.get(Calendar.DAY_OF_MONTH) + 1;


    }

    /**
     * 파라미터의 일자의 요일을 가져온다
     *
     * @param yyyymmdd형식
     * @return int
     */
    public static int getDayOfWeek(String strDate) {

        java.util.Calendar now = java.util.Calendar.getInstance();
        int iYear = Integer.parseInt(strDate.substring(0, 4));
        int iMonth = Integer.parseInt(strDate.substring(4, 6));
        int iDay = Integer.parseInt(strDate.substring(6, 8));

        now.set(iYear, iMonth - 1, iDay);

        return now.get(Calendar.DAY_OF_WEEK);

    }

    /**
     * 작성 날짜: (2003-04-01 오후 5:48:02)
     *
     * @param strFileName  java.lang.String
     * @param strFieldName java.lang.String
     * @return java.lang.String
     */
    public static String getDefField(String strFileName, String strFieldName) {

        try {
            RandomAccessFile raf = new RandomAccessFile(strFileName, "r");
            String strLine = null;
            int iIndex = -1;

            String strPre = null;
            String strNext = null;

            while (true) {
                strLine = raf.readLine();
                if (strLine == null) break;
                iIndex = strLine.indexOf("=");
                if (iIndex > -1) {
                    strPre = strLine.substring(0, iIndex);
                    strNext = strLine.substring(iIndex + 1);
                    if (strPre.trim().equals(strFieldName.trim())) strNext = strNext.trim();
                }
            }

            if (raf != null) raf.close();
            return strNext;

        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * 20020903 -> 2002-09-03 형태의 format 으로 바꾸어 돌려줌
     * 작성 날짜: (2002-09-05 오후 2:07:54)
     *
     * @param strDate java.lang.String
     * @return java.lang.String
     */
    public static String getFormatDate(String strDate) {

        strDate = strDate.trim();
        if (strDate.equals("")) return "";
        return strDate.substring(0, 4) + "-" + strDate.substring(4, 6) + "-" + strDate.substring(6, 8);

    }

    /**
     * 시간 문자열을 날짜만을 가져와서 웹상에서 표시할 형식으로 변환<BR>
     * 길이가 맞지 않거나 null일때 원래 스트링을 리턴함<BR>
     * Ex) "20011014123020" --> "2001.10.14"
     *
     * @param strDate 시간 문자열
     * @return 날짜만의 문자열
     */
    public static String getFormattedDate(String strDate) {
        if (strDate == null || !(strDate.length() == 8 || strDate.length() == 14)) {
            return strDate;
        }

        // 날짜를 가져온다.
        StringBuffer sbufferFormattedDay = new StringBuffer();

        sbufferFormattedDay.append(strDate, 0, 4);
        sbufferFormattedDay.append(".");

        String strMonth = strDate.substring(4, 6);
        sbufferFormattedDay.append(strMonth);
        sbufferFormattedDay.append(".");

        String strDay = strDate.substring(6, 8);
        sbufferFormattedDay.append(strDay);

        return sbufferFormattedDay.toString();
    }

    /**
     * 현재 시간을 가져오는 함수
     *
     * @return hh:dd:ss 형식
     */
    public static String getFormattedThisDate() {
        java.util.Calendar now = java.util.Calendar.getInstance();
        int iYear = now.get(java.util.Calendar.YEAR);
        int iMonth = now.get(java.util.Calendar.MONTH) + 1;
        int iDay = now.get(java.util.Calendar.DATE);

        return iYear + fixTextSize(String.valueOf(iMonth), '0', 2) + fixTextSize(String.valueOf(iDay), '0', 2);

    }

    /**
     * 현재 시간을 가져오는 함수
     *
     * @return hhddss 형식
     */
    public static String getFormattedThisTime() {
        java.util.Calendar now = java.util.Calendar.getInstance();

        int iHour = now.get(java.util.Calendar.HOUR_OF_DAY);
        int iMin = now.get(java.util.Calendar.MINUTE);
        int iSec = now.get(java.util.Calendar.SECOND);

        return fixTextSize(String.valueOf(iHour), '0', 2) + fixTextSize(String.valueOf(iMin), '0', 2) + fixTextSize(String.valueOf(iSec), '0', 2);

    }

    public static String HexToString(byte[] TempByte) {
        String ReturnString = "";
        for (int i = 0; i < TempByte.length; i++) {
            if ((TempByte[i] > 15) || (TempByte[i] < 0)) {
                ReturnString += java.lang.Integer.toHexString(TempByte[i] & 0xff).toUpperCase();
            } else {
                ReturnString += "0" + java.lang.Integer.toHexString(TempByte[i] & 0xff).toUpperCase();
            }
        }
        return ReturnString;
    }

    /**
     * String 이 null 일경우 바꿀 대치할 문자열
     *
     * @param String str 검사할 String
     * @param String setstr str 이 null 일 경우 리턴할 문자열
     * @return 리턴 String
     */
    public static String isNullAndSet(String str, String setstr) {
        if (str == null)
            return setstr;

        return str;
    }

    /**
     * 주어진 문자열이 숫자인지 검사
     *
     * @param String 검사할 문자열
     * @return true 숫자, false 숫자 아님..
     */
    public static boolean isNumber(String str) {

        // 나머지 글자가 알파벳인지 숫자인지를 알아본다.
        for (int iIndex = 0; iIndex < str.length(); iIndex++) {
            if (!Util.isNumeric(str.charAt(iIndex))) {
                return false;        // 숫자 아님..
            }
        }
        return true;    // 숫자임.
    }

    /**
     * 문자 하나가 숫자인지를 검사
     *
     * @param cLetter char 검사할 문자
     * @return 숫자인지 여부 (숫자이면: true, 숫자가 아니면: false)
     */
    protected static boolean isNumeric(char cLetter) {

        return '0' <= cLetter && cLetter <= '9';
    }

    public static boolean isNumericString(String value, boolean integer, boolean natural) {
        boolean bDot = false;
        value = value.trim();
        if (value.length() == 0) {
            return false;
        }
        if (value.length() == 1 && (value.charAt(0) == '.' || value.charAt(0) == '-')) {
            return false;
        }
        if (value.length() > 1 && value.charAt(0) == '0' && value.charAt(1) != '.' || value.charAt(value.length() - 1) == '.' || value.charAt(value.length() - 1) == ',') {
            return false;
        }
        for (int i = 0; i < value.length(); ++i) {
            if (value.charAt(i) < 48 || value.charAt(i) > 57) {
                if (natural) {
                    return false;
                }
                if (!integer && value.charAt(i) == ',') {
                    continue;
                }
                if (!integer && value.charAt(i) == '.' && !bDot) {
                    bDot = true;
                    continue;
                }
                if (value.charAt(i) == '-' && i == 0) {
                    continue;
                }
                return false;
            }
        }
        return true;
    }

    /**
     * 적합한 날짜인지를 검사한다.
     *
     * @param iYear  년도
     * @param iMonth 월
     * @param iDay   일
     * @return 적합한 날짜 인지 여부
     */
    public static boolean isValidDate(int iYear, int iMonth, int iDay) {
        if (iMonth >= 1 && iMonth <= 12) {
            if (iMonth == 2) {
                // 2월달의 일을 검사
                if (iYear % 4 == 0 && (iYear % 100 != 0 || iYear % 400 == 0)) {
                    return iDay >= 1 && iDay <= 29;
                } else {
                    return iDay >= 1 && iDay <= 28;
                }
            } else if (iMonth % 2 == 0) {
                // 짝수인 달의 일을 검사
                return iDay >= 1 && iDay <= 31;
            } else {
                // 홀수인 달의 일을 검사
                return iDay >= 1 && iDay <= 30;
            }
        }

        return false;
    }

    /**
     * 문자열이 null이면 주어진 문자열로 치환 시킴
     *
     * @param strTarget  문자열
     * @param strReplace 치환될 문자열
     * @return 문자열
     */
    public static String replaceIfNull(String strTarget, String strReplace) {
        if (strTarget == null) {
            return strReplace;
        } else {
            return strTarget;
        }
    }

    /**
     * 작성 날짜: (2002-08-20 오후 3:24:38)
     *
     * @param strSource  java.lang.String
     * @param strElement java.lang.String
     * @return java.lang.String
     */
    public static String replaceString(String strSource, String strElement, String strAdd) {

        if (strElement == null) strElement = "";

        int iStart = strSource.indexOf(strElement);
        int iEnd = strElement.length();
        StringBuffer strBufferReplace = new StringBuffer(strSource);

        if (iStart != -1 && iEnd != 0) {
            strBufferReplace.delete(iStart, iStart + iEnd);
            strBufferReplace.insert(iStart, strAdd);
        }
        return strBufferReplace.toString();
    }

    /**
     * 문자열의 특정 문자열을 다른 문자열로 대체 시킴
     *
     * @param strText        String 문자열
     * @param strWord        String 특정문자열
     * @param strReplaceWord String 대체될 문자열
     * @return 변경된 문자열
     */
    public static String replaceWord(String strText, String strWord, String strReplaceWord) {

        if (strText != null && strText.length() != 0) {

            StringBuffer sbufferText = new StringBuffer(strText);
            int iStart = -1;
            int iFindPoint = -1;
            int iCountOfFind = 1;

            // 특정문자열을 찾음
            iStart = strText.indexOf(strWord);
            iFindPoint = iStart;

            while (iFindPoint != -1) {

                // 다른 문자열로 대체함
                sbufferText.replace(iStart, iStart + strWord.length(), strReplaceWord);

                // 다음 특정 문자열을 찾음
                iFindPoint = strText.indexOf(strWord, iFindPoint + strWord.length());
                iStart = iFindPoint + (iCountOfFind) * (strReplaceWord.length() - strWord.length());

                iCountOfFind++;
            }

            return sbufferText.toString();

        } else {

            return strText;

        }

    }

    public static String replaceAll(String strSource, String strSearch, String strReplace) {

        if (strSource == null || strSearch == null || strSearch.equals("")) {
            return strSource;
        }
        if (strReplace == null) {
            strReplace = "";
        }

        int iStart = 0;

        try {
            iStart = strSource.indexOf(strSearch, iStart);

            while (iStart > -1) {
                strSource = strSource.substring(0, iStart) + strReplace + strSource.substring(iStart + strSearch.length());

                iStart += strReplace.length();

                iStart = strSource.indexOf(strSearch, iStart);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return strSource;
    }

    public static String replaceAllIgnoreCase(String strSource, String strSearch, String strReplace) {

        String strSourceUpper = strSource.toUpperCase();
        String strSearchUpper = strSearch.toUpperCase();
        int iStart = 0;

        try {
            iStart = strSourceUpper.indexOf(strSearchUpper, iStart);

            while (iStart > -1) {
                strSource = strSource.substring(0, iStart) + strReplace + strSource.substring(iStart + strSearch.length());

                iStart += strReplace.length();
                strSourceUpper = strSource.toUpperCase();
                iStart = strSourceUpper.indexOf(strSearchUpper, iStart);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return strSource;
    }

    public static String replaceOnlyLine(String strSource) {
        strSource = replaceAll(strSource, "\r\n", " ");
        strSource = replaceAll(strSource, "\r", " ");
        strSource = replaceAll(strSource, "\n", " ");
        strSource = replaceAll(strSource, "\t", " ");
        return strSource;
    }

    /**
     * String 배열을 바이트 배열로 변환
     *
     * @param String 변환시킬 String
     * @return 리턴 byte []
     */

    public static byte[] StringToHex(String TempString) {
        byte[] ReturnByte = new byte[TempString.length() / 2];
        int ReturnBytePointer = 0;

        for (int i = 0; i < TempString.length(); i++) {
            if (TempString.charAt(i) == '0') {
                i++;
                if ((TempString.charAt(i) >= '0') && (TempString.charAt(i) <= '9')) {
                    ReturnByte[ReturnBytePointer] = (byte) (TempString.charAt(i) - '0');
                } else if ((TempString.charAt(i) >= 'A') && (TempString.charAt(i) <= 'F')) {
                    ReturnByte[ReturnBytePointer] = (byte) (10 + TempString.charAt(i) - 'A');
                }
                ReturnBytePointer++;
            } else {
                byte TempUpper;
                byte TempLower;

                TempUpper = 0;
                if ((TempString.charAt(i) >= '0') && (TempString.charAt(i) <= '9')) {
                    TempUpper = (byte) (TempString.charAt(i) - '0');
                } else if ((TempString.charAt(i) >= 'A') && (TempString.charAt(i) <= 'F')) {
                    TempUpper = (byte) (10 + TempString.charAt(i) - 'A');
                }
                TempUpper = (byte) (TempUpper * 16);

                i++;

                TempLower = 0;
                if ((TempString.charAt(i) >= '0') && (TempString.charAt(i) <= '9')) {
                    TempLower = (byte) (TempString.charAt(i) - '0');
                } else if ((TempString.charAt(i) >= 'A') && (TempString.charAt(i) <= 'F')) {
                    TempLower = (byte) (10 + TempString.charAt(i) - 'A');
                }

                ReturnByte[ReturnBytePointer] = (byte) (TempUpper + TempLower);
                ReturnBytePointer++;
            }
        }
        return ReturnByte;
    }

    /**
     * Byte형태의 Data를 주어진 길이 만큼 잘라낸다.ㄴ
     * 작성 날짜: (2001-02-19 오후 3:44:21)
     *
     * @param bytes  byte[]
     * @param start  int
     * @param length int
     */
    public static final byte[] subByte(byte[] bytes, int start, int end) {

        byte[] returnBytes = new byte[end - start + 1];
        int index = 0;

        for (int i = start; i < end + 1; i++) {
            returnBytes[index++] = bytes[i];
        }

        return returnBytes;
    }

    /**
     * 스트링을 trim함. null일 경우 null을 리턴
     *
     * @param strTarget String trim을 할 문자열
     * @return trim이 된 문자열
     */
    public static String trim(String strTarget) {

        if (strTarget != null) {
            strTarget = strTarget.trim();
        }

        return strTarget;
    }

    /**
     * 주어진 날짜의 3개월 후의 날짜를 리턴한다.<BR>
     * "20011031" -> "20020131"<BR>
     * "20011031103040" -> "20020131103040"<BR>
     * 윤년 계산됨
     *
     * @param strCurDate 날짜
     * @return 3개월 후의 날짜
     */
    public static String afterNYear(String strCurDate, int iNextYear) {

        // 현재 날짜의 Calendar 객체를 가져온다.
        Calendar calCurDate = Util.convertStringToCal(strCurDate);
        // N년을 뺀다
        int iYear = calCurDate.get(Calendar.YEAR) + iNextYear;

        //년을 다시 설정한다.
        calCurDate.set(Calendar.DATE, iYear);

        String strResult = Util.convertCalToString(calCurDate);

        if (strCurDate.length() == 8) {
            strResult = strResult.substring(0, 8);
        }

        return strResult;
    }


    /**
     * 숫자를 금액표시법으로 변경 ( 1234560 -> 1,234,560)
     *
     * @param lAmount long 숫자
     * @return 천단위로 콤마가 들어간 문자열
     */
    public static String putCommaByThousand(double lAmount) {
        DecimalFormat dfAmount = new DecimalFormat("###,###,###,###,###,###,###.##");
        return dfAmount.format(lAmount);
    }

    // prefix를 제외하고 기본 13자리
    public static synchronized String generateKey(String prefix, int keySize) {

        try {
            Thread.sleep(1);
        } catch (Exception e) {
        }

        int blankSize = 0;
        String blankKey = "";

        java.util.Date date = new java.util.Date();
        long time = date.getTime();

        String timeKey = String.valueOf(time);

        blankSize = keySize - (prefix.length() + timeKey.length());

        for (int i = 0; i < blankSize; i++) {
            blankKey += "0";
        }

        return prefix + blankKey + timeKey;
    }

    public static String base64Encoder(byte[] data) {
        return new String(java.util.Base64.getEncoder().encode(data));
    }

    public static String getMaskingString(String data) {
        return getMaskingString(data, 0, data.length());
    }

    public static String getMaskingString(String data, int left, int right) {
        int length = data.length();
        int cnt = 0;
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < left && i < length; ++i) {
            ++cnt;
            sb.append("*");
        }
        for (int i = cnt; i < length && i < length - right; ++i) {
            ++cnt;
            sb.append(data.charAt(i));
        }
        for (int i = cnt; i < length; ++i) {
            ++cnt;
            sb.append("*");
        }

        return sb.toString();
    }

    public static int stringToint(String value) {
        if (value == null) {
            return 0;
        }
        String str = "";
        value = value.trim();
        value = replaceAll(value, ",", "");
        if (value.length() == 0) {
            return 0;
        }
        boolean first = true;
        char buf;
        int index = 0;
        while (index < value.length()) {
            buf = value.charAt(index);
            if (buf >= '0' && buf <= '9') {
                if (first && buf != '0') {
                    first = false;
                }
            } else if (first == false) {
                break;
            }
            if (first == false) {
                str += buf;
            }
            ++index;
        }
        if (str.length() == 0) {
            return 0;
        }

        return Integer.parseInt(str);
    }

    public static int getIndexFromArrayList(ArrayList<String> arrayList, String data) {
        for (int i = 0; i < arrayList.size(); ++i) {
            if (arrayList.get(i).equalsIgnoreCase(data)) {
                return i;
            }
        }
        return -1;
    }

    public static boolean isSplitData(String split, String target) {
        if (split == null || target == null || split.equals("") || target.equals("")) {
            return false;
        }
        String[] list = split.split(",");
        for (int i = 0; i < list.length; ++i) {
            if (target.equalsIgnoreCase(list[i])) {
                return true;
            }
        }
        return false;
    }

    public static String getColumnType(String type) {
        String fieldType = "CHAR";

        if (type == null) {
            type = "NULL";
            fieldType = "NULL";
        }

        if (type.contains("decimal") || type.contains("number")
                || type.contains("numeric") || type.contains("num")
                || type.contains("tinyint") || type.contains("tinyint unsigned")
                || type.contains("smallint unsigned") || type.contains("mediumint")
                || type.contains("mediumint unsigned") || type.contains("int unsigned")
                || type.contains("int identity") || type.contains("bigint unsigned")
                || type.contains("bit") || type.contains("float")
                || type.contains("int") || type.contains("integer")
                || type.contains("double") || type.contains("money")) {
            fieldType = "NUM";
        }

        return fieldType;
    }

    public static boolean fileMove(String srcFileName, String destFileName) {
        File srcFile = new File(srcFileName);
        File descFile = new File(destFileName);
        FileChannel srcFc = null, descFc = null;
        FileInputStream fis = null;
        FileOutputStream fos = null;
        boolean ret = false;
        try {
            if (!srcFile.exists()) {
                return false;
            }
            if (descFile.exists()) {
                if (descFile.delete() == false) {
                    return false;
                }
            }
            fis = new FileInputStream(srcFile);
            fos = new FileOutputStream(descFile);
            srcFc = fis.getChannel();
            descFc = fos.getChannel();
            srcFc.transferTo(0, srcFc.size(), descFc);
            srcFc.close();
            srcFc = null;
            fis.close();
            fis = null;
            descFc.close();
            descFc = null;
            fos.close();
            fos = null;
            srcFile.delete();
            ret = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (descFc != null) descFc.close();
            } catch (Exception e) {
            }
            try {
                if (srcFc != null) srcFc.close();
            } catch (Exception e) {
            }
            try {
                if (fos != null) fos.close();
            } catch (Exception e) {
            }
            try {
                if (fis != null) fis.close();
            } catch (Exception e) {
            }
        }

        return ret;
    }

    public static String stringToHex0x(String s) {
        String result = "";
        for (int i = 0; i < s.length(); i++) {
            result += String.format("%02X ", (int) s.charAt(i));
        }
        return result;
    }

    public static String makeId(String oldId) {
        String qId = "";
        boolean bMake = true;
        int i = 0;
        while (bMake) {
            ++i;
            bMake = false;
            GregorianCalendar today = new GregorianCalendar();
            qId = "Q" + today.get(Calendar.YEAR) % 100
                    + String.format("%02d", today.get(Calendar.MONTH) + 1)
                    + today.get(Calendar.DATE)
                    + today.get(Calendar.HOUR)
                    + today.get(Calendar.MINUTE)
                    + today.get(Calendar.SECOND)
                    + today.get(Calendar.MILLISECOND);
            if (i > 1000) {
                return qId;
            }
            if (oldId != "" && oldId != null) {
                if (oldId.equalsIgnoreCase(qId)) {
                    bMake = true;
                    try {
                        Thread.sleep((long) (Math.random() * 100 % 50));
                    } catch (InterruptedException e) {
                    }
                    continue;
                }
            }
        }
        return qId;
    }

    public static synchronized String getUniqueId() {
        return "QID" + ++uniqueId;
    }

    public static String getCurrentTime() {
        GregorianCalendar today = new GregorianCalendar();
        String curTime = today.get(Calendar.YEAR) + "-"
                + String.format("%02d", today.get(Calendar.MONTH) + 1) + "-"
                + today.get(Calendar.DATE) + " "
                + String.format("%02d", today.get(Calendar.HOUR)) + ":"
                + String.format("%02d", today.get(Calendar.MINUTE)) + ":"
                + String.format("%02d", today.get(Calendar.SECOND)) + "."
                + today.get(Calendar.MILLISECOND);
        return curTime;
    }
}