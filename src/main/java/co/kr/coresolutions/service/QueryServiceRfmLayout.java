package co.kr.coresolutions.service;

import co.kr.coresolutions.model.RfmLayout;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class QueryServiceRfmLayout {
    private boolean indexExists = false;
    private final Constants constants;
    private final ObjectMapper objectMapper;
    private String _rfmlayoutQueryDir;
    private boolean ownerExists = false;
    private boolean fail = false;

    @PostConstruct
    public void init() {
        _rfmlayoutQueryDir = constants.getRfmLayoutQueryDir();
    }

    public String saveRfmLayout(String owner, String rfmlayoutid, RfmLayout rfmLayout) throws IOException {
        indexExists = false;
        ownerExists = false;
        if (!Files.isDirectory(Paths.get(_rfmlayoutQueryDir)))
            Files.createDirectory(Paths.get(_rfmlayoutQueryDir));

        fail = false;
        try (Stream<Path> paths = Files.walk(Paths.get(_rfmlayoutQueryDir))) {
            paths
                    .filter(Files::isDirectory)
                    .forEach(path -> {
                        try (Stream<Path> path1 = Files.walk(Paths.get(_rfmlayoutQueryDir + File.separator + owner))) {
                            ownerExists = true;
                            path1
                                    .filter(Files::isRegularFile)
                                    .forEach(path2 -> {
                                        if (path2.getFileName().toString().equalsIgnoreCase("index.txt")) {
                                            indexExists = true;
                                        }
                                    });
                        } catch (IOException e1) {
                        }

                    });

        } catch (Exception e) {
        }


        if (fail) return "fail";
        Path ownerPath = Paths.get(_rfmlayoutQueryDir + File.separator + owner);

        if (!ownerExists) {
            ownerExists = true;
            if (!Files.isDirectory(Paths.get(_rfmlayoutQueryDir + File.separator + owner)))
                ownerPath = Files.createDirectory(ownerPath);
        }


        if (ownerExists) {
            JSONArray jsonArray = null;
            byte[] RFMLAYOUT = objectMapper.writeValueAsBytes(rfmLayout);
            JSONObject jsonObject1 = new JSONObject(new String(RFMLAYOUT));
            JSONObject RfmLAYOUTWithoutJsonField = new JSONObject(jsonObject1, Arrays.asList(JSONObject.getNames(jsonObject1)).stream().filter(s ->
                    !s.equals("rfmlayoutjson")
            ).collect(Collectors.toList()).toArray(new String[JSONObject.getNames(jsonObject1).length]));

            Files.write(Paths.get(ownerPath + File.separator + rfmlayoutid + ".txt"), RFMLAYOUT,
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);


            if (!indexExists) {
                jsonArray = new JSONArray();
                jsonArray.put(RfmLAYOUTWithoutJsonField);
                Files.write(Paths.get(ownerPath + File.separator + "index.txt"),
                        jsonArray.toString().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE);
            } else {
                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(ownerPath + File.separator + "index.txt"))));
                JsonArray _jsonArraytemp = jsonElement.getAsJsonArray();
                JSONArray returnArray = new JSONArray();

                _jsonArraytemp.iterator().forEachRemaining(e -> {
                    JSONObject _temp = new JSONObject(e.toString());
                    if (_temp.has("rfmlayoutID")) {
                        if (!_temp.get("rfmlayoutID").toString().equalsIgnoreCase(rfmlayoutid) && rfmLayout.getRfmlayoutID().equalsIgnoreCase(rfmlayoutid))
                            returnArray.put(_temp);
                    }
                });

                returnArray.put(RfmLAYOUTWithoutJsonField);
                Files.write(Paths.get(ownerPath + File.separator + "index.txt"), returnArray.toString().getBytes(StandardCharsets.UTF_8),
                        StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

            }
        }
        return "success";
    }

    public String getRfmLayouts(String owner) throws IOException {
        if (!Files.isDirectory(Paths.get(_rfmlayoutQueryDir)))
            Files.createDirectory(Paths.get(_rfmlayoutQueryDir));

        try {
            return new String(Files.readAllBytes(Paths.get(_rfmlayoutQueryDir + File.separator + owner + File.separator + "index.txt")), StandardCharsets.UTF_8);
        } catch (IOException e1) {
            return "fail";
        }
    }

    public String getRfmLayout(String owner, String layoutqueryid) throws IOException {
        if (!Files.isDirectory(Paths.get(_rfmlayoutQueryDir)))
            Files.createDirectory(Paths.get(_rfmlayoutQueryDir));

        if (!Files.isDirectory(Paths.get(_rfmlayoutQueryDir + File.separator + owner))) {
            return "failowner";
        } else {
            try {
                return new String(Files.readAllBytes(Paths.get(_rfmlayoutQueryDir + File.separator + owner + File.separator + layoutqueryid + ".txt")), StandardCharsets.UTF_8);
            } catch (IOException e) {
                return "failrfmlayoutid";
            }
        }
    }

    public boolean deleteRfmLayout(String owner, String rfmlayoutid) throws IOException {
        if (!Files.isDirectory(Paths.get(_rfmlayoutQueryDir)))
            Files.createDirectory(Paths.get(_rfmlayoutQueryDir));

        if (!Files.isDirectory(Paths.get(_rfmlayoutQueryDir + File.separator + owner))) {
            return false;
        } else {
            try {
                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(_rfmlayoutQueryDir + File.separator + owner + File.separator + "index.txt"))));
                JsonArray _jsonArraytemp = jsonElement.getAsJsonArray();

                JSONArray returnArray = new JSONArray();
                _jsonArraytemp.iterator().forEachRemaining(e -> {
                    JSONObject _temp = new JSONObject(e.toString());
                    if (!_temp.get("rfmlayoutID").toString().equalsIgnoreCase(rfmlayoutid))
                        returnArray.put(_temp);
                });

                try {
                    Files.write(Paths.get(_rfmlayoutQueryDir + File.separator + owner + File.separator + "index.txt"), returnArray.toString().getBytes(StandardCharsets.UTF_8),
                            StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                } catch (IOException e1) {
                }
                return Files.deleteIfExists(Paths.get(_rfmlayoutQueryDir + File.separator + owner + File.separator + rfmlayoutid + ".txt"));
            } catch (IOException e) {
                return false;
            }
        }
    }

}
