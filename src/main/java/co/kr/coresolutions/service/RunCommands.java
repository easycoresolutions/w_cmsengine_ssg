package co.kr.coresolutions.service;

import co.kr.coresolutions.configuration.security.JwtTokenFilter;
import co.kr.coresolutions.util.Util;
import lombok.RequiredArgsConstructor;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Component
@RequiredArgsConstructor
public class RunCommands {
    private static String OS = System.getProperty("os.name").toLowerCase();
    private final Constants constants;
    private final QueryService queryService;
    private String _logDir;
    public static boolean isWindows() {
        return (OS.indexOf("win") >= 0);
    }

    public static boolean isMac() {
        return (OS.indexOf("mac") >= 0);
    }

    public static boolean isUnix() {
        return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
    }

    private static String[] convertCommand(String command) {
        command = Util.replaceAll(command, "\"\\\"\"", "\\\"");
        ArrayList<String> cmdList = new ArrayList<>();
        StringBuffer buf = new StringBuffer();
        char temp = 0x00;
        char oldtemp = 0x00;
        boolean paramon = false;
        for (int i = 0; i < command.length(); ++i) {
            temp = command.charAt(i);
            if (paramon) {
                if (temp == '\"' && oldtemp != '\\') {
                    paramon = false;
                    String bufstr = Util.replaceAll(buf.toString(), "\\\"", "\"");
                    cmdList.add(bufstr);
                    buf = new StringBuffer();
                } else {
                    buf.append(temp);
                }
            } else {
                if (temp == ' ') {
                    if (!buf.toString().trim().equals("")) {
                        cmdList.add(buf.toString());
                    }
                    buf = new StringBuffer();
                } else if (temp == '\"' && oldtemp != '\\') {
                    paramon = true;
                } else {
                    buf.append(temp);
                }
            }
            oldtemp = temp;
        }
        cmdList.add(buf.toString());
        //System.out.println("cmd : " + cmdList.toString());
        String[] cmdstr = new String[cmdList.size()];
        return cmdList.toArray(cmdstr);
    }

    @PostConstruct
    public void init() {
        _logDir = constants.getLogDir();
    }

    public String runOsCommand(String commandFileName) {
        try {
            Process p;
            String command = new String(Files.readAllBytes(Paths.get(constants.getCommandDir() + commandFileName)));

            if (isUnix()) {
                p = Runtime.getRuntime().exec(new String[]{"bash", "-c", command});
            } else {
                p = Runtime.getRuntime().exec(command);
            }
            return getProcessResult(p);
        } catch (IOException e) {
            return "Error executing command file " + commandFileName + "" +
                    ", Please check manual of your OS!";
        }
    }

    private String getProcessResult(Process p) throws IOException {
        if (p == null) {
            return "Error executing command in this platform ";
        } else {
            String input = IOUtils.toString(p.getInputStream(), StandardCharsets.UTF_8.name());
            if (input.isEmpty()) {
                input = IOUtils.toString(p.getErrorStream(), StandardCharsets.UTF_8.name());
            }
            return input;
        }
    }


    public String runStandaloneCommand(String command) {
        try {
            Process p;
            if (isUnix()) {
                p = Runtime.getRuntime().exec(new String[]{"bash", "-c", command});
            } else {
                p = Runtime.getRuntime().exec(command);
            }
            if (p == null) {
                return "Error executing command in this platform ";
            }
            else {
                String input = IOUtils.toString(p.getInputStream(), StandardCharsets.UTF_8.name());
                if (input.isEmpty()) {
                    input = IOUtils.toString(p.getErrorStream(), StandardCharsets.UTF_8.name());
                }
                p.destroyForcibly();
                return input;
            }
        } catch (IOException e) {
            return "Error executing command " + command +
                    ", Please check manual of your OS!";
        }
    }

    public void saveCacheCommandWaiting(String commandID, String commandAfterKeyReplace, String owner) {
        if (commandID.isEmpty()) {
            return;
        }
        CacheManager cm = CacheManager.getInstance();
        if (!cm.cacheExists("cacheCommandWaiting")) {
            cm.addCache("cacheCommandWaiting");
        }

        Cache cache = cm.getCache("cacheCommandWaiting");
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("command", commandAfterKeyReplace);
            jsonObject.put("owner", owner);
            jsonObject.put("commandid", commandID);
            jsonObject.put("api_name", JwtTokenFilter.apiName);
            cache.put(new Element(commandID, jsonObject));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean removeCacheCommandWaiting(String commandID) {
        if (commandID.isEmpty()) {
            return true;
        }
        CacheManager cm = CacheManager.getInstance();
        Cache cache = cm.getCache("cacheCommandWaiting");
        if (cache != null) {
            return cache.remove(commandID);
        }
        return false;
    }

    private void saveCacheCommandProcess(String commandID, Process process) {
        QueryService.sessionProcess.put(commandID, process);
    }

    private Object getCacheCommandProcess(String commandID) {
        return QueryService.sessionProcess.get(commandID);
    }


    public void removeCacheCommandProcess(String commandID) {
        Object o = getCacheCommandProcess(commandID);
        if (o != null) {
            if (o instanceof Process) {
                Process process = (Process) o;
                if (process.isAlive()) {
                    process.destroyForcibly();
                }
            } else if (o instanceof Thread) {
                Thread thread = (Thread) o;
                if (thread.isAlive()) {
                    thread.interrupt();
                }
            } else if (o instanceof CompletableFuture) {
                CompletableFuture completableFuture = (CompletableFuture) o;
                completableFuture.cancel(true);
            }
        }
        QueryService.sessionProcess.remove(commandID);

    }

    public String runCommand(String commandAfterKeyReplace, String commandID, HttpServletResponse response) {
        final String[] executionResult = {""};
        final boolean[] commandDurationExceed = {false};
        try {
            Process process;
            if (isUnix()) {
                process = Runtime.getRuntime().exec(new String[]{"bash", "-c", commandAfterKeyReplace});
                saveCacheCommandProcess(commandID, process);
            } else {
                process = Runtime.getRuntime().exec(commandAfterKeyReplace);
                saveCacheCommandProcess(commandID, process);
            }
            Optional.ofNullable(constants.getConfigFileAsJson())
                    .ifPresent(jsonNode -> {
//                        if (jsonNode.hasNonNull("command_durationMinutes") && jsonNode.get("command_durationMinutes").isNumber()) {
//                            Thread thread = new Thread(() -> {
//                                try {
//                                    Thread.sleep(jsonNode.get("command_durationMinutes").asInt() * 60000L);
//                                    commandDurationExceed[0] = true;
//                                    process.destroy();
//                                    removeCacheCommandProcess(commandID);
//                                } catch (InterruptedException e) {
//                                    Thread.currentThread().interrupt();
//                                    executionResult[0] = "Error executing command " + commandAfterKeyReplace +
//                                            ", Please check manual of your OS!";
//                                    addLogging("POST", "/command is cancelled by request. commandID(" + commandID + ")\n\n");
//                                    jsonObjectResponse(response, false);
//                                }
//                            });
//                            thread.setDaemon(true);
//                            thread.start();
////                            thread.interrupt();
//                        }
                    });
            if (process != null) {
                String input = IOUtils.toString(process.getInputStream(), StandardCharsets.UTF_8.name());
                if (input.isEmpty()) {
                    input = "Error Command Execution\t" + IOUtils.toString(process.getErrorStream(), StandardCharsets.UTF_8.name());
                }
                try {
                    process.waitFor();
                } catch (InterruptedException e) {
                    executionResult[0] = "Error executing command " + commandAfterKeyReplace +
                            ", Please check manual of your OS!";
                    addLogging("POST", "/command is cancelled by system timeout. commandID(" + commandID + ")\n\n");
                    jsonObjectResponse(response, commandDurationExceed[0]);
                    Thread.currentThread().interrupt();
                }
                return input;
            } else {
                executionResult[0] = "Error executing command in this platform ";
            }

        } catch (IOException e) {
            if (getCacheCommandProcess(commandID) == null) {
                addLogging("POST", "Error command-execution-time is exceed, commandID\t" + commandID + "\n");
                jsonObjectResponse(response, true);
            } else {
                removeCacheCommandProcess(commandID);
                addLogging("POST", "Error Command is terminated by request, commandID\t" + commandID + "\n");
                jsonObjectResponse(response, false);
            }
        }
        return executionResult[0];
    }

    private void jsonObjectResponse(HttpServletResponse response, boolean exceed) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("message", exceed ? "command-execution-time is exceed" : "Command is terminated by request");
        jsonObject.put("errorCode", exceed ? 1000 : 1001);
        jsonObject.put("successCode", 0);
        jsonObject.put("endDateExecution", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd:HH:mm:ss")));
        if (response != null && !response.isCommitted()) {
            try {
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                response.setHeader("Content-Length", String.valueOf(jsonObject.toString().length()));
                response.setStatus(exceed ? HttpServletResponse.SC_GATEWAY_TIMEOUT : HttpServletResponse.SC_GONE);
                response.getWriter().write(jsonObject.toString());
            } catch (IOException e) {
                System.out.println("error from response as null already, message is\t" + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void addLogging(String method, String log) {
        Thread thread = queryService.processLogging(method, log, _logDir);
        thread.setDaemon(true);
        thread.start();
    }


}
