package co.kr.coresolutions.service;

import co.kr.coresolutions.facades.Impl.QueryNodeFacade;
import co.kr.coresolutions.model.Connection;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class QueryServiceNode {
    private final QueryService queryService;

    public void logToDB(String campId, String message, Boolean success, String connectionID, String sequenceNumber) {
        QueryNodeFacade.validateDateMessage = message;
        Thread thread = new Thread(() -> {
            try {
                String dateNow = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                Connection infoConnection = queryService.getInfoConnection(connectionID);
                queryService
                        .checkValidityUpdateClauses("INSERT INTO " + infoConnection.getSCHEME() +
                                "Z_LOG (LID, PROGRAM_ID, LOG_SEQ, LOG_KIND, LOG_CD, LOG_MSG, LOG_DATA, CAMP_ID, LID, LOAD_DTTM) VALUES (SQ_ZLOG_LID.NEXTVAL, 'BATCH_" + campId + "', "
                                + (sequenceNumber == null ? null : Integer.parseInt(sequenceNumber)) + ", 'B', '" + (success ? "success" : "fail") + "', '"
                                + Optional.of(message).orElse("").replaceAll("\'", "") + "', 'EMPTY', '" + campId + "', 0, '" + dateNow + "')", connectionID);
            } catch (Exception e) {
                queryService.addLogging("POST", "execNodes triggered, fail when inserting data to z_log\t" + e.getMessage());
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    public void checkHoliday(String campId, Date date, String connectionId, String holidayTableName) {
        String resultQuery = "";
        String dateString = new SimpleDateFormat("yyyyMMdd").format(date);

        try {
            Connection infoConnection = queryService.getInfoConnection(connectionId);
            resultQuery = queryService.checkValidity("SELECT HOLI_NAME FROM " + infoConnection.getSCHEME() + holidayTableName.toUpperCase()
                    + " WHERE HOLI_YEAR = '" + dateString.substring(0, 4)
                    + "' AND HOLI_MONTH = '" + dateString.substring(4, 6)
                    + "' AND HOLI_DAY = '" + dateString.substring(6, 8)
                    + "'", connectionId, "", "cache", false);
        } catch (Exception e) {
            queryService.addLogging("POST", "execNodes triggered, fail when inserting data to z_log\t" + e.getMessage());
        }

        if (!(resultQuery.equalsIgnoreCase("[]") || resultQuery.startsWith("Error")
                || resultQuery.startsWith("error") || resultQuery.startsWith("maxRunningTimeOut") || resultQuery.startsWith("MaxRows"))) {
            Object holyNameFoundObject = new JSONArray(resultQuery).getJSONObject(0).get("HOLI_NAME");
            queryService.addLogging("POST", "execNodes triggered, fail due to A given date(" + date + ") is not allowed in '" + holyNameFoundObject + "'");
            logToDB(campId, "A given date(" + date + ") is not allowed in " + holyNameFoundObject, false, connectionId, null);
        }
    }
}
