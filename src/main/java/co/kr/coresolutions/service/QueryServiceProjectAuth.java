package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.dtos.ProjectDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class QueryServiceProjectAuth {
    private final ObjectMapper objectMapper;
    private final QueryService queryService;

    public void getProjectVersion(String connectionID, String[] projectVersion, String resultSelect) {
        if (!(resultSelect.equalsIgnoreCase(connectionID) || resultSelect.startsWith("Error") || resultSelect.startsWith("error"))) {
            JsonParser parser = new JsonParser();
            JsonElement jsonElement = parser.parse(resultSelect);
            if (jsonElement.isJsonArray() && jsonElement.getAsJsonArray().size() > 0) {
                JsonObject jsonObject = jsonElement.getAsJsonArray().get(0).getAsJsonObject();
                if (jsonObject.has("projectversion")) {
                    projectVersion[0] = jsonObject.get("projectversion").getAsString();
                } else if (jsonObject.has("projectVersion")) {
                    projectVersion[0] = jsonObject.get("projectVersion").getAsString();
                }
            }
        }
    }

    public CustomResponseDto insertAuxiliaryTable(String tableName, JSONArray jsonArray, String randomId, String projectID, String connectionID) {
        StringBuilder stringBuilderKeys = new StringBuilder();
        StringBuilder stringBuilderValues = new StringBuilder();
        List<String> queries = new ArrayList<>();
        jsonArray.forEach(o -> {
            JSONObject jsonObject = new JSONObject(o.toString());
            jsonObject.put("projectID", projectID);
            jsonObject.keySet().forEach(oneKey -> {
                stringBuilderKeys.append(oneKey).append(",");
                stringBuilderValues.append("'").append(jsonObject.get(oneKey).toString().replace("'", "''")).append("',");
            });
            queries.add("insert into " + tableName
                    + "(" + stringBuilderKeys.deleteCharAt(stringBuilderKeys.length() - 1).toString() + ") values ("
                    + stringBuilderValues.deleteCharAt(stringBuilderValues.length() - 1).toString() + ")");
            stringBuilderKeys.delete(0, stringBuilderKeys.length());
            stringBuilderValues.delete(0, stringBuilderValues.length());
        });

        String resultInsert = queryService.updateClausesWithRollback(queries, connectionID, randomId);
        return CustomResponseDto.builder().DetailedMessage(resultInsert).Success(resultInsert.startsWith("success")).build();
    }

    public CustomResponseDto insertForMain(List<String> listRandomIds, String connectionID, String mainTable, String projectID, JSONObject jsonObject) {
        final String[] projectVersion = {"0"};
        String resultValidity = queryService
                .checkValidity("select projectVersion from " + mainTable + "  where projectID = '" + projectID + "'",
                        connectionID, "", "", false);
        if (!(resultValidity.equalsIgnoreCase(connectionID) || resultValidity.startsWith("Error") || resultValidity.startsWith("error"))) {
            getProjectVersion(connectionID, projectVersion, resultValidity);
        } else {
            return CustomResponseDto.builder().DetailedMessage("fail due error is\t" + resultValidity).build();
        }

        String randomDeleteMain = UUID.randomUUID().toString();
        resultValidity = queryService
                .updateClausesWithRollback(Lists.newArrayList("DELETE FROM " + mainTable + " where projectID = '" + projectID + "'"), connectionID,
                        randomDeleteMain);
        if (!resultValidity.startsWith("success")) {
            return CustomResponseDto.builder().DetailedMessage("fail due error is\t" + resultValidity).build();
        }
        listRandomIds.add(randomDeleteMain);

        StringBuilder stringBuilderKeys = new StringBuilder();
        StringBuilder stringBuilderValues = new StringBuilder();

        stringBuilderKeys.append("projectVersion,");
        stringBuilderValues.append("'").append(Integer.parseInt(projectVersion[0]) + 1).append("',");

        jsonObject.keySet().stream().filter(s -> !s.equalsIgnoreCase("projectVersion")).forEachOrdered(s -> {
            stringBuilderKeys.append(s).append(",");
            if (s.equals("projectjson")) {
                stringBuilderValues.append("'").append(jsonObject.get("projectjson").toString()
                        .replace("\\", "\\\\")
                        .replace("/", "\\/")
                        .replace("'", "''")).append("',");
            } else {
                stringBuilderValues.append("'").append(jsonObject.get(s).toString().replace("'", "''")).append("',");
            }
        });

        String randomInsertMain = UUID.randomUUID().toString();
        String resultInsert = queryService
                .updateClausesWithRollback(Lists.newArrayList("insert into " + mainTable
                                + "(" + stringBuilderKeys.deleteCharAt(stringBuilderKeys.length() - 1).toString() + ") values ("
                                + stringBuilderValues.deleteCharAt(stringBuilderValues.length() - 1).toString() + ")"), connectionID,
                        randomDeleteMain + "same" + randomInsertMain);
        if (!resultInsert.startsWith("success")) {
            return CustomResponseDto.builder().DetailedMessage("fail due " + resultInsert).build();
        }
        listRandomIds.add(randomInsertMain);
        return CustomResponseDto.ok();
    }

    public CustomResponseDto doUsualValidation(JsonNode node, String connectionID, String projectID, Boolean isGet) {
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            return CustomResponseDto.builder().DetailedMessage("fail due to connection file with name\t" + connectionID + "\t doesn't exist").build();
        }
        if (node == null) {
            return CustomResponseDto.builder().Success(true).build();
        }
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(objectMapper.writeValueAsString(node));
        } catch (JsonProcessingException e) {
            return CustomResponseDto.builder().DetailedMessage("fail due " + e.getMessage()).build();
        }
        if (!jsonObject.has("project_table")) {
            return CustomResponseDto.builder().DetailedMessage("fail due project_table missing").build();
        }

        String mainTable = jsonObject.get("project_table").toString();
        if (isGet && !jsonObject.has("auxiliary_table")) {
            return CustomResponseDto.builder().DetailedMessage("fail due auxiliary_table missing").build();
        }


        if (mainTable.isEmpty() || !jsonObject.has(mainTable)) {
            return CustomResponseDto.builder().DetailedMessage("fail due missing main table \t" + mainTable + "\t body from request").build();
        }

        try {
            ProjectDto projectDto = objectMapper.readValue(jsonObject.get(mainTable).toString(), ProjectDto.class);
            if (!projectDto.getProjectID().equals(projectID)) {
                return CustomResponseDto.builder().DetailedMessage("fail due mismatch projectID from url and body!").build();
            }
        } catch (IOException e) {
            return CustomResponseDto.builder().DetailedMessage(e.getMessage()).build();
        }

        return CustomResponseDto.builder().Success(true).DetailedMessage(mainTable).message(jsonObject.toString()).build();
    }

    public CustomResponseDto getForMainOrAuxTable(JsonNode jsonNode, String connectionID, String projectID, Boolean isMain) {
        CustomResponseDto customResponseDto = CustomResponseDto.builder().Success(true).DetailedMessage("").build();
        final String[] resultSelect = new String[1];
        if (isMain) {
            String tableName = jsonNode.get("project_table").asText();
            if (tableName.isEmpty()) {
                return CustomResponseDto.builder().DetailedMessage("invalid body request, project_table is required!").build();
            }
            resultSelect[0] = queryService
                    .checkValidity("select * from " + tableName + "  where projectID = '" + projectID + "'",
                            connectionID, "", "", false);
            if (resultSelect[0].equalsIgnoreCase(connectionID) || resultSelect[0].startsWith("Error") || resultSelect[0].startsWith("error") ||
                    resultSelect[0].equalsIgnoreCase("[]")) {
                customResponseDto.setMessage("main");
                customResponseDto.setSuccess(false);
            }
            customResponseDto.setDetailedMessage(resultSelect[0]
                    .equalsIgnoreCase("[]") ? "empty value found for main table " + tableName : resultSelect[0]);
        } else {
            final boolean[] success = {true};
            JSONObject jsonObjectAux = new JSONObject(jsonNode.get("auxiliary_table").toString());
            JSONObject jsonObjectResponseAux = new JSONObject();
            jsonObjectAux.keySet().forEach(auxiliaryTableName -> {
                if (!success[0]) {
                    return;
                }
                JSONObject jsonObject = new JSONObject(jsonObjectAux.get(auxiliaryTableName).toString());
                if (jsonObject.has("name") && !jsonObject.get("name").toString().isEmpty()) {
                    resultSelect[0] = queryService
                            .checkValidity("select " + jsonObject.get("name").toString() + " from " + auxiliaryTableName + "  where projectID = '" + projectID + "'",
                                    connectionID, "", "", false);
                    if (!(resultSelect[0].equalsIgnoreCase(connectionID) || resultSelect[0].startsWith("Error") || resultSelect[0].startsWith("error") ||
                            resultSelect[0].equalsIgnoreCase("[]"))) {
                        jsonObjectResponseAux.put(jsonObject.get("name").toString(), resultSelect[0]);
                        customResponseDto.setDetailedMessage(jsonObjectResponseAux.toString());
                    } else {
                        success[0] = false;
                        customResponseDto.setDetailedMessage(resultSelect[0].equalsIgnoreCase("[]") ? "empty value found for auxiliary table "
                                + auxiliaryTableName : resultSelect[0]);
                        customResponseDto.setMessage("aux");
                    }
                } else {
                    success[0] = false;
                    customResponseDto.setDetailedMessage("invalid body request, name is required!");
                    customResponseDto.setMessage("aux");
                }
                customResponseDto.setSuccess(success[0]);
            });
        }
        return customResponseDto;
    }
}
