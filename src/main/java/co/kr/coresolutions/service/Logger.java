package co.kr.coresolutions.service;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class Logger {
    private final Constants constants;
    private String _logScheduleDir;

    @Scheduled(cron = "0 0 0 * * ?")
    public void createTodayLogFile() {
        String format = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String path = _logScheduleDir + "qlog_" + format + ".txt";
        String pathError = _logScheduleDir + "qlog_" + format + "_error.txt";
        try {
            if (!Files.exists(Paths.get(path))) {
                Files.createFile(Paths.get(path));
            }
            if (!Files.exists(Paths.get(pathError))) {
                Files.createFile(Paths.get(pathError));
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    @PostConstruct
    public void init() {
        _logScheduleDir = constants.getLogDir();
    }
}
