package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.Chunk;
import co.kr.coresolutions.dao.OrderedJSONObject;
import co.kr.coresolutions.facades.Impl.QueryLoadFacade;
import co.kr.coresolutions.model.UnLoad;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

@Service
@RequiredArgsConstructor
public class QueryServiceLoad implements Observer {
    private final QueryServiceCSV queryServiceCSV;
    private final Constants constants;
    private String unloadQueryDir;

    @PostConstruct
    public void init() {
        unloadQueryDir = constants.getUnloadDir();
    }

    public void unload(UnLoad unLoad, String message, String fileName, String format, Boolean isSameOutput, Map<String, List<Object>> dataTypesTable) {
        if (message != null) {
            String pathOwner = unloadQueryDir + unLoad.getOwner();
            fileName = fileName.replaceAll("\"", "");
            JsonNode outfields = unLoad.getOutfields();
            StringBuilder stringBuilder = new StringBuilder();
            List<String> outFieldsRequired = new LinkedList<>();
            final boolean[] isMatched = {true};
            JSONArray jsonArray = new JSONArray(message);
            if (!outfields.isNull()) {
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                outfields.fields().forEachRemaining(s -> {
                    //if unmatched fields then return
                    if (!jsonObject.keySet().contains(s.getKey())) {
                        isMatched[0] = false;
                        return;
                    }
                    outFieldsRequired.add(s.getKey());
                    stringBuilder.append(s.getKey()
                            .replaceAll("[\\;\\_\\\\~\\%\\'\\\"\\/\\,\\@\\\t\\#\\^\\&\\<\\(\\[\\{\\^\\-\\=\\$\\!\\|\\]\\}\\)\\?\\*\\+\\.\\>]", "_")).append(",");
                    stringBuilder.append(s.getValue().toString()).append(System.lineSeparator());
                });
            } else {
                if (!dataTypesTable.isEmpty()) {
                    dataTypesTable.forEach((columnName, objects) -> {
                        stringBuilder.append(columnName
                                .replaceAll("[\\;\\_\\\\~\\%\\'\\\"\\/\\,\\@\\\t\\#\\^\\&\\<\\(\\[\\{\\^\\-\\=\\$\\!\\|\\]\\}\\)\\?\\*\\+\\.\\>]", "_")).append(",");
                        stringBuilder.append("\"").append(objects.get(0)).append("(").append(objects.get(1)).append(")").append("\"").append(System.lineSeparator());
                    });
                    dataTypesTable.clear();
                }
            }
            if (!isMatched[0]) {
                QueryLoadFacade.sessionUnload.remove(unLoad.getUnloadDetails().getUnloadID());
                QueryLoadFacade.sessionUnloadError.put(unLoad.getUnloadDetails().getUnloadID(), "unmatched fields outfields!");
                return;
            }

            if (!outFieldsRequired.isEmpty()) {
                JSONArray jsonArrayFiltered = new JSONArray();
                jsonArray.forEach(o -> {
                    OrderedJSONObject orderedJSONObject = new OrderedJSONObject(o.toString());
                    OrderedJSONObject orderedJSONObject1 = new OrderedJSONObject();
                    outFieldsRequired.forEach(field -> {
                        if (orderedJSONObject.has(field)) {
                            orderedJSONObject1.put(field, orderedJSONObject.get(field));
                        }
                    });
                    if (orderedJSONObject1.length() > 0) {
                        jsonArrayFiltered.put(orderedJSONObject1);
                    }
                });
                message = jsonArrayFiltered.toString();
            }

            String metaData = stringBuilder.toString();

            String finalFileName = fileName;
            CompletableFuture<Boolean> completableFuture = CompletableFuture.supplyAsync(() -> {
                try {
                    if (!isSameOutput) {
                        if (!Paths.get(pathOwner).toFile().exists()) {
                            Files.createDirectories(Paths.get(pathOwner));
                        }
                        Files.deleteIfExists(Paths.get(pathOwner + File.separator + finalFileName + "_meta.txt"));
                        if (!metaData.isEmpty()) {
                            Files.write(Paths.get(pathOwner + File.separator + finalFileName + "_meta.txt"),
                                    ("filename,filetype\n").concat(metaData).getBytes(StandardCharsets.UTF_8),
                                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                        }
                    }
                    return true;
                } catch (IOException e) {
                    QueryLoadFacade.sessionUnload.remove(unLoad.getUnloadDetails().getUnloadID());
                    QueryLoadFacade.sessionUnloadError.put(unLoad.getUnloadDetails().getUnloadID(), e.getMessage());
                    return false;
                }
            });
            structureCSVAndLoadData(unLoad, message, format, isSameOutput, pathOwner, finalFileName, completableFuture);
        }
    }

    private void structureCSVAndLoadData(UnLoad unLoad, String message, String format, Boolean isSameOutput, String pathOwner, String finalFileName, CompletableFuture<Boolean> completableFuture) {
        completableFuture.thenApply(isFirstSuccess -> {
            try {
                if (!QueryLoadFacade.sessionUnload.containsKey(unLoad.getUnloadDetails().getUnloadID())) {
                    return false;
                }
                if (isFirstSuccess) {
                    String structuredCSV = new String(queryServiceCSV.getCsvWithHeader(message, isSameOutput ? false : unLoad.getHeader()).getBytes(), StandardCharsets.UTF_8);
                    fillDataToFile(finalFileName, format, pathOwner, structuredCSV, isSameOutput);
                    return true;
                } else {
                    return false;
                }
            } catch (IOException e) {
                QueryLoadFacade.sessionUnload.remove(unLoad.getUnloadDetails().getUnloadID());
                QueryLoadFacade.sessionUnloadError.put(unLoad.getUnloadDetails().getUnloadID(), e.getMessage());
                return false;
            }
        }).exceptionally(success -> false).join();
    }

    public void unloadClickHouse(UnLoad unLoad, String message, String fileName, String format, String dbms, Boolean isSameOutput, Map<String, List<Object>> dataTypesTable) {
        if (message != null) {
            JsonNode outfields = unLoad.getOutfields();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("dbms", dbms);
            jsonObject.put("fileName", fileName);

            OrderedJSONObject jsonObjectFieldsChild = new OrderedJSONObject();
            if (!outfields.isNull()) {
                outfields.fields().forEachRemaining(s -> {
                    String fieldValue = s.getValue().asText();
                    OrderedJSONObject jsonObjectFieldsChildValues = new OrderedJSONObject();
                    jsonObjectFieldsChildValues.put("type", fieldValue.contains("(") ? fieldValue.substring(0, fieldValue.indexOf("(")) : fieldValue);
                    jsonObjectFieldsChildValues.put("length",
                            fieldValue.contains("(") && fieldValue.contains(")") ? fieldValue.substring(fieldValue.indexOf("(") + 1, fieldValue.indexOf(")")) : 12);
                    if (jsonObjectFieldsChildValues.get("length").toString().isEmpty()) {
                        jsonObjectFieldsChildValues.put("length", 12);
                    }
                    jsonObjectFieldsChild.put(s.getKey(), jsonObjectFieldsChildValues);
                });
            } else {
                if (!dataTypesTable.isEmpty()) {
                    dataTypesTable.forEach((columnName, objects) -> {
                        OrderedJSONObject jsonObjectFieldsChildValues = new OrderedJSONObject();
                        jsonObjectFieldsChildValues.put("type", objects.get(0));
                        jsonObjectFieldsChildValues.put("length", objects.get(1));
                        jsonObjectFieldsChild.put(columnName, jsonObjectFieldsChildValues);
                    });
                    dataTypesTable.clear();
                }
            }

            if (jsonObjectFieldsChild.length() > 0) {
                jsonObject.put("fields", jsonObjectFieldsChild);
            }

            String pathOwner = unloadQueryDir + unLoad.getOwner();
            fileName = fileName.replaceAll("\"", "");
            String finalFileName = fileName;
            CompletableFuture<Boolean> completableFuture = CompletableFuture.supplyAsync(() -> {
                try {
                    if (!isSameOutput) {
                        if (!Paths.get(pathOwner).toFile().exists()) {
                            Files.createDirectories(Paths.get(pathOwner));
                        }
                        Files.deleteIfExists(Paths.get(pathOwner + File.separator + finalFileName + "_meta.txt"));
                        if (!jsonObject.isEmpty()) {
                            Files.write(Paths.get(pathOwner + File.separator + finalFileName + "_meta.txt"),
                                    jsonObject.toString().getBytes(StandardCharsets.UTF_8),
                                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                        }
                    }
                    return true;
                } catch (IOException e) {
                    QueryLoadFacade.sessionUnload.remove(unLoad.getUnloadDetails().getUnloadID());
                    QueryLoadFacade.sessionUnloadError.put(unLoad.getUnloadDetails().getUnloadID(), e.getMessage());
                    return false;
                }
            });

            structureCSVAndLoadData(unLoad, message, format, isSameOutput, pathOwner, finalFileName, completableFuture);
        }
    }

    public String getInsertSqlQuery(Set<String> columnsNames, JSONArray jsonArrayInput, String targetTable) {

        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder globalValues = new StringBuilder();

        jsonArrayInput.forEach(o -> {

            org.json.JSONObject jsonObject = new org.json.JSONObject(o.toString());

            stringBuilder.append("(");

            //todo we must found a solution for columnname for upper and lower case, so jsonobject can check the columnname exist or not
            columnsNames.forEach(columnName -> {

                if (jsonObject.has(columnName)) {
                    if (jsonObject.get(columnName) instanceof Number) {
                        stringBuilder.append(jsonObject.get(columnName)).append(",");
                    } else stringBuilder.append("'").append(jsonObject.get(columnName)).append("',");
                } else {
                    Iterator iterator = jsonObject.keys();
                    while (iterator.hasNext()) {
                        String key = iterator.next().toString();
                        if (key.contains(columnName) || key.equals("")) {
                            if (jsonObject.get(key) instanceof Number) {
                                stringBuilder.append(jsonObject.get(key)).append(",");
                            } else stringBuilder.append("'").append(jsonObject.get(key)).append("',");
                            break;
                        }
                    }
                }
            });

            globalValues.append(stringBuilder.substring(0, stringBuilder.length() - 1)).append("),");
            stringBuilder.delete(0, stringBuilder.length());

        });


        stringBuilder.delete(0, stringBuilder.length());
        stringBuilder.append("(");
        columnsNames.forEach(s -> stringBuilder.append(s).append(","));

        String finalNames = stringBuilder.substring(0, stringBuilder.length() - 1) + ")";

        String insertQuery = "INSERT INTO " + targetTable + finalNames + " VALUES " + globalValues.substring(0, globalValues.length() - 1) + ";";

        return insertQuery;
    }

    private void fillDataToFile(String fileName, String format, String pathOwner, String structuredCSV, Boolean isSameOutput) throws IOException {
        byte[] csv;
        if (format.equalsIgnoreCase("csv")) {
            csv = structuredCSV.getBytes();
            Files.write(Paths.get(pathOwner + File.separator + fileName + "." + format), csv,
                    StandardOpenOption.CREATE, isSameOutput ? StandardOpenOption.APPEND : StandardOpenOption.TRUNCATE_EXISTING);
        } else {
            csv = (structuredCSV.replaceAll("\\\\t", " ").replaceAll(",", "\t")).getBytes();
            Files.write(Paths.get(pathOwner + File.separator + fileName + ".txt"), csv,
                    StandardOpenOption.CREATE, isSameOutput ? StandardOpenOption.APPEND : StandardOpenOption.TRUNCATE_EXISTING);
        }
    }

    @Override
    public void update(Observable observable, Object arg) {
        List arrayList = (ArrayList) arg;
        if (arrayList.get(0) instanceof UnLoad) {
            UnLoad unLoad = (UnLoad) arrayList.get(0);
            if (QueryLoadFacade.sessionUnload.containsKey(unLoad.getUnloadDetails().getUnloadID())) {
                Chunk chunk = (Chunk) observable;
                //if is unload
                if ((boolean) arrayList.get(4)) {
                    unload(unLoad, chunk.getContent(), (String) arrayList.get(2), (String) arrayList.get(1), chunk.isSameOutput(), chunk.getMetadata());
                } else { //if unload2
                    unloadClickHouse(unLoad, chunk.getContent(),
                            (String) arrayList.get(2), (String) arrayList.get(1), (String) arrayList.get(3), chunk.isSameOutput(), chunk.getMetadata());
                }
            }
        }
    }
}
