package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.ErrorCode;
import co.kr.coresolutions.commons.HelperUtil;
import co.kr.coresolutions.commons.Monitor;
import co.kr.coresolutions.commons.QueryChunkPublisher;
import co.kr.coresolutions.dao.DataBase;
import co.kr.coresolutions.dao.NativeQuery;
import co.kr.coresolutions.dao.OrderedJSONObject;
import co.kr.coresolutions.model.Command;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.UnLoad;
import co.kr.coresolutions.util.AES256Cipher;
import co.kr.coresolutions.util.Util;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import lombok.RequiredArgsConstructor;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerMapping;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class QueryService {
    private static final ExecutorService executorService = Executors.newCachedThreadPool();
    public static volatile Map<String, JSONObject> sessionMap = new HashMap<>();
    private final PasswordEncoder passwordEncoder;
    public static Set<String> numList = new LinkedHashSet<>(Arrays.asList(
            "int",
            "number",
            "float",
            "double",
            "uint8",
            "uint16",
            "uint32",
            "uint64",
            "int8",
            "int16",
            "int32",
            "int64",
            "float32",
            "float64",
            "tinyint",
            "unsigned",
            "smallint",
            "unsigned",
            "int unsigned",
            "bigint unsigned",
            "tinyint signed",
            "smallint signed",
            "int signed",
            "bigint signed",
            "binary_float",
            "binary_double",
            "bit",
            "decimal"
    ));
    public static Map<String, List<Object>> transactionMap = new HashMap<>();
    public static Map<String, Object> sessionFutureMap = new HashMap<>();
    public static Map<String, Object> sessionProcess = new ConcurrentHashMap<>();
    public static Map<String, String> savedDateTimeFunctions = new HashMap<>();
    public static Map<Integer, String> notResolvedDateTimeFunctions = new HashMap<>();
    private static final String queryID = "cache";
    private final QueryServiceLoad queryServiceLoad;
    private final QueryChunkPublisher queryChunkPublisher;
    Pattern patternDateTime = Pattern.compile
            ("(%%)\\s*(TO|to|To|tO)_(DAY|MONTH|YEAR|HOUR|MINUTE|SECOND)\\s*\\(\\s*(\\-){0,1}\\d+\\s*,\\s*" +
                    "(MMDD|YYYY-MM|DAYOFWEEK|DAYOFYEAR|YYYYMMFIRSTDAY|YYYYMMLASTDAY|YYYYMMDD|YYYY-MM-DD|YYYY/MM/DD|HHMMSS|HH\\:MM\\:SS" +
                    "|YYYY|MM|YcoYYY-MM|YYYY/MM|YYYYMM|HH|HH\\:MM|HHMM)\\s*\\)\\s*(%%)");
    Pattern patternCase = Pattern.compile
            ("(%%)\\s*(CASE_WHEN_BIN_NUM|CASE_WHEN_BIN_LABEL|CASE_WHEN_RANGE_NUM" +
                    "|CASE_WHEN_RANGE_LABEL|CASE_WHEN_RANGE|CASE_WHEN_BIN)\\s*\\(\\s*(.*?)\\s*,\\s*\\d+\\s*,\\s*\\d+\\s*,\\s*\\d+\\s*\\)\\s*(%%)");

    private final QueryServiceFiles queryServiceFile;
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyyMMdd:HHmmss");

    @Autowired
    private RunCommands runCommands;
    private final Constants constants;
    private final ObjectMapper objectMapper;
    private final MustacheFactory mustacheFactory;
    private static AES256Cipher aes256Cipher;
    @Autowired
    private QueryServiceJSON queryServiceJSON;
    static Semaphore semaphore;
    @Autowired
    private NativeQuery nativeQuery;
    @Autowired
    private DataBase dataBase;
    private Integer counterDateNR = 0;
    private String _connectionDir;
    private String _rootDir;
    private String _fileQueryDir;
    private String _logDir;
    private String _projectQueryDir;
    private String _commandDir;
    private String _unloadQueryDir;
    private String jarsDir;
    private boolean _isCache;
    @Autowired
    private QueryServiceCommand queryServiceCommand;
    JsonNode jsonNodes;
    @Autowired
    private RunCommandsInternal runCommandsInternal;

    public String getFormattedString(String sql) {
        return sql.replaceAll("\\\\t", " ").replaceAll("\\\\n", " ");
    }

    @PostConstruct
    public void init() {
        jarsDir = constants.getJarsDir();
        _connectionDir = constants.getConnectionDir();
        _rootDir = constants.getRootDir();
        _fileQueryDir = constants.getFileQueryDir();
        _logDir = constants.getLogDir();
        _projectQueryDir = constants.getProjectQueryDir();
        _unloadQueryDir = constants.getUnloadDir();
        _isCache = constants.getIsCache();
        jsonNodes = constants.getConfigFileAsJson();
        aes256Cipher = new AES256Cipher();
        Optional.ofNullable(jsonNodes)
                .ifPresent(jsonNode -> {
                    if (jsonNode.hasNonNull("maxCommandThreads") && jsonNode.get("maxCommandThreads").isNumber()) {
                        int maxThreads = jsonNode.get("maxCommandThreads").asInt();
                        if (maxThreads > 0) {
                            semaphore = new Semaphore(jsonNode.get("maxCommandThreads").asInt());
                        }
                    }
                });
        new ConcurrentTaskScheduler().execute(this::loadListDateTimeFunctions, 5000);
    }

    public static <T, E> T getKeysByValue(Map<T, E> map, E value) {
        return map.entrySet()
                .stream()
                .filter(entry -> Objects.equals(entry.getValue(), value))
                .map(Map.Entry::getKey)
                .findFirst().get();
    }

    /**
     * Description : check if file exists in connection dir
     *
     * @param connectionID : take the input number of file.
     * @return boolean: true if exist and false if not.
     */
    public boolean isConnectionIdFileExists(String connectionID) {
        return Files.exists(Paths.get(_connectionDir + connectionID + ".txt"));

    }


    /**
     * Description : run the query against the server.
     *
     * @param connectionID     : take the input the connectionID.
     * @param query            : take the input as query String.
     * @param sqliteUrlDB      : sqlite url
     * @param queryID          : query running thread name
     * @param systemPredicates : weather to use limits of the system or not.
     * @return String: nothing if success or the status where failed otherwise
     * @throws IOException
     */
    public String checkValidity(String query, String connectionID, String sqliteUrlDB, String queryID, boolean systemPredicates) {
        String resultQuery;
        String cntResult;

        if (!isConnectionIdFileExists(connectionID)) {
            return connectionID;
        }

        Connection infoConnection = getInfoConnection(connectionID);
        if (!sqliteUrlDB.isEmpty()) {
            infoConnection.setURL(sqliteUrlDB);
        }
        String resultGetConfigFile = queryServiceFile.getConfigFile();
        int maxRunningTime = 0;
        int maxRows = 0;
        if (!resultGetConfigFile.equalsIgnoreCase("fail")) {
            OrderedJSONObject jsonobject = new OrderedJSONObject(resultGetConfigFile);
            if (systemPredicates) {
                if (jsonobject.has("MaxRunningTime")) {
                    maxRunningTime = jsonobject.getInt("MaxRunningTime");
                    System.out.println("MaxRunningTime : " + maxRunningTime);
                }
                if (jsonobject.has("MaxRows")) {
                    maxRows = jsonobject.getInt("MaxRows");
                    System.out.println("MaxRows : " + maxRows);
                }
            }
        }

        queryID = makeQueryId(queryID);
        String finalQueryID = queryID;
        Future<String> future = executorService.submit(() ->
                dataBase.getQueryCountJDBC(connectionID, query, finalQueryID));

        try {
            cntResult = systemPredicates && maxRunningTime > 0 ? future.get(maxRunningTime, TimeUnit.SECONDS) : future.get();
        } catch (TimeoutException e) {
            future.cancel(true);
            cntResult = "Error : { query timeout - " + queryID + " }";
        } catch (InterruptedException e) {
            cntResult = "Error : { query  " + queryID + " killed by user request }";
        } catch (ExecutionException e) {
            cntResult = "Error : { query  " + queryID + " existed with internal issue: " + e.getMessage() + " }";
        }

        if (!cntResult.equals("")) {
            if (!cntResult.toLowerCase().contains("error")) {
                JSONArray successArray = new JSONArray(cntResult.toLowerCase());
                if (successArray.getJSONObject(0).getInt("cnt") > maxRows && maxRows != 0) {
                    clearResources(queryID);
                    return "Error : {The results exceed a max records(" + maxRows + ") allowed in the system, Ask to Administrator}";
                }
            } else {
                clearResources(queryID);
                return cntResult;
            }
        }

        future = executorService.submit(() ->
                dataBase
                        .executeQuerySelectJDBC(infoConnection, connectionID, query, finalQueryID, false, false, -1));
        try {
            resultQuery = systemPredicates && maxRunningTime > 0 ? future.get(maxRunningTime, TimeUnit.SECONDS) : future.get();
        } catch (TimeoutException e) {
            future.cancel(true);
            return "Error : { query timeout - " + queryID + " }";
        } catch (InterruptedException e) {
            return "Error : { query  " + queryID + " killed by user request }";
        } catch (ExecutionException e) {
            return "Error : { query  " + queryID + " existed with internal issue: " + e.getMessage() + " }";
        } finally {
            clearResources(queryID);
        }
        return resultQuery;
    }

    public String loadWithLimits(String query, String connectionID, String sqliteUrlDB, String queryID, boolean systemPredicates, int limits) {
        String resultQuery;
        String cntResult;

        if (!isConnectionIdFileExists(connectionID)) {
            return connectionID;
        }

        Connection infoConnection = getInfoConnection(connectionID);
        if (!sqliteUrlDB.isEmpty()) {
            infoConnection.setURL(sqliteUrlDB);
        }
        String resultGetConfigFile = queryServiceFile.getConfigFile();
        int maxRunningTime = 0;
        int maxRows = 0;
        if (!resultGetConfigFile.equalsIgnoreCase("fail")) {
            OrderedJSONObject jsonobject = new OrderedJSONObject(resultGetConfigFile);
            if (systemPredicates) {
                if (jsonobject.has("MaxRunningTime")) {
                    maxRunningTime = jsonobject.getInt("MaxRunningTime");
                    System.out.println("MaxRunningTime : " + maxRunningTime);
                }
                if (jsonobject.has("MaxRows")) {
                    maxRows = jsonobject.getInt("MaxRows");
                    System.out.println("MaxRows : " + maxRows);
                }
            }
        }

        queryID = makeQueryId(queryID);
        String finalQueryID = queryID;
        Future<String> future = executorService.submit(() ->
                dataBase.getQueryCountJDBC(connectionID, query, finalQueryID));

        try {
            cntResult = systemPredicates && maxRunningTime > 0 ? future.get(maxRunningTime, TimeUnit.SECONDS) : future.get();
        } catch (InterruptedException e) {
            return "Error : { query  " + queryID + " killed by user request }";
        } catch (ExecutionException e) {
            return "Error : { query  " + queryID + " existed with internal issue: " + e.getMessage() + " }";
        } catch (TimeoutException e) {
            future.cancel(true);
            return "Error : { query timeout - " + queryID + " }";
        }
        if (!cntResult.equals("")) {
            if (!cntResult.toLowerCase().contains("error")) {
                JSONArray successArray = new JSONArray(cntResult.toLowerCase());
                if (successArray.getJSONObject(0).getInt("cnt") > maxRows && maxRows != 0) {
                    clearResources(queryID);
                    return "Error : {The results exceed a max records(" + maxRows + ") allowed in the system, Ask to Administrator}";
                }
            } else {
                clearResources(queryID);
                return cntResult;
            }
        }

        future = executorService.submit(() ->
                dataBase
                        .executeQuerySelectJDBCLimits
                                (infoConnection, connectionID, query, finalQueryID, false, limits));
        try {
            resultQuery = systemPredicates && maxRunningTime > 0 ? future.get(maxRunningTime, TimeUnit.SECONDS) : future.get();
        } catch (TimeoutException e) {
            future.cancel(true);
            return "Error : { query timeout - " + queryID + " }";
        } catch (InterruptedException e) {
            return "Error : { query  " + queryID + " killed by user request }";
        } catch (ExecutionException e) {
            return "Error : { query  " + queryID + " existed with internal issue: " + e.getMessage() + " }";
        } finally {
            clearResources(queryID);
        }
        return resultQuery;
    }


    public String convertGlobalVariable(String queryBeforeConversion) {
        try {
            String[] convertedQuery = {convertDateTime(queryBeforeConversion)};
            if (convertedQuery[0].equalsIgnoreCase("NOT RESOLVED")) return "NOT RESOLVED";

            Table<String, String, String> cacheData = null;
            if (_isCache) {
                cacheData = getCacheData();
            }

            Pattern pattern = Pattern.compile
                    ("(\\:\\:){1}(.*?)\\s*\\(\\s*(.*?)\\s*\\)\\s*(\\:\\:){1}");

            Matcher matcher = pattern.matcher(convertedQuery[0]);
            final String[] finalConvertedQuery = {convertedQuery[0]};
            if (matcher.find()) {
                if (cacheData != null && !cacheData.isEmpty()) {
                    cacheData.cellSet().stream().forEachOrdered(stringStringStringCell -> finalConvertedQuery[0] = finalConvertedQuery[0].replaceAll(
                            "(\\:\\:){1}\\s*" + stringStringStringCell.getRowKey() + "\\s*\\(\\s*" + stringStringStringCell.getColumnKey() + "\\s*\\)\\s*(\\:\\:){1}", stringStringStringCell.getValue()));
                }
                convertedQuery[0] = finalConvertedQuery[0];
            }

            pattern = Pattern.compile
                    ("(\\:\\:){1}(.*?)(\\:\\:){1}");
            matcher = pattern.matcher(convertedQuery[0]);
            if (matcher.find()) {
                convertedQuery[0] = matcher.replaceAll("NOT RESOLVED");
            }

            return convertedQuery[0];
        } catch (Exception e) {
            return "NOT RESOLVED";
        }
    }

    private Table<String, String, String> getCacheData() {
        Table<String, String, String> cacheData = HashBasedTable.create();
        CacheManager cm = CacheManager.getInstance();
        Cache cache = cm.getCache("cacheReference");
        cache.getKeys().forEach(key -> {
            org.json.JSONObject jsonObjectGlobal = new org.json.JSONObject(queryServiceJSON.structureJson(queryServiceJSON.structureJsonFromCSV((String) cache.get(key.toString()).getObjectValue()), false, new HashMap<>(), "cache"));
            if (jsonObjectGlobal.has("Input")) {
                org.json.JSONArray jsonArrayInput = jsonObjectGlobal.getJSONArray("Input");
                jsonArrayInput.forEach(o -> {
                    org.json.JSONObject jsonObject = new org.json.JSONObject(o.toString());
                    if (jsonObject.has("REFERENCE_NAME") && jsonObject.has("KEY_NAME") && jsonObject.has("REF_VALUE")) {
                        cacheData.put(jsonObject.get("REFERENCE_NAME").toString(), jsonObject.get("KEY_NAME").toString(), jsonObject.get("REF_VALUE").toString());
                    }
                });
            }
        });
        return cacheData;
    }

    public String getConnectionFile(String connectionID) throws IOException {
        return new String(Files.readAllBytes(Paths.get(_connectionDir + connectionID + ".txt")));
    }

    public Connection getInfoConnection(String connectionID) {
        try {
            Connection connection = objectMapper.readValue(getConnectionFile(connectionID), Connection.class);
            if (connection.getPwConnection() != null) {
                connection = getPWConnectionFromJar(connection.getPwConnection());
            }
            if (connection.getSCHEME() == null) {
                connection.setSCHEME("");
            }
            if (connection.getPW() == null) {
                connection.setPW("");
            } else {
                JsonNode jsonNode = constants.getConfigFileAsJson();
                if (jsonNode != null && !jsonNode.isNull()) {
                    if (jsonNode.hasNonNull("pwEncrypted") && jsonNode.get("pwEncrypted").asBoolean()) {
                        connection.setPW(aes256Cipher.decode(connection.getPW()));
                    }
                }
            }
            if (connection.getID() == null) {
                connection.setID("");
            }
            if (connection.getUrlHttp() == null) {
                connection.setUrlHttp("");
            }
            if (connection.getDRIVER() == null) {
                connection.setDRIVER("");
            } else {
                if (connection.getDRIVER().equals("org.sqlite.JDBC") && connection.getURL().trim().isEmpty()) {
                    connection.setURL("jdbc:sqlite:" + constants.getSQLiteQueryDir() + QueryServiceSQLite.SQLITE_USERID + File.separator + QueryServiceSQLite.DBFILE);
                }
            }
            if (connection.getURL() == null) {
                connection.setURL("");
            }
            if (connection.getDBMS() == null) {
                connection.setDBMS("");
            }
            if (connection.getHost() == null) {
                connection.setHost("");
            }
            if (connection.getPort() == null) {
                connection.setPort("");
            }
            if (connection.getCONNNAME() == null) {
                connection.setCONNNAME("");
            }
            if (connection.getETC() == null) {
                connection.setETC("");
            }
            if (connection.getOWNER() == null) {
                connection.setOWNER("");
            }
            return connection;
        } catch (IOException e) {
            return Connection.builder().build();
        }
    }

    private Connection getPWConnectionFromJar(String pwConnectionId) {
        try {
            String path = jarsDir + pwConnectionId + ".jar";
            File file = Paths.get(path).toFile();
            URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{file.toURI().toURL()}, this.getClass().getClassLoader());
            Class classToLoad = Class.forName("co.kr.coresolutions.jars.Main", true, urlClassLoader);
            Method method = classToLoad.getDeclaredMethod("getConnection");
            Object instance = classToLoad.newInstance();
            return objectMapper.readValue(method.invoke(instance).toString(), Connection.class);
        } catch (Exception e) {
            return Connection.builder().build();
        }
    }

    Thread processLogging(String method, String log, String logDir) {
        return new Thread(() -> {
            if (!method.equalsIgnoreCase("EhCache - cacheReference")) {
                processLoggingError(method, log, logDir);
            }
            logProcessing(method, log, logDir, false);
        });
    }

    public JsonNode getConfigFileAsJson() {
        return constants.getConfigFileAsJson();
    }

    private void processLoggingError(String method, String log, String logDir) {
        Optional.ofNullable(constants.getConfigFileAsJson())
                .ifPresent(thisNode -> {
                    if (thisNode.hasNonNull("errorLogEnabled") && thisNode.get("errorLogEnabled").isBoolean()) {
                        if (thisNode.get("errorLogEnabled").asBoolean()) {
                            if (thisNode.hasNonNull("errorKeywords") && thisNode.get("errorKeywords").isArray()) {
                                final Boolean[] firstAccess = {true};
                                StreamSupport
                                        .stream(thisNode.get("errorKeywords").spliterator(), false)
                                        .map(JsonNode::asText)
                                        .filter(log::contains)
                                        .forEach(oneKey -> {
                                            if (firstAccess[0]) {
                                                firstAccess[0] = false;
                                                logProcessing(method, log, logDir, true);
                                            }
                                        });
                            }
                        }
                    }
                });
    }

    private void logProcessing(String method, String log, String logDir, boolean isError) {
        StringBuilder sb = new StringBuilder();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String date = "[" + sdf.format(new Date()) + "] ";
        sb.append(date);
        if (!method.isEmpty()) {
            sb.append("Method executed is: ").append(method).append(" ");
        }
        sb.append(log);
        if (!Paths.get(logDir).toFile().isDirectory()) {
            try {
                Files.createDirectory(Paths.get(logDir));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        sb.append(System.lineSeparator());
        String currentDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
        String path = "qlog_" + currentDate + (isError ? "_error" : "") + ".txt";
        try {
            Files.write(Paths.get(logDir + path), sb.toString().getBytes(StandardCharsets.UTF_8),
                    StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addLogging(String method, String log) {
        Thread thread = processLogging(method, log, _logDir);
        thread.setDaemon(true);
        thread.start();
    }

    public String deleteFile(String owner, String filename) {
        try {
            Files.deleteIfExists(Paths.get(_fileQueryDir + owner + File.separator + filename));
        } catch (IOException e) {
            return "fail" + e.getMessage();
        }
        return "success";
    }

    public String getOwners() {
        JSONArray jsonArray = new JSONArray();
        Set<String> owners = new HashSet<>();
        owners.add("file");
        owners.add("project");
        try (Stream<Path> paths = Files.walk(Paths.get(_rootDir))) {
            paths
                    .filter(Files::isDirectory)
                    .filter(path ->
                            path.toString().equalsIgnoreCase(_fileQueryDir.substring(0, _fileQueryDir.length() - 1))
                                    || path.toString().equalsIgnoreCase(_projectQueryDir.substring(0, _projectQueryDir.length() - 1))
                    )
                    .forEach(path -> {
                        try (Stream<Path> pathsInside = Files.walk(path)) {
                            pathsInside.filter(Files::isDirectory)
                                    .forEachOrdered(pathinside -> {
                                        String pathname = pathinside.getFileName().toString();
                                        if (!owners.contains(pathname)) {
                                            owners.add(pathname);
                                            jsonArray.put(pathname);
                                        }
                                    });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    });
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }
        return jsonArray.toString();
    }

    public String getFormattedQuery(String query, ObjectNode json) {
        if (json.has("TEMPLATE") && json.get("TEMPLATE").size() > 0) {
            Map map = objectMapper.convertValue(json.get("TEMPLATE"), Map.class);
            Writer writer = new StringWriter();
            Mustache mustache = mustacheFactory.compile(new StringReader(query), "coreSolutionTemplate");
            mustache.execute(writer, map);
            query = writer.toString();
            try {
                writer.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }

        final String[] finalQuery = {query};
        String finalQuery1 = query;
        json.fields().forEachRemaining(stringJsonNodeEntry -> {
            if (finalQuery1.contains(stringJsonNodeEntry.getKey())) {
                finalQuery[0] = finalQuery[0].replace(stringJsonNodeEntry.getKey(), stringJsonNodeEntry.getValue().asText());
            }
        });
        finalQuery[0] = finalQuery[0].replaceAll("\\[\\[.*\\]\\]", "");
        return finalQuery[0];
    }


    public String checkValidityUpdateClauses(String query, String connectionID) {
        String resultQuery;
        if (!isConnectionIdFileExists(connectionID)) {
            return connectionID;
        }
        try {
            Connection connection = getInfoConnection(connectionID);
            String resultGetConfigFile = queryServiceFile.getConfigFile();
            int maxRunningTime = 0;
            int maxRows = 0;
            if (!resultGetConfigFile.equalsIgnoreCase("fail")) {
                OrderedJSONObject jsonobject = new OrderedJSONObject(resultGetConfigFile);
                if (jsonobject.has("MaxRunningTime")) {
                    maxRunningTime = jsonobject.getInt("MaxRunningTime");
                    System.out.println("MaxRunningTime : " + maxRunningTime);
                }
                if (jsonobject.has("MaxRows")) {
                    maxRows = jsonobject.getInt("MaxRows");
                    System.out.println("MaxRows : " + maxRows);
                }
            }

            resultQuery = dataBase.executeQuery(connection, query, maxRunningTime, maxRows);
        } catch (Exception e) {
            return "error:{" + e.getCause().getMessage() + "}";
        }
        return resultQuery;
    }

    public String updateClausesWithRollback(List<String> queries, String connectionID, String randomID) {
        String resultQuery;
        if (!isConnectionIdFileExists(connectionID)) {
            return connectionID;
        }
        try {
            Connection connection = getInfoConnection(connectionID);
            String resultGetConfigFile = queryServiceFile.getConfigFile();
            int maxRunningTime = 0;
            int maxRows = 0;
            if (!resultGetConfigFile.equalsIgnoreCase("fail")) {
                OrderedJSONObject jsonobject = new OrderedJSONObject(resultGetConfigFile);
                if (jsonobject.has("MaxRunningTime")) {
                    maxRunningTime = jsonobject.getInt("MaxRunningTime");
                    System.out.println("MaxRunningTime : " + maxRunningTime);
                }
                if (jsonobject.has("MaxRows")) {
                    maxRows = jsonobject.getInt("MaxRows");
                    System.out.println("MaxRows : " + maxRows);
                }
            }
            resultQuery = dataBase.executeBatchQueryForLater(connection, queries, maxRunningTime, maxRows, randomID);
        } catch (Exception e) {
            return "error:{" + e.getCause().getMessage() + "}";
        }
        return resultQuery;
    }

    public void rollBack(List<String> listRandomIds) {
        dataBase.rollBack(listRandomIds);
    }

    public void commit(List<String> listRandomIds) {
        dataBase.doCommit(listRandomIds);
    }

    public String FetchwithCsvJdbc(String connectionID, String sql, String queryid, String ownerId) throws IOException {
        String resultGetConfigFile = queryServiceFile.getConfigFile();
        int maxRunningTime = 0;
        int maxRows = 0;
        if (!resultGetConfigFile.equalsIgnoreCase("fail")) {
            OrderedJSONObject jsonobject = new OrderedJSONObject(resultGetConfigFile);
            if (jsonobject.has("MaxRunningTime")) {
                maxRunningTime = jsonobject.getInt("MaxRunningTime");
                System.out.println("MaxRunningTime : " + maxRunningTime);
            }
            if (jsonobject.has("MaxRows")) {
                maxRows = jsonobject.getInt("MaxRows");
                System.out.println("MaxRows : " + maxRows);
            }
        }
        Connection connection = getInfoConnection(connectionID);
        connection.setDRIVER("org.relique.jdbc.csv.CsvDriver");
        connection.setURL("jdbc:relique:csv:" + _fileQueryDir + File.separator + ownerId + "?charset=utf-8");
        queryid = makeQueryId(queryid);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("query", sql);
        jsonObject.put("isStop", false);
        sessionMap.put(queryid, jsonObject);

        String resultQuery = dataBase.executeQuerySelectJDBC(connection, connectionID, sql, queryid, false, false, -1);
        if (!resultQuery.contains("Error")) {
            JSONArray successArray = new JSONArray(resultQuery);
            if (successArray.length() > maxRows && maxRows != 0) {
                return "Error : {The results exceed a max records(" + maxRows + ") allowed in the system, Ask to Administrator}";
            }
        }
        return resultQuery;
    }

    public boolean isCommandFileExists(String commandFileName, boolean isMainCommandDir) {
        if (isMainCommandDir) {
            _commandDir = constants.getMainCommandDir();
        } else {
            _commandDir = constants.getCommandDir();
        }

        if (RunCommands.isUnix()) {
            return Files.exists(Paths.get(_commandDir + commandFileName + ".sh"));
        } else if (RunCommands.isWindows()) {
            return Files.exists(Paths.get(_commandDir + commandFileName + ".bat"));
        }
        return false;
    }

    public boolean isFileExists(String filename) {
        return Files.exists(Paths.get(_unloadQueryDir + filename));
    }

    public String executeCommandFile(String commandFileName) {
        if (RunCommands.isUnix())
            return runCommands.runOsCommand(commandFileName + ".sh");
        else if (RunCommands.isWindows())
            return runCommands.runOsCommand(commandFileName + ".bat");
        return "";
    }

    public String getFormattedCommand(String commandFileName, ObjectNode parameter, boolean isMainCommandDir) throws IOException {
        if (isMainCommandDir) {
            _commandDir = constants.getMainCommandDir();
        } else {
            _commandDir = constants.getCommandDir();
        }

        String content = "";
        if (RunCommands.isUnix()) {
            content = new String(Files.readAllBytes(Paths.get(_commandDir + commandFileName + ".sh")));
        } else if (RunCommands.isWindows()) {
            content = new String(Files.readAllBytes(Paths.get(_commandDir + commandFileName + ".bat")));
        }

        if (content.length() > 0) {
            final String[] finalCommand = {content};
            String finalContent = content;
            parameter.fields().forEachRemaining(stringJsonNodeEntry -> {
                if (finalContent.contains(stringJsonNodeEntry.getKey())) {
                    finalCommand[0] = finalCommand[0].replace(stringJsonNodeEntry.getKey(), stringJsonNodeEntry.getValue().asText());
                }
            });
            return finalCommand[0];
        }
        return "Error : some keys doesn't exists or they are wrong, Please correct them!";
    }

    public String executeCommand(String commandName, String commandAfterKeyReplace, String commandID, String owner, HttpServletResponse response) {
        return executeCommand(commandName, commandAfterKeyReplace, commandID, owner, response, "EXTERNAL");
    }

    public String executeCommand(String commandName, String commandAfterKeyReplace, String commandID, String owner, HttpServletResponse response, String runMode) {
        if (commandAfterKeyReplace != null) {
            if (semaphore != null && semaphore.availablePermits() <= 0) {
                runCommands.saveCacheCommandWaiting(commandID, commandAfterKeyReplace, owner);
            }
            if (semaphore != null) {
                semaphore.acquireUninterruptibly();
            }
            queryServiceCommand.saveCacheCommand(commandID, Command
                    .CommandDetails.builder()
                    .owner(owner)
                    .CommandID(commandID)
                    .commadName(commandName)
                    .startdatetime(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()))
                    .build());

            String runCommandResult;

            if("EXTERNAL".equals(runMode)) {
                runCommandResult = runCommands.runCommand(commandAfterKeyReplace, commandID, response);
            } else if("INTERNAL".equals(runMode)) {
                runCommandResult = runCommandsInternal.runCommand(commandAfterKeyReplace, commandID, response);
            } else {
                runCommandResult = "Error : wrong runMode";
            }

            runCommands.removeCacheCommandProcess(commandID);
            queryServiceCommand.removeCacheCommand(commandID);
            if (semaphore != null) {
                semaphore.release();
                runCommands.removeCacheCommandWaiting(commandID);
            }
            return runCommandResult;
        }
        return "Error";
    }

    public List<String> getCacheCommandsWaiting() {
        CacheManager cm = CacheManager.getInstance();
        Cache cache = cm.getCache("cacheCommandWaiting");
        List<JSONObject> jsonObjects = new ArrayList<>();
        if (cache != null) {
            List<String> cacheKeys = cache.getKeys();
            jsonObjects = cacheKeys.stream().map(oneKey -> {
                Element element = cache.get(oneKey);
                if (element != null) {
                    return (JSONObject) element.getObjectValue();
                }
                return null;
            }).collect(Collectors.toList());
        }
        return jsonObjects.stream().filter(Objects::nonNull).map(JSONObject::toString).collect(Collectors.toList());
    }

    public Map<String, String> getDataTypes(String connectionID, String tableName) {
        Map<String, Object> columns = nativeQuery.getColumns(connectionID, tableName);
        if (columns.containsKey("datatypes")) {
            return (Map<String, String>) columns.get("datatypes");
        } else return new HashMap<>();
    }

    public Map<String, String> getQueryDataTypes(String connectionID, String query, boolean isLabel) {
        Map<String, Object> columns = new HashMap<>();
        if (nativeQuery != null) {
            if (!connectionID.contains("clickhouse")) {
                query = query.toLowerCase();
            }
            columns = nativeQuery.getQueryColumns(connectionID, query, isLabel);
        }
        if (columns.containsKey("datatypes")) {
            return (Map<String, String>) columns.get("datatypes");
        } else return new HashMap<>();
    }

    public Map<String, String> getColumnsDataType(String query, String connectionID) {
        Map<String, String> ColumnsDataType = new HashMap<>();
        try {
            String tableName = (StringUtils.split(query.toLowerCase().substring(query.toLowerCase().indexOf("from") + 4)))[0];
            if (tableName.length() > 0) {
                ColumnsDataType = getDataTypes(connectionID, tableName);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ColumnsDataType;
    }

    public CustomResponseDto loadListDateTimeFunctions() {
        try {
            if (isConnectionIdFileExists("quadmax")) {
                String resultQuery = checkValidity("SELECT LOOKUP_CODE, LOOKUP_NAME FROM " + getInfoConnection("quadmax").getSCHEME() + "t_lookup_values WHERE lookup_type='I_CMS_DATE_TIME' AND LANG = 'ko_KR' AND DEL_F='N'", "quadmax", "", "cache", false);
                if (!(resultQuery.equalsIgnoreCase("[]") || resultQuery.startsWith("Error")
                        || resultQuery.startsWith("error") || resultQuery.startsWith("maxRunningTimeOut") || resultQuery.startsWith("MaxRows"))) {
                    JSONArray jsonArray = new JSONArray(resultQuery);
                    QueryService.savedDateTimeFunctions = new HashMap<>();
                    StreamSupport.stream(jsonArray.spliterator(), false).map(o -> new JSONObject(o.toString()))
                            .forEach(jsonObject -> QueryService.savedDateTimeFunctions.put(jsonObject.get("LOOKUP_CODE").toString(), "%%" + jsonObject.get("LOOKUP_NAME").toString() + "%%"));
                }
                return CustomResponseDto.ok();
            } else {
                addLogging("POST", "refresh date triggered , fail due to connection file with name quadmax doesn't exist");
                return CustomResponseDto.builder().ErrorCode(ErrorCode.ONE.getCode()).ErrorMessage("Connection file with name quadmax doesn't exist").build();
            }
        } catch (Exception e) {
            addLogging("POST", "refresh date triggered , fail due\t" + e.getMessage());
            return CustomResponseDto.builder().ErrorCode(ErrorCode.TWO.getCode()).ErrorMessage(e.getMessage()).build();
        }
    }

    public String convertGlobalDateTime(String queryBeforeConversion) {
        if (queryBeforeConversion == null) {
            return "";
        }
        QueryService.notResolvedDateTimeFunctions = new HashMap<>();
        String[] queryAfterFirstConversion = {queryBeforeConversion};
        QueryService.savedDateTimeFunctions.forEach((key, value) -> queryAfterFirstConversion[0] = queryAfterFirstConversion[0].replace(key, value));
        final String[] queryAfterConversion = {convertDynamic(convertDateTime(queryAfterFirstConversion[0]))};
        QueryService.notResolvedDateTimeFunctions
                .forEach((key, value) -> queryAfterConversion[0] = queryAfterConversion[0]
                        .replace("NOT RESOLVED" + key, getKeysByValue(QueryService.savedDateTimeFunctions, value)));
        return queryAfterConversion[0];
    }

    private String convertDynamic(String queryBeforeConversion) {
        final String[] convertedQuery = {queryBeforeConversion};
        Matcher matcher = patternCase.matcher(queryBeforeConversion);
        if (matcher.find()) {
            String subQuery = matcher.group().toUpperCase().substring(matcher.group().toUpperCase().indexOf("%%CASE_WHEN_"));
            //either range or bin
            String rangeOrBinKey = (subQuery.substring(subQuery.toUpperCase().indexOf("%%CASE_WHEN_") + 12, subQuery.indexOf("("))).trim();
            boolean isNum = rangeOrBinKey.endsWith("_NUM");
            boolean isLabel = rangeOrBinKey.endsWith("_LABEL");
            boolean isRange = rangeOrBinKey.startsWith("RANGE");
            String[] columns = subQuery.substring(subQuery.indexOf("(") + 1, subQuery.indexOf(")%%")).trim().split(",");
            String columnName = columns[0].trim();
            long min = Long.parseLong(columns[1].trim());
            long max = Long.parseLong(columns[2].trim());
            long rangeOrBinValue = Long.parseLong(columns[3].trim());
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("CASE\n");
            if (isRange) {
                long numberOfIteration = ((max - min) / rangeOrBinValue);
                final long[] end = {0};
                LongStream.range(0, numberOfIteration)
                        .forEach(value -> {
                            long start = (min + (rangeOrBinValue * value));
                            end[0] = (min + (rangeOrBinValue * (value + 1)));
                            String result = (isNum ? "" + (value + 1) + "" : isLabel ? "'" + start + "-" + end[0] + "'" : "'" + (value + 1) + "'");
                            stringBuilder
                                    .append("WHEN " + columnName + " >= " + start
                                            + " AND " + columnName + " < " + end[0] + "\nTHEN " + result + "\n");
                        });
                stringBuilder.append("ELSE "
                        + (isNum ? numberOfIteration + 1 : isLabel ? "'>" + end[0] + "'" : "'" + (numberOfIteration + 1) + "'") +
                        "\n");

            } else {
                long binValue = ((max - min + 1) / rangeOrBinValue);
                long numberOfIteration = ((max - min) / binValue);
                final int[] countIteration = {1};
                final long[] end = {0};
                LongStream.range(0, numberOfIteration)
                        .forEach(value -> {
                            long startValue = min + (binValue * value);
                            end[0] = (min + (binValue * (value + 1)));
                            String result = (isNum ? "" + (value + 1) + "" : isLabel ? "'" + startValue + "-" + end[0] + "'" : "'" + (value + 1) + "'");
                            if (startValue + binValue < max) {
                                ++countIteration[0];
                                stringBuilder
                                        .append("WHEN " + columnName + " >= "
                                                + startValue
                                                + " AND " + columnName + " < " + end[0] + "\nTHEN " + result + "\n");
                            }
                        });
                stringBuilder.append("ELSE "
                        + (isNum ? countIteration[0] : isLabel ? "'>" + end[0] + "'" : "'" + countIteration[0] + "'") +
                        "\n");
            }
            stringBuilder.append("END\n");

            convertedQuery[0] = matcher.replaceFirst(stringBuilder.toString());
        }
        matcher = patternCase.matcher(convertedQuery[0]);
        if (matcher.find()) {
            convertedQuery[0] = convertDateTime(convertedQuery[0]);
        }

        return convertedQuery[0];
    }

    public String convertDateTime(String queryBeforeConversion) {
        counterDateNR++;
        LocalDateTime localDateTime = LocalDateTime.now();
        final String[] convertedQuery = {queryBeforeConversion};
        Matcher matcher = patternDateTime.matcher(queryBeforeConversion);
        boolean isDayOfWeek = false;
        boolean isDayOfYear = false;
        if (matcher.find()) {
            String subQuery = matcher.group().toUpperCase().substring(matcher.group().toUpperCase().indexOf("%%TO_"));
            String date = (subQuery.substring(subQuery.toUpperCase().indexOf("%%TO_") + 5, subQuery.indexOf("("))).trim();
            long variableDate = Long.parseLong(subQuery.substring(subQuery.indexOf("(") + 1, subQuery.indexOf(",")).trim());
            String format = (subQuery.substring(subQuery.indexOf(",") + 1, subQuery.indexOf(")%%"))).trim();
            if ((date.equalsIgnoreCase("DAY") || date.equalsIgnoreCase("MONTH")
                    || date.equalsIgnoreCase("YEAR")) && (format.equalsIgnoreCase("YYYYMMDD") ||
                    format.equalsIgnoreCase("YYYY-MM-DD") || format.equalsIgnoreCase("YYYY/MM/DD")
                    || format.equalsIgnoreCase("YYYY") || format.equalsIgnoreCase("MM")
                    || format.equalsIgnoreCase("YYYY-MM") || format.equalsIgnoreCase("YYYY/MM")
                    || format.equalsIgnoreCase("DAYOFWEEK") || format.equalsIgnoreCase("DAYOFYEAR")
                    || format.equalsIgnoreCase("YYYYMM") || format.equalsIgnoreCase("YYYYMMFIRSTDAY")
                    || format.equalsIgnoreCase("YYYYMMLASTDAY") || format.equalsIgnoreCase("MMDD"))) {
                if (format.equalsIgnoreCase("DAYOFWEEK")) {
                    isDayOfWeek = true;
                    format = "YYYYMMDD";
                } else if (format.equalsIgnoreCase("DAYOFYEAR")) {
                    isDayOfYear = true;
                    format = "YYYYMMDD";
                }
                format = format.replace("YYYY", "yyyy");
                format = format.replace("DD", "dd");
                if (format.equalsIgnoreCase("YYYYMMFIRSTDAY") || format.equalsIgnoreCase("YYYYMMLASTDAY")) {
                    if (date.equalsIgnoreCase("MONTH")) {
                        localDateTime = localDateTime.plusMonths(variableDate);
                        localDateTime = LocalDateTime.of(LocalDate.of(localDateTime.getYear(), localDateTime.getMonthValue(),
                                format.equalsIgnoreCase("YYYYMMFIRSTDAY") ? 01 : localDateTime.toLocalDate().lengthOfMonth()), localDateTime.toLocalTime());
                        format = "yyyyMMdd";
                        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
                        convertedQuery[0] = matcher.replaceFirst(dateFormat.format(Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant())));
                    } else if (date.equalsIgnoreCase("DAY")) {
                        localDateTime = localDateTime.plusDays(variableDate);
                        localDateTime = LocalDateTime.of(LocalDate.of(localDateTime.getYear(), localDateTime.getMonthValue(), format.equalsIgnoreCase("YYYYMMFIRSTDAY") ? 01 : localDateTime.getMonth().maxLength()), localDateTime.toLocalTime());
                        format = "yyyyMMdd";
                        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
                        convertedQuery[0] = matcher.replaceFirst(dateFormat.format(Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant())));
                    } else if (date.equalsIgnoreCase("YEAR")) {
                        localDateTime = localDateTime.plusYears(variableDate);
                        localDateTime = LocalDateTime.of(LocalDate.of(localDateTime.getYear(), localDateTime.getMonthValue(), format.equalsIgnoreCase("YYYYMMFIRSTDAY") ? 01 : localDateTime.getMonth().maxLength()), localDateTime.toLocalTime());
                        format = "yyyyMMdd";
                        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
                        convertedQuery[0] = matcher.replaceFirst(dateFormat.format(Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant())));
                    }
                } else {
                    SimpleDateFormat dateFormat = new SimpleDateFormat(format);
                    dateFormat.setLenient(true);
                    String replacedDate = "";
                    if (date.equalsIgnoreCase("DAY")) {
                        replacedDate = dateFormat.format(Date.from(localDateTime.atZone(ZoneId.systemDefault()).plusDays(variableDate).toInstant()));
                    } else if (date.equalsIgnoreCase("MONTH")) {
                        replacedDate = dateFormat.format(Date.from(localDateTime.atZone(ZoneId.systemDefault()).plusMonths(variableDate).toInstant()));
                    } else if (date.equalsIgnoreCase("YEAR")) {
                        replacedDate = dateFormat.format(Date.from(localDateTime.atZone(ZoneId.systemDefault()).plusYears(variableDate).toInstant()));
                    }
                    if (!replacedDate.isEmpty()) {
                        int dayOfWeek;
                        try {
                            dayOfWeek = LocalDate.parse(replacedDate, DateTimeFormatter.ofPattern(format)).getDayOfWeek().getValue();
                        } catch (Exception e) {
                            dayOfWeek = 0;
                        }
                        dayOfWeek = dayOfWeek > 6 ? 0 : dayOfWeek;
                        convertedQuery[0] = matcher
                                .replaceFirst(isDayOfWeek ? String.valueOf(dayOfWeek) : isDayOfYear ? String.valueOf(LocalDate.parse(replacedDate,
                                        DateTimeFormatter.ofPattern(format)).getDayOfYear()) : replacedDate);
                    }
                }
            } else if ((date.equalsIgnoreCase("HOUR") || date.equalsIgnoreCase("MINUTE")
                    || date.equalsIgnoreCase("SECOND"))
                    && (format.equalsIgnoreCase("HHMMSS") || format.equalsIgnoreCase("HH:MM:SS")
                    || format.equalsIgnoreCase("HH") || format.equalsIgnoreCase("MM")
                    || format.equalsIgnoreCase("HH:MM") || format.equalsIgnoreCase("HHMM"))) {
                format = format.replace("MM", "mm");
                format = format.replace("SS", "ss");
                SimpleDateFormat dateFormat = new SimpleDateFormat(format);
                dateFormat.setLenient(true);
                if (date.equalsIgnoreCase("HOUR")) {
                    convertedQuery[0] = matcher
                            .replaceFirst(dateFormat.format(Date.from(localDateTime.atZone(ZoneId.systemDefault()).plusHours(variableDate).toInstant())));
                } else if (date.equalsIgnoreCase("MINUTE")) {
                    convertedQuery[0] = matcher
                            .replaceFirst(dateFormat.format(Date.from(localDateTime.atZone(ZoneId.systemDefault()).plusMinutes(variableDate).toInstant())));
                } else if (date.equalsIgnoreCase("SECOND")) {
                    convertedQuery[0] = matcher
                            .replaceFirst(dateFormat.format(Date.from(localDateTime.atZone(ZoneId.systemDefault()).plusSeconds(variableDate).toInstant())));
                }
            } else {
                convertedQuery[0] = convertedQuery[0].replace("%%TO_" + date + "(" + variableDate + "," + format + ")%%", "NOT RESOLVED" + counterDateNR);
                notResolvedDateTimeFunctions.put(counterDateNR, "%%TO_" + date + "(" + variableDate + "," + format + ")%%");
            }
        }
        matcher = patternDateTime.matcher(convertedQuery[0]);
        if (matcher.find()) {
            convertedQuery[0] = convertDateTime(convertedQuery[0]);
        }

        return convertedQuery[0];
    }

    public void setRunStop(String queryId) {
        clearResources(queryId);
    }

    public boolean isRunStop(String queryId) {
        boolean rt;
        if (queryId.equalsIgnoreCase("cache")) {
            rt = false;
        } else {
            JSONObject jsonObject = QueryService.sessionMap.get(queryId);
            if (jsonObject != null) {
                rt = (boolean) jsonObject.get("isStop");
            } else {
                rt = true;
            }
        }
        return rt;
    }

    public String executeQueryStop(String queryID) {
        String query;
        try {
            JSONObject runqueryMap = QueryService.sessionMap.get(queryID);
            if (runqueryMap != null) {
                query = (String) runqueryMap.get("query");
                runqueryMap.put("isStop", true);
                QueryService.sessionMap.put(queryID, runqueryMap);
                JSONObject jsonObject = QueryService.sessionMap.get(queryID);
                if (jsonObject != null) {
                    if (jsonObject.has("con")) {
                        Statement statement = (Statement) jsonObject.get("con");
                        try {
                            if (statement != null && !statement.isClosed()) {
                                statement.cancel();
                                statement.close();
                            }
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                    }
                }
            } else {
                return "there is no session with queryID: " + queryID;
            }
        } catch (Exception e) {
            return "THREAD Exception";
        }
        return "stop Queryid : " + queryID + " Query : " + query;
    }

    public static boolean isCacheQueryIDExist(String queryID) {
        return HelperUtil.queryIDObjectMultiMap.containsKey(queryID);
    }

    public String getExecQueryList() {
        JSONArray myArray = new JSONArray();
        Iterator iter = sessionMap.keySet().iterator();
        JSONObject jsonObject;
        while (iter.hasNext()) {
            String key = (String) iter.next();
            jsonObject = new JSONObject();
            jsonObject.put("queryid", key);
            jsonObject.put("startTime", sessionMap.get(key).get("startTime"));
            jsonObject.put("runningTime", sessionMap.get(key).get("runningTime"));
            jsonObject.put("query", sessionMap.get(key).get("query"));
            jsonObject.put("userid", sessionMap.get(key).get("userid"));

            myArray.put(jsonObject);
        }
        return myArray.toString();
    }

    public boolean ExistQueryId(String queryid) {
        return sessionMap.containsKey(queryid);
    }

    public String getFileFromDir(String fileName, String dir) {
        try {
            return new String(Files.readAllBytes(Paths.get(_rootDir + dir + File.separator + fileName)));
        } catch (IOException e) {
            return "";
        }
    }


    public String checkValidityWithLabel(String query, String connectionID, String sqliteUrlDB, String queryID, String dbFile, boolean systemPredicates) {
        String resultQuery;
        String cntResult;

        if (!isConnectionIdFileExists(connectionID)) {
            return connectionID;
        }

        Connection infoConnection = getInfoConnection(connectionID);
        if (!sqliteUrlDB.isEmpty() && dbFile != null) {
            synchronized (QueryServiceSQLite.SQLITE_USERID) {
                // in this method sqliteUrlDB is the userID
                QueryServiceSQLite.SQLITE_USERID = sqliteUrlDB;
                synchronized (QueryServiceSQLite.DBFILE) {
                    if (!dbFile.isEmpty()) {
                        QueryServiceSQLite.DBFILE = dbFile;
                    } else {
                        QueryServiceSQLite.DBFILE = "mydbfile.db";
                    }
                }
            }
        }
        String resultGetConfigFile = queryServiceFile.getConfigFile();
        int maxRunningTime = 0;
        int maxRows = 0;
        if (!resultGetConfigFile.equalsIgnoreCase("fail")) {
            OrderedJSONObject jsonobject = new OrderedJSONObject(resultGetConfigFile);
            if (systemPredicates) {
                if (jsonobject.has("MaxRunningTime")) {
                    maxRunningTime = jsonobject.getInt("MaxRunningTime");
                    System.out.println("MaxRunningTime : " + maxRunningTime);
                }
                if (jsonobject.has("MaxRows")) {
                    maxRows = jsonobject.getInt("MaxRows");
                    System.out.println("MaxRows : " + maxRows);
                }
            }
        }

        queryID = makeQueryId(queryID);
        String finalQueryID = queryID;
        Future<String> future = executorService.submit(() ->
                dataBase.getQueryCountJDBC(connectionID, query, finalQueryID));

        try {
            cntResult = systemPredicates && maxRunningTime > 0 ? future.get(maxRunningTime, TimeUnit.SECONDS) : future.get();
        } catch (TimeoutException e) {
            future.cancel(true);
            return "Error : { query timeout - " + queryID + " }";
        } catch (InterruptedException e) {
            return "Error : { query  " + queryID + " killed by user request }";
        } catch (ExecutionException e) {
            return "Error : { query  " + queryID + " existed with internal issue: " + e.getMessage() + " }";
        }
        if (!cntResult.equals("")) {
            if (!cntResult.toLowerCase().contains("error")) {
                JSONArray successArray = new JSONArray(cntResult.toLowerCase());
                if (successArray.getJSONObject(0).getInt("cnt") > maxRows && maxRows != 0) {
                    clearResources(queryID);
                    return "Error : {The results exceed a max records(" + maxRows + ") allowed in the system, Ask to Administrator}";
                }
            } else {
                clearResources(queryID);
                return cntResult;
            }
        }

        future = executorService.submit(() ->
                dataBase
                        .executeQuerySelectJDBC
                                (infoConnection, connectionID, query, finalQueryID, true, false, -1));
        try {
            resultQuery = systemPredicates && maxRunningTime > 0 ? future.get(maxRunningTime, TimeUnit.SECONDS) : future.get();
        } catch (TimeoutException e) {
            future.cancel(true);
            return "Error : { query timeout - " + queryID + " }";
        } catch (InterruptedException e) {
            return "Error : { query  " + queryID + " killed by user request }";
        } catch (ExecutionException e) {
            return "Error : { query  " + queryID + " existed with internal issue: " + e.getMessage() + " }";
        } finally {
            clearResources(queryID);
        }

        synchronized (QueryServiceSQLite.SQLITE_USERID) {
            // in this method sqliteUrlDB is the userID
            QueryServiceSQLite.SQLITE_USERID = "mss";
            synchronized (QueryServiceSQLite.DBFILE) {
                QueryServiceSQLite.DBFILE = "mydbfile.db";
            }
        }
        return resultQuery;
    }


    public Map<String, Object> getCountQuery(String connectionID, String query, String queryID) {
        Map<String, Object> objectMap = new HashMap<>();
        String cntResult;
        Future<String> future = Executors.newCachedThreadPool().submit(() ->
                dataBase.getQueryCountJDBC(connectionID, query, queryID));

        try {
            cntResult = future.get();
        } catch (InterruptedException e) {
            cntResult = "Error : { query  " + queryID + " killed by user request }";
        } catch (ExecutionException e) {
            cntResult = "Error : { query  " + queryID + " existed with internal issue: " + e.getMessage() + " }";
        } finally {
            clearResources(queryID);
        }

        int count;
        if (!cntResult.isEmpty()) {
            if (!cntResult.toLowerCase().contains("error")) {
                count = new JSONArray(cntResult.toLowerCase()).getJSONObject(0).getInt("cnt");
                objectMap.put("count", count);
            } else {
                objectMap.put("error", cntResult);
            }
        } else {
            objectMap.put("error", "can't get query count rows!");
        }

        return objectMap;
    }

    public CustomResponseDto loadInChunk(UnLoad unLoad, String connectionID, String query, String fileName, String format, boolean isUnLoad, int maxRowsForSingleCommit) {
        Connection infoConnection = getInfoConnection(connectionID);
        CustomResponseDto customResponseDto = CustomResponseDto.builder().Success(true).message("Success").build();
        int querySize = 0;
        //get full query count
        Map<String, Object> objectMap = getCountQuery(connectionID, query, QueryService.queryID);
        if (objectMap.containsKey("error")) {
            customResponseDto.setSuccess(false);
            customResponseDto.setMessage(objectMap.get("error").toString());
        } else {
            if (objectMap.containsKey("count")) {
                querySize = Integer.parseInt(objectMap.get("count").toString());
            }
        }
        if (unLoad == null) {
            String resultUnload = dataBase.unloadQuerySelect(infoConnection, connectionID, "", query, maxRowsForSingleCommit);
            if (!resultUnload.equalsIgnoreCase("success")) {
                customResponseDto.setMessage("fail due " + resultUnload);
                customResponseDto.setSuccess(false);
            }
        } else {
            queryChunkPublisher.attach(queryServiceLoad, unLoad, format, fileName, infoConnection.getDBMS(), isUnLoad);
            String resultUnload = dataBase.unloadQuerySelect(infoConnection, connectionID, unLoad.getUnloadDetails().getUnloadID(), query, maxRowsForSingleCommit);
            queryChunkPublisher.detach(queryServiceLoad);
            if (!resultUnload.equalsIgnoreCase("success")) {
                customResponseDto.setMessage("fail due " + resultUnload);
                customResponseDto.setSuccess(false);
            }
        }
        if (customResponseDto.getSuccess()) {
            customResponseDto.setMessage(String.valueOf(querySize));
        }
        return customResponseDto;
    }

    public CustomResponseDto getResultQuery(String connectionID, String query) {
        boolean isConnectionFileExists = isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            return CustomResponseDto.builder().ErrorMessage("fail due to connection file with name\t" + connectionID + "\tdoesn't exist").build();
        }

        String queryID = UUID.randomUUID().toString();
        String resultValidity = checkValidityWithLabel(query, connectionID, "", queryID, "", false);
        clearResources(queryID);
        if (resultValidity.toLowerCase().startsWith("error") || resultValidity.equalsIgnoreCase("[]")
                || resultValidity.startsWith("maxRunningTimeOut") || resultValidity.startsWith("MaxRows")) {
            return CustomResponseDto.builder().ErrorMessage("fail due " + resultValidity).build();
        }
        return CustomResponseDto.builder().message(resultValidity).Success(true).build();
    }

    public CustomResponseDto getResultQueryCUD(String connectionID, String query, JsonNode jsonNode, Integer limit) {
        boolean isConnectionFileExists = isConnectionIdFileExists(connectionID);
        if (!isConnectionFileExists) {
            return CustomResponseDto.builder().ErrorMessage("fail due to connection file with name\t" + connectionID + "\tdoesn't exist").build();
        }

        List<String> queries = new ArrayList<>();
        if (query.toLowerCase().startsWith("insert") && jsonNode.size() > 0) {
            if (query.toUpperCase().contains("VALUES") && query.length() > 16) {
                String valuesToInsert = query.substring(query.toUpperCase().indexOf("VALUES") + 6);
                String partSqlBeforeValues = query.substring(0, query.toUpperCase().indexOf("VALUES") + 6);
                StringBuilder finalQuery = new StringBuilder(partSqlBeforeValues);
                final boolean[] accessAtLeast = {false};
                StreamSupport.stream(jsonNode.spliterator(), false).forEach(o -> {
                    JSONObject jsonObject = new JSONObject(o.toString());
                    final String[] oneValueInsert = {valuesToInsert};
                    jsonObject.keySet().forEach(key -> {
                        oneValueInsert[0] = oneValueInsert[0].toUpperCase().replace("?" + key.toUpperCase(), jsonObject.get(key).toString());
                        accessAtLeast[0] = true;
                    });
                    finalQuery.append(oneValueInsert[0]).append(",");
                });
                if (accessAtLeast[0]) {
                    queries.add(finalQuery.toString().substring(0, finalQuery.toString().length() - 1));
                } else {
                    queries.add(query);
                }
            } else {
                queries.add(query);
            }
        }

        if ((queries.isEmpty() && query.toLowerCase().startsWith("update")) && jsonNode.size() > 0) {
            StreamSupport.stream(jsonNode.spliterator(), false).forEach(o -> {
                final String[] finalQuery = {query};
                JSONObject jsonObject = new JSONObject(o.toString());
                jsonObject.keySet().forEach(key -> finalQuery[0] = finalQuery[0].toUpperCase().replace("?" + key.toUpperCase(), jsonObject.get(key).toString()));
                queries.add(finalQuery[0]);
            });
        } else {
            if (queries.isEmpty()) {
                queries.add(query);
            }
        }

        String resultValidity = nativeQuery.executeBatchQuery(connectionID, queries);
        if (resultValidity.toLowerCase().startsWith("error") || resultValidity.equalsIgnoreCase("[]")
                || resultValidity.startsWith("maxRunningTimeOut") || resultValidity.startsWith("MaxRows")) {
            return CustomResponseDto.builder().ErrorMessage("fail due " + resultValidity).build();
        }
        return CustomResponseDto.builder().message(resultValidity).Success(true).build();
    }

    public String getBaseUrl(HttpServletRequest request, boolean withContextPath) {
        String scheme = request.getScheme();
        int port = request.getServerPort();
        return scheme + "://" + request.getServerName()
                + ((("http".equals(scheme) && port == 80) || ("https".equals(scheme) && port == 443)) ? "" : ":" + port)
                + (withContextPath ? request.getContextPath() : "");
    }

    public synchronized String makeQueryId(String queryId) {
        if (queryId.equalsIgnoreCase("")) {
            queryId = Util.getUniqueId();
        }

        JSONObject runQueryJson = new JSONObject();
        runQueryJson.put("isStop", false);
        runQueryJson.put("userid", "");
        runQueryJson.put("startTime", "");
        runQueryJson.put("runningTime", "");
        runQueryJson.put("query", queryId);
        sessionMap.put(queryId, runQueryJson);

        return queryId;
    }

    public boolean removeCacheQueryID(String queryID) {
        if (queryID != null && !queryID.equalsIgnoreCase("cache")) {
            executeQueryStop(queryID);
            return HelperUtil.queryIDObjectMultiMap.remove(queryID) != null;
        }
        return false;
    }

    public void saveCacheQueryID(String queryID, Monitor monitor) {
        HelperUtil.queryIDObjectMultiMap.put(queryID, monitor);
    }

    public Object encrypt(HttpServletRequest request, boolean encrypt) {
        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        String matchPattern = (String) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);
        String text = new AntPathMatcher().extractPathWithinPattern(matchPattern, path);
        return encrypt ? passwordEncoder.encode(text) : aes256Cipher.decode(text);
    }

    public synchronized void clearResources(String queryID) {
        JSONObject jsonObject = sessionMap.get(queryID);
        if (jsonObject != null) {
            if (jsonObject.has("con")) {
                Statement statement = (Statement) jsonObject.get("con");
                try {
                    if (statement != null && !statement.isClosed()) {
                        statement.cancel();
                        statement.close();
                    }
                } catch (SQLException ignored) {
                }
            }
            sessionMap.remove(queryID);
        }
    }
    public Object getStatusAuthorization() {
        JsonNode jsonNodes = constants.getConfigFileAsJson();
        CustomResponseDto customResponseDto = CustomResponseDto.builder().Success(true).build();
        customResponseDto.setMessage("N");
        Optional.ofNullable(jsonNodes)
                .ifPresent(jsonNode -> {
                    if (jsonNode.hasNonNull("authorizationOption") && jsonNode.get("authorizationOption").isBoolean()) {
                        customResponseDto.setMessage(jsonNode.get("authorizationOption").asBoolean() ? "Y" : "N");
                    }
                });
        return customResponseDto;
    }

}
