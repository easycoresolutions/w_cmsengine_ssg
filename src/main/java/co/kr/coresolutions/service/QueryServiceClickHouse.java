package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.Impl.QueryLoadFacade;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.TableLoadClickHouse;
import co.kr.coresolutions.model.UnLoad;
import co.kr.coresolutions.util.Util;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.yandex.clickhouse.ClickHouseConnection;
import ru.yandex.clickhouse.ClickHouseDriver;
import ru.yandex.clickhouse.ClickHouseStatement;
import ru.yandex.clickhouse.domain.ClickHouseFormat;
import ru.yandex.clickhouse.settings.ClickHouseProperties;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class QueryServiceClickHouse {
    private final Constants constants;
    private final QueryService queryService;
    private final ObjectMapper objectMapper;
    private final RunCommands runCommands;
    private String fileQueryDir;
    private String fieldMappingDir;
    private String unloadQueryDir;

    @PostConstruct
    public void init() {
        fileQueryDir = constants.getFileQueryDir();
        unloadQueryDir = constants.getUnloadDir();
        fieldMappingDir = constants.getFieldMappingDir();
    }

    public CustomResponseDto tableLoad(TableLoadClickHouse tableLoadClickHouse) {
        String inputDir = tableLoadClickHouse.getInputDirectory();
        String userID = tableLoadClickHouse.getUserID();
        String metaFile = Util.replaceAll(tableLoadClickHouse.getFileName(), ".csv", "_meta.txt");
        Path pathMeta = null;
        if (inputDir.equalsIgnoreCase("file")
                && Files.exists(Paths.get(fileQueryDir + userID + File.separator + metaFile))) {
            pathMeta = Paths.get(fileQueryDir + userID + File.separator + metaFile);
        } else if (inputDir.equalsIgnoreCase("unload")
                && Files.exists(Paths.get(unloadQueryDir + userID + File.separator + metaFile))) {
            pathMeta = Paths.get(unloadQueryDir + userID + File.separator + metaFile);
        }

        StringBuilder stringBuilder = new StringBuilder();

        if (pathMeta != null) {
            try {
                JsonNode jsonNode = objectMapper.readTree(Files.readAllBytes(pathMeta));
                Map<String, String> hashTable = new Hashtable<>();
                if (jsonNode.has("dbms")) {
                    String dbms = jsonNode.get("dbms").asText();
                    MappingIterator<Map> maps = new CsvMapper()
                            .readerFor(Map.class)
                            .with(CsvSchema.emptySchema().withHeader())
                            .readValues(Files.newInputStream(Paths.get(fieldMappingDir + dbms + ".txt")));
                    maps.readAll().stream().filter(map1 -> map1.containsKey(dbms) && map1.containsKey("CLICKHOUSE"))
                            .forEach(map1 -> hashTable.put(map1.get(dbms).toString(), map1.get("CLICKHOUSE").toString()));

                }
                if (jsonNode.has("fields") && jsonNode.get("fields").isObject()) {
                    Map finalMap = hashTable;
                    jsonNode.get("fields").fields().forEachRemaining(columnNameValueEntry -> {
                        stringBuilder.append(columnNameValueEntry.getKey()).append(" ");
                        JsonNode jsonNodeValue = columnNameValueEntry.getValue();
                        if (jsonNodeValue.has("type") && jsonNodeValue.get("type").isTextual()) {
                            stringBuilder.append("Nullable (").append(!finalMap.isEmpty() ? finalMap.get(jsonNodeValue.get("type").asText())
                                    : jsonNodeValue.get("type").asText()).append(")").append(",");
//                            if(jsonNodeValue.has("length") && jsonNodeValue.get("length").isNumber()){
//                                stringBuilder.append("(").append(jsonNodeValue.get("length").asText()).append("),");
//                            }else {
//                                stringBuilder.append(",");
//                            }
                        } else {
                            stringBuilder.append("Nullable(String),");
                        }
                    });
                    if (stringBuilder.length() > 0) {
                        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                    }
                }
            } catch (IOException e) {
                return CustomResponseDto.builder().message(e.getMessage()).build();
            }
        }
        return execute(tableLoadClickHouse, userID, inputDir, stringBuilder.toString());
    }

    private CustomResponseDto execute(TableLoadClickHouse tableLoadClickHouse, String userID, String inputDir, String createContent) {
        String database = tableLoadClickHouse.getDatabase();
        String tableName = tableLoadClickHouse.getTableName();
        String fileName = tableLoadClickHouse.getFileName();
        String method = tableLoadClickHouse.getMethod();
        String queryID = tableLoadClickHouse.getClickHouseDetails().getQueryID();

        Connection localConnection = queryService.getInfoConnection(tableLoadClickHouse.getConnectionID());
        String filePath = (inputDir.equals("unload") ? unloadQueryDir : fileQueryDir) + (userID + File.separator + fileName);
        if (method != null && method.equalsIgnoreCase("cli")) {

            String commandStandalone;
            String firstQueries = "DROP TABLE IF EXISTS " + tableName + ";\n" +
                    "CREATE TABLE IF NOT EXISTS " + tableName + "( " + createContent + " ) ENGINE=File(CSVWithNames);";
            String insertQuery = "\"INSERT INTO " + tableName + " FORMAT " + (tableLoadClickHouse.getHeader() ? "CSVWithNames" : "CSV") + "\" < " + filePath;

            Path pathHelper = null;
            try {
                pathHelper = Files.write(Paths.get(fileQueryDir + tableLoadClickHouse.getTableName() + "_" + UUID.randomUUID().toString() + "_" +
                                QueryServiceSQLite.SIMPLE_DATE_FORMAT.format(new Date()) + ".sql"),
                        firstQueries.getBytes(), StandardOpenOption.CREATE_NEW, StandardOpenOption.TRUNCATE_EXISTING);
                if (RunCommands.isUnix() || RunCommands.isMac()) {
                    String props = "--host=" + (localConnection.getHost().isEmpty() ? "localhost" : localConnection.getHost())
                            + " --port=" + (localConnection.getPort().isEmpty() ? 9000 : localConnection.getPort())
                            + " --user=" + (localConnection.getID().isEmpty() ? "default" : localConnection.getID())
                            + (localConnection.getPW().isEmpty() ? "" : " --password=" + localConnection.getPW())
                            + " --database=" + database;
                    commandStandalone = "clickhouse-client " + props + " --multiquery < " + pathHelper.toFile().getAbsolutePath();
                    if (!QueryLoadFacade.sessionClickHouseImport.containsKey(queryID)) {
                        return CustomResponseDto.builder().message("Error : killed by request!").build();
                    }
                    String resultExecution = runCommands.runStandaloneCommand(commandStandalone);
                    if (resultExecution.startsWith("Error") || resultExecution.contains("DB::Exception") || resultExecution.startsWith("Bad arguments")) {
                        return CustomResponseDto.builder().message(resultExecution).build();
                    }
                    if (!QueryLoadFacade.sessionClickHouseImport.containsKey(queryID)) {
                        return CustomResponseDto.builder().message("Error : killed by request!").build();
                    }
                    resultExecution = runCommands.runStandaloneCommand("clickhouse-client " + props + " --query=" + insertQuery);
                    if (resultExecution.startsWith("Error") || resultExecution.contains("DB::Exception") || resultExecution.startsWith("Bad arguments")) {
                        return CustomResponseDto.builder().message(resultExecution).build();
                    }
                } else {
                    return CustomResponseDto.builder().message("can't run clickHouse under this OS!").build();
                }

            } catch (Exception e) {
                return CustomResponseDto.builder().message(e.getMessage()).build();
            } finally {
                try {
                    Files.deleteIfExists(pathHelper);
                } catch (IOException e) {
                    return CustomResponseDto.builder().message(e.getMessage()).build();
                }
            }

        } else {
            try {
                ClickHouseProperties clickHouseProperties = new ClickHouseProperties();
                clickHouseProperties.setDatabase(database);
                clickHouseProperties.setHost(localConnection.getHost());
                clickHouseProperties.setPort(localConnection.getPort().isEmpty() ? 9000 : Integer.parseInt(localConnection.getPort()));
                clickHouseProperties.setUser(localConnection.getID());
                clickHouseProperties.setPassword(localConnection.getPW());
                try (ClickHouseConnection connection = new ClickHouseDriver().connect(localConnection.getURL(), clickHouseProperties);
                     ClickHouseStatement clickHouseStatement = connection.createClickHouseStatement()) {
                    if (!QueryLoadFacade.sessionClickHouseImport.containsKey(queryID)) {
                        return CustomResponseDto.builder().message("Error : killed by request!").build();
                    }
                    clickHouseStatement.executeUpdate("drop table if exists " + tableName);
                    if (!QueryLoadFacade.sessionClickHouseImport.containsKey(queryID)) {
                        return CustomResponseDto.builder().message("Error : killed by request!").build();
                    }
                    clickHouseStatement.executeUpdate("create table if not exists " + tableName + "(" + createContent + ") ENGINE=File(CSVWithNames)");
                    if (!QueryLoadFacade.sessionClickHouseImport.containsKey(queryID)) {
                        return CustomResponseDto.builder().message("Error : killed by request!").build();
                    }
                    clickHouseStatement
                            .write()
                            .option("format_csv_delimiter", ",")
                            .option("format_csv_allow_single_quotes", "1")
                            .sendToTable(tableName,
                                    Files.newInputStream(Paths.get(filePath)),
                                    tableLoadClickHouse.getHeader() ? ClickHouseFormat.CSVWithNames : ClickHouseFormat.CSV);
                    if (!QueryLoadFacade.sessionClickHouseImport.containsKey(queryID)) {
                        return CustomResponseDto.builder().message("Error : killed by request!").build();
                    }
                }
            } catch (SQLException | IOException e) {
                return CustomResponseDto.builder().message(e.getMessage()).build();
            }
        }

        return CustomResponseDto.ok();
    }

    public List<UnLoad.UnloadDetails> getCacheUnLoad() {
        return new ArrayList<>(QueryLoadFacade.sessionUnload.values());
    }

    public boolean removeCacheUnload(String unloadID) {
        QueryLoadFacade.sessionUnloadError.put(unloadID, "/unload2 killed by request!");
        return QueryLoadFacade.sessionUnload.remove(unloadID) != null;
    }

    public CustomResponseDto getListProcess(String connectionID) {
        return queryService.getResultQuery(connectionID, "SELECT query_id ,query FROM system.processes;");
    }

    public CustomResponseDto removeProcess(String connectionID, String queryID) {
        String resultDelete = queryService.checkValidityUpdateClauses(connectionID, "ALTER TABLE system.processes DELETE WHERE query_id = '" + queryID + "';");
        if (!resultDelete.isEmpty() && !resultDelete.startsWith("success")) {
            return CustomResponseDto.builder().message(resultDelete).build();
        }
        return CustomResponseDto.ok();
    }

    public Object getCacheImport() {
        return new ArrayList<>(QueryLoadFacade.sessionClickHouseImport.values());
    }

    public boolean removeCacheClickHouseImport(String queryID) {
        return QueryLoadFacade.sessionClickHouseImport.remove(queryID) != null;
    }

    public CustomResponseDto listTables(String connectionID, String dbName) {
        return queryService.getResultQuery(connectionID, "SHOW TABLES FROM " + dbName + ";");
    }
}
