package co.kr.coresolutions.service;

import co.kr.coresolutions.model.QueryLayout;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class QueryServiceLayout {
    private boolean indexExists = false;
    private final Constants constants;
    private final ObjectMapper objectMapper;
    private String _layoutQueryDir;
    private boolean ownerExists = false;
    private boolean fail = false;

    @PostConstruct
    public void init() {
        _layoutQueryDir = constants.getLayoutQueryDir();
    }

    public String saveLayout(String owner, String querylayoutid, QueryLayout queryLayout) throws IOException {
        indexExists = false;
        ownerExists = false;
        if (!Files.isDirectory(Paths.get(_layoutQueryDir)))
            Files.createDirectory(Paths.get(_layoutQueryDir));

        fail = false;
        try (Stream<Path> paths = Files.walk(Paths.get(_layoutQueryDir))) {
            paths
                    .filter(Files::isDirectory)
                    .forEach(path -> {
                        try (Stream<Path> path1 = Files.walk(Paths.get(_layoutQueryDir + File.separator + owner))) {
                            ownerExists = true;
                            path1
                                    .filter(Files::isRegularFile)
                                    .forEach(path2 -> {
                                        if (path2.getFileName().toString().equalsIgnoreCase("index.txt")) {
                                            indexExists = true;
                                        }
                                    });
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }

                    });

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (fail) return "fail";
        Path ownerPath = Paths.get(_layoutQueryDir + File.separator + owner);

        if (!ownerExists) {
            ownerExists = true;
            if (!Files.isDirectory(Paths.get(_layoutQueryDir + File.separator + owner)))
                ownerPath = Files.createDirectory(ownerPath);
        }

        //if(!Files.isDirectory(Paths.get(_layoutQueryDir+File.separator+owner+File.separator+"layouts")))
        // Files.createDirectory(Paths.get(_layoutQueryDir+File.separator+owner+File.separator+"layouts"));


        if (ownerExists) {
            JSONArray jsonArray = null;
            byte[] QUERYlAYOUT = objectMapper.writeValueAsBytes(queryLayout);
            JSONObject jsonObject1 = new JSONObject(new String(QUERYlAYOUT));
            JSONObject QueryLAYOUTWithoutJsonField = new JSONObject(jsonObject1, Arrays.asList(JSONObject.getNames(jsonObject1)).stream().filter(s ->
                    !s.equals("querylayoutjson")
            ).collect(Collectors.toList()).toArray(new String[JSONObject.getNames(jsonObject1).length]));

            Files.write(Paths.get(ownerPath + File.separator + querylayoutid + ".txt"), QUERYlAYOUT,
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

            if (!indexExists) {
                jsonArray = new JSONArray();
                jsonArray.put(QueryLAYOUTWithoutJsonField);
                Files.write(Paths.get(ownerPath + File.separator + "index.txt"),
                        jsonArray.toString().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE);
            } else {
                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(ownerPath + File.separator + "index.txt"))));
                JsonArray _jsonArraytemp = jsonElement.getAsJsonArray();
                JSONArray returnArray = new JSONArray();

                _jsonArraytemp.iterator().forEachRemaining(e -> {
                    org.json.JSONObject _temp = new org.json.JSONObject(e.toString());
                    if (_temp.has("querylayoutID")) {
                        if (!_temp.get("querylayoutID").toString().equalsIgnoreCase(querylayoutid) && queryLayout.getQuerylayoutID().equalsIgnoreCase(querylayoutid))
                            returnArray.put(_temp);
                    }
                });

                returnArray.put(QueryLAYOUTWithoutJsonField);
                Files.write(Paths.get(ownerPath + File.separator + "index.txt"), returnArray.toString().getBytes(StandardCharsets.UTF_8),
                        StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
            }

        }

        return "success";
    }

    public String getLayouts(String owner) throws IOException {
        if (!Files.isDirectory(Paths.get(_layoutQueryDir)))
            Files.createDirectory(Paths.get(_layoutQueryDir));

        try {
            return new String(Files.readAllBytes(Paths.get(_layoutQueryDir + File.separator + owner + File.separator + "index.txt")), StandardCharsets.UTF_8);
        } catch (IOException e1) {
            e1.printStackTrace();
            return "fail";
        }
    }

    public String getLayout(String owner, String layoutqueryid) throws IOException {
        if (!Files.isDirectory(Paths.get(_layoutQueryDir)))
            Files.createDirectory(Paths.get(_layoutQueryDir));

        if (!Files.isDirectory(Paths.get(_layoutQueryDir + File.separator + owner))) {
            return "failowner";
        } else {
            try {
                return new String(Files.readAllBytes(Paths.get(_layoutQueryDir + File.separator + owner + File.separator + layoutqueryid + ".txt")), StandardCharsets.UTF_8);
            } catch (IOException e) {
                e.printStackTrace();
                return "faillayoutid";
            }
        }
    }

    public boolean deleteLayout(String owner, String layoutid) throws IOException {
        if (!Files.isDirectory(Paths.get(_layoutQueryDir)))
            Files.createDirectory(Paths.get(_layoutQueryDir));

        if (!Files.isDirectory(Paths.get(_layoutQueryDir + File.separator + owner))) {
            return false;
        } else {
            try {
                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(_layoutQueryDir + File.separator + owner + File.separator + "index.txt"))));
                JsonArray _jsonArraytemp = jsonElement.getAsJsonArray();

                JSONArray returnArray = new JSONArray();
                _jsonArraytemp.iterator().forEachRemaining(e -> {
                    org.json.JSONObject _temp = new org.json.JSONObject(e.toString());
                    if (!_temp.get("querylayoutID").toString().equalsIgnoreCase(layoutid))
                        returnArray.put(_temp);
                });

                try {
                    Files.write(Paths.get(_layoutQueryDir + File.separator + owner + File.separator + "index.txt"), returnArray.toString().getBytes(StandardCharsets.UTF_8),
                            StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                return Files.deleteIfExists(Paths.get(_layoutQueryDir + File.separator + owner + File.separator + layoutid + ".txt"));
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

}
