package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.jpa.domain.User;
import co.kr.coresolutions.model.Connection;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class QueryServiceAuth {
    private final QueryServiceSession queryServiceSession;
    //    private final PasswordEncoder passwordEncoder;
    private final QueryService queryService;
//    private static final String COMPLEX_PASSWORD_REGEX = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@,+<>/?`^_=*~;:\".&|!(){}\\[\\]'#$%]).{10,16})";

    public CustomResponseDto getDetails(String userId) {
        List<String> internalAuth = new ArrayList<>();
        final String[] resultSelect = {""};
        final String[] connectionID = {""};
//        final boolean[] exceedPwChanged = {false};
//        final boolean[] failALlowedLoginExceed = {false};
        Optional.ofNullable(queryServiceSession.readFile())
                .ifPresent(jsonNode -> {
                    if (jsonNode.hasNonNull("LogonConnectionId") && jsonNode.get("LogonConnectionId").isTextual()) {
                        connectionID[0] = jsonNode.get("LogonConnectionId").asText();
                        Connection infoConnection = queryService.getInfoConnection(connectionID[0]);
                        resultSelect[0] = queryService
                                .checkValidity("SELECT * FROM " + infoConnection.getSCHEME() + "T_EMP_AUTH WHERE EMP_ID = '" + userId + "'", connectionID[0], "", "cache", false);

                        if (!(resultSelect[0].equalsIgnoreCase(connectionID[0]) || resultSelect[0].startsWith("Error") || resultSelect[0].startsWith("error") ||
                                resultSelect[0].startsWith("maxRunningTimeOut") || resultSelect[0].startsWith("MaxRows"))) {
                            try {
                                JsonParser parser = new JsonParser();
                                JsonElement jsonElement = parser.parse(resultSelect[0]);
                                JsonArray jsonArray = jsonElement.getAsJsonArray();
                                StreamSupport.stream(jsonArray.spliterator(), false).forEach(jsonElement1 -> {
                                    JsonObject jsonObject = jsonElement1.getAsJsonObject();
                                    internalAuth.add(jsonObject.has("EMP_ID") ? jsonObject.get("MATH_ID").getAsString() : null);
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
/*
                            resultSelect[0] = queryService
                                    .checkValidity("select LOG_DATE, LOG_TYPE from H_AUTH_HISTORY where USERID = '" + userId + "'", connectionID[0], "", "", false);


                            if (!(resultSelect[0].equalsIgnoreCase(connectionID[0]) || resultSelect[0].startsWith("Error") || resultSelect[0].startsWith("error") ||
                                    resultSelect[0].startsWith("maxRunningTimeOut") || resultSelect[0].startsWith("MaxRows"))
                                    && jsonNode.hasNonNull("LastPwchangedThresholdDays") && jsonNode.get("LastPwchangedThresholdDays").isNumber()
                                    && jsonNode.has("HowManyLoginFailed") && jsonNode.get("HowManyLoginFailed").isNumber()) {
                                try {
                                    JsonParser parser = new JsonParser();
                                    JsonElement jsonElement = parser.parse(resultSelect[0]);
                                    JsonArray jsonArray = jsonElement.getAsJsonArray();
                                    int LastPwchangedThresholdDays = jsonNode.get("LastPwchangedThresholdDays").asInt();
                                    final int[] howManyLoginFailedAllowed = {jsonNode.get("HowManyLoginFailed").asInt()};
                                    final int[] countFail = {0};
                                    StreamSupport.stream(jsonArray.spliterator(), false).forEach(jsonElement1 -> {
                                        JsonObject jsonObject = jsonElement1.getAsJsonObject();
                                        if (jsonObject.has("LOG_TYPE") && jsonObject.has("LOG_DATE")) {
                                            int logType = jsonObject.get("LOG_TYPE").getAsInt();
                                            LocalDate logDate;
                                            logDate = LocalDate.parse(jsonObject.get("LOG_DATE").getAsString(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                                            if (logType == 2) {
                                                if (!exceedPwChanged[0] && logDate != null && LocalDate.now().minusDays(LastPwchangedThresholdDays).isAfter(logDate)) {
                                                    //today - laswp >  log_date
                                                    exceedPwChanged[0] = true;
                                                }
                                            }
                                            if (logType == 3) {
                                                countFail[0]++;
                                            }
                                        }
                                    });
                                    if (countFail[0] > howManyLoginFailedAllowed[0]) {
                                        failALlowedLoginExceed[0] = true;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            */

                        }
                    }
                });
        if (internalAuth.isEmpty()) {
            return null;
        }
//        if (exceedPwChanged[0]) {
//            return CustomResponseDto.builder().ErrorCode(4).build();
//        }
//        if (failALlowedLoginExceed[0]) {
//            return CustomResponseDto.builder().ErrorCode(5).build();
//        }

        return CustomResponseDto.builder().InternalAuths(internalAuth).build();
    }

    public User findUserById(String userId) {
        final User[] user = {null};
        final String[] resultSelect = {""};
        final String[] connectionID = {""};
        Optional.ofNullable(queryServiceSession.readFile())
                .ifPresent(jsonNode -> {
                    if (jsonNode.hasNonNull("LogonConnectionId") && jsonNode.get("LogonConnectionId").isTextual()) {
                        connectionID[0] = jsonNode.get("LogonConnectionId").asText();
                        Connection infoConnection = queryService.getInfoConnection(connectionID[0]);
                        resultSelect[0] = queryService
                                .checkValidity("SELECT * FROM " + infoConnection.getSCHEME() + "T_EMP WHERE EMP_ID = '" + userId + "'", connectionID[0], "", "cache", false);

                        if (!(resultSelect[0].equalsIgnoreCase(connectionID[0]) || resultSelect[0].startsWith("Error") || resultSelect[0].startsWith("error") ||
                                resultSelect[0].startsWith("maxRunningTimeOut") || resultSelect[0].startsWith("MaxRows"))) {
                            try {
                                JsonParser parser = new JsonParser();
                                JsonElement jsonElement = parser.parse(resultSelect[0]);
                                JsonArray jsonArray = jsonElement.getAsJsonArray();
                                if (jsonArray.size() > 0) {
                                    JsonObject jsonObject = jsonArray.get(0).getAsJsonObject();
                                    user[0] = User.builder()
                                            .userid(jsonObject.has("EMP_ID") ? jsonObject.get("EMP_ID").getAsString() : null)
                                            .username(jsonObject.has("EMP_NAME") ? jsonObject.get("EMP_NAME").getAsString() : null)
                                            .userpw(jsonObject.has("EMP_PW") ? jsonObject.get("EMP_PW").getAsString() : null)
//                                            .accessLevel(jsonObject.has("ACCESS_LEVEL") ? jsonObject.get("ACCESS_LEVEL").getAsString() : null)
                                            .deptId(jsonObject.has("DEPT_ID") ? jsonObject.get("DEPT_ID").getAsString() : null)
                                            .userTel(jsonObject.has("EMP__TEL") ? jsonObject.get("EMP__TEL").getAsString() : null)
                                            .userIp(jsonObject.has("EMP__IP") ? jsonObject.get("EMP__IP").getAsString() : null)
                                            .userMobile(jsonObject.has("EMP__MOBILE") ? jsonObject.get("EMP__MOBILE").getAsString() : null)
                                            .userEmail(jsonObject.has("EMP__EMAIL") ? jsonObject.get("EMP__EMAIL").getAsString() : null)
                                            .userLang(jsonObject.has("EMP__LANG") ? jsonObject.get("EMP__LANG").getAsString() : null)
                                            .regDate(jsonObject.has("REG_DATE") ? jsonObject.get("REG_DATE").getAsString() : null)
                                            .regTime(jsonObject.has("REG_TIME") ? jsonObject.get("REG_TIME").getAsString() : null)
                                            .userkey(jsonObject.has("EMP_KEY") ? jsonObject.get("EMP_KEY").getAsString() : null)
                                            .build();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
        return user[0];
    }



/*

    public Map<String, String> addUser(LoginDto loginDto) {
        Map<String, String> resultMap = new HashMap<>();
        final String[] resultInsert = {""};
        final String[] connectionID = {""};
        Optional.ofNullable(queryServiceSession.readFile())
                .ifPresent(jsonNode -> {
                    if (jsonNode.hasNonNull("LogonConnectionId") && jsonNode.get("LogonConnectionId").isTextual()) {
                        connectionID[0] = jsonNode.get("LogonConnectionId").asText();
                        String userKey = UUID.randomUUID().toString().substring(0, 16);
                        resultInsert[0] = queryService
                                .checkValidityUpdateClauses("insert into T_USER (USERID, USERNAME, USERPW, ACCESS_LEVEL, DEPT_ID, USER_TEL, USER_IP, USER_MOBILE, USER_EMAIL, USER_LANG, REG_DATE, REG_TIME, USERKEY) values ('" + loginDto.getUserId() + "', '" + loginDto.getUserName() + "', '" + passwordEncoder.encode(loginDto.getPassword() + userKey) + "', '" +
                                        loginDto.getAccessLevel() + "', '" + loginDto.getDeptId() + "', '" + loginDto.getUserTel() + "', '" + loginDto.getUserIp() + "', '" +
                                        loginDto.getUserMobile() + "', '" + loginDto.getUserEmail() + "', '" + loginDto.getUserLang() + "', '" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + "', '" + new SimpleDateFormat("HHmmss").format(new Date()) + "', '" + userKey + "')", connectionID[0]);
                        if (resultInsert[0].startsWith("success")) {
                            if (!loginDto.getInternalAuths().isEmpty()) {
                                loginDto.getInternalAuths().forEach(auth_code -> {
                                    resultInsert[0] = queryService
                                            .checkValidityUpdateClauses("insert into T_USER_AUTH (USERID, AUTH_CODE) values ('" +
                                                    loginDto.getUserId() + "', '" + auth_code + "')", connectionID[0]);
                                    if (!resultInsert[0].startsWith("success")) {
                                        resultMap.clear();
                                        resultMap.put("ErrorCode", "ONE");
                                        resultMap.put("ErrorMessage", resultInsert[0]);
                                    } else {
                                        resultInsert[0] = queryService
                                                .checkValidityUpdateClauses("insert into H_AUTH_HISTORY (USERID, LOG_DATE, LOG_TIME) values ('" +
                                                        loginDto.getUserId() + "', '" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + "', '" +
                                                        new SimpleDateFormat("HHmmss").format(new Date()) + "')", connectionID[0]);
                                        resultMap.clear();
                                        resultMap.put("ErrorMessage", "");
                                    }
                                });
                            } else {
                                resultMap.clear();
                                resultMap.put("ErrorMessage", "");
                            }
                        } else {
                            resultMap.clear();
                            resultMap.put("ErrorCode", "ONE");
                            resultMap.put("ErrorMessage", resultInsert[0]);
                        }
                    }
                });
        return resultMap;
    }
    public Map<String, String> removeUser(LoginDto loginDto) {
        Map<String, String> resultMap = new HashMap<>();
        final String[] resultInsert = {""};
        final String[] connectionID = {""};
        Optional.ofNullable(queryServiceSession.readFile())
                .ifPresent(jsonNode -> {
                    if (jsonNode.hasNonNull("LogonConnectionId") && jsonNode.get("LogonConnectionId").isTextual()) {
                        connectionID[0] = jsonNode.get("LogonConnectionId").asText();
                        resultInsert[0] = queryService
                                .checkValidityUpdateClauses("DELETE FROM T_USER where USERID = '" + loginDto.getUserId() + "'", connectionID[0]);
                        if (resultInsert[0].startsWith("success")) {
                            resultInsert[0] = queryService
                                    .checkValidityUpdateClauses("DELETE FROM T_USER_AUTH where USERID = '" + loginDto.getUserId() + "'", connectionID[0]);
                            returnResult(resultMap, resultInsert);
                        }
                    }
                });
        return resultMap;
    }

    public Map<String, String> validateAndOrChangePassword(String userId, String password, String newPassword, String passwordToken, String userKey, boolean save) {
        Map<String, String> resultMap = new HashMap<>();
        if (!save) {
            if (password.length() < 10 || password.length() > 16) {
                resultMap.put("ErrorCode", "TWO");
                resultMap.put("ErrorMessage", "A PW should have a length of 10~16 bytes.");
                return resultMap;
            }
            if (!Pattern.compile(COMPLEX_PASSWORD_REGEX).matcher(password).matches()) {
                resultMap.put("ErrorCode", "THREE");
                resultMap.put("ErrorMessage", "A PW should have a combination of lower-case , upper-case ,number and special character. ");
                return resultMap;
            }
            if (userId.equals(password)) {
                resultMap.put("ErrorCode", "FOUR");
                resultMap.put("ErrorMessage", "An userid can't have a same old pw.");
                return resultMap;
            }
        } else {
            if (newPassword == null) {
                resultMap.put("ErrorCode", "TWO");
                resultMap.put("ErrorMessage", "A PW should have a length of 10~16 bytes.");
                return resultMap;
            }
            if (newPassword.length() < 10 || newPassword.length() > 16) {
                resultMap.put("ErrorCode", "TWO");
                resultMap.put("ErrorMessage", "A PW should have a length of 10~16 bytes.");
                return resultMap;
            }

            if (!Pattern.compile(COMPLEX_PASSWORD_REGEX).matcher(newPassword).matches()) {
                resultMap.put("ErrorCode", "THREE");
                resultMap.put("ErrorMessage", "A PW should have a combination of lower-case , upper-case ,number and special character. ");
                return resultMap;
            }
            if (userId.equals(password)) {
                resultMap.put("ErrorCode", "FOUR");
                resultMap.put("ErrorMessage", "An userid can't have a same old pw.");
                return resultMap;
            }

            if (password.equals(newPassword)) {
                resultMap.put("ErrorCode", "FIVE");
                resultMap.put("ErrorMessage", "A PW can't have a same with previous used pw.");
                return resultMap;
            }

            if (userId.equals(newPassword)) {
                resultMap.put("ErrorCode", "SIX");
                resultMap.put("ErrorMessage", "A PW can't have same string as userid.");
                return resultMap;
            }

            if (!passwordEncoder.matches(password + userKey, passwordToken)) {
                resultMap.put("ErrorCode", "FIVE");
                resultMap.put("ErrorMessage", "PW is not same as token password");
                return resultMap;
            }
            commonUpdatePw(userId, userKey, newPassword, resultMap, 2, password);

        }
        return resultMap;
    }
*/

/*
    public void commonUpdatePw(String userId, String userKey, String newPassword, Map<String, String> resultMap, int logType, String oldPassword) {
        final String[] resultInsert = {""};
        final String[] connectionID = {""};
        Optional.ofNullable(queryServiceSession.readFile())
                .ifPresent(jsonNode -> {
                    if (jsonNode.hasNonNull("LogonConnectionId") && jsonNode.get("LogonConnectionId").isTextual()) {
                        connectionID[0] = jsonNode.get("LogonConnectionId").asText();
                        resultInsert[0] = queryService
                                .checkValidityUpdateClauses("UPDATE T_USER\n" +
                                        "SET USERPW = '" + passwordEncoder.encode(newPassword + userKey) + "'\n" +
                                        "WHERE USERID = '" + userId + "';", connectionID[0]);
                        if (resultInsert[0].startsWith("success")) {
                            saveUserHistory(userId, Optional.ofNullable(userKey).orElse(""), logType, newPassword, oldPassword);
                            returnResult(resultMap, resultInsert);
                        } else {
                            resultMap.clear();
                            resultMap.put("ErrorCode", "ONE");
                            resultMap.put("ErrorMessage", resultInsert[0]);
                        }
                    }
                });
    }

        public void saveUserHistory(String userId, String userKey, int logType, String pwa, String pwb) {
        Optional.ofNullable(queryServiceSession.readFile())
                .ifPresent(jsonNode -> {
                    if (jsonNode.hasNonNull("LogonConnectionId") && jsonNode.get("LogonConnectionId").isTextual()) {
                        String connectionID = jsonNode.get("LogonConnectionId").asText();
                        String pwaHash = null;
                        String pwbHash = null;
                        if (pwa != null) {
                            pwaHash = "'" + passwordEncoder.encode(pwa + userKey) + "'";
                        }
                        if (pwb != null) {
                            pwbHash = "'" + passwordEncoder.encode(pwb + userKey) + "'";
                        }
                        queryService
                                .checkValidityUpdateClauses("insert into H_AUTH_HISTORY(USERID, LOG_TYPE, LOG_DATE, LOG_TIME, PW_A, PW_B) values('" +
                                        userId + "', '" + logType + "', '" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + "', '" +
                                        new SimpleDateFormat("HHmmss").format(new Date()) + "', " + (pwaHash) +
                                        ", " + (pwbHash) + ")", connectionID);
                    }
                });
    }

    private void returnResult(Map<String, String> resultMap, String[] resultInsert) {
        if (resultInsert[0].startsWith("success")) {
            resultMap.clear();
            resultMap.put("ErrorMessage", "");
        } else {
            resultMap.clear();
            resultMap.put("ErrorCode", "ONE");
            resultMap.put("ErrorMessage", resultInsert[0]);
        }
    }

*/


}
