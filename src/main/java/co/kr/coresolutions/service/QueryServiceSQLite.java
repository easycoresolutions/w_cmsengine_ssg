package co.kr.coresolutions.service;

import co.kr.coresolutions.dao.NativeQuery;
import co.kr.coresolutions.model.SqliteFileRepo;
import co.kr.coresolutions.model.TableLoad;
import co.kr.coresolutions.model.dtos.SqliteFileRepoDto;
import co.kr.coresolutions.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class QueryServiceSQLite {
    public static String DBFILE = "mydbfile.db";
    public static final String fileRepo = "mydbfile.txt";
    private final NativeQuery nativeQuery;
    private final RunCommands runCommands;
    private final ObjectMapper objectMapper;
    public static String SQLITE_USERID = "mss";
    private final Constants constants;
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
    private String SQLiteQueryDir;
    private String FileQueryDir;
    private String UnloadQueryDir;

    @PostConstruct
    public void init() {
        SQLiteQueryDir = constants.getSQLiteQueryDir();
        FileQueryDir = constants.getFileQueryDir();
        UnloadQueryDir = constants.getUnloadDir();
    }

    public boolean isDBFileNameExists(String userID, String dbFileName) {
        return Paths.get(SQLiteQueryDir + userID + File.separator + dbFileName + ".db").toFile().exists();
    }

    public String createDBFile(String connectionId, String userID, String dbFileName) {
        try {
            Files.createDirectories(Paths.get(SQLiteQueryDir + userID));
            nativeQuery
                    .createSQLiteDB(connectionId, "jdbc:sqlite:" + File.separator + SQLiteQueryDir, userID + File.separator, dbFileName + ".db");
        } catch (IOException | SQLException | ClassNotFoundException e) {
            return "error : {" + e.getMessage() + "}";
        }
        return "success";
    }

    public boolean deleteDbFile(String userid, String dbFileName) {
        return Paths.get(SQLiteQueryDir + userid + File.separator + dbFileName + ".db").toFile().delete();
    }

    public boolean deleteTableFromDBfile(String connectionId, String userID, String dbFileName, String tableName) {
        try {
            Files.createDirectories(Paths.get(SQLiteQueryDir));
            if (Paths.get(SQLiteQueryDir + userID + File.separator + dbFileName + ".db").toFile().exists()) {
                return nativeQuery.deleteTableSQLite(connectionId, "jdbc:sqlite:" + SQLiteQueryDir,
                        userID + File.separator + dbFileName + ".db", tableName);
            }
        } catch (IOException e) {
            return false;
        }
        return false;
    }

    public String getDBFile(String userID, String dbFileName) {
        return "jdbc:sqlite:" + SQLiteQueryDir + userID + File.separator + dbFileName + ".db";
    }

    public String isFileNameExists(String userID, String input_directory, String file_name) {
        try {
            if (!(Arrays.asList(new String[]{"unload", "file"}).contains(input_directory))) {
                return "error : {input_directory " + input_directory + " not valid}";
            }

            if (input_directory.equals("file")) {
                if (!Paths.get(FileQueryDir + userID + File.separator + file_name).toFile().exists()) {
                    return "error : {file_name " + file_name + " not valid}";
                } else
                    return new String(Files.readAllBytes(Paths.get(FileQueryDir + userID + File.separator + file_name)), StandardCharsets.UTF_8);
            } else {
                if (!Paths.get(UnloadQueryDir + userID + File.separator + file_name).toFile().exists()) {
                    return "error : {file_name " + file_name + " not valid}";
                } else
                    return new String(Files.readAllBytes(Paths.get(UnloadQueryDir + userID + File.separator + file_name)), StandardCharsets.UTF_8);
            }

        } catch (IOException e) {
            return "error : { " + e.getMessage() + "}";
        }

    }

    public String tableLoad(TableLoad tableLoad) {
        String metaFile = Util.replaceAll(tableLoad.getFile_name(), ".csv", "_meta.txt");
        String unloadDir = Util.replaceAll(UnloadQueryDir, "/", "//");
        unloadDir = Util.replaceAll(unloadDir, "\\", "\\\\");
        String fileDir = Util.replaceAll(FileQueryDir, "/", "//");
        fileDir = Util.replaceAll(fileDir, "\\", "\\\\");
        String commandSqlite;
        String tableMeta = "";
        String delMeta = "";
        Path pathMeta = null;
        if (tableLoad.getInput_directory().equalsIgnoreCase("file")
                && Files.exists(Paths.get(FileQueryDir + tableLoad.getUserid() + File.separator + metaFile))) {
            pathMeta = Paths.get(FileQueryDir + tableLoad.getUserid() + File.separator + metaFile);
        } else if (tableLoad.getInput_directory().equalsIgnoreCase("unload")
                && Files.exists(Paths.get(UnloadQueryDir + tableLoad.getUserid() + File.separator + metaFile))) {
            pathMeta = Paths.get(UnloadQueryDir + tableLoad.getUserid() + File.separator + metaFile);
        }

        if (pathMeta != null) {
            tableMeta = " \"CREATE TABLE " + tableLoad.getTable_name() + "(";
            delMeta = " \"DELETE FROM " + tableLoad.getTable_name() + " WHERE ";
            try (BufferedReader reader = Files.newBufferedReader(pathMeta, StandardCharsets.UTF_8)) {
                String lineStr;
                String fieldStr = "";
                String delStr = "";
                int cnt = 0;
                while ((lineStr = reader.readLine()) != null) {
                    String[] fieldInfo = lineStr.split("\r\t");
                    if (cnt != 0) {
                        if (cnt != 1) {
                            fieldStr += ", ";
                            delStr += " and ";
                        }
                        for (int i = 0; i < fieldInfo.length; ++i) {
                            String[] fields = fieldInfo[i].split(",");
                            if (fields.length <= 1) {
                                fields = fieldInfo[i].split("\t");
                            }
                            if (i != 0) {
                                fieldStr += ", ";
                                delStr += " and ";
                            }
                            if (fields[1].toLowerCase().contains("char")
                                    || fields[1].toLowerCase().contains("text")
                                    || fields[1].toLowerCase().contains("clob")) {
                                fieldStr += "'" + fields[0] + "' TEXT";
                            } else if (fields[1].toLowerCase().contains("blob")) {
                                fieldStr += "'" + fields[0] + "' BLOB";
                            } else if (fields[1].toLowerCase().contains("REAL")
                                    || fields[1].toLowerCase().contains("DOUBLE")
                                    || fields[1].toLowerCase().contains("float")) {
                                fieldStr += "'" + fields[0] + "' REAL";
                            } else if (fields[1].toLowerCase().contains("NUMERIC")
                                    || fields[1].toLowerCase().contains("DECIMAL")
                                    || fields[1].toLowerCase().contains("BOOLEAN")
                                    || fields[1].toLowerCase().contains("DATE")
                                    || fields[1].toLowerCase().contains("DATETIME")) {
                                fieldStr += "'" + fields[0] + "' NUMERIC";
                            } else {
                                fieldStr += "'" + fields[0] + "' INTEGER";
                            }
                            delStr += fields[0] + " = '" + fields[0] + "'";
                        }
                    }
                    cnt++;
                }
                tableMeta += fieldStr + ");\"";
                delMeta += delStr + "\"";
            } catch (IOException e) {
                return "Error : " + e.getMessage();
            }

        }

        tableMeta = Util.replaceAll(tableMeta, "'", "");
        String tempFileContent = "drop table if exists " + tableLoad.getTable_name() + ";" +
                "\n" + tableMeta.replace("\"", "") + ";" +
                "\n.mode csv\n" +
                ".import " +
                (tableLoad.getInput_directory().equals("unload") ? unloadDir : fileDir) + (tableLoad.getUserid() + File.separator + tableLoad.getFile_name() +
                " " + tableLoad.getTable_name() + "\n"
                + delMeta.replace("\"", "") + ";");
        Path path;
        try {
            path = Files.write(Paths.get(FileQueryDir + tableLoad.getTable_name() + "_" + UUID.randomUUID().toString() + "_" +
                            SIMPLE_DATE_FORMAT.format(new Date()) + ".txt"),
                    tempFileContent.getBytes(), StandardOpenOption.CREATE_NEW, StandardOpenOption.TRUNCATE_EXISTING);
            if (RunCommands.isUnix()) {
                commandSqlite = "sqlite3 " + (SQLiteQueryDir + tableLoad.getUserid() + File.separator + tableLoad.getDb_file().concat(".db")) +
                        " '.read " + path.toFile().getAbsolutePath() + "'";
            } else {
                commandSqlite = SQLiteQueryDir + "sqlite3.exe " + (SQLiteQueryDir + tableLoad.getUserid() + File.separator + tableLoad.getDb_file().concat(".db")) +
                        " '.read " + path.toFile().getAbsolutePath() + "'";
            }
        } catch (IOException e) {
            return "Error: " + e.getMessage();
        }

        String resultExecution = runCommands.runStandaloneCommand(commandSqlite);
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            return "Error: " + e.getMessage();
        }
        return resultExecution;
    }

    public String createOrUpdateFileRepo(SqliteFileRepoDto sqliteFileRepoDto) {
        String userID = sqliteFileRepoDto.getUserID();
        String dbFileName = sqliteFileRepoDto.getDbFile();
        String desc = sqliteFileRepoDto.getDesc();
        //in case of true
        SqliteFileRepo sqliteFileRepo = SqliteFileRepo.builder().dbFile(dbFileName).desc(desc).build();
        try {
            Path path = Paths.get(SQLiteQueryDir + userID + File.separator + fileRepo);
            Files.createDirectories(Paths.get(SQLiteQueryDir + userID));
            String oldContentFileRepo = null;
            if (path.toFile().exists()) {
                oldContentFileRepo = new String(Files.readAllBytes(path));
            } else {
                sqliteFileRepo.setActive(true);
            }

            List<SqliteFileRepo> sqliteFileRepoList = oldContentFileRepo != null
                    ? new LinkedList<>(Arrays.asList(objectMapper.readValue(oldContentFileRepo, SqliteFileRepo[].class))) : new LinkedList<>();
            boolean dbAlreadyExist = sqliteFileRepoList
                    .stream()
                    .anyMatch(sqliteFileRepo1 -> sqliteFileRepo1.getDbFile().equalsIgnoreCase(sqliteFileRepoDto.getDbFile()));
            if (dbAlreadyExist) {
                return "fail due " + dbFileName + " already existing under /" + userID + File.separator + fileRepo;
            }
            sqliteFileRepoList.add(sqliteFileRepo);
            Files.write(path, objectMapper.writeValueAsBytes(sqliteFileRepoList), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            return "error : {" + e.getMessage() + "}";
        }
        return "success";
    }

    public String deleteFileRepo(String userID, String dbFileName) {
        try {
            Path path = Paths.get(SQLiteQueryDir + userID + File.separator + fileRepo);
            if (!path.toFile().exists()) {
                return fileRepo + " doesn't exist!";
            }

            List<SqliteFileRepo> sqliteFileRepoList =
                    new LinkedList<>(Arrays.asList(objectMapper.readValue(new String(Files.readAllBytes(path)), SqliteFileRepo[].class)));
            boolean dbFileNotExist = sqliteFileRepoList
                    .stream()
                    .noneMatch(sqliteFileRepo1 -> sqliteFileRepo1.getDbFile().equalsIgnoreCase(dbFileName));
            if (dbFileNotExist) {
                return "fail due " + dbFileName + " doesn't exist";
            }

            if (sqliteFileRepoList.removeIf(sqliteFileRepo1 -> sqliteFileRepo1.getDbFile().equalsIgnoreCase(dbFileName) && !sqliteFileRepo1.isActive())) {
                if (sqliteFileRepoList.isEmpty()) {
                    Files.delete(path);
                } else {
                    Files.write(path, objectMapper.writeValueAsBytes(sqliteFileRepoList), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                }
            } else {
                return dbFileName + " can't be removed!";
            }
        } catch (IOException e) {
            return "error : {" + e.getMessage() + "}";
        }
        return "success";
    }

    public String deleteTableFileRepo(String userID) {
        try {
            Path path = Paths.get(SQLiteQueryDir + userID + File.separator + fileRepo);
            if (!path.toFile().exists()) {
                return fileRepo + " doesn't exist!";
            }

            boolean anyDBFileActive = Stream
                    .of(objectMapper.readValue(new String(Files.readAllBytes(path)), SqliteFileRepo[].class))
                    .anyMatch(SqliteFileRepo::isActive);
            if (!anyDBFileActive) {
                return "There is no active dbfile defined!";
            }
        } catch (IOException e) {
            return "error : {" + e.getMessage() + "}";
        }
        return "success";
    }

    public String getDBFileRepo(String userID) {
        try {
            Path path = Paths.get(SQLiteQueryDir + userID + File.separator + fileRepo);
            if (!path.toFile().exists()) {
                return fileRepo + " doesn't exist!";
            }

            final String[] dbFileRepo = {""};
            Stream
                    .of(objectMapper.readValue(new String(Files.readAllBytes(path)), SqliteFileRepo[].class))
                    .filter(SqliteFileRepo::isActive)
                    .findAny()
                    .ifPresent(sqliteFileRepo -> dbFileRepo[0] = sqliteFileRepo.getDbFile());
            if (dbFileRepo[0].isEmpty()) {
                return "There is no active dbfile defined!";
            } else {
                return dbFileRepo[0];
            }
        } catch (IOException e) {
            return "error : {" + e.getMessage() + "}";
        }
    }

    public String getDBFiles(String userID) {
        Path path = Paths.get(SQLiteQueryDir + userID + File.separator + fileRepo);
        if (!path.toFile().exists()) {
            return fileRepo + " doesn't exist!";
        }
        try {
            return new String(Files.readAllBytes(path));
        } catch (IOException e) {
            return "error : {" + e.getMessage() + "}";
        }
    }

    public String activate(String userID, String dbFile) {
        try {
            Path path = Paths.get(SQLiteQueryDir + userID + File.separator + fileRepo);
            if (!path.toFile().exists()) {
                return fileRepo + " doesn't exist!";
            }

            List<SqliteFileRepo> sqliteFileRepoList =
                    new LinkedList<>(Arrays.asList(objectMapper.readValue(new String(Files.readAllBytes(path)), SqliteFileRepo[].class)));
            final boolean[] dbFileRepo = {false};
            sqliteFileRepoList = sqliteFileRepoList
                    .stream()
                    .peek(sqliteFileRepo -> {
                        if (sqliteFileRepo.getDbFile().equals(dbFile)) {
                            dbFileRepo[0] = true;
                            sqliteFileRepo.setActive(true);
                        } else {
                            sqliteFileRepo.setActive(false);
                        }
                    }).collect(Collectors.toList());

            if (!dbFileRepo[0]) {
                return "fail due " + dbFile + " doesn't exist";
            }
            Files.write(path, objectMapper.writeValueAsBytes(sqliteFileRepoList), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            return "error : {" + e.getMessage() + "}";
        }
        return "success";
    }

    public String getActiveDBMS(String userID) {
        try {
            Path path = Paths.get(SQLiteQueryDir + userID + File.separator + fileRepo);
            if (!path.toFile().exists()) {
                return "error " + fileRepo + " doesn't exist!";
            }

            SqliteFileRepo sqliteFileRepo =
                    new LinkedList<>(Arrays.asList(objectMapper.readValue(new String(Files.readAllBytes(path)), SqliteFileRepo[].class)))
                            .stream()
                            .filter(SqliteFileRepo::isActive)
                            .findFirst()
                            .orElse(null);
            if (sqliteFileRepo != null) {
                return objectMapper.writeValueAsString(sqliteFileRepo);
            }
            return "error there is no active db-file in the index-file";
        } catch (Exception e) {
            return "error" + e.getMessage();
        }
    }
}
