package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.ResponseCodes;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.model.dtos.HttpDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.Collections;

@Service
@RequiredArgsConstructor
public class QueryDataFlowHttp {
    private static final String[] safeMethods = {HttpMethod.GET.name()};
    private final RestTemplate restTemplate;

    //    @SneakyThrows
    public ResponseDto execute(@Valid HttpDto httpDto) {
        try {
            HttpHeaders headers = new HttpHeaders();

            headers.add("Content-Type", "application/json");
            HttpEntity<String> httpEntity;

            String body = httpDto.getBody();

            HttpMethod httpMethod = httpDto.getMethod();

            if (!Arrays.asList(safeMethods).contains(httpMethod.name())) {
                httpEntity = new HttpEntity<>(body, headers);
            } else httpEntity = new HttpEntity<>(headers);

            ResponseEntity responseEntity = restTemplate
                    .exchange(UriComponentsBuilder.fromHttpUrl(httpDto.getApi()).build().toUri(), httpMethod, httpEntity, String.class);

            int statusCode = responseEntity.getStatusCodeValue();
            String responseBody = responseEntity.getBody() != null ? responseEntity.getBody().toString().toLowerCase() : "";

            if (responseBody.startsWith("error") || responseBody.startsWith("fatal") || responseBody.startsWith("fail")) {
                return ResponseDto.builder().message(responseBody).statusCode(ResponseCodes.NOT_FOUND_RESPONSE_ERROR)
                        .successCode(ResponseCodes.NOT_FOUND_RESPONSE_ERROR).build();
                }
            return ResponseDto.builder().message(responseBody).successCode(ResponseCodes.OK_RESPONSE).statusCode(statusCode).build();
        } catch (Exception e) {
            return ResponseDto.builder().message(e.getMessage()).errorCode(ResponseCodes.GENERAL_ERROR).statusCode(ResponseCodes.GENERAL_ERROR).build();
        }
    }

    public CustomResponseDto executeNormal(@Valid HttpDto httpDto) {
        try {
            HttpHeaders headers = new HttpHeaders();

            headers.add("Content-Type", "application/json;charset=utf-8");
            headers.setAccept(Collections.singletonList(MediaType.ALL));
            HttpEntity<String> httpEntity;

            String body = httpDto.getBody();

            HttpMethod httpMethod = httpDto.getMethod();

            if (!Arrays.asList(safeMethods).contains(httpMethod.name())) {
                httpEntity = new HttpEntity<>(body, headers);
            } else httpEntity = new HttpEntity<>(headers);

            ResponseEntity<String> responseEntity = restTemplate
                    .exchange(UriComponentsBuilder.fromHttpUrl(httpDto.getApi()).build().toUri(), httpMethod, httpEntity, String.class);

            int statusCode = responseEntity.getStatusCodeValue();
            String responseBody = responseEntity.getBody() != null ? responseEntity.getBody() : "";

            if (responseBody.startsWith("error") || responseBody.startsWith("fatal")
                    || responseBody.startsWith("fail") || statusCode != ResponseCodes.OK_RESPONSE) {
                return CustomResponseDto.builder().message(responseBody).build();
            }
            return CustomResponseDto.builder().Success(true).message(responseBody).build();
        } catch (Exception e) {
            e.printStackTrace();
            return CustomResponseDto.builder().message(e.getMessage()).build();
        }
    }

}
