package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.ResponseCodes;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQueryEmailFacade;
import co.kr.coresolutions.model.dtos.EmailDto;
import co.kr.coresolutions.model.dtos.HttpDto;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class QueryServiceControl {
    private static final String GET_DATA = "GETDATA";
    private static final String CREATE_REPORT = "CREATE_REPORT";
    private static final String SEND_EMAIL = "SEND_EMAIL";
    private static final String STATUS_CODE = "STATUS CODE ";
    private static final String TASK_START = "TASK START\n";
    private static final String TASK_END = "TASK END\n";
    private static final String NORMAL = "Normal\n";
    private static final String ERROR = "Error\n";
    private final QueryDataFlowHttp queryDataFlowHttp;
    private final QueryService queryService;
    private final IQueryEmailFacade queryEmailFacade;

    public ResponseDto callRestServer(JsonNode getData, String pathRoot) {
        queryService.addLogging(GET_DATA, TASK_START);
        CustomResponseDto customResponseDto = queryDataFlowHttp.executeNormal(HttpDto
                .builder()
                .api(Optional.of(getData.get("url").asText()).orElse(null))
                .body(Optional.of(getData.get("body").toString()).orElse(null))
                .method(Optional.of(HttpMethod.valueOf(getData.get("method").asText().toUpperCase())).orElse(null))
                .build());
        if (customResponseDto.getSuccess()) {
            try {
                Files.write(Paths.get(pathRoot + File.separator + "data.json"),
                        customResponseDto.getMessage().getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
            } catch (IOException e) {
                return ResponseDto.builder().errorCode(ResponseCodes.GENERAL_ERROR).message(e.getMessage()).build();
            }
            queryService.addLogging(GET_DATA, STATUS_CODE + NORMAL);
        } else {
            queryService.addLogging(GET_DATA, STATUS_CODE + ERROR);
        }
        queryService.addLogging(GET_DATA, TASK_END);
        return ResponseDto.ok();
    }

    public void createReport(JsonNode createReport, String owner, HttpServletResponse response) {
        queryService.addLogging(CREATE_REPORT, TASK_START);
        String resultExecution = queryService.executeCommand("reportsubmit_", Optional.of(createReport.get("command").asText()).orElse(null), "", owner, response);
        if (resultExecution.startsWith(ERROR)) {
            queryService.addLogging(CREATE_REPORT, STATUS_CODE + NORMAL);
        } else {
            queryService.addLogging(CREATE_REPORT, STATUS_CODE + ERROR);
        }
        queryService.addLogging(CREATE_REPORT, TASK_END);
    }

    public ResponseDto sendEmail(JsonNode sendEmail) {
        queryService.addLogging(SEND_EMAIL, TASK_START);
        final String attachment = "attachment";
        final String cc = "cc";
        final String to = "to";
        ResponseDto responseDto = queryEmailFacade.sendSimpleEmail(EmailDto
                .builder()
                .attachment(sendEmail.get(attachment).isArray() ?
                        StreamSupport.stream(sendEmail.get(attachment).spliterator(), false)
                                .map(JsonNode::asText).collect(Collectors.toList()) : Collections.singletonList(sendEmail.get(attachment).asText()))
                .cc(sendEmail.get(cc).isArray() ?
                        StreamSupport.stream(sendEmail.get(cc).spliterator(), false)
                                .map(JsonNode::asText).collect(Collectors.toList()) : Collections.singletonList(sendEmail.get(cc).asText()))
                .to(sendEmail.get(to).isArray() ?
                        StreamSupport.stream(sendEmail.get(to).spliterator(), false)
                                .map(JsonNode::asText).collect(Collectors.toList()) : Collections.singletonList(sendEmail.get(to).asText()))
                .data(Optional.of(sendEmail.get("data").asText()).orElse(""))
                .emailType(Optional.of(sendEmail.get("emailType").asText()).orElse(""))
                .from(Optional.of(sendEmail.get("from").asText()).orElse(""))
                .connectionId(Optional.of(sendEmail.get("connectionid").asText()).orElse(""))
                .subject(Optional.of(sendEmail.get("subject").asText()).orElse(""))
                .build());
        if (responseDto.getSuccessCode() == ResponseCodes.OK_RESPONSE) {
            queryService.addLogging(SEND_EMAIL, STATUS_CODE + NORMAL);
        } else {
            queryService.addLogging(SEND_EMAIL, STATUS_CODE + ERROR);
        }
        queryService.addLogging(SEND_EMAIL, TASK_END);
        return responseDto;
    }

    public ResponseDto checkExist(JsonNode checkExists) {
        if (!checkExists.isArray()) {
            return ResponseDto.ERROR_PARSE;
        }
        final ResponseDto[] responseDto = {null};
        val deleteOnExists = new ArrayList<>();
        StreamSupport
                .stream(checkExists.spliterator(), false)
                .filter(node -> node.hasNonNull("file") && node.hasNonNull("not_exist") && node.hasNonNull("delete_completion"))
                .filter(node -> node.get("file").isTextual() && node.get("not_exist").isTextual() && node.get("delete_completion").isBoolean())
                .forEach(node -> {
                    if (responseDto[0] == null || responseDto[0].getSuccessCode() == ResponseCodes.OK_RESPONSE) {
                        if (!Paths.get(node.get("file").asText()).toFile().exists()) {
                            responseDto[0] = ResponseDto.builder().errorCode(ResponseCodes.GENERAL_ERROR).message(node.get("not_exist").asText()).build();
                        } else {
                            responseDto[0] = ResponseDto.ok();
                            if (node.get("delete_completion").asBoolean()) {
                                deleteOnExists.add(node.get("file").asText());
                            }
                        }
                    }
                });

        Optional.ofNullable(responseDto[0]).ifPresent(responseDtoExist ->
        {
            if (responseDtoExist.getSuccessCode() == ResponseCodes.OK_RESPONSE) {
                responseDto[0].setMessage(StringUtils.join(deleteOnExists, ","));
            }
        });

        return responseDto[0];
    }

    public void deleteBulkFiles(List<String> deleteOnExists) {
        deleteOnExists.forEach(o -> {
            try {
                Files.deleteIfExists(Paths.get(o));
            } catch (IOException ignored) {
            }
        });
    }
}
