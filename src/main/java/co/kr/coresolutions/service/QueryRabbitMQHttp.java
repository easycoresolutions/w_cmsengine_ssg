package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.HelperUtil;
import co.kr.coresolutions.model.dtos.JsonExternalDataDto;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Arrays;

@Service
@RequiredArgsConstructor
@RequestScope
public class QueryRabbitMQHttp {
    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;
    private final Constants constants;

    private HttpHeaders createHeaders(String username, String password) {
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName(HelperUtil.formatUSASCII)));
            String authHeader = "Basic " + new String(encodedAuth);
            set("Authorization", authHeader);
        }};
    }

    public ObjectNode[] getListQueue(String urlHttp, String username, String password) {
        ResponseEntity<ObjectNode[]> responseEntity = restTemplate
                .exchange(urlHttp + constants.pathListQueues, HttpMethod.GET, new HttpEntity<ObjectNode[]>(createHeaders(username, password)), ObjectNode[].class);

        if (responseEntity.getBody() == null)
            return null;

        return responseEntity.getBody();
    }

    public boolean checkQueue(String queueName, String urlHttp, String username, String password) {
        ObjectNode[] jsonArray = getListQueue(urlHttp, username, password);

        if (jsonArray == null) return false;

        return Arrays.stream(jsonArray).filter(jsonNodes -> jsonNodes.has("name"))
                .anyMatch(jsonNodes -> {
                    try {
                        JsonExternalDataDto jsonExternalDataDto = objectMapper.readValue(jsonNodes.toString(), JsonExternalDataDto.class);
                        return jsonExternalDataDto.getName().equals(queueName);
                    } catch (IOException e) {
                        return false;
                    }
                });
    }

    public JsonNode getExchanges(String urlHttp, String username, String password, String queueName) {

        URI uri = UriComponentsBuilder.fromHttpUrl(urlHttp + constants.pathListExchanges).path("/%2f/" + queueName).build(true).toUri();
        ResponseEntity<JsonNode> responseEntity;
        try {
            responseEntity = restTemplate
                    .exchange(uri, HttpMethod.GET, new HttpEntity<JsonNode>(createHeaders(username, password)), JsonNode.class);
        } catch (Exception e) {
            return null;
        }

        if (responseEntity.getBody() == null)
            return null;

        return responseEntity.getBody();
    }
}
