package co.kr.coresolutions.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.kafka.common.errors.InterruptException;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import co.kr.coresolutions.model.Connection;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@RequiredArgsConstructor
public class RunCommandsInternal {

    private final Constants constants;
    private final QueryService queryService;
    private final RunCommands runCommands;
    private final ObjectMapper objectMapper;
    
    public void saveCacheCommandObject(String commandID, Object process) {
        if (commandID.isEmpty()) {
            return;
        }
        CacheManager cm = CacheManager.getInstance();
        if (!cm.cacheExists("cacheCommandProcess"))
            cm.addCache("cacheCommandProcess");

        Cache cache = cm.getCache("cacheCommandProcess");
        try {
            cache.put(new Element(commandID, process));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public Object getCacheCommandObject(String commandID) {
        CacheManager cm = CacheManager.getInstance();
        Cache cache = cm.getCache("cacheCommandProcess");
        if (cache != null) {
            Element element = cache.get(commandID);
            if (element != null) {
                return element.getObjectValue();
            }
        }
        return null;
    }
    
    /**
     * run command Internal : class file loading
     * @param commandAfterKeyReplace
     * @param commandID
     * @param response
     * @return
     */
    public String runCommand(String commandAfterKeyReplace, String commandID, HttpServletResponse response) {
        final String[] executionResult = {"Error"};
        final boolean[] commandDurationExceed = {false};
        
        String startPackgeName = "cmsbatch";
        String clazzName = "";
        String packgeName = "";
        ByteClassLoader clazzLoader = null;
        
        File clazzPath = new File(constants.getMainCommandDir() + File.separator + "bin" + File.separator + startPackgeName);
        
        try {
            /*cammand에서 실행 명령 검색 */
            String clazzNameFind = commandAfterKeyReplace;
            Pattern pattern = Pattern.compile(" ([a-zA-Z]+\\.)+([a-zA-Z]+){1} ");
            Matcher matcher = pattern.matcher(clazzNameFind);
            
            if(matcher.find()) {
                clazzNameFind = matcher.group().substring(1, matcher.group().length()-1);
                int pos = clazzNameFind.lastIndexOf( "." );
                packgeName = clazzNameFind.substring(0, pos );
                clazzName = clazzNameFind.substring( pos + 1 );
            } else {
                return "Error : className not found";
            }
            
            clazzLoader = new ByteClassLoader(this.getClass().getClassLoader());
            
            File[] files = clazzPath.listFiles();
            //class 파일 로드
            loadClass(clazzLoader, startPackgeName, files);
            
            Class<?> classToLoad = Class.forName(packgeName + "." + clazzName, true, clazzLoader);
            Object instance = classToLoad.newInstance();
            Method method = classToLoad.getMethod("main", String[].class);
            
            /* find paramKey */
            String paramKeyReplace = commandAfterKeyReplace;
            Pattern patternDetail = Pattern.compile("(\"[^\"]*)\"");
            Matcher matcherDetail = patternDetail.matcher(commandAfterKeyReplace);
            
            if(matcherDetail.find()) {
                paramKeyReplace = matcherDetail.group().substring(1, matcherDetail.group().length()-1);
            }
            
            Object[] args = new Object[1];
            args[0] = new String[] {paramKeyReplace};
            
            Process p;
            String commandDummy = "java -h";
            Runtime r = Runtime.getRuntime();
            
            if (RunCommands.isUnix()) {
                p = r.exec(new String[]{"bash", "-c", commandDummy});
            } else {
                p = r.exec(commandDummy);
            }
//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            PrintStream threadOut = new PrintStream(stream);
//            PrintStream outStream = System.out;
            
            Thread threadMethod = new Thread(() -> {
                try {
//                  System.setOut(threadOut);
                    method.invoke(instance,args);
                } catch (InterruptException e) {
                    Thread.currentThread().interrupt();
                } catch (Exception e) {
                    System.out.println("Error : method invoke - " + e.toString());
                } finally {
//                  System.setOut(outStream);
                }
            });
            
            saveCacheCommandObject(commandID, threadMethod);
            threadMethod.setDaemon(true);
            threadMethod.start();
            p.destroy();
            
            Optional.ofNullable(constants.getConfigFileAsJson())
                .ifPresent(jsonNode -> {
                    if (jsonNode.hasNonNull("command_durationMinutes") && jsonNode.get("command_durationMinutes").isNumber()) {
                        Thread thread = new Thread(() -> {
                            try {
//                                Thread.sleep(jsonNode.get("command_durationMinutes").asInt() * 60000L);
                                Thread.sleep(jsonNode.get("command_durationMinutes").asInt() * 60000L);
                                threadMethod.interrupt();
                                p.destroy();
                                commandDurationExceed[0] = true;
                                runCommands.removeCacheCommandProcess(commandID);
                            } catch (InterruptedException e) {
                                Thread.currentThread().interrupt();
                                executionResult[0] = "Error executing command " + commandAfterKeyReplace +
                                        ", Please check manual of your OS!";
                                queryService.addLogging("POST", "/command is cancelled by request. commandID(" + commandID + ")\n\n");
                                jsonObjectResponse(response, false);
                            }
                        });
                        thread.setDaemon(true);
                        thread.start();
                        try {
                            while (true) {
                                threadMethod.join(100);
                                if(threadMethod.isInterrupted()) {
                                    break;
                                }
                                if(threadMethod.getState() == Thread.State.TERMINATED) {
                                    break;
                                }
                            }
                        } catch (InterruptedException e) {
                            System.out.println("method InterruptedException : "+e.toString());
                            executionResult[0] = "Error executing command " + commandAfterKeyReplace +
                                    ", Please check manual of your OS!";
                            queryService.addLogging("POST", "/command is cancelled by system timeout. commandID(" + commandID + ")\n\n");
                            jsonObjectResponse(response, commandDurationExceed[0]);
                            Thread.currentThread().interrupt();
                        }
                    }
                });
            
            if (Thread.State.TERMINATED == threadMethod.getState()) {
//              String output = new String(stream.toByteArray());
                String connectionId = "quadmax";
                String campId = "";
                String nodeId = "";
                String execSeq = "";
                StringBuilder sb = new StringBuilder();
                Connection infoConnection = queryService.getInfoConnection(connectionId);
                
                if(null != paramKeyReplace) {
                    String[] params = paramKeyReplace.split(";");
                    if(null != params) {
                        for(String param : params) {
                            String[] values = param.split(":");
                            if(null != values && values.length > 1) {
                                String key = values[0].trim();
                                String value = values[1].trim();
                                switch (key) {
                                case "CAMP_ID":
                                    campId = value;
                                    break;
                                case "NODE_ID":
                                    nodeId = value;
                                    break;
                                case "EXEC_SEQ":
                                    execSeq = value;
                                    break;
                                default:
                                    break;
                                }
                            }
                        }
                    }
                }
                
                if(infoConnection.getDBMS().equalsIgnoreCase("ORACLE")) {
                    sb.append(" SELECT LOG_MSG, NODE_CNT FROM ( SELECT LOG_MSG, NODE_CNT FROM QUADMAX.T_CAMP_NODEINFO A WHERE 1=1 ");
                    sb.append(String.format(" AND A.CAMP_ID = '%s'", campId));
                    sb.append(String.format(" AND A.NODE_ID = '%s'", nodeId));
                    if(null != execSeq && !"".equals(execSeq)) sb.append(String.format(" AND A.EXEC_SEQ = '%s'", execSeq));
                    sb.append(" ORDER BY END_DTTM DESC) WHERE ROWNUM <= 1 ");
                } else {
                    sb.append(" SELECT LOG_MSG, NODE_CNT FROM QUADMAX.T_CAMP_NODEINFO A WHERE 1=1 ");
                    sb.append(String.format(" AND A.CAMP_ID = '%s'", campId));
                    sb.append(String.format(" AND A.NODE_ID = '%s'", nodeId));
                    if(null != execSeq && !"".equals(execSeq)) sb.append(String.format(" AND A.EXEC_SEQ = '%s'", execSeq));
                    sb.append(" ORDER BY END_DTTM DESC LIMIT 1 ");
                }
//                System.out.println("sql:"+sb.toString());
                
                String result = queryService.checkValidity(sb.toString(),connectionId,"","campNodeInfo",false);
//                System.out.println("result:"+result);
                
                if(null != result && !result.isEmpty()) {
                    String resultCnt = "-1";
                    String resultMsg = null;
                    JsonNode jsonNodes = objectMapper.readValue(result, JsonNode.class);
                    if(jsonNodes.isArray()) {
                        for(JsonNode jsonNode : jsonNodes) {
                            if(jsonNode.has("NODE_CNT")) {
                                resultCnt = jsonNode.get("NODE_CNT").asText();
                            }
                            if(jsonNode.has("LOG_MSG")) {
                                resultMsg = jsonNode.get("LOG_MSG").asText();
                            }
                        }
                    }
                    if(null != resultMsg && !"".equals(resultMsg)) {
                        executionResult[0] = "fail," + resultMsg;
                    } else {
                        executionResult[0] = "Success," + resultCnt;
                    }
                } else {
                    executionResult[0] = "Error no result";
                }
            } else {
                if (getCacheCommandObject(commandID) == null) {
                    queryService.addLogging("POST", "Error command-execution-time is exceed, commandID\t" + commandID + "\n");
                    jsonObjectResponse(response, true);
                } else {
                    getCacheCommandObject(commandID);
                    queryService.addLogging("POST", "Error Command is terminated by request, commandID\t" + commandID + "\n");
                    jsonObjectResponse(response, false);
                }
                Thread threadMain = Thread.currentThread();
                Thread threadInterrupt = new Thread(() -> {
                    try {
                        Thread.sleep(1000);
                        threadMain.interrupt();
                    } catch (InterruptException | InterruptedException e) {
                      Thread.currentThread().interrupt();
                    }
                });
                threadInterrupt.setDaemon(true);
                threadInterrupt.start();
                
//                System.out.println("Thread.currentThread() : interrupt");
//                Thread.currentThread().interrupt();
            }
        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | SecurityException e) {
            System.out.println("Main Try Exception e :" + e.toString());
            if (null != getCacheCommandObject(commandID)) {
                runCommands.removeCacheCommandProcess(commandID);
            }
            executionResult[0] = "Error : " + e.toString();
        } catch (Exception e ) {
            System.out.println("Main Try Exception e :" + e.toString());
            if (null != getCacheCommandObject(commandID)) {
                runCommands.removeCacheCommandProcess(commandID);
            }
            executionResult[0] = "Error : " + e.toString();
        }
        return executionResult[0];
    }
    
    private void jsonObjectResponse(HttpServletResponse response, boolean exceed) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("message", exceed ? "command-execution-time is exceed" : "Command is terminated by request");
        jsonObject.put("errorCode", exceed ? 1000 : 1001);
        jsonObject.put("successCode", 0);
        jsonObject.put("endDateExecution", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd:HH:mm:ss")));
        if (response != null && !response.isCommitted()) {
            try {
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                response.setHeader("Content-Length", String.valueOf(jsonObject.toString().length()));
                response.setStatus(exceed ? HttpServletResponse.SC_GATEWAY_TIMEOUT : HttpServletResponse.SC_GONE);
                response.getWriter().write(jsonObject.toString());
            } catch (IOException e) {
                System.out.println("error from response as null already, message is\t" + e.getMessage());
                e.printStackTrace();
            }
        }
    }
    
    /**
     * class file load.
     * @param clazzLoader
     * @param packgeName
     * @param files
     */
    private void loadClass(ByteClassLoader clazzLoader, String packgeName, File[] files) {
        String fileExt = "class";
        
        for(File f : files){
            String fileName = f.getName();
            int pos = fileName.lastIndexOf( "." );
            String ext = fileName.substring( pos + 1 );
            if(f.isFile() && fileExt.equals(ext)) {
                String clazzName = fileName.substring(0, pos);
                if((clazzName+"."+fileExt).equals(f.getName())) {
                    try {
                        clazzLoader.defineClass(packgeName + "." + clazzName, Files.readAllBytes(f.toPath()));
                    } catch (Exception e) {
                        e.fillInStackTrace();
                    }
                }
            } else if(f.isDirectory()) {
                //하위폴더의 class 검색
                String directoryName = f.getName();
                
                Pattern patternDetail = Pattern.compile("^[a-zA-Z]*$"); //영문의 폴더만 검색 백업폴더가 존재하여 제외함.
                Matcher matcherDetail = patternDetail.matcher(directoryName);
                
                if(matcherDetail.find()) {
                    String cPackgeName = packgeName+"."+f.getName();
                    File[] cFiles = f.listFiles();
                    if(null != cFiles && cFiles.length > 0) {
                        loadClass(clazzLoader,cPackgeName, cFiles);
                    }
                }
            }
        }
    }
    
    protected class ByteClassLoader extends ClassLoader {
        public ByteClassLoader(ClassLoader classLoader) {
            super(classLoader);
        }

        public Class<?> defineClass(String name, byte[] classBytes) {
            return defineClass(name, classBytes, 0, classBytes.length);
        }
    }
}