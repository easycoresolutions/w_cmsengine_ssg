package co.kr.coresolutions.service;

import co.kr.coresolutions.model.GAConnection;
import co.kr.coresolutions.model.dtos.GATemplateDto;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.analyticsreporting.v4.AnalyticsReporting;
import com.google.api.services.analyticsreporting.v4.AnalyticsReportingScopes;
import com.google.api.services.analyticsreporting.v4.model.DateRange;
import com.google.api.services.analyticsreporting.v4.model.Dimension;
import com.google.api.services.analyticsreporting.v4.model.DynamicSegment;
import com.google.api.services.analyticsreporting.v4.model.GetReportsRequest;
import com.google.api.services.analyticsreporting.v4.model.GetReportsResponse;
import com.google.api.services.analyticsreporting.v4.model.Metric;
import com.google.api.services.analyticsreporting.v4.model.OrFiltersForSegment;
import com.google.api.services.analyticsreporting.v4.model.Report;
import com.google.api.services.analyticsreporting.v4.model.ReportRequest;
import com.google.api.services.analyticsreporting.v4.model.Segment;
import com.google.api.services.analyticsreporting.v4.model.SegmentDefinition;
import com.google.api.services.analyticsreporting.v4.model.SegmentDimensionFilter;
import com.google.api.services.analyticsreporting.v4.model.SegmentFilter;
import com.google.api.services.analyticsreporting.v4.model.SegmentFilterClause;
import com.google.api.services.analyticsreporting.v4.model.SimpleSegment;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class QueryServiceGA {
    private static final String APPLICATION_NAME = "queryServer_Backend";
    private static JsonFactory jsonFactory = new JacksonFactory();
    private final Constants constants;
//    private static Dimension segmentDimensions = new Dimension().setName("ga:segment");

    public AnalyticsReporting initializeAnalyticsReporting(GAConnection gaConnection) throws IOException {
        HttpTransport httpTransport = new NetHttpTransport();
        GoogleCredential googleCredential = GoogleCredential
                .fromStream(new FileInputStream(constants.getRootDir() + gaConnection.getJsonKey()))
                .createScoped(AnalyticsReportingScopes.all());
        return new AnalyticsReporting.Builder(httpTransport, jsonFactory, googleCredential)
                .setApplicationName(APPLICATION_NAME).build();
    }

    public GetReportsResponse getResponse(AnalyticsReporting service, GAConnection gaConnection, GATemplateDto.QueryGA queryGA) throws IOException {
        DateRange dateRange = new DateRange();
        dateRange.setStartDate(queryGA.getStartDate());
        dateRange.setEndDate(queryGA.getEndDate());

        List<Metric> metrics = queryGA.getMetrics().stream().map(s -> new Metric().setExpression(s)).collect(Collectors.toList());
        List<Dimension> dimensions = queryGA.getDimensions().stream().map(s -> new Dimension().setName(s)).collect(Collectors.toList());
//        dimensions.add(segmentDimensions);
        List<Segment> segments = queryGA.getFilters().toMap().entrySet()
                .stream().map(stringObjectEntry -> buildSimpleSegment(stringObjectEntry.getKey(), stringObjectEntry.getValue().toString())).collect(Collectors.toList());

        val request = new ReportRequest()
                .setViewId(gaConnection.getProfileId())
                .setDateRanges(Collections.singletonList(dateRange))
                .setDimensions(dimensions)
                .setMetrics(metrics)
                .setSegments(segments)
                .setPageSize(queryGA.getMaxResults());
        return service.reports().batchGet(new GetReportsRequest().setReportRequests(Collections.singletonList(request))).execute();
    }

    private Segment buildSimpleSegment(String dimension, String dimensionFilterExpression) {
        SegmentDimensionFilter dimensionFilter = new SegmentDimensionFilter().setDimensionName(dimension).setExpressions(Collections.singletonList(dimensionFilterExpression));
        SegmentFilterClause segmentFilterClause = new SegmentFilterClause().setDimensionFilter(dimensionFilter);
        OrFiltersForSegment orFiltersForSegment = new OrFiltersForSegment().setSegmentFilterClauses(Collections.singletonList(segmentFilterClause));
        SimpleSegment simpleSegment = new SimpleSegment().setOrFiltersForSegment(Collections.singletonList(orFiltersForSegment));
        SegmentFilter segmentFilter = new SegmentFilter().setSimpleSegment(simpleSegment);
        SegmentDefinition segmentDefinition = new SegmentDefinition().setSegmentFilters(Collections.singletonList(segmentFilter));
        DynamicSegment dynamicSegment = new DynamicSegment().setSessionSegment(segmentDefinition);
        return new Segment().setDynamicSegment(dynamicSegment);
    }

    public String saveResponse(List<Report> reports, String outputFile) throws IOException {
        Path pathOutDir = Paths.get(constants.getRootDir() + outputFile);
        StringBuilder stringBuffer = new StringBuilder();
        reports.stream().filter(report -> report != null && report.getData() != null).forEach(report -> {
            try {
                stringBuffer.append(report.getData().toPrettyString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        if (!outputFile.equalsIgnoreCase("native")) {
            Files.write(pathOutDir, stringBuffer.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
            return null;
        }
        return stringBuffer.toString();
    }
}
