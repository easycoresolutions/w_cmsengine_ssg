package co.kr.coresolutions.service;

import co.kr.coresolutions.model.Project;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class QueryServiceProjects {
    private boolean indexExists = false;
    private boolean projectExists = false;
    private final Constants constants;
    private final ObjectMapper objectMapper;
    private String _projectQueryDir;
    private boolean ownerExists = false;

    @PostConstruct
    public void init() {
        _projectQueryDir = constants.getProjectQueryDir();
    }

    public String saveProject(String owner, String projectid, Project project) throws IOException {
        indexExists = false;
        projectExists = false;
        ownerExists = false;
        if (!Files.isDirectory(Paths.get(_projectQueryDir)))
            Files.createDirectory(Paths.get(_projectQueryDir));
        boolean fail = false;
        try (Stream<Path> paths = Files.walk(Paths.get(_projectQueryDir))) {
            paths
                    .filter(Files::isDirectory)
                    .forEach(path -> {

                        try (Stream<Path> path1 = Files.walk(Paths.get(_projectQueryDir + File.separator + owner))) {
                            ownerExists = true;
                            path1
                                    .filter(Files::isRegularFile)
                                    .forEach(path2 -> {
                                        if (path2.getFileName().toString().equalsIgnoreCase("index.txt")) {
                                            indexExists = true;
                                        } else if (path2.getFileName().toString().equalsIgnoreCase(projectid + ".txt")) {
                                            projectExists = true;
                                        }
                                    });
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }

                    });
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }
        if (fail) return "fail";
        Path ownerPath = Paths.get(_projectQueryDir + File.separator + owner);
        if (!ownerExists) {
            ownerExists = true;
            try {
                if (!Files.isDirectory(Paths.get(_projectQueryDir + File.separator + owner)))
                    ownerPath = Files.createDirectory(Paths.get(_projectQueryDir + File.separator + owner));
            } catch (IOException e) {
                e.printStackTrace();
                return "fail";
            }
        }

        JSONArray jsonArray = null;

        if (ownerExists) {
            byte[] projectQuery = objectMapper.writeValueAsBytes(project);
            JSONObject jsonObject1 = new JSONObject(new String(projectQuery));
            JSONObject PROJECTWithoutJsonField = new JSONObject(jsonObject1, Arrays.asList(JSONObject.getNames(jsonObject1)).stream().filter(s ->
                    !s.equals("Proj_json")
            ).collect(Collectors.toList()).toArray(new String[JSONObject.getNames(jsonObject1).length]));

            if (indexExists) {
                //update the index file.
                try {

                    JsonParser parser = new JsonParser();
                    //JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(ownerPath + File.separator+"index.txt"))));
                    JsonElement jsonElement = parser.parse(new String(Files.readAllBytes(Paths.get(_projectQueryDir + File.separator + owner + File.separator + "index.txt")), StandardCharsets.UTF_8));
                    JsonArray _jsonArraytemp = jsonElement.getAsJsonArray();

                    JSONArray returnArray = new JSONArray();
                    _jsonArraytemp.iterator().forEachRemaining(e -> {
                        org.json.JSONObject _temp = new org.json.JSONObject(e.toString());
                        if (!_temp.has("Proj_ID") || (_temp.has("Proj_ID") && !_temp.get("Proj_ID").toString().equalsIgnoreCase(projectid))) {
                            returnArray.put(_temp);
                        }
                    });

                    returnArray.put(PROJECTWithoutJsonField);
                    try {
                        File file = new File(ownerPath + File.separator + "index.txt");
                        if (!file.exists()) {
                            file.createNewFile();
                        }

                        BufferedWriter output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getPath()), StandardCharsets.UTF_8));
                        output.write(returnArray.toString());
                        output.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                        return "fail";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return "fail";
                }
            } else {
                //create new index file and add the props.
                jsonArray = new JSONArray();
                jsonArray.put(PROJECTWithoutJsonField);
                try {
                    //if(!Files.isDirectory(Paths.get(_projectQueryDir+File.separator+owner+File.separator+"projects")))
                    //  Files.createDirectory(Paths.get(_projectQueryDir+File.separator+owner+File.separator+"projects"));
                    File file = new File(ownerPath + File.separator + "index.txt");
                    if (!file.exists()) {
                        file.createNewFile();
                    }

                    BufferedWriter output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getPath()), StandardCharsets.UTF_8));
                    output.write(jsonArray.toString());
                    output.close();
                    //Files.write(Paths.get(ownerPath +File.separator+"index.txt"), jsonArray.toString().getBytes("UTF-8"),
                    //        StandardOpenOption.CREATE);
                } catch (IOException e1) {
                    e1.printStackTrace();
                    return "fail";
                }
            }
            if (projectExists) {
                //update it
                try {
                    File file = new File(ownerPath + File.separator + projectid + ".txt");
                    if (!file.exists()) {
                        file.createNewFile();
                    }

                    BufferedWriter output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getPath()), StandardCharsets.UTF_8));
                    output.write(jsonObject1.toString());
                    output.close();
                    //Files.write(Paths.get(ownerPath+File.separator+projectid+".txt"), projectQuery,
                    //        StandardOpenOption.CREATE,StandardOpenOption.TRUNCATE_EXISTING);
                } catch (IOException e) {
                    e.printStackTrace();
                    return "fail";
                }
            } else {
                //create new one
                try {
                    //if(!Files.isDirectory(Paths.get(_projectQueryDir+File.separator+owner+File.separator+"projects")))
                    //Files.createDirectory(Paths.get(_projectQueryDir+File.separator+owner+File.separator+"projects"));
                    File file = new File(ownerPath + File.separator + projectid + ".txt");
                    if (!file.exists()) {
                        file.createNewFile();
                    }

                    BufferedWriter output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getPath()), StandardCharsets.UTF_8));

                    output.write(jsonObject1.toString());
                    output.close();
                    //Files.write(Paths.get(ownerPath+File.separator+projectid+".txt"),projectQuery,
                    //        StandardOpenOption.CREATE);
                } catch (IOException e) {
                    e.printStackTrace();
                    return "fail";
                }
            }
        }

        return "success";
    }

    public String getProjects(String owner) throws IOException {
        if (!Files.isDirectory(Paths.get(_projectQueryDir)))
            Files.createDirectory(Paths.get(_projectQueryDir));

        try {
            return new String(Files.readAllBytes(Paths.get(_projectQueryDir + File.separator + owner + File.separator + "index.txt")), StandardCharsets.UTF_8);
        } catch (IOException e1) {
            e1.printStackTrace();
            return "fail";
        }
    }

    public String getProject(String owner, String projectID) throws IOException {
        if (!Files.isDirectory(Paths.get(_projectQueryDir)))
            Files.createDirectory(Paths.get(_projectQueryDir));

        if (!Files.isDirectory(Paths.get(_projectQueryDir + File.separator + owner))) {
            return "failowner";
        } else {
            try {
                return new String(Files.readAllBytes(Paths.get(_projectQueryDir + File.separator + owner + File.separator + projectID + ".txt")), StandardCharsets.UTF_8);
            } catch (IOException e) {
                e.printStackTrace();
                return "failprojectid";
            }
        }
    }

    public boolean deleteProject(String owner, String projectID) throws IOException {
        if (!Files.isDirectory(Paths.get(_projectQueryDir)))
            Files.createDirectory(Paths.get(_projectQueryDir));

        if (!Files.isDirectory(Paths.get(_projectQueryDir + File.separator + owner))) {
            return false;
        } else {
            try {
                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(_projectQueryDir + File.separator + owner + File.separator + "index.txt"))));
                JsonArray _jsonArraytemp = jsonElement.getAsJsonArray();

                JSONArray returnArray = new JSONArray();
                _jsonArraytemp.iterator().forEachRemaining(e -> {
                    org.json.JSONObject _temp = new org.json.JSONObject(e.toString());
                    if (!_temp.get("Proj_ID").toString().equalsIgnoreCase(projectID))
                        returnArray.put(_temp);
                });

                try {
                    Files.write(Paths.get(_projectQueryDir + File.separator + owner + File.separator + "index.txt"), returnArray.toString().getBytes(StandardCharsets.UTF_8),
                            StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                return Files.deleteIfExists(Paths.get(_projectQueryDir + File.separator + owner + File.separator + projectID + ".txt"));
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
    }

}
