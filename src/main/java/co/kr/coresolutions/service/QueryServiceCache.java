package co.kr.coresolutions.service;

import lombok.RequiredArgsConstructor;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

import static org.apache.ignite.cache.CacheAtomicityMode.TRANSACTIONAL;

@Service
@RequiredArgsConstructor
public class QueryServiceCache {
    private final QueryService queryService;
    private final Constants constants;
    private String referenceQueryDir;

    @PostConstruct
    public void init() {
        referenceQueryDir = constants.getReferenceDir();
    }

    public void refresh() {
        Ignite ignite = Ignition.ignite();
        ignite.cacheNames().forEach(s -> ignite.destroyCache("cacheReference"));

        CacheConfiguration cfg = new CacheConfiguration();
        AtomicBoolean failCaching = new AtomicBoolean(false);
        cfg.setName("cacheReference");
        cfg.setAtomicityMode(TRANSACTIONAL);

        // Create cache with given name, if it does not exist.
        IgniteCache<String, String> cache = ignite.getOrCreateCache(cfg);
        try (Stream<Path> paths = Files.walk(Paths.get(referenceQueryDir))) {
            paths
                    .filter(Files::isRegularFile)
                    .forEach(path1 -> {
                                failCaching.set(false);
                                try {
                                    cache.putIfAbsent(path1.getFileName().toString(), new String(Files.readAllBytes(path1), StandardCharsets.UTF_8));
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                    failCaching.set(true);
                                }
                                try {
                                    if (!failCaching.get())
                                        queryService.addLogging("Refresh Cache", "REFERENCE_NAME : " + path1.getFileName().toString() + " (" + (Files.readAllLines(path1).size() - 1) + ") is cached successfully." + "\n");
                                    else
                                        queryService.addLogging("Refresh Cache", "REFERENCE_NAME : " + path1.getFileName().toString() + " has an error, not cached." + "\n");

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                    );
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
