package co.kr.coresolutions.service;

import co.kr.coresolutions.dao.OrderedJSONObject;
import co.kr.coresolutions.util.Util;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class QueryServiceJSON {
    private final QueryService queryService;

    public String structureJsonFromTAB(String resultgetFile) {
        String tsvOutput = resultgetFile.replaceAll("\t", ",");
        return structureJsonFromCSV(tsvOutput);
    }

    public String structureJsonFromCSV(String resultgetFile) {
        CsvSchema csvSchema = CsvSchema.builder().setUseHeader(true).build();
        CsvMapper csvMapper = new CsvMapper();
        List<Object> readAll = null;
        try {
            readAll = csvMapper.readerFor(Map.class).with(csvSchema).readValues(resultgetFile).readAll();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ObjectMapper mapper = new ObjectMapper();
        try {
            String returnContent = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(readAll);
            return returnContent;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String structureJson(String resultValidity, boolean isTransfer, Map<String, String> columnsDataType, String queryID) {
        class BreakException extends RuntimeException {
        }

        String finalQueryID = queryService.makeQueryId(queryID);
        JSONArray firstArray = new JSONArray();
        JSONArray secondArray = new JSONArray();
        JsonParser parser = new JsonParser();
        JsonElement jsonElement;
        JsonArray jsonArray;
        org.json.JSONObject global = new org.json.JSONObject();
        try {
            jsonElement = parser.parse(resultValidity);
            jsonArray = jsonElement.getAsJsonArray();
        } catch (Exception e) {
            queryService.setRunStop(finalQueryID);
            return global.toString();
        }

        for (int i = 0; i < jsonArray.size(); ++i) {
            if (jsonArray.get(i) == null || jsonArray.get(i).toString().equals("{}")) {
                jsonArray.remove(i);
            }
        }
        if (jsonArray.size() > 0) {
            OrderedJSONObject one = new OrderedJSONObject(jsonArray.get(0).toString());
            try {
                one.keySet().forEach(s -> {
                    if (queryService.isRunStop(finalQueryID)) {
                        System.out.println("[" + queryID + "] data to json process !! Stop !!!!");
                        throw new BreakException();
                    }

                    OrderedJSONObject tempFields = new OrderedJSONObject();
                    if (s.length() > 0) {
                        tempFields.put("FieldName", s);
                        if (!isTransfer) {
                            tempFields.put("FieldDesc", s);
                            if (!columnsDataType.containsKey(s.toLowerCase())) {
                                tempFields.put("FieldType", getValueType(jsonElement.getAsJsonArray(), s));
                            } else {
                                String columnType = columnsDataType.get(s.toLowerCase());
                                if (columnType.equalsIgnoreCase("null")) {
                                    tempFields.put("FieldType", getValueType(jsonElement.getAsJsonArray(), s));
                                } else {
                                    tempFields.put("FieldType", Util.getColumnType(columnType));
                                }
                            }
                        }
                        firstArray.put(tempFields);
                    }
                });
            } catch (BreakException be) {
                System.out.println("query time out");
                queryService.setRunStop(finalQueryID);
                return ("Error : {\"query Timeout\"}");
            }
            global.put("Fields", firstArray);

            try {
                jsonArray.iterator().forEachRemaining(e -> {
                    if (queryService.isRunStop(finalQueryID)) {
                        System.out.println("[" + queryID + "] data to json process !! Stop !!!!");
                        throw new BreakException();
                    }

                    OrderedJSONObject temp = new OrderedJSONObject(e.toString());

                    OrderedJSONObject secondTemp = new OrderedJSONObject(temp, Arrays.stream(OrderedJSONObject.getNames(temp)).filter(s ->
                            (s.length() > 0)
                    ).collect(Collectors.toList()).toArray(new String[OrderedJSONObject.getNames(temp).length]));

                    Iterator<String> iterator = temp.keys();
                    while (iterator.hasNext()) {
                        String s = iterator.next();
                        if (columnsDataType.containsKey(s.toLowerCase())) {
                            String value = secondTemp.get(s).toString();
                            String valueTemp = temp.get(s).toString();
                            String dataType = columnsDataType.get(s.toLowerCase());
                            if (valueTemp.equalsIgnoreCase("null")) {
                                if (!dataType.contains("char")) {
                                    secondTemp.remove(s);
                                }
                            } else if (value.equalsIgnoreCase("NotDefine")) {
                                secondTemp.put(s, "");
                            } else if (dataType.contains("char") || dataType.equals("null")) {
                                secondTemp.put(s, value);
                            } else if (temp.get(s) instanceof Number) {
                                iterator.remove();
                                secondTemp.put(s, new BigDecimal(value));
                            }
                        }
                    }
                    secondArray.put(secondTemp);
                });
            } catch (BreakException be) {
                System.out.println("query time out");
                queryService.setRunStop(finalQueryID);
                return ("Error : {\"query Timeout\"}");
            }
            global.put("Input", secondArray);

            return global.toString();
        } else {
            return global.toString();
        }
    }

    public String structureJsonWithExclusion(String resultValidity, boolean isTransfer, Map<String, String> columnsDataType, String queryID, String exclude) {
        class BreakException extends RuntimeException {
        }

        String finalQueryID = queryService.makeQueryId(queryID);
        List<String> excludes = Arrays.asList(exclude.split(","));
        JSONArray firstArray = new JSONArray();
        JSONArray secondArray = new JSONArray();
        JsonParser parser = new JsonParser();
        JsonElement jsonElement;
        JsonArray jsonArray;
        org.json.JSONObject global = new org.json.JSONObject();
        try {
            jsonElement = parser.parse(resultValidity);
            jsonArray = jsonElement.getAsJsonArray();
        } catch (Exception e) {
            queryService.setRunStop(finalQueryID);
            e.printStackTrace();
            return global.toString();
        }

        for (int i = 0; i < jsonArray.size(); ++i) {
            if (jsonArray.get(i) == null || jsonArray.get(i).toString().equals("{}")) {
                jsonArray.remove(i);
            }
        }
        if (jsonArray.size() > 0) {
            OrderedJSONObject one = new OrderedJSONObject(jsonArray.get(0).toString());
            try {
                one.keySet().forEach(s -> {
                    if (queryService.isRunStop(finalQueryID)) {
                        System.out.println("[" + queryID + "] data to json process !! Stop !!!!");
                        throw new BreakException();
                    }

                    OrderedJSONObject tempFields = new OrderedJSONObject();
                    if (s.length() > 0 && !excludes.contains(s)) {
                        tempFields.put("FieldName", s);
                        if (!isTransfer) {
                            tempFields.put("FieldDesc", s);
                            if (!columnsDataType.containsKey(s.toLowerCase())) {
                                tempFields.put("FieldType", getValueType(jsonElement.getAsJsonArray(), s));
                            } else {
                                String columnType = columnsDataType.get(s.toLowerCase());
                                if (columnType.equalsIgnoreCase("null")) {
                                    tempFields.put("FieldType", getValueType(jsonElement.getAsJsonArray(), s));
                                } else {
                                    tempFields.put("FieldType", Util.getColumnType(columnType));
                                }
                            }
                        }
                        firstArray.put(tempFields);
                    }
                });
            } catch (BreakException be) {
                System.out.println("query time out");
                queryService.setRunStop(finalQueryID);
                return ("Error : {\"query Timeout\"}");
            }
            global.put("Fields", firstArray);

            try {
                jsonArray.iterator().forEachRemaining(e -> {
                    if (queryService.isRunStop(finalQueryID)) {
                        System.out.println("[" + queryID + "] data to json process !! Stop !!!!");
                        throw new BreakException();
                    }

//                    OrderedJSONObject temp = new OrderedJSONObject(Util.replaceAll(e.toString(), "\"\"", "NotDefine"));
                    OrderedJSONObject temp = new OrderedJSONObject(e.toString());

                    OrderedJSONObject secondTemp = new OrderedJSONObject(temp, Arrays.stream(OrderedJSONObject.getNames(temp)).filter(s ->
                            (s.length() > 0) && !excludes.contains(s)
                    ).collect(Collectors.toList()).toArray(new String[OrderedJSONObject.getNames(temp).length]));

                    Iterator<String> iterator = temp.keys();
                    while (iterator.hasNext()) {
                        String s = iterator.next();
                        if (columnsDataType.containsKey(s.toLowerCase()) && !excludes.contains(s)) {
                            String value = secondTemp.get(s).toString();
                            String valueTemp = temp.get(s).toString();
                            String dataType = columnsDataType.get(s.toLowerCase());
                            if (valueTemp.equalsIgnoreCase("null")) {
                                if (!dataType.contains("char")) {
                                    secondTemp.remove(s);
                                }
                            } else if (value.equalsIgnoreCase("NotDefine")) {
                                secondTemp.put(s, "");
                            } else if (dataType.contains("char") || dataType.equals("null")) {
                                secondTemp.put(s, value);
                            } else if (temp.get(s) instanceof Number) {
                                iterator.remove();
                                secondTemp.put(s, new BigDecimal(value));
                            }
                        }
                    }
                    secondArray.put(secondTemp);
                });
            } catch (BreakException be) {
                System.out.println("query time out");
                queryService.setRunStop(finalQueryID);
                return ("Error : {\"query Timeout\"}");
            }
            global.put("Input", secondArray);

            return global.toString();
        } else {
            return global.toString();
        }
    }


    public String structureJsonForCommandStructure(String resultValidity, List<String> chars, List<String> nums) {
        JSONArray firstArray = new JSONArray();
        JSONArray secondArray = new JSONArray();
        JsonParser parser = new JsonParser();
        JsonElement jsonElement = parser.parse(resultValidity);
        JsonArray jsonArray = jsonElement.getAsJsonArray();
        org.json.JSONObject _global = new org.json.JSONObject();
        AtomicBoolean charsExists = new AtomicBoolean(false);
        AtomicBoolean numsExists = new AtomicBoolean(false);
        if (jsonArray.size() > 0) {
            JsonElement one = jsonArray.get(0);
            one.getAsJsonObject().entrySet().forEach(e -> {
                org.json.JSONObject _temp = new org.json.JSONObject();
                _temp.put("FieldName", e.getKey());
                _temp.put("FieldDesc", e.getKey());
                if (chars != null && nums == null) {
                    if (chars.contains(e.getKey())) {
                        charsExists.set(true);
                        _temp.put("FieldType", "CHAR");
                    } else _temp.put("FieldType", "NUM");
                } else if (chars == null && nums != null) {
                    if (nums.contains(e.getKey())) {
                        numsExists.set(true);
                        _temp.put("FieldType", "NUM");
                    } else _temp.put("FieldType", "CHAR");

                } else if (charsExists.get()) _temp.put("FieldType", "NUM");
                else if (numsExists.get()) _temp.put("FieldType", "CHAR");

                else
                    _temp.put("FieldType", chars != null && chars.contains(e.getKey()) ? "CHAR" :
                            nums != null && nums.contains(e.getKey()) ? "NUM" :
                                    (StringUtils.isNumeric(e.getValue().toString()) ? "NUM" : "CHAR"));
                     /*else{
                    _temp.put("FieldType", (isInteger(e.getValue().toString()) ? "INT" :
                            isFloat(e.getValue().toString())?"FLOAT" : isDouble(e.getValue().toString()) ? "DOUBLE":"VARCHAR"));
                }*/
                firstArray.put(_temp);
            });

            //if(!isTransfer)
            List<String> numsFields = new LinkedList<>();
            _global.put("Fields", firstArray);

            firstArray.forEach(oneField -> {
                org.json.JSONObject _temp = new org.json.JSONObject(oneField.toString());
                if (_temp.get("FieldType").toString().equalsIgnoreCase("NUM")) {
                    numsFields.add(_temp.get("FieldName").toString());
                }
            });


            jsonArray.iterator().forEachRemaining(e -> {
                org.json.JSONObject _temp = new org.json.JSONObject(e.toString());
                org.json.JSONObject _secondTemp = new org.json.JSONObject(e.toString());
                Iterator<String> iterator = _temp.keys();
                while (iterator.hasNext()) {
                    String s = iterator.next();
                    if (numsFields.contains(s)) {
                        iterator.remove();
                        String valueOfKeyS = _secondTemp.get(s).toString();
                        _secondTemp.put(s, new BigDecimal(valueOfKeyS));
                    }
                }
                secondArray.put(_secondTemp);
            });


            _global.put("Input", secondArray);
            return _global.toString();
        } else {
            return _global.toString();
        }
    }

    private String getValueType(JsonArray jsonArray, String key) {
        Map<String, String> typeMap = new HashMap<>();
        for (int i = 0; i < jsonArray.size(); ++i) {
            if (i >= 20) {
                break;
            }
            OrderedJSONObject row = new OrderedJSONObject(jsonArray.get(i).toString());
            // 정의된 type이 없을경우 20row의 value값을 읽어 numeric 스트링인지 판별하여 타입 정의
            row.keySet().forEach(s -> {
                String type;
                if (s.length() > 0 && s.equalsIgnoreCase(key)) {
                    type = row.get(s) instanceof Number ? "NUM" : "CHAR";
                    if (typeMap.get(s) != null && typeMap.get(s).equalsIgnoreCase("CHAR")) {
                        type = "CHAR";
                    }
                    typeMap.put(s, type);
                }
            });
        }
        return typeMap.get(key);
    }

}
