package co.kr.coresolutions.service;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class QueryServiceFiles {
    private final Constants constants;
    private String _connectionDir;
    private String _rootDir;
    private String _businessQueryDir;
    private String _fileQueryDir;
    private boolean ownerExists = false;
    private boolean fileExists = false;
    private String contentFile = "";

    @PostConstruct
    public void init() {
        _connectionDir = constants.getConnectionDir();
        _rootDir = constants.getRootDir();
        _businessQueryDir = constants.getBusinessQueryDir();
        _fileQueryDir = constants.getFileQueryDir();
    }
    /**
     * Description : check if file exists in connection dir
     *
     * @param connectionID : take the input number of file.
     * @return boolean: true if exist and false if not.
     */
    public boolean isConnectionIdFileExists(String connectionID) throws IOException {
        if (!Files.isDirectory(Paths.get(_fileQueryDir)))
            Files.createDirectory(Paths.get(_fileQueryDir));

        return Files.exists(Paths.get(_connectionDir + connectionID + ".txt"));
    }

    public String fetchQueryFromFile(String query_id) throws IOException {
        if (!Files.isDirectory(Paths.get(_businessQueryDir)))
            Files.createDirectory(Paths.get(_businessQueryDir));

        String path = String.valueOf(Paths.get(_businessQueryDir + query_id + ".txt"));
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(path), StandardCharsets.UTF_8);
        BufferedReader bfReader = new BufferedReader(inputStreamReader);
        StringBuilder builder = new StringBuilder();
        String line;

        while ((line = bfReader.readLine()) != null) {
        	builder.append(line).append("\r\n");
        }
        inputStreamReader.close();
        bfReader.close();
        return builder.toString();
        //return new String( Files.readAllBytes(Paths.get(_businessQueryDir+query_id+".txt")));
    }

    /**
     * Description : check the existance of the file query.
     *
     * @param _query_id : take the input as query id.
     * @return Boolean
     */
    public boolean isQueryFileExist(String _query_id) throws IOException {
        if (!Files.isDirectory(Paths.get(_businessQueryDir)))
            Files.createDirectory(Paths.get(_businessQueryDir));

        return Files.exists(Paths.get(_businessQueryDir + _query_id + ".txt"));
    }

    /**
     * Description: create directory in the root dir if doesn't exists, and create file within the directory if doesn't exists, otherweise update the content
     */
    synchronized public String uploadFile(String fileContents, String owner, String filename, String ext) throws IOException {
        ownerExists = false;
        fileExists = false;
        if (!Files.isDirectory(Paths.get(_fileQueryDir)))
            Files.createDirectory(Paths.get(_fileQueryDir));

        try (Stream<Path> paths = Files.walk(Paths.get(_fileQueryDir))) {
            paths
                    .filter(Files::isDirectory)
                    .forEach(e -> {

                        try (Stream<Path> path = Files.walk(Paths.get(_fileQueryDir + File.separator + owner))) {
                            ownerExists = true;
                            path
                                    .filter(Files::isRegularFile)
                                    .forEach(path1 -> {
                                        if (path1.getFileName().toString().equalsIgnoreCase(filename + "." + ext)) {
                                            fileExists = true;
                                            try {

                                                File file = new File(_fileQueryDir + File.separator + owner + File.separator + filename + "." + ext);
                                                if (!file.exists()) {
                                                    file.createNewFile();
                                                }

                                                BufferedWriter output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getPath()), StandardCharsets.UTF_8));
                                                output.write(fileContents);
                                                output.close();
                                                //Files.write(Paths.get(_fileQueryDir+File.separator+owner+File.separator+filename+"."+ext), fileContents.getBytes("UTF-8"),
                                                //        StandardOpenOption.CREATE,StandardOpenOption.TRUNCATE_EXISTING);
                                            } catch (IOException e1) {
                                                e1.printStackTrace();
                                            }
                                        }
                                    });
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }

                    });
            paths.close();
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }
        Path ownerPath = Paths.get(_fileQueryDir + File.separator + owner);
        if (!ownerExists) {
            ownerExists = true;
            try {
                if (!Files.isDirectory(Paths.get(_fileQueryDir + File.separator + owner)))
                    ownerPath = Files.createDirectory(Paths.get(_fileQueryDir + File.separator + owner));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (!fileExists) {
            try {
                Files.write(Paths.get(ownerPath + File.separator + filename + "." + ext), fileContents.getBytes(StandardCharsets.UTF_8),
                        StandardOpenOption.CREATE_NEW);
            } catch (IOException e) {
                e.printStackTrace();
                return "fail";
            }
        }
        return "success";
    }

    public String deleteFile(String owner, String filename) throws IOException {
        if (!Files.isDirectory(Paths.get(_fileQueryDir)))
            Files.createDirectory(Paths.get(_fileQueryDir));
        try {
            Files.deleteIfExists(Paths.get(_fileQueryDir + File.separator + owner + File.separator + filename));
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }
        return "success";
    }

    public String getFile(String owner, String filename) throws IOException {
        contentFile = "";
        fileExists = false;
        if (!Files.isDirectory(Paths.get(_fileQueryDir)))
            Files.createDirectory(Paths.get(_fileQueryDir));
        try (Stream<Path> paths = Files.walk(Paths.get(_fileQueryDir + File.separator + owner))) {
            paths
                    .filter(Files::isRegularFile)
                    .forEach(path1 -> {
                        if (path1.getFileName().toString().equalsIgnoreCase(filename)) {
                            fileExists = true;
                            try {
                                contentFile = new String(Files.readAllBytes(path1), StandardCharsets.UTF_8);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }

        if (!fileExists) return "fail";
        return contentFile;
    }

    public Boolean FileExist(String owner, String filename) throws IOException {
        if (!Files.isDirectory(Paths.get(_fileQueryDir)))
            Files.createDirectory(Paths.get(_fileQueryDir));
        fileExists = false;
        try (Stream<Path> paths = Files.walk(Paths.get(_fileQueryDir + File.separator + owner))) {
            paths
                    .filter(Files::isRegularFile)
                    .forEach(path1 -> {
                        if (path1.getFileName().toString().equalsIgnoreCase(filename)) {
                            fileExists = true;
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return fileExists;
    }


    public String getFiles(boolean business, boolean connection, String owner) throws IOException {
        if (!Files.isDirectory(Paths.get(_fileQueryDir)))
            Files.createDirectory(Paths.get(_fileQueryDir));
        JSONArray myArray = new JSONArray();

        try (Stream<Path> paths = Files.walk(Paths.get(business ? _businessQueryDir : connection ? _connectionDir : _fileQueryDir + File.separator + owner))) {
            paths
                    .filter(Files::isRegularFile)
                    .forEach(path1 -> {
                        org.json.JSONObject _temp = new org.json.JSONObject();
                        _temp.put("filename", path1.getFileName().toString());

                        try {
                            _temp.put("size", Files.size(path1));
                            _temp.put("created", Files.readAttributes(path1, BasicFileAttributes.class).creationTime()
                                    .toInstant()
                                    .atZone(ZoneId.systemDefault())
                                    .toLocalDateTime());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        myArray.put(_temp);
                    });
        } catch (IOException e) {
            e.printStackTrace();
            return "fail";
        }
        return myArray.toString();
    }

    public boolean saveConfigFile(JsonNode jsonNode) {
        String resource = constants.getConfigDir();
        String pathConfig = resource + "config.txt";
        if (Files.exists(Paths.get(pathConfig))) {
            try {
                Files.move(Paths.get(pathConfig), Paths.get(pathConfig).resolveSibling("config_backup_" + new SimpleDateFormat("yyyyMMddHHmmss")
                        .format(new Date()) + ".txt"), StandardCopyOption.REPLACE_EXISTING);
                Files.write(Paths.get(pathConfig), jsonNode.toString().getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
            } catch (IOException e) {
                return false;
            }
        }
        return true;
    }

    public String getConfigFile() {
        JsonNode jsonNode = constants.getConfigFileAsJson();
        if (jsonNode != null) {
            return jsonNode.toString();
        }
        return "fail";
    }

    public void SaveOrUpdateQueryFile(String queryID, String sql) throws IOException {
        File file = new File(_businessQueryDir + queryID + ".txt");
        if (!file.exists()) {
            file.createNewFile();
        }

        BufferedWriter output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getPath()), StandardCharsets.UTF_8));
        output.write(sql);
        output.close();
    }

    public void deleteQueryFile(String queryID) throws IOException {
        Files.deleteIfExists(Paths.get(_businessQueryDir + queryID + ".txt"));
    }

    public void SaveOrUpdateConnectionFile(String connectionID, String contentFile) throws IOException {
        File file = new File(_connectionDir + connectionID + ".txt");
        if (!file.exists()) {
            file.createNewFile();
        }

        BufferedWriter output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file.getPath()), StandardCharsets.UTF_8));
        output.write(contentFile);
        output.close();
        //Files.write(Paths.get(_connectionDir+connectionID+".txt"), contentFile.getBytes("UTF-8"),
        //        StandardOpenOption.CREATE,StandardOpenOption.TRUNCATE_EXISTING);
    }

    public void deleteConnectionFile(String connectionID) throws IOException {
        Files.deleteIfExists(Paths.get(_connectionDir + connectionID + ".txt"));
    }

}
