package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.Connection;
import co.kr.coresolutions.model.RModel;
import co.kr.coresolutions.model.RModelActivated;
import co.kr.coresolutions.model.RModelData;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.json.JSONArray;
import org.json.JSONObject;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.RList;
import org.rosuda.REngine.Rserve.RConnection;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class QueryServiceRServe {
    private final QueryService queryService;
    private final QueryServiceJSON queryServiceJSON;
    private final QueryServiceCSV queryServiceCSV;
    private final ObjectMapper objectMapper;
    private final Constants constants;
    private String _RmodelQueryDir;
    private String _RActivatedQueryDir;
    private String _SystemQueryDir;
    private boolean indexExists = false;
    private boolean userIDExists = false;

    @PostConstruct
    public void init() {
        _RmodelQueryDir = constants.getRModelQueryDir();
        _RActivatedQueryDir = constants.getRActivationQueryDir();
        _SystemQueryDir = constants.getSystemQueryDir();
    }

    public String openConnection(String _connectionID, String userid) {
        try {
            Connection connection = queryService.getInfoConnection(_connectionID);
            if (getRConnection(userid, false) == null) {
                RConnection rConnection = new RConnection(connection.getHost(), Integer.parseInt(connection.getPort()));
                rConnection.login(userid, connection.getPW());
                saveRconnection(userid, rConnection);

//                evalMultipleCommand(rConnection, new String(Files.readAllBytes(Paths.get(_SystemQueryDir + "init_r.r"))), "");
            }
        } catch (REngineException e) {
            return "error : {" + e.getMessage() + "}";
        }
        return "";
    }


    public void saveRconnection(String userid, RConnection rConnection) {
        CacheManager cm = CacheManager.getInstance();

        if (!cm.cacheExists("Rcache"))
            cm.addCache("Rcache");

        Cache cache = cm.getCache("Rcache");
        try {
            cache.put(new Element(userid, rConnection));

        } catch (Exception e) {
        }
    }

    public RConnection getRConnection(String userid, boolean removeCache) {
        CacheManager cm = CacheManager.getInstance();
        Cache cache = cm.getCache("Rcache");

        Element ele = cache.get(userid);
        if (ele != null) {

            RConnection rConnection = (RConnection) ele.getObjectValue();
            if (rConnection != null && rConnection.isConnected()) {
                if (removeCache) cache.remove(userid);

                return rConnection;
            } else return null;

        } else {
            return null;
        }
    }

    public CustomResponseDto closeConnection(String userid) {
        RConnection rConnection = getRConnection(userid, true);
        CustomResponseDto customResponseDto = CustomResponseDto.builder().message("fail due connection doesn't exist").build();
        if (rConnection != null) {
            if (rConnection.isConnected()) {
                customResponseDto.setMessage("success closed connection");
                customResponseDto.setSuccess(rConnection.close());
            } else {
                customResponseDto.setMessage("fail due userid " + userid + " is not connected to Rserve");
            }
        }
        return customResponseDto;
    }

    public String evaluate(String userid, String script) {
        RConnection rConnection = getRConnection(userid, false);
        StringBuilder stringBuilder = new StringBuilder();
        if (rConnection != null && rConnection.isConnected()) {
            try {
                stringBuilder.append(evalMultipleCommand(rConnection, script, ""));
            } catch (REXPMismatchException | REngineException e) {
                return "error : { " + e.getMessage() + " }";
            }
        } else return "error : {the user " + userid + " is not connected to Rserve}";

        return stringBuilder.toString();
    }

    public String importFile(String userid, String filePath) {
        RConnection rConnection = getRConnection(userid, false);
        StringBuilder stringBuilder = new StringBuilder();
        if (rConnection != null && rConnection.isConnected()) {
            try {
                String content = new String(Files.readAllBytes(Paths.get(filePath)));
                stringBuilder.append(evalMultipleCommand(rConnection, content, "importapi"));
            } catch (IOException e) {
                return "error : {file " + filePath + " not found}";
            } catch (REngineException | REXPMismatchException e) {
                return "error : { " + e.getMessage() + " }";
            }
        } else return "error : {the user " + userid + " is not connected to Rserve}";
        return stringBuilder.toString();
    }

    private String evalMultipleCommand(RConnection rConnection, String content, String outputtype) throws REXPMismatchException, REngineException {
        List<String> commands;
        if (!outputtype.equals("importapi")) {
            commands = Arrays.asList(content.split(";"));
        } else commands = Arrays.asList(content.split("\n"));
        Iterator<String> stringIterator = commands.iterator();
        StringBuilder stringBuilderLocal = new StringBuilder();
        while (stringIterator.hasNext()) {
            String script = stringIterator.next();
            REXP rexp = null;
            if (script.length() > 0 && script.matches(".*[a-zA-Z0-9]+.*")) {
                if (!outputtype.equals("importapi"))
                    rexp = rConnection.parseAndEval("try(eval(" + script + "),silent=TRUE)");
                else {
                    rexp = rConnection.parseAndEval(script);
                }
            }

            if (rexp != null)
                if (rexp.inherits("try-error")) {

                    throw new REngineException(null, rexp.asString());
                } else if (rexp.isNumeric()) {
                    stringBuilderLocal.append(Arrays.asList(rexp.asStrings()).toString());
                } else if (rexp.isList() && !rexp.isComplex()) {
                    RList rList = rexp.asList();
                    if (rList != null)
                        if (rList.size() > 0)
                            if (formatRListToTAB(rList).length() > 0)
                                if (outputtype.length() > 0) {
                                    if (outputtype.equalsIgnoreCase("tab"))
                                        stringBuilderLocal.append(new String(formatRListToTAB(rList).getBytes(), StandardCharsets.UTF_8));
                                    else if (outputtype.equalsIgnoreCase("csv")) {
                                        stringBuilderLocal.append(new String(queryServiceCSV.structureCSV(queryServiceJSON.structureJsonFromTAB(formatRListToTAB(rList))).getBytes(), StandardCharsets.UTF_8));
                                    } else if (outputtype.equalsIgnoreCase("json")) {
                                        stringBuilderLocal.append(queryServiceJSON.structureJson(queryServiceJSON.structureJsonFromTAB(formatRListToTAB(rList)), false, new HashMap<>(), ""));
                                    }
                                } else
                                    stringBuilderLocal.append(new String(formatRListToTAB(rList).getBytes(), StandardCharsets.UTF_8));
                } else if (rexp.isString()) {
                    AtomicReference<String> localStr = new AtomicReference<>("");

                    Arrays.asList(rexp.asStrings()).stream().forEachOrdered(s -> {
                        localStr.updateAndGet(v -> v + " " + s);
                    });
                    stringBuilderLocal.append(localStr.get());
                } else if (rexp.isVector() && rexp.isNumeric()) {

                    stringBuilderLocal.append(Arrays.asList(rexp.asDoubles()));

                }

            stringBuilderLocal.append("\n");
        }

        return stringBuilderLocal.toString();
    }

    @SneakyThrows
    public String uploadFile(String connectionID, RModel rModel, String modelid) {
        RConnection rConnection = getRConnection(String.valueOf(rModel.getUserid()), false);
        if (rConnection != null && rConnection.isConnected()) {
            return saveFile(rModel, modelid);
        } else return "error : {the user " + rModel.getUserid() + " is not connected to Rserve}";
    }

    public String saveFile(RModel rModel, String modelid) throws IOException {
        indexExists = false;
        userIDExists = false;
        try {
            if (!Files.isDirectory(Paths.get(_RmodelQueryDir)))
                Files.createDirectory(Paths.get(_RmodelQueryDir));
            try (Stream<Path> paths = Files.walk(Paths.get(_RmodelQueryDir))) {
                paths
                        .filter(Files::isDirectory)
                        .forEach(path -> {
                            try (Stream<Path> path1 = Files.walk(Paths.get(_RmodelQueryDir + File.separator + rModel.getUserid()))) {
                                userIDExists = true;
                                path1
                                        .filter(Files::isRegularFile)
                                        .forEach(path2 -> {
                                            if (path2.getFileName().toString().equalsIgnoreCase("index.txt")) {
                                                indexExists = true;
                                            }

                                        });
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        });
            }
        } catch (IOException e) {
            return "error : { " + e.getMessage() + " }";
        }

        Path userIDPath = Paths.get(_RmodelQueryDir + File.separator + rModel.getUserid());

        if (!userIDExists) {

            try {
                if (!Files.isDirectory(Paths.get(_RmodelQueryDir + File.separator + rModel.getUserid())))
                    userIDPath = Files.createDirectory(Paths.get(_RmodelQueryDir + File.separator + rModel.getUserid()));
                userIDExists = true;
            } catch (IOException e) {
                return "error : { " + e.getMessage() + " }";
            }
        }
        JSONArray jsonArray = null;
        if (userIDExists) {
            byte[] rModelQuery = objectMapper.writeValueAsBytes(rModel);
            JSONObject jsonObjectRModelQuery = new JSONObject(new String(rModelQuery));
            JSONObject rModelMetaQuery = new JSONObject(jsonObjectRModelQuery, Arrays.asList(JSONObject.getNames(jsonObjectRModelQuery)).stream().filter(s ->
                    !s.equals("file")
            ).collect(Collectors.toList()).toArray(new String[JSONObject.getNames(jsonObjectRModelQuery).length]));


            if (rModelMetaQuery.get("modified").toString().equalsIgnoreCase("yyyymmdd:hh:mm:ss")) {
                rModelMetaQuery.put("modified", new SimpleDateFormat("yyyyMMdd:HH:mm:ss").format(Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant())));
            }
            rModelMetaQuery.put("modelid", modelid);


            if (indexExists) {
                //update the index file.
                try {

                    JsonParser parser = new JsonParser();
                    JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(userIDPath + File.separator + "index.txt"))));
                    JsonArray _jsonArraytemp = jsonElement.getAsJsonArray();

                    JSONArray returnArray = new JSONArray();
                    _jsonArraytemp.iterator().forEachRemaining(e -> {
                        JSONObject _temp = new JSONObject(e.toString());
                        if (!_temp.get("modelid").toString().concat("." + _temp.get("ext").toString()).equalsIgnoreCase(modelid + "." + rModel.getExt()))
                            returnArray.put(_temp);
                        else {
                            rModelMetaQuery.put("modified", new SimpleDateFormat("yyyyMMdd:HH:mm:ss").format(Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant())));
                        }
                    });


                    returnArray.put(rModelMetaQuery);
                    try {
                        Files.write(Paths.get(userIDPath + File.separator + "index.txt"), returnArray.toString().getBytes(StandardCharsets.UTF_8),
                                StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                    } catch (IOException e1) {
                        return "error : { " + e1.getMessage() + " }";
                    }
                } catch (IOException e) {
                    return "error : { " + e.getMessage() + " }";
                }

            } else {
                //create new index file and add the props.
                jsonArray = new JSONArray();
                jsonArray.put(rModelMetaQuery);
                try {
                    Files.write(Paths.get(userIDPath + File.separator + "index.txt"), jsonArray.toString().getBytes(StandardCharsets.UTF_8),
                            StandardOpenOption.CREATE);
                } catch (IOException e1) {
                    return "error : { " + e1.getMessage() + " }";
                }
            }

            try {
                Files.write(Paths.get(userIDPath + File.separator + modelid + "." + rModel.getExt()), rModel.getFile().getBytes(),
                        StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
            } catch (IOException e) {
                return "error : { " + e.getMessage() + " }";
            }


            try {
                Files.write(Paths.get(userIDPath + File.separator + modelid + "_meta.txt"), rModelMetaQuery.toString().getBytes(StandardCharsets.UTF_8),
                        StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
            } catch (IOException e) {
                return "error : { " + e.getMessage() + " }";
            }


        }
        return "success";
    }

    public String getModels() {
        if (!Files.isDirectory(Paths.get(_RmodelQueryDir))) {
            try {
                Files.createDirectory(Paths.get(_RmodelQueryDir));
            } catch (IOException e) {
                return "error : { " + e.getMessage() + " }";
            }
        }

        StringBuilder stringBuilder = new StringBuilder();
        try (Stream<Path> paths = Files.walk(Paths.get(_RmodelQueryDir))) {
            paths
                    .filter(Files::isDirectory)
                    .forEach(path -> {
                        if (!path.toAbsolutePath().toString().equalsIgnoreCase(Paths.get(_RmodelQueryDir).toAbsolutePath().toString()))

                            try (Stream<Path> path1 = Files.walk(path)) {
                                path1
                                        .filter(Files::isRegularFile)
                                        .forEach(path2 -> {
                                            if (path2.getFileName().toString().equalsIgnoreCase("index.txt")) {
                                                try {
                                                    stringBuilder.append(new String(Files.readAllBytes(path2), StandardCharsets.UTF_8));
                                                } catch (IOException e) {
                                                }
                                            }
                                        });
                            } catch (IOException e) {
                            }

                    });
        } catch (IOException e) {
            return "error : { " + e.getMessage() + " }";
        }

        return stringBuilder.toString();
    }

    public boolean deleteModel(String userid, String modelid) throws IOException {

        if (!Files.isDirectory(Paths.get(_RmodelQueryDir)))
            Files.createDirectory(Paths.get(_RmodelQueryDir));

        if (!Files.isDirectory(Paths.get(_RmodelQueryDir + File.separator + userid))) {
            return false;
        } else if (modelid.contains(".")) {
            try {
                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(_RmodelQueryDir + File.separator + userid + File.separator + "index.txt"))));
                JsonArray _jsonArraytemp = jsonElement.getAsJsonArray();

                JSONArray returnArray = new JSONArray();
                _jsonArraytemp.iterator().forEachRemaining(e -> {
                    org.json.JSONObject _temp = new org.json.JSONObject(e.toString());
                    if (!_temp.get("modelid").toString().concat("." + _temp.get("ext").toString()).equalsIgnoreCase(modelid))
                        returnArray.put(_temp);
                });

                try {
                    Files.write(Paths.get(_RmodelQueryDir + File.separator + userid + File.separator + "index.txt"), returnArray.toString().getBytes(StandardCharsets.UTF_8),
                            StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
                } catch (IOException e1) {
                }
                Files.deleteIfExists(Paths.get(_RmodelQueryDir + File.separator + userid + File.separator + modelid.substring(0, modelid.indexOf(".")).concat("_meta.txt")));
                return Files.deleteIfExists(Paths.get(_RmodelQueryDir + File.separator + userid + File.separator + modelid));
            } catch (IOException e) {
                return false;
            }
        }
        return false;
    }

    public String activate(RModelActivated rModelActivated) {
        String userid = String.valueOf(rModelActivated.getUserid());
        RConnection rConnection = getRConnection(userid, false);
        StringBuilder stringBuilder = new StringBuilder();

        if (rConnection != null && rConnection.isConnected()) {

            String ext = getSpecificFieldValue(userid, rModelActivated.getModelid(), "ext");
            if (!ext.equals(rModelActivated.getExt()))
                return "error : { ext " + rModelActivated.getExt() + " doesn't found with specified information }";

            String modelid = getSpecificFieldValue(userid, rModelActivated.getModelid(), "modelid");
            if (!modelid.equals(rModelActivated.getModelid()))
                return "error : { modelid " + rModelActivated.getModelid() + " doesn't found with specified information }";

            String useridfrommeta = getSpecificFieldValue(userid, rModelActivated.getModelid(), "userid");
            if (!useridfrommeta.equals(rModelActivated.getUserid()))
                return "error : { userid " + rModelActivated.getUserid() + " doesn't found with specified information }";

            String script = getSpecificFieldValue(userid, rModelActivated.getModelid(), "activationscript");
            if (script.startsWith("not Found"))
                return "error : { activationscript doesn't found with specified information }";

            try {
                stringBuilder.append(evalMultipleCommand(rConnection, script, ""));
            } catch (REXPMismatchException | REngineException e) {
                return "error : { " + e.getMessage() + " }";
            }


        } else return "error : {the user " + rModelActivated.getUserid() + " is not connected to Rserve}";
        if (stringBuilder.length() > 0) {
            try {
                if (!Files.isDirectory(Paths.get(_RActivatedQueryDir)))
                    Files.createDirectory(Paths.get(_RActivatedQueryDir));
                JSONObject activatedBody = new JSONObject();
                activatedBody.put("modelid", rModelActivated.getModelid());
                activatedBody.put("modelname", getSpecificFieldValue(userid, rModelActivated.getModelid(), "modelname"));
                activatedBody.put("source", getSpecificFieldValue(userid, rModelActivated.getModelid(), "source"));
                activatedBody.put("status", "OK");
                activatedBody.put("activated", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd:HH:mm:ss")));
                JSONArray activatesBody = new JSONArray();

                if (Files.exists(Paths.get(_RActivatedQueryDir + File.separator + "activated.txt"))) {
                    JsonParser parser = new JsonParser();
                    JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(_RActivatedQueryDir + File.separator + "activated.txt"))));
                    JsonArray _jsonArraytemp = jsonElement.getAsJsonArray();

                    JSONArray returnArray = new JSONArray();
                    _jsonArraytemp.iterator().forEachRemaining(e -> {
                        org.json.JSONObject _temp = new org.json.JSONObject(e.toString());
                        if (!_temp.get("modelid").toString().equalsIgnoreCase(rModelActivated.getModelid()))
                            activatesBody.put(_temp);
                    });
                    activatesBody.put(activatedBody);
                } else activatesBody.put(activatedBody);

                Files.write(Paths.get(_RActivatedQueryDir + File.separator + "activated.txt"), activatesBody.toString().getBytes(StandardCharsets.UTF_8),
                        StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

            } catch (IOException e) {
                return "error : { " + e.getMessage() + " }";
            }


        }
        return stringBuilder.toString();
    }

    private String getSpecificFieldValue(String userid, String modelid, String fieldName) {
        try {
            if (!Files.isDirectory(Paths.get(_RmodelQueryDir)))
                Files.createDirectory(Paths.get(_RmodelQueryDir));

            String metaFile = new String(Files.readAllBytes(Paths.get(_RmodelQueryDir + File.separator + userid + File.separator + modelid + "_meta.txt")));
            org.json.JSONObject metaFileAsJson = new org.json.JSONObject(metaFile);
            if (!metaFileAsJson.has(fieldName)) return "not Found";
            return metaFileAsJson.get(fieldName).toString();
        } catch (IOException e) {
            return "not Found";
        }
    }

    public String getActivates() {
        if (!Files.isDirectory(Paths.get(_RActivatedQueryDir))) {
            try {
                Files.createDirectory(Paths.get(_RActivatedQueryDir));
            } catch (IOException e) {
                return "error : { " + e.getMessage() + " }";
            }
        }

        StringBuilder stringBuilder = new StringBuilder();
        try (Stream<Path> paths = Files.walk(Paths.get(_RActivatedQueryDir))) {
            paths
                    .filter(Files::isRegularFile)
                    .forEach(path -> {
                        if (path.getFileName().toString().equalsIgnoreCase("activated.txt")) {
                            try {
                                stringBuilder.append(new String(Files.readAllBytes(path), StandardCharsets.UTF_8));
                            } catch (IOException e) {
                            }
                        }


                    });
        } catch (IOException e) {
            return "error : { " + e.getMessage() + " }";
        }

        return stringBuilder.toString();

    }


    public String formatRListToTAB(RList list) {
        try {
            StringBuffer sb = new StringBuffer("\t");
            String[][] data = new String[list.names.size()][];
            for (int i = 0; i < list.size(); i++) {
                String n = list.keyAt(i);
                if (n.length() > 0) {
                    sb.append(n + "\t");
                    data[i] = list.at(n).asStrings();
                }
            }
            sb.append("\n");
            for (int i = 0; i < data[0].length; i++) {
                sb.append((i + 1) + "\t");
                for (int j = 0; j < data.length; j++) {
                    if (data[j][i].length() > 0)
                        sb.append(data[j][i] + "\t");
                }
                sb.append("\n");
            }
            return sb.toString();
        } catch (REXPMismatchException r) {
            return "";
        }
    }


    public String getData(RModelData rModelData, String userid) {
        RConnection rConnection = getRConnection(userid, false);
        StringBuilder stringBuilder = new StringBuilder();

        if (rConnection != null && rConnection.isConnected()) {

            String datatype = rModelData.getDatatype();
            String outputtype = rModelData.getOutputtype();
            String script = rModelData.getScript();
            String[] datatypes = {"dataframe", "string", "vector"};
            String[] outputtypes = {"json", "csv", "tab"};
            if (!Arrays.asList(datatypes).contains(datatype))
                return "error : { unknow datatype " + datatype + " }";
            if (!Arrays.asList(outputtypes).contains(outputtype))
                return "error : { unknow outputtype " + outputtype + " }";
            if (datatype.equals("string") || datatype.equals("vector")) {
                try {
                    stringBuilder.append(evalMultipleCommand(rConnection, script, ""));
                } catch (REXPMismatchException | REngineException e) {
                    return "error : { " + e.getMessage() + " }";
                }
            } else if (datatype.equals("dataframe")) {
                try {
                    stringBuilder.append(evalMultipleCommand(rConnection, script, outputtype));
                } catch (REXPMismatchException | REngineException e) {
                    return "error : { " + e.getMessage() + " }";
                }
            }

        } else return "error : {the user " + userid + " is not connected to Rserve}";

        return stringBuilder.toString();
    }

    public boolean deActivate(String modelid) {
        try {
            JSONArray activatesBody = new JSONArray();
            if (Files.exists(Paths.get(_RActivatedQueryDir + File.separator + "activated.txt"))) {
                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(new FileReader(String.valueOf(Paths.get(_RActivatedQueryDir + File.separator + "activated.txt"))));
                JsonArray _jsonArraytemp = jsonElement.getAsJsonArray();

                _jsonArraytemp.iterator().forEachRemaining(e -> {
                    org.json.JSONObject _temp = new org.json.JSONObject(e.toString());
                    if (!_temp.get("modelid").toString().equalsIgnoreCase(modelid))
                        activatesBody.put(_temp);
                });
            } else return false;

            Files.write(Paths.get(_RActivatedQueryDir + File.separator + "activated.txt"), activatesBody.toString().getBytes(StandardCharsets.UTF_8),
                    StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public List<String> getImages(String userid, Set<String> imagesNames) {
        RConnection rConnection = getRConnection(userid, false);
        List<String> resultGetImagesfromR = new LinkedList<>();
        if (rConnection != null && rConnection.isConnected()) {
            return getImagesFromRserve(rConnection, imagesNames);
        } else {
            resultGetImagesfromR.add("error : {the user " + userid + " is not connected to Rserve}");
        }
        return resultGetImagesfromR;
    }

    private List<String> getImagesFromRserve(RConnection rConnection, Set<String> imagesNames) {
        List<String> images = new LinkedList<>();
        imagesNames.stream().forEachOrdered(imagename -> {
            if (imagename.length() > 0 && imagename.contains(".")) {
                String ext = imagename.substring(imagename.lastIndexOf(".") + 1);
                REXP rexp = null;
                try {
                    rexp = rConnection.parseAndEval("r=readBin('" + imagename + "','raw',1024*1024);r");
                } catch (REngineException | REXPMismatchException e) {
                }
                if (rexp.isRaw()) {
                    String encoding = "";
                    try {
                        encoding = "data:image/" + ext + ";base64," + Base64.getEncoder().encodeToString(rexp.asBytes());
                    } catch (REXPMismatchException e) {
                    }
                    if (encoding.length() > 0) {
                        String output = "<div><img src='" + encoding + "' alt='" + imagename + " not found'></div>";
                        images.add(output);
                    }
                }
            }
        });

        return images;
    }
}

