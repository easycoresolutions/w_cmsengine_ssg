package co.kr.coresolutions.service;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.model.dtos.CommentDto;
import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class QueryServiceCommentTable {
    private final QueryService queryService;

    private void populateCMTSequence(String[] cmtSequence, String resultSelect) {
        JsonParser parser = new JsonParser();
        JsonElement jsonElement = parser.parse(resultSelect);
        if (jsonElement.isJsonArray() && jsonElement.getAsJsonArray().size() > 0) {
            JsonObject jsonObject = jsonElement.getAsJsonArray().get(0).getAsJsonObject();
            if (jsonObject.has("MAX(CMT_SEQUENCE)")) {
                String maxCmtSequence = jsonObject.get("MAX(CMT_SEQUENCE)").getAsString();
                cmtSequence[0] = String.valueOf(Long.parseLong(maxCmtSequence.equalsIgnoreCase("null") ? "0" : maxCmtSequence) + 1);
            }
        }
    }

    public CustomResponseDto insertForMain(List<String> listRandomIds, String connectionID, String mainTable, String projectID, CommentDto commentDto) {
        String[] cmtSequence = {commentDto.getCmtSequence()};
        String randomDeleteMain = null;
        String resultValidity;
        resultValidity = queryService
                .checkValidity("SELECT MAX(CMT_SEQUENCE) FROM " + mainTable + " WHERE projectID = '" + projectID + "'",
                        connectionID, "", "", false);
        if (!(resultValidity.equalsIgnoreCase(connectionID) || resultValidity.startsWith("Error") || resultValidity.startsWith("error"))) {
            populateCMTSequence(cmtSequence, resultValidity);
        } else {
            return CustomResponseDto.builder().message("fail due error is " + resultValidity).build();
        }

        if (!commentDto.getCmtSequence().isEmpty()) {
            randomDeleteMain = UUID.randomUUID().toString();
            resultValidity = queryService
                    .updateClausesWithRollback(Lists.newArrayList("DELETE FROM " + mainTable + " WHERE projectID = '" + projectID + "'"), connectionID,
                            randomDeleteMain);
            if (!resultValidity.startsWith("success")) {
                return CustomResponseDto.builder().message("fail due error is\t" + resultValidity).build();
            }
            listRandomIds.add(randomDeleteMain);
            cmtSequence[0] = commentDto.getCmtSequence();
        }

        String randomInsertMain = UUID.randomUUID().toString();
        try {
            String resultInsert = queryService
                    .updateClausesWithRollback(Lists.newArrayList("INSERT INTO " + mainTable + "(projectID, CMT_SOURCE, CMT_TITLE, CMT_SHARED, " +
                                    "CMT_REGDATE, CMT_CONTENTS, CMT_OWNER, CMT_SEQUENCE, DEL_F) VALUES ('" + projectID + "', '" + commentDto.getCmtSource() + "', '"
                                    + commentDto.getCmtTitle() + "', '" + commentDto.getCmtShared() + "', '" + commentDto.getCmtRegDate() + "', '"
                                    + commentDto.getCmtContents()
                                    .replace("\\", "\\\\")
                                    .replace("/", "\\/")
                                    .replace("'", "''") + "', '"
                                    + commentDto.getCmtOwner() + "', "
                                    + cmtSequence[0]
                                    + ", '" + commentDto.getDelF() + "')"),
                            connectionID, randomDeleteMain != null ? randomDeleteMain + "same" + randomInsertMain : randomInsertMain);
            if (!resultInsert.startsWith("success")) {
                return CustomResponseDto.builder().message("fail due " + resultInsert).build();
            }
        } catch (Exception e) {
            return CustomResponseDto.builder().message("fail due " + e.getMessage()).build();
        }
        listRandomIds.add(randomInsertMain);
        return CustomResponseDto.ok();
    }

    public CustomResponseDto deleteAndInsertAuxiliaryTable(String tableName, List<String> authIDS, List<String> listRandomIds, String projectID, String connectionID) {
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();
        //delete if exist and insert in auxiliary tables
        String randomDeleteAuxiliary = UUID.randomUUID().toString();
        String resultDelete = queryService
                .updateClausesWithRollback(
                        Lists.newArrayList("DELETE FROM " + tableName + " WHERE projectID = '" + projectID + "'"), connectionID, randomDeleteAuxiliary);
        if (!resultDelete.startsWith("success")) {
            queryService.rollBack(listRandomIds);
            customResponseDto.setMessage(resultDelete);
            customResponseDto.setSuccess(false);
            return customResponseDto;
        }
        listRandomIds.add(randomDeleteAuxiliary);

        String randomInsertAuxiliary = UUID.randomUUID().toString();

        StringBuilder stringBuilderKeys = new StringBuilder();
        StringBuilder stringBuilderValues = new StringBuilder();
        List<String> queries = new ArrayList<>();
        authIDS.forEach(authID -> {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("projectID", projectID);
            jsonObject.put("AUTH_ID", authID);
            jsonObject.keySet().forEach(oneKey -> {
                stringBuilderKeys.append(oneKey).append(",");
                stringBuilderValues.append("'").append(jsonObject.get(oneKey).toString().replace("'", "''")).append("',");
            });
            queries.add("insert into " + tableName
                    + "(" + stringBuilderKeys.deleteCharAt(stringBuilderKeys.length() - 1).toString() + ") values ("
                    + stringBuilderValues.deleteCharAt(stringBuilderValues.length() - 1).toString() + ")");
            stringBuilderKeys.delete(0, stringBuilderKeys.length());
            stringBuilderValues.delete(0, stringBuilderValues.length());
        });

        String resultInsert = queryService.updateClausesWithRollback(queries, connectionID, randomDeleteAuxiliary + "same" + randomInsertAuxiliary);
        if (resultInsert.startsWith("success")) {
            listRandomIds.add(randomInsertAuxiliary);
        }
        return CustomResponseDto.builder().message(resultInsert).Success(resultInsert.startsWith("success")).build();
    }
}
