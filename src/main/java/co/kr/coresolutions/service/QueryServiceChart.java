package co.kr.coresolutions.service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.StreamSupport;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.ErrorCode;
import co.kr.coresolutions.commons.HelperUtil;
import co.kr.coresolutions.enums.ChartOutputType;
import co.kr.coresolutions.enums.ChartStatType;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.facades.Impl.QueryChartFacade;
import co.kr.coresolutions.model.dtos.ChartDto;
import co.kr.coresolutions.model.dtos.HttpDto;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class QueryServiceChart {
    private static final String HTML_LINE_REGEX = "(.*?)(%%GROUPBY\\(.*?\\)VAR\\(.*?\\)STAT\\((SUM|COUNT|AVG)\\)" +
            "OUTPUTTYPE\\((CATEGORY|PIE|STACKBAR|BUBBLE|LINE|HEATMAP|SCATTER|PACKEDBUBBLE|WORDCLOUD|UNIQUE_ITEM_LIST|BUBBLELEGEND|BASE_RESULTSET|STATS|HISTOGRAM|PARETO)\\)%%)(.*?)";
    private final ObjectMapper objectMapper;
    DecimalFormat decimalFormat = new DecimalFormat("#");
    private final Constants constants;
    private final QueryDataFlowHttp queryDataFlowHttp;
    private final QueryService queryService;
    private final IQuerySocketFacade querySocketFacade;
    private final QueryServiceJSON queryServiceJSON;

    public CustomResponseDto postForObject(String url, String body, ObjectNode prompt, String userID, String partMessage, String socketKey) {
        CustomResponseDto customResponseDto = queryDataFlowHttp.executeNormal(HttpDto
                .builder()
                .api(url)
                .body(queryService.convertGlobalDateTime(queryService.getFormattedQuery(body, prompt)))
                .method(HttpMethod.POST)
                .build());

        querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey)
                .message(partMessage + (customResponseDto.getSuccess() ? "success" : "fail")).build());
        queryService.addLogging("POST", partMessage + (customResponseDto.getSuccess() ? "success" : "fail"));

        return customResponseDto;
    }

    public CustomResponseDto getResultFetchCommand(String queryID, String url, List<String> keys, String userID, String partMessage, String commandConverted, ObjectNode prompt, String api) {
        //datasetname, commandProgram, script_file_name, option, command
        String scriptFileName = keys.get(2);
        String workDir = constants.getRootDir() + "work" + File.separator + userID + File.separator;

        String formattedCommand = queryService.convertGlobalDateTime(queryService.getFormattedQuery(commandConverted, prompt));

        if (!Files.isDirectory(Paths.get(workDir))) {
            try {
                Files.createDirectories(Paths.get(workDir));
                Files.write(Paths.get(workDir + scriptFileName), formattedCommand.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.CREATE_NEW);
            } catch (IOException e) {
                QueryChartFacade.message[0] = "fail creating file " + scriptFileName + " under dir\t" + workDir;
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(api).message(partMessage + QueryChartFacade.message[0]).build());
                queryService.addLogging("POST", partMessage + QueryChartFacade.message[0]);
                return CustomResponseDto.builder().ErrorCode(ErrorCode.SIX.getCode()).ErrorMessage(QueryChartFacade.message[0]).build();
            }
        } else {
            try {
                Files.write(Paths.get(workDir + scriptFileName), formattedCommand.getBytes(StandardCharsets.UTF_8), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
            } catch (IOException e) {
                QueryChartFacade.message[0] = "fail writing to file " + scriptFileName + " under dir\t" + workDir;
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(api).message(partMessage + QueryChartFacade.message[0]).build());
                queryService.addLogging("POST", partMessage + QueryChartFacade.message[0]);
                return CustomResponseDto.builder().ErrorCode(ErrorCode.SIX.getCode()).ErrorMessage(QueryChartFacade.message[0]).build();
            }
        }

        String body = "{\n" +
                "  \"Command\": \"" + keys.get(1) + "\",\n" +
                "  \"Output\": \"csv\",\n" +
                "  \"parameter\":{\"@RFILE\" : \"" + workDir + scriptFileName + "\"},\n" +
                "  \"userid\": \"mss_scheduler\",\n" +
                "  \"owner\": \"" + userID + "\"\n" +
                "}";

        if (!QueryService.isCacheQueryIDExist(queryID)) {
            return CustomResponseDto.builder().message("fail due: killed by request!").build();
        }

        CustomResponseDto customResponseDto = queryDataFlowHttp.executeNormal(HttpDto
                .builder()
                .api(url)
                .body(body)
                .method(HttpMethod.POST)
                .build());

        if (!QueryService.isCacheQueryIDExist(queryID)) {
            return CustomResponseDto.builder().message("fail due: killed by request!").build();
        }

        if (keys.get(3).trim().equalsIgnoreCase("delete")) {
            try {
                Files.deleteIfExists(Paths.get(workDir + scriptFileName));
            } catch (IOException e) {
                QueryChartFacade.message[0] = "fail delete file " + scriptFileName + " under dir\t" + workDir;
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(api).message(partMessage + QueryChartFacade.message[0]).build());
                queryService.addLogging("POST", partMessage + QueryChartFacade.message[0]);
                return CustomResponseDto.builder().message(e.getMessage()).build();
            }
        }

        if (customResponseDto.getSuccess()) {
            QueryChartFacade.message[0] = "Successfully";
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(api).message(partMessage + QueryChartFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + QueryChartFacade.message[0]);
            return customResponseDto;
        }

        QueryChartFacade.message[0] = "fail due " + customResponseDto.getMessage();
        querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(api).message(partMessage + QueryChartFacade.message[0]).build());
        queryService.addLogging("POST", partMessage + QueryChartFacade.message[0]);
        return CustomResponseDto.builder().ErrorCode(ErrorCode.FIVE.getCode()).ErrorMessage(QueryChartFacade.message[0]).build();
    }


    public CustomResponseDto getResultFetchQuery(String queryID, String dataSet, String userID, String partMessage, String connectionID, String query
            , ObjectNode prompt, String socketKey, String dbFile) {
        boolean isConnectionFileExists = queryService.isConnectionIdFileExists(connectionID);
        String formattedQuery = queryService.convertGlobalDateTime(queryService.getFormattedQuery(queryService.getFormattedString(query), prompt));
        if (!isConnectionFileExists) {
            HelperUtil.queryIDColumnDataTypeMap.remove(queryID);
            QueryChartFacade.message[0] = "fail due to connection file with name\t" + connectionID + "\tdoesn't exist";
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey)
                    .message(partMessage + "sql is:\tSQL/" + dataSet + "/" + connectionID + "\n" + formattedQuery + "\n response is\t" + QueryChartFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + "sql is:\tSQL/" + dataSet + "/" + connectionID + "\n" + formattedQuery + "\n response is\t" + QueryChartFacade.message[0]);
            return CustomResponseDto.builder().ErrorCode(ErrorCode.ONE.getCode()).ErrorMessage(QueryChartFacade.message[0]).build();
        }
        String resultValidity = queryService.checkValidityWithLabel(formattedQuery, connectionID, userID, queryID, dbFile, false);
        if (resultValidity.startsWith("Error") || resultValidity.startsWith("error") || resultValidity.startsWith("maxRunningTimeOut") || resultValidity.startsWith("MaxRows")) {
            HelperUtil.queryIDColumnDataTypeMap.remove(queryID);
            QueryChartFacade.message[0] = resultValidity;
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey)
                    .message(partMessage + "sql is:\tSQL/" + dataSet + "/" + connectionID + "\n" + formattedQuery + "\n response is\t" + QueryChartFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + "sql is:\tSQL/" + dataSet + "/" + connectionID + "\n" + formattedQuery + "\n response is\t" + QueryChartFacade.message[0]);
            return CustomResponseDto.builder().ErrorCode(ErrorCode.THREE.getCode()).ErrorMessage(QueryChartFacade.message[0]).build();
        }

        JSONObject dataTypeObject = HelperUtil.queryIDColumnDataTypeMap.get(queryID);
        if (dataTypeObject == null) {
            dataTypeObject = new JSONObject(queryService.getQueryDataTypes(connectionID, formattedQuery, true));
        }

        String structuredJson;
        try {
            structuredJson = new String(queryServiceJSON.structureJson(resultValidity, false, objectMapper.readValue(dataTypeObject.toString(), HashMap.class), "cache")
                    .getBytes(), StandardCharsets.UTF_8);
        } catch (JsonProcessingException e) {
            structuredJson = "Error " + e.getMessage();
        }
        if (structuredJson.startsWith("Error") || structuredJson.startsWith("error") || structuredJson.equalsIgnoreCase("{}")) {
            QueryChartFacade.message[0] = structuredJson;
            HelperUtil.queryIDColumnDataTypeMap.remove(queryID);
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey)
                    .message(partMessage + "sql is:\tSQL/" + dataSet + "/" + connectionID + "\n" + formattedQuery + "\n response is\t" + QueryChartFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + "sql is:\tSQL/" + dataSet + "/" + connectionID + "\n" + formattedQuery + "\n response is\t" + QueryChartFacade.message[0]);
            return CustomResponseDto.builder().ErrorCode(ErrorCode.THREE.getCode()).ErrorMessage(structuredJson).build();
        }

        HelperUtil.queryIDColumnDataTypeMap.remove(queryID);
        if (!structuredJson.isEmpty()) {
            if (structuredJson.startsWith("Error") || structuredJson.startsWith("error")) {
                QueryChartFacade.message[0] = "can't transform result as valid json";
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey)
                        .message(partMessage + "sql is:\tSQL/" + dataSet + "/" + connectionID + "\n" + formattedQuery + "\n response is\t" + QueryChartFacade.message[0]).build());
                queryService.addLogging("POST", partMessage + "sql is:\tSQL/" + dataSet + "/" + connectionID + "\n" + formattedQuery + "\n response is\t" + QueryChartFacade.message[0]);
                return CustomResponseDto.builder().ErrorCode(ErrorCode.FOUR.getCode()).ErrorMessage(QueryChartFacade.message[0]).build();
            } else {
                querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey)
                        .message(partMessage + "sql is:\tSQL/" + dataSet + "/" + connectionID + "\n" + formattedQuery + "\nSuccess").build());
                queryService.addLogging("POST", partMessage + "sql is:\tSQL/" + dataSet + "/" + connectionID + "\n" + formattedQuery + "\nSuccess");
                return CustomResponseDto.builder().Success(true).message(structuredJson).build();
            }
        } else {
            QueryChartFacade.message[0] = "can't transform result as valid json";
            querySocketFacade.send(SocketBodyDto.builder().userId(userID).key(socketKey)
                    .message(partMessage + "sql is:\tSQL/" + dataSet + "/" + connectionID + "\n" + formattedQuery + "\n response is\t" + QueryChartFacade.message[0]).build());
            queryService.addLogging("POST", partMessage + "sql is:\tSQL/" + dataSet + "/" + connectionID + "\n" + formattedQuery + "\n response is\t" + QueryChartFacade.message[0]);
            return CustomResponseDto.builder().ErrorCode(ErrorCode.SIX.getCode()).ErrorMessage(QueryChartFacade.message[0]).build();
        }
    }

    public String constructDetails(JSONObject jsonObjectGlobal, ChartDto chartDto, String fileChartBody) {
        String dataType = chartDto.getDataParameter().getDataType();
        String category = chartDto.getDataParameter().getCategory();

        JSONArray jsonResults = new JSONArray();
        List<String> categoryResults = new ArrayList<>();
        Map<String, Object> mapUtil = new HashMap<>();
        TreeMap<String, Object> treeMap = new TreeMap<>();

        if (fileChartBody.contains(dataType)) {
            JSONArray jsonArrayInput = jsonObjectGlobal.getJSONArray("Input");
            if (dataType.equals("@@PIE@@")) {
                String xAxis = chartDto.getDataParameter().getXAxis().get(0);
                String yAxis = chartDto.getDataParameter().getYAxis().get(0);
                jsonArrayInput.forEach(o -> {
                    org.json.JSONObject jsonObject = new org.json.JSONObject(o.toString());
                    if (!xAxis.isEmpty() && !yAxis.isEmpty() && jsonObject.has(xAxis) && jsonObject.has(yAxis)) {
                        JSONObject jsonObjectStructured = new JSONObject();
                        jsonObjectStructured.put("name", "'" + jsonObject.get(xAxis) + "'");
                        jsonObjectStructured.put("y", jsonObject.get(yAxis));
                        jsonResults.put(jsonObjectStructured);
                    }
                });
            }
            if (dataType.equals("@@STACKBAR@@")) {
                String xAxis = chartDto.getDataParameter().getXAxis().get(0);
                String yAxis = chartDto.getDataParameter().getYAxis().get(0);
                //required , if  xAxis as data value then will fail so this random is to split.
                String randomData = UUID.randomUUID().toString();
                String randomXAXIS = UUID.randomUUID().toString();
                Set<String> xaxisArray = new HashSet<>();
                Set<String> categoryArray = new HashSet<>();
                jsonArrayInput.forEach(o -> {
                    org.json.JSONObject jsonObject = new org.json.JSONObject(o.toString());
                    categoryArray.add(jsonObject.getString(category));
                    xaxisArray.add(jsonObject.getString(xAxis));
                });

                Map<String, String> missingcategoriesXaxis = new HashMap<>();

                xaxisArray.forEach(oneXasis -> categoryArray.forEach(oneCategory -> {
                    if (!missingcategoriesXaxis.containsKey(oneXasis + randomXAXIS + oneCategory)) {
                        missingcategoriesXaxis.put(oneXasis + randomXAXIS + oneCategory, oneCategory);
                    }
                }));

                JSONArray fullJsonArrayInput = new JSONArray();

                jsonArrayInput.forEach(o -> {
                    org.json.JSONObject jsonObject = new org.json.JSONObject(o.toString());
                    fullJsonArrayInput.put(jsonObject);
                    missingcategoriesXaxis.remove(jsonObject.getString(xAxis) + randomXAXIS + jsonObject.getString(category));
                });

                missingcategoriesXaxis.forEach((key, value) -> {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(xAxis, key.substring(0, key.indexOf(randomXAXIS)));
                    jsonObject.put(yAxis, 0);
                    jsonObject.put(category, value);
                    fullJsonArrayInput.put(jsonObject);
                });

                StreamSupport.stream(fullJsonArrayInput.spliterator(), false).sorted(
                        Comparator.comparing(a -> (String) ((JSONObject) a).get(category)).thenComparing(a -> (String) ((JSONObject) a).get(xAxis))
                ).forEachOrdered(o -> {
                    org.json.JSONObject jsonObject = new org.json.JSONObject(o.toString());
                    if (!xAxis.isEmpty() && jsonObject.has(xAxis) && !yAxis.isEmpty() && jsonObject.has(yAxis)) {
                        if (treeMap.containsKey("name" + jsonObject.getString(xAxis) + "data" + randomData)) {
                            List<Object> objects = (List<Object>) treeMap.get("name" + jsonObject.getString(xAxis) + "data" + randomData);
                            objects.add(jsonObject.get(yAxis));
                            treeMap.replace("name" + jsonObject.getString(xAxis) + "data" + randomData, objects);
                        } else {
                            treeMap.put("name" + jsonObject.getString(xAxis) + "data" + randomData, Lists.newArrayList(jsonObject.get(yAxis)));
                        }
                    }
                    if (jsonObject.has(category) && !categoryResults.contains("'" + jsonObject.getString(category) + "'") && !jsonObject.getString(category).startsWith(randomXAXIS)) {
                        categoryResults.add("'" + jsonObject.getString(category) + "'");
                    }
                });

                fetchMap(jsonResults, treeMap, randomData);
            }
            if (dataType.equals("@@SCATTER@@")) {
                List<String> yAxis = chartDto.getDataParameter().getYAxis();
                if (yAxis.size() == 2) {
                    String randomData = UUID.randomUUID().toString();
                    jsonArrayInput.forEach(o -> {
                        org.json.JSONObject jsonObject = new org.json.JSONObject(o.toString());
                        if (jsonObject.has(category) && jsonObject.has(yAxis.get(0)) && jsonObject.has(yAxis.get(1))) {
                            String name = jsonObject.getString(category);
                            if (mapUtil.containsKey("name" + name + "data" + randomData)) {
                                List<Object> objects = (List<Object>) mapUtil.get("name" + name + "data" + randomData);
                                objects.add("[" + jsonObject.get(yAxis.get(0)) + ", " + jsonObject.get(yAxis.get(1)) + "]");
                                mapUtil.put("name" + name + "data" + randomData, objects);
                            } else {
                                mapUtil.put("name" + name + "data" + randomData,
                                        Lists.newArrayList("[" + jsonObject.get(yAxis.get(0)) + ", " + jsonObject.get(yAxis.get(1)) + "]"));
                            }
                        }
                    });
                    fetchMap(jsonResults, mapUtil, randomData);
                }
            }
            if (dataType.equals("@@BUBBLE@@")) {
                List<String> yAxis = chartDto.getDataParameter().getYAxis();
                if (yAxis.size() == 3) {
                    jsonArrayInput.forEach(o -> {
                        org.json.JSONObject jsonObject = new org.json.JSONObject(o.toString());
                        if (jsonObject.has(category) && jsonObject.has(yAxis.get(0)) && jsonObject.has(yAxis.get(1)) && jsonObject.has(yAxis.get(2))) {
                            if (jsonResults.isEmpty()) {
                                JSONObject jsonObjectStructured = new JSONObject();
                                JSONArray jsonArrayInside = new JSONArray();
                                JSONObject jsonObjectStructuredInside = new JSONObject();
                                jsonObjectStructuredInside.put("x", jsonObject.get(yAxis.get(0)));
                                jsonObjectStructuredInside.put("y", jsonObject.get(yAxis.get(1)));
                                jsonObjectStructuredInside.put("z", jsonObject.get(yAxis.get(2)));
                                jsonObjectStructuredInside.put("name", "'" + jsonObject.get(category) + "'");
                                jsonArrayInside.put(jsonObjectStructuredInside);
                                jsonObjectStructured.put("data", jsonArrayInside);
                                jsonResults.put(0, jsonObjectStructured);
                            } else {
                                JSONObject jsonObjectStructured = jsonResults.getJSONObject(0);
                                JSONArray jsonArrayInside = jsonObjectStructured.getJSONArray("data");
                                JSONObject jsonObjectStructuredInside = new JSONObject();
                                jsonObjectStructuredInside.put("x", jsonObject.get(yAxis.get(0)));
                                jsonObjectStructuredInside.put("y", jsonObject.get(yAxis.get(1)));
                                jsonObjectStructuredInside.put("z", jsonObject.get(yAxis.get(2)));
                                jsonObjectStructuredInside.put("name", "'" + jsonObject.get(category) + "'");
                                jsonArrayInside.put(jsonObjectStructuredInside);
                                jsonObjectStructured.put("data", jsonArrayInside);
                                jsonResults.put(0, jsonObjectStructured);
                            }
                        }
                    });
                }
            }
        }
        Collections.sort(categoryResults);
        JsonNode parameter = chartDto.getParameter();
        final String[] result = {fileChartBody
                .replaceAll(dataType, jsonResults.toString().replaceAll("\"", ""))
                .replaceAll("@@CATEGORY@@", Arrays.toString(categoryResults.toArray()))};
        parameter.fieldNames().forEachRemaining(s -> result[0] = result[0].replaceAll(s, parameter.get(s).asText()));
        return result[0].replaceAll("\\s+", "    ").replaceAll("\\r", "  ").trim();
    }

    private void fetchMap(JSONArray jsonResults, Map<String, Object> mapUtil, String randomData) {
        mapUtil.forEach((s, objects) -> {
            JSONObject jsonObjectStructured = new JSONObject();
            jsonObjectStructured.put("name", "'" + s.substring(4, s.indexOf("data" + randomData)) + "'");
            jsonObjectStructured.put("data", objects);
            jsonResults.put(jsonObjectStructured);
        });
    }

    public String constructDetailsData(JSONObject jsonObjectGlobal, ChartDto chartDto, String fileChartBody) {
        String dataType = chartDto.getDataParameter().getDataType();
        JsonNode parameter = chartDto.getParameter();
        final String[] result = {fileChartBody};
        if (dataType.equals("@@JSONDATA@@")) {
            result[0] = fileChartBody.replaceAll(dataType, jsonObjectGlobal.toString());
        }
        parameter.fieldNames().forEachRemaining(s -> result[0] = result[0].replaceAll(s, StringEscapeUtils.unescapeJava(parameter.get(s).asText())));
        return result[0].replaceAll("\\s", "    ").replaceAll("\\r", "  ").trim();
    }

    public String getMixedResultJson(JSONObject json, String html) {
        decimalFormat.setMaximumFractionDigits(1000);
        JSONArray jsonArrayInput = new JSONArray(json.get("Input").toString());
        List<String> listObjectOutput = new LinkedList<>();
        Arrays.asList(html.split("\\s+")).forEach(oneLine -> {
            if (Pattern.compile(HTML_LINE_REGEX).matcher(oneLine).matches()) {
                List<String> groupsBy = Arrays.asList(oneLine.substring(oneLine.indexOf("GROUPBY(") + 8,
                        oneLine.indexOf(")VAR")).trim().toLowerCase().split(","));
                String outputType = ChartOutputType.valueOf(oneLine.substring(oneLine.indexOf("OUTPUTTYPE(") + 11, oneLine.lastIndexOf(")"))).name();
                if (outputType.equals(ChartOutputType.BASE_RESULTSET.name())) {
                    listObjectOutput.add(json.toString() + "\n");
                }
                if (!groupsBy.toString().equals("[]")) {
                    List<String> vars = Arrays.asList(oneLine.substring(oneLine.indexOf("VAR(") + 4, oneLine.indexOf(")STAT")).trim().toLowerCase().split(","));
                    String statType = ChartStatType.valueOf(oneLine.substring(oneLine.indexOf("STAT(") + 5, oneLine.indexOf(")OUTPUTTYPE"))).name();
                    String firstGroupBy = groupsBy.get(0);
                    String firstVar = vars.get(0);
                    if (outputType.equals(ChartOutputType.CATEGORY.name())) {
                        Set<String> jsonArrayCategory = new LinkedHashSet<>();
                        StreamSupport.stream(jsonArrayInput.spliterator(), false)
                                .map(o -> {
                                    JSONObject jsonObject = new org.json.JSONObject(o.toString());
                                    JSONObject jsonObject1 = new JSONObject();
                                    new ArrayList<>(jsonObject.keySet()).forEach(s -> jsonObject1.put(s.toLowerCase(), jsonObject.get(s)));
                                    return jsonObject1;
                                })
                                .filter(jsonObject -> jsonObject.has(firstGroupBy))
                                .forEach(jsonObject -> jsonArrayCategory.add("'" + jsonObject.get(firstGroupBy).toString() + "'"));
                        if (!jsonArrayCategory.isEmpty()) {
                            listObjectOutput.add(jsonArrayCategory.toString() + "\n");
                        }
                    } else if (outputType.equals(ChartOutputType.SCATTER.name())) {
                        TreeMap<String, String> treeMap = new TreeMap<>();
                        StreamSupport.stream(jsonArrayInput.spliterator(), false)
                                .map(o -> {
                                    JSONObject jsonObject = new org.json.JSONObject(o.toString());
                                    JSONObject jsonObject1 = new JSONObject();
                                    new ArrayList<>(jsonObject.keySet()).forEach(s -> jsonObject1.put(s.toLowerCase(), jsonObject.get(s)));
                                    return jsonObject1;
                                })
                                .filter(jsonObject -> jsonObject.has(firstGroupBy) && vars.stream().allMatch(jsonObject::has))
                                .forEach(jsonObject -> {
                                    String keyName = "'" + jsonObject.get(firstGroupBy).toString() + "'";
                                    List<String> values = new LinkedList<>();
                                    vars.forEach(oneVar -> values.add(jsonObject.get(oneVar).toString()));
                                    if (treeMap.containsKey(keyName)) {
                                        String oldValue = treeMap.get(keyName);
                                        treeMap.replace(keyName, oldValue.substring(0, oldValue.length() - 1) + "," + values + "]");
                                    } else {
                                        treeMap.put(keyName, "[" + values + "]");
                                    }
                                });
                        if (!treeMap.isEmpty()) {
                            JSONArray jsonArray = new JSONArray();
                            treeMap.entrySet().forEach(stringStringEntry -> {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("name", stringStringEntry.getKey());
                                jsonObject.put("data", stringStringEntry.getValue());
                                jsonArray.put(jsonObject);
                            });
                            listObjectOutput.add(jsonArray.toString().replaceAll("\"", "") + "\n");
                        }
                    } else if (outputType.equals(ChartOutputType.PIE.name()) || outputType.equals(ChartOutputType.WORDCLOUD.name())
                            || outputType.equals(ChartOutputType.PARETO.name())) {
                        JSONArray jsonArrayPie = new JSONArray();
                        TreeMap<String, String> treeMap = new TreeMap<>();
                        StreamSupport.stream(jsonArrayInput.spliterator(), false)
                                .map(o -> {
                                    JSONObject jsonObject = new org.json.JSONObject(o.toString());
                                    JSONObject jsonObject1 = new JSONObject();
                                    new ArrayList<>(jsonObject.keySet()).forEach(s -> jsonObject1.put(s.toLowerCase(), jsonObject.get(s)));
                                    return jsonObject1;
                                })
                                .filter(jsonObject -> jsonObject.has(firstGroupBy) && jsonObject.has(firstVar))
                                .forEach(jsonObject -> {
                                    String key = "'" + jsonObject.get(firstGroupBy) + "'";
                                    Long firstVarSupposedInt = jsonObject.getLong(firstVar);
                                    if (treeMap.containsKey(key)) {
                                        if (statType.equals(ChartStatType.SUM.name())) {
                                            treeMap.replace(key, decimalFormat.format(Long.parseLong(treeMap.get(key).replace("'", "")) + firstVarSupposedInt));
                                        } else if (statType.equals(ChartStatType.AVG.name()) || statType.equals(ChartStatType.COUNT.name())) {
                                            treeMap.replace(key, treeMap.get(key) + "|" + firstVarSupposedInt);
                                        }
                                    } else {
                                        treeMap.put(key, String.valueOf(firstVarSupposedInt));
                                    }
                                });
                        if (outputType.equals(ChartOutputType.PARETO.name())) {
                            listObjectOutput.add(treeMap.values().toString() + "\n");
                        } else {
                            String secondKey = "y";
                            if (outputType.equals(ChartOutputType.WORDCLOUD.name())) {
                                secondKey = "weight";
                            }
                            String finalSecondKey = secondKey;
                            treeMap.forEach((key, value) -> {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("name", key);
                                if (statType.equals(ChartStatType.SUM.name())) {
                                    jsonObject.put(finalSecondKey, value);
                                } else if (statType.equals(ChartStatType.AVG.name())) {
                                    jsonObject.put(finalSecondKey,
                                            decimalFormat.format(Arrays.stream(value.split("\\|")).mapToLong(value1 -> Long.parseLong(value1.replace("'", ""))).average().orElse(0)));
                                } else if (statType.equals(ChartStatType.COUNT.name())) {
                                    jsonObject.put(finalSecondKey, decimalFormat.format(Arrays.stream(value.split("\\|")).count()));
                                }
                                jsonArrayPie.put(jsonObject);
                            });

                            if (!jsonArrayPie.isEmpty()) {
                                listObjectOutput.add(jsonArrayPie.toString().replaceAll("\"", "") + "\n");
                            }
                        }
                    } else if (outputType.equals(ChartOutputType.STACKBAR.name()) || outputType.equals(ChartOutputType.HEATMAP.name())
                            || outputType.equals(ChartOutputType.PACKEDBUBBLE.name())
                            || outputType.equals(ChartOutputType.UNIQUE_ITEM_LIST.name())) {
                        TreeMap<String, String> treeMap = new TreeMap<>();
                        StreamSupport.stream(jsonArrayInput.spliterator(), false)
                                .map(o -> {
                                    JSONObject jsonObject = new JSONObject(o.toString());
                                    JSONObject jsonObject1 = new JSONObject();
                                    new ArrayList<>(jsonObject.keySet()).forEach(s -> jsonObject1.put(s.toLowerCase(), jsonObject.get(s)));
                                    return jsonObject1;
                                })
                                .filter(jsonObject -> groupsBy.stream().allMatch(jsonObject::has) && vars.stream().allMatch(jsonObject::has))
                                .forEach(jsonObject -> {
                                    final String[] key = {""};
                                    final String[] value = {""};
                                    groupsBy.forEach(oneGroupBy -> key[0] = key[0] + "," + jsonObject.get(oneGroupBy).toString());
                                    if (outputType.equals(ChartOutputType.UNIQUE_ITEM_LIST.name())) {
                                        vars.forEach(oneVar -> value[0] = value[0] + "," + jsonObject.get(oneVar).toString());
                                    } else {
                                        value[0] = jsonObject.get(vars.get(0)).toString();
                                    }
                                    if (treeMap.containsKey(key[0])) {
                                        treeMap.replace(key[0], treeMap.get(key[0]) + "\n" + value[0]);
                                    } else {
                                        treeMap.put(key[0], value[0]);
                                    }
                                });

                        if (!treeMap.isEmpty()) {
                            fetchResults(listObjectOutput, treeMap, outputType, statType);
                        }
                    } else if (outputType.equals(ChartOutputType.BUBBLE.name())) {
                        TreeMap<String, String> treeMap = new TreeMap<>();
                        StreamSupport.stream(jsonArrayInput.spliterator(), false)
                                .map(o -> {
                                    JSONObject jsonObject = new org.json.JSONObject(o.toString());
                                    JSONObject jsonObject1 = new JSONObject();
                                    new ArrayList<>(jsonObject.keySet()).forEach(s -> jsonObject1.put(s.toLowerCase(), jsonObject.get(s)));
                                    return jsonObject1;
                                })
                                .filter(jsonObject -> vars.stream().allMatch(jsonObject::has) && jsonObject.has(firstGroupBy))
                                .forEach(jsonObject -> {
                                    final String[] key = {""};
                                    final String[] value = {""};
                                    vars.forEach(oneVar -> value[0] = value[0] + "," + jsonObject.get(oneVar).toString());
                                    key[0] = jsonObject.get(firstGroupBy).toString();
                                    if (treeMap.containsKey(key[0])) {
                                        treeMap.replace(key[0], treeMap.get(key[0]) + "\n" + value[0].substring(1));
                                    } else {
                                        treeMap.put(key[0], value[0].substring(1));
                                    }
                                });

                        if (!treeMap.isEmpty()) {
                            fetchResults(listObjectOutput, treeMap, outputType, statType);
                        }
                    } else if (outputType.equals(ChartOutputType.BUBBLELEGEND.name())) {
                        TreeMap<String, String> treeMap = new TreeMap<>();
                        StreamSupport.stream(jsonArrayInput.spliterator(), false)
                                .map(o -> {
                                    JSONObject jsonObject = new org.json.JSONObject(o.toString());
                                    JSONObject jsonObject1 = new JSONObject();
                                    new ArrayList<>(jsonObject.keySet()).forEach(s -> jsonObject1.put(s.toLowerCase(), jsonObject.get(s)));
                                    return jsonObject1;
                                })
                                .filter(jsonObject -> groupsBy.stream().allMatch(jsonObject::has) && vars.stream().allMatch(jsonObject::has))
                                .forEach(jsonObject -> {
                                    final String[] key = {""};
                                    final String[] value = {""};
                                    groupsBy.forEach(oneGroupBy -> key[0] = key[0] + "," + jsonObject.get(oneGroupBy).toString());
                                    vars.forEach(oneVar -> value[0] = value[0] + "," + jsonObject.get(oneVar).toString());
                                    if (treeMap.containsKey(key[0])) {
                                        treeMap.replace(key[0], treeMap.get(key[0]) + "\n" + value[0].substring(1));
                                    } else {
                                        treeMap.put(key[0], value[0].substring(1));
                                    }
                                });
                        if (!treeMap.isEmpty()) {
                            fetchResults(listObjectOutput, treeMap, outputType, statType);
                        }
                    } else if (outputType.equals(ChartOutputType.LINE.name())) {
                        TreeMap<String, String> treeMap = new TreeMap<>();
                        StreamSupport.stream(jsonArrayInput.spliterator(), false)
                                .map(o -> {
                                    JSONObject jsonObject = new org.json.JSONObject(o.toString());
                                    JSONObject jsonObject1 = new JSONObject();
                                    new ArrayList<>(jsonObject.keySet()).forEach(s -> jsonObject1.put(s.toLowerCase(), jsonObject.get(s)));
                                    return jsonObject1;
                                })
                                .filter(jsonObject -> groupsBy.stream().allMatch(jsonObject::has) && jsonObject.has(firstVar))
                                .forEach(jsonObject -> {
                                    final String[] key = {""};
                                    final String[] value = {""};
                                    groupsBy.forEach(oneGroupBy -> key[0] = key[0] + "," + jsonObject.get(oneGroupBy).toString());
                                    value[0] = jsonObject.get(firstVar).toString();
                                    if (treeMap.containsKey(key[0])) {
                                        treeMap.replace(key[0], treeMap.get(key[0]) + "\n" + value[0]);
                                    } else {
                                        treeMap.put(key[0], value[0]);
                                    }
                                });

                        if (!treeMap.isEmpty()) {
                            fetchResults(listObjectOutput, treeMap, outputType, statType);
                        }
                    }
                } else {
                    List<String> vars = Arrays.asList(oneLine.substring(oneLine.indexOf("VAR(") + 4, oneLine.indexOf(")STAT")).toLowerCase().split(","));
                    String statType = ChartStatType.valueOf(oneLine.substring(oneLine.indexOf("STAT(") + 5, oneLine.indexOf(")OUTPUTTYPE"))).name();
                    Map<String, List<Long>> hashMap = new LinkedHashMap<>();
                    List<Long> integerList = new LinkedList<>();
                    StreamSupport.stream(jsonArrayInput.spliterator(), false)
                            .map(o -> {
                                JSONObject jsonObject = new org.json.JSONObject(o.toString());
                                JSONObject jsonObject1 = new JSONObject();
                                new ArrayList<>(jsonObject.keySet()).forEach(s -> jsonObject1.put(s.toLowerCase(), jsonObject.get(s)));
                                return jsonObject1;
                            })
                            .filter(jsonObject -> vars.stream().allMatch(jsonObject::has))
                            .forEach(jsonObject -> {
                                if (outputType.equals(ChartOutputType.STATS.name())) {
                                    vars.forEach(oneVar -> {
                                        Long varValue = Long.parseLong(jsonObject.get(oneVar).toString().replace("'", ""));
                                        if (hashMap.containsKey(oneVar)) {
                                            List<Long> integers = hashMap.get(oneVar);
                                            integers.add(varValue);
                                            hashMap.replace(oneVar, integers);
                                        } else {
                                            hashMap.put(oneVar, Lists.newArrayList(varValue));
                                        }
                                    });
                                } else if (outputType.equals(ChartOutputType.HISTOGRAM.name())) {
                                    integerList.add(Long.parseLong(jsonObject.get(vars.get(0)).toString().replace("'", "")));
                                }
                            });
                    if (!hashMap.isEmpty()) {
                        String result = Joiner.on(",").join(hashMap.values().stream().map(integers -> {
                            String value = "";
                            value = getValue(statType, value, integers.stream().mapToLong(i -> i));
                            return value;
                        }).collect(Collectors.toList()));
                        listObjectOutput.add(result + "\n");
                    } else if (!integerList.isEmpty()) {
                        listObjectOutput.add(integerList.toString() + "\n");
                    }
                }
            } else {
                listObjectOutput.add(oneLine + "    ");
            }
        });
        return String.join("", listObjectOutput);
    }

    private void fetchResults(List<String> listObjectOutput, Map<String, String> treeMap, String outputType, String statType) {
        JSONArray jsonArray = new JSONArray();
        TreeMap<String, Object> helperTreeMap = new TreeMap<>();
        Map<String, Object> helperHashMap = new LinkedHashMap<>();

        if (outputType.equals(ChartOutputType.UNIQUE_ITEM_LIST.name())) {
            treeMap.forEach((key, value) -> {
                List<String> keys = Arrays.asList(key.substring(1).split(",")).stream().map(s -> "'" + s + "'").collect(Collectors.toList());
                Arrays.stream(value.split("\n")).forEach(oneLineValue -> {
                    final int[] counter = {0};
                    List<String> values = Arrays.asList(oneLineValue.substring(1).split(","));
                    IntStream
                            .range(0, values.size())
                            .forEach(i -> {
                                String helperKey = keys.toString() + counter[0];
                                if (helperHashMap.containsKey(helperKey)) {
                                    List<String> valuesVertical = (List<String>) helperHashMap.get(helperKey);
                                    valuesVertical.add(values.get(i));
                                    helperHashMap.replace(helperKey, valuesVertical);
                                } else {
                                    helperHashMap.put(helperKey, Lists.newArrayList(values.get(i)));
                                }
                                counter[0]++;
                            });
                });
            });
            Map<String, Object> finalHelperHashMap = new LinkedHashMap<>();
            helperHashMap.forEach((s, o) -> {
                String key = s.substring(1, s.indexOf("]"));
                String value = "";
                value = getValue(statType, value, ((List<String>) o).stream().mapToLong(value1 -> Long.parseLong(value1.replace("'", ""))));
                if (finalHelperHashMap.containsKey(key)) {
                    List<String> finalaValues = (List<String>) finalHelperHashMap.get(key);
                    finalaValues.add(value);
                    finalHelperHashMap.replace(key, finalaValues);
                } else {
                    finalHelperHashMap.put(key, Lists.newArrayList(value));
                }
            });
            if (!finalHelperHashMap.isEmpty()) {
                finalHelperHashMap.forEach((key, value) -> {
                    jsonArray.put("[" + key + ", " + value.toString().substring(1, value.toString().length() - 1) + "]");
                });
            }
        } else if (outputType.equals(ChartOutputType.BUBBLE.name()) || outputType.equals(ChartOutputType.BUBBLELEGEND.name())) {
            treeMap.forEach((key, value) -> {
                int length = value.split("\n")[0].split(",").length;
                String[] finalResult = new String[length];
                char[] dynAlpha = "xyzabcdefghijklmnopqrstuvw".concat("xyzabcdefghijklmnopqrstuvw".toUpperCase()).toCharArray();
                long[] results = new long[length];
                int lengthValue = value.split("\n").length;
                Arrays
                        .stream(value.split("\n"))
                        .map(oneLine -> Arrays.asList(oneLine.split(",")))
                        .forEach(oneLine -> {
                            if (statType.equals(ChartStatType.SUM.name()) || statType.equals(ChartStatType.AVG.name())) {
                                Arrays.setAll(results, i -> results[i] + Long.parseLong(oneLine.get(i).replace("'", "")));
                            } else if (statType.equals(ChartStatType.COUNT.name())) {
                                Arrays.setAll(results, i -> results[i] + 1);
                            }
                        });

                if (length > dynAlpha.length) {
                    return;
                }
                IntStream
                        .range(0, length)
                        .forEach(i -> finalResult[i] = "\"" + dynAlpha[i] + "\":" +
                                (statType.equals(ChartStatType.AVG.name())
                                        ? decimalFormat.format((float) results[i] / lengthValue) : decimalFormat.format(results[i])));

                if (outputType.equals(ChartOutputType.BUBBLELEGEND.name())) {
                    try {
                        ObjectNode jsonNode = objectMapper.readValue("{" + String.join(",", Arrays.asList(finalResult)) + "}", ObjectNode.class);
                        jsonNode.put("name", "'" + key.substring(1).split(",")[1] + "'");
                        String insideKey = "'" + key.substring(1).split(",")[0] + "'";
                        if (helperTreeMap.containsKey(insideKey)) {
                            JSONArray oldJsonArray = (JSONArray) helperTreeMap.get(insideKey);
                            oldJsonArray.put(jsonNode);
                            helperTreeMap.put(insideKey, oldJsonArray);
                        } else {
                            JSONArray arrayNode = new JSONArray();
                            arrayNode.put(jsonNode);
                            helperTreeMap.put(insideKey, arrayNode);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        ObjectNode jsonNode = objectMapper.readValue("{" + String.join(",", Arrays.asList(finalResult)) + "}", ObjectNode.class);
                        jsonArray.put(jsonNode.put("name", "'" + key + "'"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            if (!helperTreeMap.isEmpty()) {
                JSONArray jsonArrayLegend = new JSONArray();
                helperTreeMap.forEach((keyH, valueH) -> {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("name", keyH);
                    jsonObject.put("data", valueH);
                    jsonArrayLegend.put(jsonObject);
                });
                jsonArray.put(jsonArrayLegend);
            }
        } else if (outputType.equals(ChartOutputType.STACKBAR.name()) || outputType.equals(ChartOutputType.LINE.name())
                || outputType.equals(ChartOutputType.HEATMAP.name()) || outputType.equals(ChartOutputType.PACKEDBUBBLE.name())) {
            Map<String, String> helperKeysTreeMap = new HashMap<>();
            treeMap.forEach((key, value) -> {
                String fullKey = key;
                key = key.substring(1).split(",")[0];
                String otherKeys = fullKey.substring(fullKey.indexOf("," + key + ",") + (key.length() + 2));
                if (key.length() > 0 && otherKeys.length() > 0) {
                    if (helperKeysTreeMap.containsKey(key)) {
                        helperKeysTreeMap.replace(key, helperKeysTreeMap.get(key) + "\n" + otherKeys);
                    } else {
                        helperKeysTreeMap.put(key, otherKeys);
                    }
                }
                LongStream streamValue = Arrays.stream(value.split("\n")).mapToLong(value1 -> Long.parseLong(value1.replace("'", "")));
                value = getValue(statType, value, streamValue);
                if (helperTreeMap.containsKey(key)) {
                    String oldValue = (String) helperTreeMap.get(key);
                    helperTreeMap.replace(key, oldValue.substring(0, oldValue.length() - 1) + "," + value + "]");
                } else {
                    helperTreeMap.put(key, "[" + value + "]");
                }
            });
            Set<String> globalKeysSet = new HashSet<>();
            helperKeysTreeMap.values().forEach(oneValue -> globalKeysSet.addAll(Arrays.asList(oneValue.split("\n"))));
            List<String> globalKeys = globalKeysSet.stream().sorted().collect(Collectors.toList());
            final int[] firstPosition = {0};
            helperTreeMap.forEach((key, value) -> {
                LinkedList<String> dataList = Lists.newLinkedList(Arrays.asList(((String) value).substring(1, ((String) value).length() - 1).split(",")));
                List<String> valueKeys = new ArrayList<>();
                if (!helperKeysTreeMap.isEmpty()) {
                    valueKeys = Arrays.asList(helperKeysTreeMap.get(key).split("\n"));
                }
                List<String> missing = new ArrayList<>(globalKeys);
                missing.removeAll(valueKeys);
                IntStream
                        .range(0, globalKeys.size())
                        .forEach(index -> {
                            if (missing.contains(globalKeys.get(index))) {
                                dataList.add(index, "null");
                            }
                        });

                if (outputType.equals(ChartOutputType.PACKEDBUBBLE.name())) {
                    JSONObject jsonObjectPackedBubble = new JSONObject();
                    JSONArray jsonArrayPackedBubble = new JSONArray();
                    IntStream
                            .range(0, dataList.size())
                            .filter(i -> !dataList.get(i).equals("null"))
                            .forEach(secondPosition -> {
                                if (!globalKeys.isEmpty()) {
                                    JSONObject jsonObjectDataPackedBubble = new JSONObject();
                                    jsonObjectDataPackedBubble.put("name", "'" + globalKeys.get(secondPosition) + "'");
                                    jsonObjectDataPackedBubble.put("value", dataList.get(secondPosition));
                                    jsonArrayPackedBubble.put(jsonObjectDataPackedBubble);
                                }
                            });
                    if (!jsonArrayPackedBubble.isEmpty()) {
                        jsonObjectPackedBubble.put("name", "'" + key + "'");
                        jsonObjectPackedBubble.put("data", jsonArrayPackedBubble);
                    }
                    jsonArray.put(jsonObjectPackedBubble);
                } else if (outputType.equals(ChartOutputType.HEATMAP.name())) {
                    IntStream
                            .range(0, dataList.size())
                            .filter(i -> !dataList.get(i).equals("null"))
                            .forEach(secondPosition -> jsonArray.put("[" + firstPosition[0] + "," + secondPosition + "," + dataList.get(secondPosition) + "]"));
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("name", "'" + key + "'");
                    jsonObject.put("data", dataList);
                    jsonArray.put(jsonObject);
                }
                firstPosition[0]++;
            });
        }
        String result = jsonArray.toString();
        if (outputType.equals(ChartOutputType.UNIQUE_ITEM_LIST.name())) {
            listObjectOutput.add(result.substring(1, result.length() - 1).replaceAll("\"", "") + "\n");
            return;
        }
        listObjectOutput.add(result.replaceAll("\"", "") + "\n");
    }

    private String getValue(String statType, String value, LongStream streamValue) {
        if (statType.equals(ChartStatType.SUM.name())) {
            value = decimalFormat.format(streamValue.sum());
        } else if (statType.equals(ChartStatType.AVG.name())) {
            value = String.valueOf(decimalFormat.format(streamValue.average().orElse(0)));
        } else if (statType.equals(ChartStatType.COUNT.name())) {
            value = decimalFormat.format(streamValue.count());
        }
        return value;
    }

    public String getMixedResultJson2(String dataSetName, String message, List<String> htmlList, String fullHtml) {
        JSONObject json = new JSONObject(message);
        final String[] finalResult = {fullHtml};
        htmlList.forEach(oneHtmlLine -> finalResult[0] = finalResult[0].replace(oneHtmlLine, getMixedResultJson(json,
                oneHtmlLine.substring(oneHtmlLine.indexOf("%%" + dataSetName + "%%.") + (dataSetName.length() + 5)))));
        return finalResult[0];
    }

//    @Cacheable(value = "cacheMultiMap", cacheManager = "cacheManagerCaffeine")
    public MultiKeyMap getMatrixForConversion(final String html, String fileChartBody) {
        MultiKeyMap<String, List<String>> multiMap = MultiKeyMap.multiKeyMap(new LinkedMap<>());

        if (fileChartBody.contains("``API/") && fileChartBody.contains("``ENDAPI")) {
            List<String> apiParts = Arrays.asList(StringUtils.substringsBetween(fileChartBody, "``API/", "``ENDAPI"));
            apiParts.forEach(s -> {
                String firstLine = s.split("\n")[0].trim();
                if (firstLine.matches(".*?.{1,}/+URL\\((.*?.{1,})\\)")) {
                    String dataSetName = firstLine.substring(0, firstLine.indexOf("/URL"));
                    String apiUrl = firstLine.substring(firstLine.indexOf("/URL") + 4).replace("(", "").replace(")", "");
                    String finalHtml = html;
                    if (!html.contains("%%" + dataSetName + "%%")) {
                        finalHtml = html.concat("%%" + dataSetName + "%%.RESPONSE");
                    }
                    String body = s.substring(s.indexOf(firstLine) + firstLine.length());
                    Arrays.asList(finalHtml.split("\\s+")).forEach(oneHtmlLine -> {
                        if (Pattern.compile("(%%" + dataSetName + "%%.)" + "RESPONSE").matcher(oneHtmlLine).matches()) {
                            //api, datasetName, url, body
                            if (!multiMap.containsKey("api", dataSetName, apiUrl, body)) {
                                multiMap.put("api", dataSetName, apiUrl, body, Lists.newArrayList(oneHtmlLine));
                            } else {
                                ArrayList<String> htmlList = (ArrayList<String>) multiMap.get("api", dataSetName, apiUrl, body);
                                htmlList.add(oneHtmlLine);
                                multiMap.put("api", dataSetName, apiUrl, body, htmlList);
                            }
                        }
                    });
                }
            });
        }

        if (fileChartBody.contains("``SQL/") && fileChartBody.contains("``ENDSQL")) {
            List<String> sqlParts = Arrays.asList(StringUtils.substringsBetween(fileChartBody, "``SQL/", "``ENDSQL"));
            sqlParts.forEach(s -> {
                String firstLine = s.split("\n")[0].trim();
                String[] sqlLine = firstLine.split("/");
                if (sqlLine.length > 1) {
                    String dataSetName = sqlLine[0];
                    String connectionId = sqlLine[1];
                    String finalHtml = html;
                    if (!html.contains("%%" + dataSetName + "%%")) {
                        finalHtml = html.concat("%%" + dataSetName + "%%.%%GROUPBY()VAR()STAT(SUM)OUTPUTTYPE(BASE_RESULTSET)%%");
                    }
                    String sql = s.substring(s.indexOf(firstLine) + firstLine.length());
                    Arrays.asList(finalHtml.split("\\s+")).forEach(oneHtmlLine -> {
                        if (Pattern.compile("(%%" + dataSetName + "%%.)" + HTML_LINE_REGEX).matcher(oneHtmlLine).matches()) {
                            //datasetname, connectionID, sql
                            if (!multiMap.containsKey(dataSetName, connectionId, sql)) {
                                if (sqlLine.length > 2) {
                                    multiMap.put(dataSetName, connectionId, sqlLine[2], sql, Lists.newArrayList(oneHtmlLine));
                                } else {
                                    multiMap.put(dataSetName, connectionId, sql, Lists.newArrayList(oneHtmlLine));
                                }
                            } else {
                                ArrayList<String> htmlList = (ArrayList<String>) multiMap.get(dataSetName, connectionId, sql);
                                htmlList.add(oneHtmlLine);
                                if (sqlLine.length > 2) {
                                    multiMap.put(dataSetName, connectionId, sqlLine[2], sql, htmlList);
                                } else {
                                    multiMap.put(dataSetName, connectionId, sql, htmlList);
                                }
                            }
                        }
                    });
                }
            });
        }
        return multiMap;
    }

//    @Cacheable(value = "cacheMultiMapCommand", cacheManager = "cacheManagerCaffeine")
    public MultiKeyMap getMatrixForConversionCommand(String html, String fileCommandBody) {
        List<String> sqlParts = Arrays.asList(StringUtils.substringsBetween(fileCommandBody, "``COMMAND/", "``ENDCOMMAND"));
        MultiKeyMap<String, List<String>> multiMap = MultiKeyMap.multiKeyMap(new LinkedMap<>());
        sqlParts.forEach(s -> {
            String firstLine = s.split("\n")[0].trim();
            String[] sqlLine = firstLine.split("/");
            if (sqlLine.length > 2) {
                /*/DATASET1/runRfile/myscript1.r/KEEP*/
                String dataSetName = sqlLine[0]; // i.e DATASET1
                String commandProgram = sqlLine[1]; //i.e runRfile
                String scriptFile = sqlLine[2]; //i.e myscript1.r
                String option = sqlLine[3]; //i.e KEEP or delete
                String command = s.substring(s.indexOf(firstLine) + firstLine.length());
                Arrays.asList(html.split("\\s+")).forEach(oneHtmlLine -> {
                    if (Pattern.compile("(%%" + dataSetName + "%%)").matcher(oneHtmlLine).matches() &&
                            (option.equalsIgnoreCase("keep") || option.equalsIgnoreCase("delete"))) {
                        if (!multiMap.containsKey(dataSetName, commandProgram, scriptFile, option, command)) {
                            //datasetname, commandProgram, script_file_name, option, command
                            multiMap.put(dataSetName, commandProgram, scriptFile, option, command, Lists.newArrayList(oneHtmlLine));
                        } else {
                            ArrayList<String> htmlList = (ArrayList<String>) multiMap.get(dataSetName, commandProgram, scriptFile, option, command);
                            htmlList.add(oneHtmlLine);
                            multiMap.put(dataSetName, commandProgram, scriptFile, option, command, htmlList);
                        }
                    }
                });
            }
        });
        return multiMap;
    }

    public Object getCacheQueryIDS() {
        return new ArrayList<>(HelperUtil.queryIDObjectMultiMap.values());
    }
}
