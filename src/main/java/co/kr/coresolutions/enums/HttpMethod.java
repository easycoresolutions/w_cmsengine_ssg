package co.kr.coresolutions.enums;

public enum HttpMethod {
    GET,
    POST,
    PUT,
    DELETE
}
