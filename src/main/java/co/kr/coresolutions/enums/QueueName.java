package co.kr.coresolutions.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum QueueName {
    LONGQ("LONGQ"),
    LONGMIDDLEQ("LONGMIDDLEQ"),
    MIDDLEQ("MIDDLEQ"),
    MIDDLELOWQ("MIDDLELOWQ"),
    LOWQ("LOWQ");
    String type;

    public String getName() {
        return type;
    }
}
