package co.kr.coresolutions.enums;

public enum ChartStatType {
    SUM, AVG, COUNT
}
