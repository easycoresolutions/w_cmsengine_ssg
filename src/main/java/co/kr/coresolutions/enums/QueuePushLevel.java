package co.kr.coresolutions.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum QueuePushLevel {
    HIGH("HIGH"),
    MEDIUM("MEDIUM"),
    LOW("LOW");
    String level;

    public String getName() {
        return level;
    }
}
