package co.kr.coresolutions.enums;

public enum OutputType {

    csv("csv"),
    json("json"),
    text("text");

    String type;

    OutputType(String type) {
        this.type = type;
    }

    public String getName() {
        return type;
    }

}
