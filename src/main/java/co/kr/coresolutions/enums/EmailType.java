package co.kr.coresolutions.enums;

public enum EmailType {
    text("plain/text"),
    html("html");

    String type;

    EmailType(String type) {
        this.type = type;
    }

    public String getName() {
        return type;
    }

}
