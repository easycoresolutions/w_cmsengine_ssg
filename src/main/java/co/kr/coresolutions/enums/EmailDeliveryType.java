package co.kr.coresolutions.enums;

public enum EmailDeliveryType {
    attached("attached"),
    inline("inline");

    String type;

    EmailDeliveryType(String type) {
        this.type = type;
    }

    public String getName() {
        return type;
    }

}
