package co.kr.coresolutions.resource;

import co.kr.coresolutions.facades.IQueryPythonFacade;
import co.kr.coresolutions.model.dtos.SocketDto;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Scope(value = "request")
@RequestMapping(value = QueryResourcePythonSocket.V1_PY_SOCKET_PATH, consumes = "*/*;charset=UTF-8")
@RequiredArgsConstructor
public class QueryResourcePythonSocket {
    static final String V1_PY_SOCKET_PATH = "python/";
    private final IQueryPythonFacade queryPythonFacade;
    private final IQueryPythonFacade.IAdmin queryPythonFacadeAdmin;
    private final IQueryPythonFacade.IUser queryPythonFacadeUser;

    @PostMapping(value = "{connectionid}/evaluate")
    public ResponseEntity evaluateSocket(@Valid @RequestBody SocketDto socketDto,
                                         @PathVariable("connectionid") String connectionID) {
        return ResponseEntity.ok(queryPythonFacade.evaluate(socketDto, connectionID));
    }

    @PostMapping(value = "{connectionid}/open/{owner}")
    public ResponseEntity open(@PathVariable("connectionid") String connectionID,
                               @PathVariable("owner") String owner) {
        return ResponseEntity.ok(queryPythonFacadeAdmin.open(owner, connectionID));
    }

    @PostMapping(value = "{connectionid}/close/{owner}")
    public ResponseEntity close(@PathVariable("connectionid") String connectionID,
                                @PathVariable("owner") String owner) {
        return ResponseEntity.ok(queryPythonFacadeAdmin.close(owner, connectionID));
    }

    @GetMapping("users")
    public ResponseEntity listUsers() {
        return ResponseEntity.ok(queryPythonFacadeUser.listUsers());
    }

    @GetMapping("{connectionid}/running")
    public ResponseEntity listUsersRunning(@PathVariable("connectionid") String connectionID) {
        return ResponseEntity.ok(queryPythonFacadeUser.listUsersRunning(connectionID));
    }

//    @DeleteMapping("{connectionid}/user/{userid}")
//    public ResponseEntity deleteUser(@Valid @RequestBody SocketLoginDto socketLoginDto,
//                                     @PathVariable("connectionid") String connectionID,
//                                     @PathVariable(value = "userid") String userID) {
//        return ResponseEntity.ok(queryPythonFacadeUser.deleteUser(socketLoginDto, connectionID, userID));
//    }

}
