package co.kr.coresolutions.resource;

import co.kr.coresolutions.dao.NativeQuery;
import co.kr.coresolutions.model.Message;
import co.kr.coresolutions.model.Transfer;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceJSON;
import co.kr.coresolutions.service.QueryServiceLoad;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@RestController
@Scope(value = "request")
@RequiredArgsConstructor
public class QueryResourceTransfer {
    private final QueryService queryService;
    private final NativeQuery nativeQuery;
    private final QueryServiceJSON queryServiceJson;
    private final QueryServiceLoad queryServiceLoad;

    protected void handleAudit(Transfer transfer, int size) {
        LocalDateTime localDate = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYYMMDD");
        String created_date = localDate.format(formatter);
        formatter = DateTimeFormatter.ofPattern("hh:mm:ss");
        String created_time = localDate.format(formatter);
        if (nativeQuery.isTableExist(transfer.getAudit_connid(), transfer.getAudit_table()).startsWith("no")) {
            //create audit table.
            nativeQuery.executeQuery(transfer.getAudit_connid(), nativeQuery.getCreateTableAuditSchema(transfer.getAudit_table()));
        }
        //insert into audit
        nativeQuery.executeQuery(transfer.getAudit_connid(), "insert into "
                + transfer.getAudit_table() + " VALUES ('" + transfer.getTargetTable() + "','" + created_date + "','"
                + created_time + "'," + size + ")");

    }


    @RequestMapping(value = "/transfer", method = RequestMethod.POST, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> transfer(@Valid @RequestBody Transfer transfer,
                                           UriComponentsBuilder _ucBuilder, BindingResult result) {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/transfer").buildAndExpand().toUri());
        __headers.add("Content-Type", "application/json; charset=utf-8");

        Message _message = Message.getInstance();

        String queryid = "";
//      if(json.has("queryid")){
//   	   queryid = json.get("queryid").asText();
//      } 

        queryid = queryService.makeQueryId(queryid);

        if (result.hasErrors()) {
            queryService.addLogging("POST", "transfer : Some errors , Please check ur json content!" + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>("Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }

        if (!queryService.isConnectionIdFileExists(transfer.getSourceId())) {
            queryService.addLogging("POST", "source connection " + transfer.getSourceId() + " (information) doesn't exist." + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>("source connection " + transfer.getSourceId() + " (information) doesn't exist." + "\n", __headers, HttpStatus.UNAUTHORIZED);
        }
        if (!queryService.isConnectionIdFileExists(transfer.getTargetId())) {
            queryService.addLogging("POST", "target connection " + transfer.getSourceId() + " (information) doesn't exist." + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>("target connection " + transfer.getSourceId() + " (information) doesn't exist." + "\n", __headers, HttpStatus.UNAUTHORIZED);
        }
        String queryAfterConversion = queryService.convertGlobalVariable(queryService.getFormattedString(transfer.getSql()));
        if (queryAfterConversion.contains("NOT RESOLVED")) {
            _message.setMessage(new String(queryAfterConversion.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.PAYMENT_REQUIRED);
        }

        String resultValidity = queryService.checkValidity(queryAfterConversion, transfer.getSourceId(), "", queryid, false);

        if (resultValidity.startsWith("Error") || resultValidity.startsWith("error")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", resultValidity + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.PAYMENT_REQUIRED);
        }
        String sourceTable = (StringUtils.split(transfer.getSql().substring(transfer.getSql().indexOf("from") + 4)))[0];

        String isTableExist = nativeQuery.isTableExist(transfer.getTargetId(), transfer.getTargetTable());

        if (isTableExist.startsWith("error")) {
            _message.setMessage(new String(isTableExist.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", resultValidity + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String resultQuery = "";

        if (isTableExist.startsWith("yes")) {
            if (!transfer.getDeleteOption()) {
                _message.setMessage("Table already exists");
                queryService.addLogging("POST", _message.getMessage() + "\n");
                queryService.setRunStop(queryid);
                return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
            } else {
                resultQuery = nativeQuery.executeQuery(transfer.getTargetId(), "drop table " + transfer.getTargetTable() + ";");
                if (resultQuery.length() > 0) {
                    _message.setMessage(new String(resultQuery.getBytes(), StandardCharsets.UTF_8));
                    queryService.addLogging("POST", _message.getMessage() + "\n");
                    queryService.setRunStop(queryid);
                    return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
                }
            }
        }

        org.json.JSONObject output_Schema = transfer.getOutput_scheme();

        if (output_Schema.has("name") && output_Schema.has("type") && output_Schema.has("length")) {
            resultQuery = nativeQuery.CreateTable(transfer.getTargetTable(), transfer.getTargetId(), output_Schema);
            if (resultQuery.length() > 0) {
                _message.setMessage(new String(resultQuery.getBytes(), StandardCharsets.UTF_8));
                queryService.addLogging("POST", _message.getMessage() + "\n");
                queryService.setRunStop(queryid);
                return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.BAD_REQUEST);
            }
        } else {
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>("Not valid output_scheme syntax", __headers, HttpStatus.BAD_REQUEST);
        }

        String structuredJson = new String(queryServiceJson.structureJson(resultValidity, true, new HashMap<>()/*todo later if needs queryService.getColumnsDataType(query,connectionid)*/, queryid).getBytes(), StandardCharsets.UTF_8);

        JSONObject jsonObjectGlobal = new JSONObject(structuredJson);

        if (!jsonObjectGlobal.has("Input")) {
            String log = "transfer: the source table " + transfer.getSourceId() + " is Empty " + "\n\n";
            queryService.addLogging("POST", log);
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(log, __headers, HttpStatus.OK);
        }

        JSONArray jsonArrayInput = jsonObjectGlobal.getJSONArray("Input");

        Map<String, Object> stringMap = nativeQuery.getColumns(transfer.getTargetId(), transfer.getTargetTable());

        if (stringMap.containsKey("keys")) {
            resultQuery = nativeQuery.executeQuery(transfer.getTargetId(), queryServiceLoad.getInsertSqlQuery((Set<String>) stringMap.get("keys"), jsonArrayInput, transfer.getTargetTable()));
        } else {
            String message = new String(stringMap.get("error").toString().getBytes(), StandardCharsets.UTF_8);
            _message.setMessage(message);
            queryService.addLogging("POST", message + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        }

        if (resultQuery.length() > 0) {
            _message.setMessage(new String(resultQuery.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", resultQuery + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        }

        if (transfer.getAudit_enabled()) {
            if (transfer.getAudit_connid() != null && transfer.getAudit_table() != null) {
                handleAudit(transfer, jsonArrayInput.length());
            } else {
                _message.setMessage("Tansfer api: audit_connid and audit_table parameters must be not null");
                queryService.addLogging("POST", _message.getMessage() + "\n");
                queryService.setRunStop(queryid);
                return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
            }
        }

        String log = "transfer: data has been exchanged successfully from " + transfer.getSourceId() + " to " + transfer.getTargetId() + "\n\t with Query Sql is: " + queryAfterConversion + "\n\n";
        queryService.addLogging("POST", log);
        queryService.setRunStop(queryid);
        return new ResponseEntity<Object>(log, __headers, HttpStatus.OK);
    }
}
