package co.kr.coresolutions.resource;

import co.kr.coresolutions.model.ContentsLayout;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceContentsLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
@Scope(value = "request")
public class QueryResourceContentsLayout {

    @Autowired
    QueryService queryService;

    @Autowired
    QueryServiceContentsLayout queryServiceContentsLayout;

    @RequestMapping(value = "/deletecontentslayout/{ownerid}/{contentlayoutid}", method = RequestMethod.DELETE, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> deleteContentLayout(@PathVariable(value = "ownerid") String owner,
                                                      @PathVariable(value = "contentlayoutid") String contentlayoutid,
                                                      UriComponentsBuilder _ucBuilder) throws Exception {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/deletecontentslayout/" + owner + "/" + contentlayoutid).buildAndExpand().toUri());

        boolean resultDelete = queryServiceContentsLayout.deleteContentsLayout(owner, contentlayoutid);

        if (!resultDelete) {
            queryService.addLogging("DELETE", contentlayoutid + " doesn't exist.\n");
            return new ResponseEntity<Object>(contentlayoutid + " doesn't exist.", __headers, HttpStatus.CONFLICT);
        }

        String log = "deleteContentLayout: " + contentlayoutid;
        queryService.addLogging("DELETE", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(contentlayoutid + " Deleted successfully", __headers, HttpStatus.OK);
    }


    @RequestMapping(value = {"/getcontentslayouts/{owner}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getContentslayouts(@PathVariable String owner,
                                                     UriComponentsBuilder _ucBuilder) throws IOException {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/getcontentslayouts/" + owner).buildAndExpand().toUri());

        String resultgetContentsLayouts = queryServiceContentsLayout.getContentsLayouts(owner);

        if (resultgetContentsLayouts.equalsIgnoreCase("fail")) {
            queryService.addLogging("GET", owner + " doesn't exist.\n");
            return new ResponseEntity<Object>(owner + " doesn't exist.", __headers, HttpStatus.GONE);
        }
        __headers.add("Content-Type", "application/json; charset=utf-8");
        String log = " getContentslayouts called " + owner + "\n";
        queryService.addLogging("GET", log);

        return new ResponseEntity<Object>(new String(resultgetContentsLayouts.getBytes(), StandardCharsets.UTF_8), __headers, HttpStatus.OK);

    }

    @RequestMapping(value = {"/getcontentslayout/{ownerid}/{contentslayoutid}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getContentsLayout(@PathVariable(value = "ownerid") String owner,
                                                    @PathVariable(value = "contentslayoutid") String contentslayoutid,
                                                    UriComponentsBuilder _ucBuilder) throws IOException {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/getcontentslayout/" + owner + "/" + contentslayoutid).buildAndExpand().toUri());

        String resultgetContentsLayout = queryServiceContentsLayout.getContentsLayout(owner, contentslayoutid);
        if (resultgetContentsLayout.equalsIgnoreCase("failcontentslayoutid")) {
            queryService.addLogging("GET", contentslayoutid + " doesn't exist.\n");
            return new ResponseEntity<Object>(contentslayoutid + " doesn't exist.", __headers, HttpStatus.CONFLICT);
        } else if (resultgetContentsLayout.equalsIgnoreCase("failowner")) {
            queryService.addLogging("GET", owner + " directory doesn't exist.\n");
            return new ResponseEntity<Object>(owner + " doesn't exist.", __headers, HttpStatus.GONE);
        }
        String log = " getContentsLayout called " + owner + " , " + contentslayoutid + "\n";
        queryService.addLogging("GET", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(new String(resultgetContentsLayout.getBytes(), StandardCharsets.UTF_8), __headers, HttpStatus.OK);

    }


    @RequestMapping(value = "/savecontentslayout/{owner}/{contentslayoutid}", method = RequestMethod.POST, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> saveContensLayout(@Valid @RequestBody ContentsLayout _contentsLayout,
                                                    @PathVariable(value = "owner") String owner,
                                                    @PathVariable(value = "contentslayoutid") String contentslayoutid,
                                                    UriComponentsBuilder _ucBuilder, BindingResult result) throws Exception {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/savecontentslayout/" + owner + "/" + contentslayoutid).buildAndExpand().toUri());

        if (result.hasErrors()) {
            queryService.addLogging("POST", "SaveContentslayout : Some errors , Please check ur json content!" + "\n");
            return new ResponseEntity<Object>("SaveContentslayout : Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }

        String resultsaveLayout = queryServiceContentsLayout.saveContentsLayout(owner, contentslayoutid, _contentsLayout);
        if (resultsaveLayout.equalsIgnoreCase("fail")) {
            queryService.addLogging("POST", "can't create the contents layout " + contentslayoutid + " for the owner " + owner + "\n");
            return new ResponseEntity<Object>("can't create the contents layout " + contentslayoutid + " for the owner " + owner, __headers, HttpStatus.BANDWIDTH_LIMIT_EXCEEDED);
        }

        String log = contentslayoutid + " contents layout uploaded successfully\n";
        queryService.addLogging("POST", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(contentslayoutid + " contents layout uploaded successfully", __headers, HttpStatus.OK);
    }

}
