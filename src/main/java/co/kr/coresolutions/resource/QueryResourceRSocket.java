package co.kr.coresolutions.resource;

import co.kr.coresolutions.facades.IQueryRFacade;
import co.kr.coresolutions.model.dtos.SocketDto;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Scope(value = "request")
@RequestMapping(value = QueryResourceRSocket.V1_RSOCKET_PATH, consumes = "*/*;charset=UTF-8")
@RequiredArgsConstructor
public class QueryResourceRSocket {
    static final String V1_RSOCKET_PATH = "R/";
    private final IQueryRFacade queryRFacade;

    @PostMapping(value = "{connectionid}/evaluate2")
    public ResponseEntity evaluate(@Valid @RequestBody SocketDto socketDto,
                                   @PathVariable("connectionid") String connectionID) {
        return ResponseEntity.ok(queryRFacade.evaluate(socketDto, connectionID));
    }

    @PostMapping(value = "{connectionid}/ropen/{owner}")
    public ResponseEntity open(@PathVariable("connectionid") String connectionID,
                               @PathVariable("owner") String owner) {
        return ResponseEntity.ok(queryRFacade.open(owner, connectionID));
    }

    @PostMapping(value = "{connectionid}/rclose/{owner}")
    public ResponseEntity close(@PathVariable("connectionid") String connectionID,
                                @PathVariable("owner") String owner) {
        return ResponseEntity.ok(queryRFacade.close(owner, connectionID));
    }

    @GetMapping("rusers")
    public ResponseEntity listUsers() {
        return ResponseEntity.ok(queryRFacade.listUsers());
    }

    @GetMapping("{connectionid}/running")
    public ResponseEntity listUsersRunning(@PathVariable("connectionid") String connectionID) {
        return ResponseEntity.ok(queryRFacade.listUsersRunning(connectionID));
    }

//    @DeleteMapping("{connectionid}/ruser/{userid}")
//    public ResponseEntity deleteUser(@Valid @RequestBody SocketLoginDto socketLoginDto,
//                                     @PathVariable("connectionid") String connectionID,
//                                     @PathVariable(value = "userid") String userID) {
//        return ResponseEntity.ok(queryRFacade.deleteUser(socketLoginDto, connectionID, userID));
//    }

}
