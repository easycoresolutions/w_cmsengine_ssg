package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryChartMonitorFacade;
import co.kr.coresolutions.facades.IQueryTemplateFacade;
import co.kr.coresolutions.model.dtos.Chart3Dto;
import co.kr.coresolutions.model.dtos.Chart4Dto;
import co.kr.coresolutions.model.dtos.CommandTemplateLiquid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = QueryResourceTemplate.V1_TEMPLATE_PATH)
public class QueryResourceTemplate {
    static final String V1_TEMPLATE_PATH = "/template/";
    private final IQueryTemplateFacade queryTemplateFacade;
    private final IQueryChartMonitorFacade queryChartMonitorFacade;

    @PostMapping(value = "query_json")
    public ResponseEntity formatQueryOutputJson(@Valid @RequestBody Chart3Dto chartDto, HttpServletRequest httpServletRequest) {
        CustomResponseDto customResponseDto = (CustomResponseDto) queryTemplateFacade.execChartInternal(chartDto, httpServletRequest);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }

    @PostMapping(value = "command_output")
    public ResponseEntity formatOutputCommandTemplate(HttpServletRequest httpServletRequest, @Valid @RequestBody CommandTemplateLiquid commandTemplateLiquid) {
        CustomResponseDto customResponseDto = (CustomResponseDto) queryTemplateFacade.execCommandTemplate(httpServletRequest, commandTemplateLiquid);
        return ResponseEntity.ok(customResponseDto.getSuccess() ? customResponseDto.getMessage() : customResponseDto);
    }

    @PostMapping("select_json/{connectionid}")
    public ResponseEntity formatSelectOutputJson(@PathVariable("connectionid") String connectionId, @Valid @RequestBody Chart4Dto chartDto) {
        CustomResponseDto customResponseDto = (CustomResponseDto) queryTemplateFacade.execChartInternalSelectJson(chartDto, connectionId);
        return ResponseEntity.status(HttpStatus.OK)
                .header("Content-Type", "application/json; charset=utf-8")
                .body(customResponseDto.getSuccess() ? customResponseDto.getMessage() : customResponseDto);
    }

    @GetMapping("monitorQuery")
    public ResponseEntity listTemplates() {
        return ResponseEntity.ok(queryChartMonitorFacade.listTemplates());
    }

    @DeleteMapping("monitor/releaseQuery/{queryid}")
    public ResponseEntity deleteTemplate(@PathVariable(value = "queryid") String queryID) {
        return ResponseEntity.ok(queryChartMonitorFacade.deleteTemplate(queryID));
    }


}
