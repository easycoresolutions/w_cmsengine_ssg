package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryProjectAuthFacade;
import co.kr.coresolutions.model.dtos.ProjectAuthDto;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = QueryResourceProjectAuth.V1_PROJECT_AUTH_PATH)
public class QueryResourceProjectAuth {
    static final String V1_PROJECT_AUTH_PATH = "/projectauth/";
    private final IQueryProjectAuthFacade queryProjectAuthFacade;

    @PostMapping(value = "{connectionid}/{projectid}", consumes = "application/json; charset=utf-8")
    public ResponseEntity<CustomResponseDto> saveProjectAuth(@RequestBody JsonNode jsonNode,
                                                             @PathVariable(value = "connectionid") String connectionID,
                                                             @PathVariable(value = "projectid") String projectID) {
        return ResponseEntity.ok(queryProjectAuthFacade.saveProject(jsonNode, connectionID, projectID));
    }

    @DeleteMapping("{connectionid}/{projectid}")
    public ResponseEntity<CustomResponseDto> deleteRow(@Valid @RequestBody ProjectAuthDto projectAuthDto,
                                                       @PathVariable(value = "connectionid") String connectionID,
                                                       @PathVariable(value = "projectid") String projectID) {
        return ResponseEntity.ok(queryProjectAuthFacade.deleteRow(projectAuthDto, connectionID, projectID));
    }

    @PostMapping("getdetails/{connectionid}/{projectid}")
    public ResponseEntity getDetails(@RequestBody JsonNode jsonNode,
                                     @PathVariable(value = "connectionid") String connectionID,
                                     @PathVariable(value = "projectid") String projectID) {
        CustomResponseDto customResponseDto = queryProjectAuthFacade.getDetails(jsonNode, connectionID, projectID);
        return ResponseEntity.status(HttpStatus.OK)
                .body(customResponseDto.getSuccess() ? customResponseDto.getDetailedMessage() : customResponseDto);
    }
}
