package co.kr.coresolutions.resource;

import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceFiles;
import co.kr.coresolutions.service.QueryServiceJSON;
import co.kr.coresolutions.util.Util;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.HashMap;

@RestController
@Scope(value = "request")
@RequiredArgsConstructor
public class QueryResourceFiles {
    private final QueryService queryService;
    private final QueryServiceFiles queryServiceFile;
    private final QueryServiceJSON queryServiceJSON;

    @PostMapping(value = "/config", consumes = "*/*;charset=UTF-8")
    public ResponseEntity uploadSystemControl(@RequestBody JsonNode jsonNode) {
        boolean isSaved = queryServiceFile.saveConfigFile(jsonNode);
        queryService.addLogging("POST", isSaved ? "config: file has been uploaded successfully" : "fail saving");
        return ResponseEntity.status(isSaved ? HttpStatus.CREATED : HttpStatus.NOT_FOUND).header("Content-Type", "application/json; charset=utf-8")
                .body(isSaved ? "config File uploaded successfully" : "fail saving");
    }

    @RequestMapping(value = {"/config"}, method = RequestMethod.GET)
    public ResponseEntity getConfigFile() {
        String resultGetConfigFile = queryServiceFile.getConfigFile();
        if (resultGetConfigFile.equalsIgnoreCase("fail")) {
            queryService.addLogging("GET", "config file doesn't exist.\n");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("config file doesn't exist.");
        }

        String log = "getConfigFile called" + "\n";
        queryService.addLogging("GET", log);
        return ResponseEntity.status(HttpStatus.OK).header("Content-Type", "application/json; charset=utf-8").body(resultGetConfigFile);
    }

    @RequestMapping(value = "/fileupload", method = RequestMethod.POST, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> fileUpload(@RequestBody String jsonObject,
                                             UriComponentsBuilder _ucBuilder, BindingResult result) throws Exception {

        org.json.JSONObject json = new org.json.JSONObject(jsonObject);
        String owner = json.getString("owner");
        owner = Util.replaceAll(owner, " ", "");
        String fileContents = json.getString("file");
        String filename = json.getString("filename");
        filename = Util.replaceAll(filename, " ", "");
        String ext = json.getString("ext");
        ext = Util.replaceAll(ext, " ", "");
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/fileupload").buildAndExpand().toUri());

        if (result.hasErrors() || !(fileContents.length() > 0 || owner.length() > 0 || filename.length() > 0 || ext.length() > 0
                || ext.equalsIgnoreCase("csv") || ext.equalsIgnoreCase("txt"))) {
            queryService.addLogging("POST", "fileupload : Some errors , Please check ur json content!" + "\n");
            return new ResponseEntity<Object>("fileupload : Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }

        String resultUpload = queryServiceFile.uploadFile(fileContents, owner, filename, ext);

        if (resultUpload.equalsIgnoreCase("fail")) {
            queryService.addLogging("POST", "Failed upload " + filename + "\n");
            return new ResponseEntity<Object>(filename + " Upload failed.", __headers, HttpStatus.METHOD_NOT_ALLOWED);
        }

        String log = "UploadFile: file " + filename + " has been uploaded successfully\n";
        queryService.addLogging("POST", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(filename + " uploaded successfully", __headers, HttpStatus.OK);
    }


    @RequestMapping(value = "/filedelete/{owner}/{filename:.+}", method = RequestMethod.DELETE, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> deleteFile(@PathVariable(value = "owner") String owner,
                                             @PathVariable(value = "filename") String filename,
                                             UriComponentsBuilder _ucBuilder) {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/filedelete/" + owner + "/" + filename).buildAndExpand().toUri());

        String resultDelete = queryService.deleteFile(owner, filename);

        if (resultDelete.equalsIgnoreCase("fail")) {
            queryService.addLogging("DELETE", "Delete Failed " + owner + "\t" + filename + "\n");
            return new ResponseEntity<Object>("Delete Failed " + owner + "\t" + filename + "\n", __headers, HttpStatus.PROXY_AUTHENTICATION_REQUIRED);
        }

        String log = "DeleteFile: file " + filename + " has been Deleted successfully";
        queryService.addLogging("DELETE", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(filename + " Deleted successfully", __headers, HttpStatus.OK);
    }


    @RequestMapping(value = {"/getfile/{owner}/{filename}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getFile(@PathVariable(value = "owner") String owner,
                                          @PathVariable(value = "filename") String filename,
                                          UriComponentsBuilder _ucBuilder) throws IOException {

        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/getfile/" + owner + "/" + filename).buildAndExpand().toUri());

        String resultGetFile = queryServiceFile.getFile(owner, filename);
        if (resultGetFile.equalsIgnoreCase("fail")) {
            queryService.addLogging("GET", filename + " doesn't exist.\n");
            return new ResponseEntity<Object>(filename + " doesn't exist.", __headers, HttpStatus.REQUEST_TIMEOUT);
        }

        __headers.add("Content-Type", "application/json; charset=utf-8");
        String log = "getFile called " + owner + " , " + filename + "\n";
        queryService.addLogging("GET", log);

        return new ResponseEntity<Object>(resultGetFile, __headers, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getfile_tojson/{owner}/{filename}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getFileToJson(@PathVariable(value = "owner") String owner,
                                                @PathVariable(value = "filename") String filename,
                                                UriComponentsBuilder _ucBuilder) throws IOException {

        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/getfile_tojson/" + owner + "/" + filename).buildAndExpand().toUri());

        String resultgetFile = queryServiceFile.getFile(owner, filename);
        if (resultgetFile.equalsIgnoreCase("fail")) {
            queryService.addLogging("GET", filename + " doesn't exist.\n");
            return new ResponseEntity<Object>(filename + " doesn't exist.", __headers, HttpStatus.REQUEST_TIMEOUT);
        }

        __headers.add("Content-Type", "application/json; charset=utf-8");
        String log = "getfile_tojson called " + owner + " , " + filename + "\n";
        queryService.addLogging("GET", log);
        if (filename.endsWith(".csv"))
            return new ResponseEntity<Object>(queryServiceJSON.structureJson(queryServiceJSON.structureJsonFromCSV(resultgetFile), false, new HashMap<>(), ""), __headers, HttpStatus.OK);
        else
            return new ResponseEntity<Object>(queryServiceJSON.structureJson(queryServiceJSON.structureJsonFromTAB(resultgetFile), false, new HashMap<>(), ""), __headers, HttpStatus.OK);
    }


    @RequestMapping(value = {"/getfiles/{owner}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getFiles(@PathVariable String owner,
                                           UriComponentsBuilder _ucBuilder) throws IOException {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/getfiles/" + owner).buildAndExpand().toUri());

        String resultgetFiles = queryServiceFile.getFiles(false, false, owner);
        if (resultgetFiles.equalsIgnoreCase("fail")) {
            queryService.addLogging("GET", owner + " doesn't exist.\n");
            return new ResponseEntity<Object>(owner + " doesn't exist.", __headers, HttpStatus.METHOD_NOT_ALLOWED);
        }
        __headers.add("Content-Type", "application/json; charset=utf-8");
        String log = "getfiles called " + owner + "\n";
        queryService.addLogging("GET", log);

        return new ResponseEntity<Object>(resultgetFiles, __headers, HttpStatus.OK);

    }

}
