package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQuerySqliteFacade;
import co.kr.coresolutions.model.TableLoad;
import co.kr.coresolutions.model.dtos.SqliteFileRepoDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import javax.validation.Valid;

@RestController
@RequestScope
@RequestMapping(value = QueryResourceSQLite.V1_SQLITE_PATH, consumes = "*/*;charset=UTF-8")
@RequiredArgsConstructor
public class QueryResourceSQLite {
    static final String V1_SQLITE_PATH = "sqlite/";
    private final IQuerySqliteFacade querySqliteFacade;

    @PostMapping(value = "createdb_file")
    public ResponseEntity createDBFile(@Valid @RequestBody SqliteFileRepoDto sqliteFileRepoDto) {
        return ResponseEntity.ok(querySqliteFacade.createDBFile(sqliteFileRepoDto));
    }

    @DeleteMapping(value = "{userid}/db/{db_file_name}")
    public ResponseEntity deleteDBfile(@PathVariable(value = "db_file_name") String dbFileName, @PathVariable(value = "userid") String userID) {
        return ResponseEntity.ok(querySqliteFacade.deleteDbFile(userID, dbFileName));
    }

    @DeleteMapping(value = "{userid}/table/{table_name}")
    public ResponseEntity<Object> deleteTableFromDBfile(@PathVariable(value = "userid") String userID,
                                                        @PathVariable(value = "table_name") String tableName) {
        return ResponseEntity.ok(querySqliteFacade.deleteTableFromDBfile(userID, tableName));
    }

    @PostMapping(value = "tableload")
    public ResponseEntity tableLoad(@Valid @RequestBody TableLoad tableLoad) {
        return ResponseEntity.ok(querySqliteFacade.tableLoad(tableLoad));
    }

    @GetMapping(value = "{userID}/db_files")
    public ResponseEntity getDBFiles(@PathVariable("userID") String userID) {
        return ResponseEntity.ok(querySqliteFacade.getDBFiles(userID));
    }

    @PostMapping(value = "{userid}/active/{dbfile}")
    public ResponseEntity activate(@PathVariable("userid") String userID,
                                   @PathVariable("dbfile") String dbFile) {
        return ResponseEntity.ok(querySqliteFacade.activate(userID, dbFile));
    }

    @GetMapping(value = "{userid}/activefile")
    public ResponseEntity getActiveDBMS(@PathVariable("userid") String userID) {
        CustomResponseDto customResponseDto = (CustomResponseDto) querySqliteFacade.getActiveDbms(userID);
        return ResponseEntity.ok(customResponseDto.getSuccess() ? customResponseDto.getMessage() : customResponseDto);
    }
}
