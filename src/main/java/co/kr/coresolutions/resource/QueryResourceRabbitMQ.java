package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQueryRabbitMQFacade;
import co.kr.coresolutions.model.dtos.JsonDataDto;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = QueryResourceRabbitMQ.V1_AMQP_PATH)
@Slf4j
public class QueryResourceRabbitMQ {

    static final String V1_AMQP_PATH = "/rabbitmq";

    private final IQueryRabbitMQFacade queryRabbitMQFacade;

    @PostMapping("/send/{connectionId}/{queueName}")
    @ApiOperation(value = "use the connectionId file to publish data to the queueName")
    @SneakyThrows
    public ResponseEntity<ResponseDto> publish(@Valid @RequestBody JsonDataDto jsonDataDto,
                                               @PathVariable("connectionId") String connectionId,
                                               @PathVariable("queueName") String queueName) {

//        System.out.println("call Post rabbitMQ !!!");
        ResponseDto responseDto = queryRabbitMQFacade.publish(jsonDataDto, connectionId, queueName);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).body(responseDto);
    }

    @GetMapping("/getqueues/{connectionId}")
    @ApiOperation(value = "get list of queues from http call")
    @SneakyThrows
    public ResponseEntity findAllQueues(@PathVariable("connectionId") String connectionId) {

        ResponseDto responseDto = queryRabbitMQFacade.findAllQueues(connectionId);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).body(responseDto.getMessage());
    }

    @GetMapping("/getexchanges/{connectionId}/{queueName}")
    @ApiOperation(value = "get list of exchanges in specified queue")
    @SneakyThrows
    public ResponseEntity findAllExchanges(@PathVariable("connectionId") String connectionId,
                                           @PathVariable("queueName") String queueName) {

        ResponseDto responseDto = queryRabbitMQFacade.findAllExchanges(connectionId, queueName);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).body(responseDto.getMessage());
    }
}
