package co.kr.coresolutions.resource;

import co.kr.coresolutions.enums.QueuePushLevel;
import co.kr.coresolutions.facades.IQueryQueuePushedFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

@RestController
@RequestScope
@RequestMapping(value = QueryResourceQueuePushed.V1_QUEUE_PATH, consumes = "*/*;charset=UTF-8")
@RequiredArgsConstructor
public class QueryResourceQueuePushed {
    static final String V1_QUEUE_PATH = "queuepushed/";
    private final IQueryQueuePushedFacade queryQueuePushedFacade;

    @GetMapping(value = "{queueLevel}")
    public ResponseEntity getQueues(@PathVariable("queueLevel") QueuePushLevel queuePushLevel) {
        return ResponseEntity.ok(queryQueuePushedFacade.getQueues(queuePushLevel).toString());
    }

    @DeleteMapping(value = "{queueLevel}/{queryID}")
    public ResponseEntity<Object> deleteQueueQueryID(@PathVariable(value = "queueLevel") QueuePushLevel queueType,
                                                     @PathVariable(value = "queryID") String queryID) {
        return ResponseEntity.ok(queryQueuePushedFacade.deleteQueueQueryID(queueType, queryID));
    }

}
