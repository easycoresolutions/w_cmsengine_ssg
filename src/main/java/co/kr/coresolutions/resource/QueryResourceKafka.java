package co.kr.coresolutions.resource;

import co.kr.coresolutions.facades.IQueryKafkaFacade;
import co.kr.coresolutions.model.dtos.Kafka2Dto;
import co.kr.coresolutions.model.dtos.Kafka3Dto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = QueryResourceKafka.V1_KAFKA_PATH)
public class QueryResourceKafka {
    static final String V1_KAFKA_PATH = "/kafka/";
    private final IQueryKafkaFacade queryKafkaFacade;

    @PostMapping("send/{kakfka_connectionID}")
    public ResponseEntity send(@PathVariable("kakfka_connectionID") String connectionID,
                               @Valid @RequestBody Kafka3Dto kafka3Dto) {
        return ResponseEntity.ok(queryKafkaFacade.send(connectionID, kafka3Dto));
    }

    @PostMapping("sendrecords/{kafka_connectionID}")
    public ResponseEntity sendRecords(@PathVariable("kafka_connectionID") String connectionID,
                                      @Valid @RequestBody Kafka2Dto kafka2Dto) {
        return ResponseEntity.ok(queryKafkaFacade.sendRecords(connectionID, kafka2Dto));
    }
}
