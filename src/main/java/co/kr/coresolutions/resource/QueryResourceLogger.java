package co.kr.coresolutions.resource;

import co.kr.coresolutions.facades.IQueryLogFacade;
import co.kr.coresolutions.model.dtos.LogDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = QueryResourceLogger.V1_LOGGER_PATH)
@Slf4j
public class QueryResourceLogger {
    static final String V1_LOGGER_PATH = "/log/";
    private final IQueryLogFacade queryLogFacade;

    @PostMapping(value = "send", consumes = "*/*;charset=UTF-8")
    public ResponseEntity send(@Valid @RequestBody LogDto logDto) {
        queryLogFacade.send(logDto);
        return ResponseEntity.ok().build();
    }
}
