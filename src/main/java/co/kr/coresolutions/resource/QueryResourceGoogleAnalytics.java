package co.kr.coresolutions.resource;

import co.kr.coresolutions.facades.IQueryGAFacade;
import co.kr.coresolutions.model.dtos.GATemplateDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = QueryResourceGoogleAnalytics.V1_GA_PATH)
@Slf4j
public class QueryResourceGoogleAnalytics {
    static final String V1_GA_PATH = "/ga/";
    private final IQueryGAFacade queryGAFacade;

    @PostMapping("{connectionFile}")
    public ResponseEntity getReports(@PathVariable("connectionFile") String connectionFile, @Valid @RequestBody GATemplateDto gaTemplateDto) {
        return ResponseEntity.ok(queryGAFacade.getReports(connectionFile, gaTemplateDto));
    }
}
