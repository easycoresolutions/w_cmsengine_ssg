package co.kr.coresolutions.resource;

import co.kr.coresolutions.model.RfmLayout;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceRfmLayout;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
@Scope(value = "request")
@RequiredArgsConstructor
public class QueryResourceRfmLayout {
    private final QueryService queryService;
    private final QueryServiceRfmLayout queryServiceRfmLayout;

    @RequestMapping(value = "/deleterfmlayout/{ownerid}/{rfmlayoutid}", method = RequestMethod.DELETE, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> deleteRfmLayout(@PathVariable(value = "ownerid") String owner,
                                                  @PathVariable(value = "rfmlayoutid") String rfmlayoutid,
                                                  UriComponentsBuilder _ucBuilder) throws Exception {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/deleterfmlayout/" + owner + "/" + rfmlayoutid).buildAndExpand().toUri());

        boolean resultDelete = queryServiceRfmLayout.deleteRfmLayout(owner, rfmlayoutid);

        if (!resultDelete) {
            queryService.addLogging("DELETE", rfmlayoutid + " doesn't exist.\n");
            return new ResponseEntity<Object>(rfmlayoutid + " doesn't exist.", __headers, HttpStatus.CONFLICT);
        }

        String log = "deleteRfmLayout: " + rfmlayoutid;
        queryService.addLogging("DELETE", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(rfmlayoutid + " Deleted successfully", __headers, HttpStatus.OK);
    }


    @RequestMapping(value = {"/getrfmlayouts/{owner}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getRfmlayouts(@PathVariable String owner,
                                                UriComponentsBuilder _ucBuilder) throws IOException {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/getrfmlayouts/" + owner).buildAndExpand().toUri());

        String resultgetRfmLayouts = queryServiceRfmLayout.getRfmLayouts(owner);

        if (resultgetRfmLayouts.equalsIgnoreCase("fail")) {
            queryService.addLogging("GET", owner + " doesn't exist.\n");
            return new ResponseEntity<Object>(owner + " doesn't exist.", __headers, HttpStatus.GONE);
        }
        __headers.add("Content-Type", "application/json; charset=utf-8");
        String log = " getRfmlayouts called " + owner + "\n";
        queryService.addLogging("GET", log);
        return new ResponseEntity<Object>(new String(resultgetRfmLayouts.getBytes(), StandardCharsets.UTF_8), __headers, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getrfmlayout/{ownerid}/{rfmlayoutid}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getRfmLayout(@PathVariable(value = "ownerid") String owner,
                                               @PathVariable(value = "rfmlayoutid") String rfmlayoutid,
                                               UriComponentsBuilder _ucBuilder) throws IOException {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/getrfmlayout/" + owner + "/" + rfmlayoutid).buildAndExpand().toUri());

        String resultgetRfmLayout = queryServiceRfmLayout.getRfmLayout(owner, rfmlayoutid);
        if (resultgetRfmLayout.equalsIgnoreCase("failrfmlayoutid")) {
            queryService.addLogging("GET", rfmlayoutid + " doesn't exist.\n");
            return new ResponseEntity<Object>(rfmlayoutid + " doesn't exist.", __headers, HttpStatus.CONFLICT);
        } else if (resultgetRfmLayout.equalsIgnoreCase("failowner")) {
            queryService.addLogging("GET", owner + " directory doesn't exist.\n");
            return new ResponseEntity<Object>(owner + " doesn't exist.", __headers, HttpStatus.GONE);
        }
        String log = " getRfmLayout called owner " + owner + " , rfmlayoutid " + rfmlayoutid + "\n";
        queryService.addLogging("GET", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(new String(resultgetRfmLayout.getBytes(), StandardCharsets.UTF_8), __headers, HttpStatus.OK);
    }


    @RequestMapping(value = "/saverfmlayout/{owner}/{rfmlayoutid}", method = RequestMethod.POST, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> saveRfmLayout(@Valid @RequestBody RfmLayout _rfmLayout,
                                                @PathVariable(value = "owner") String owner,
                                                @PathVariable(value = "rfmlayoutid") String rfmlayoutid,
                                                UriComponentsBuilder _ucBuilder, BindingResult result) throws Exception {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/saverfmlayout/" + owner + "/" + rfmlayoutid).buildAndExpand().toUri());

        if (result.hasErrors()) {
            queryService.addLogging("POST", "SaveRfmlayout : Some errors , Please check ur json content!" + "\n");
            return new ResponseEntity<Object>("SaveRfmlayout : Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }

        String resultsaveLayout = queryServiceRfmLayout.saveRfmLayout(owner, rfmlayoutid, _rfmLayout);
        if (resultsaveLayout.equalsIgnoreCase("fail")) {
            queryService.addLogging("POST", "can't create the rfm layout " + rfmlayoutid + " for the owner " + owner + "\n");
            return new ResponseEntity<Object>("can't create the rfm layout " + rfmlayoutid + " for the owner " + owner, __headers, HttpStatus.BANDWIDTH_LIMIT_EXCEEDED);
        }

        String log = rfmlayoutid + " rfm layout uploaded successfully\n";
        queryService.addLogging("POST", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(rfmlayoutid + " rfm layout uploaded successfully", __headers, HttpStatus.OK);

    }

}
