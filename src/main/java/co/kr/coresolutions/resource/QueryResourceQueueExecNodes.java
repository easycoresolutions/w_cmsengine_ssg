package co.kr.coresolutions.resource;

import co.kr.coresolutions.facades.IQueryQueueExecNodesFacade;
import co.kr.coresolutions.model.dtos.QueueExecNodesDto;
import co.kr.coresolutions.util.Util;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = QueryResourceQueueExecNodes.V1_QUEUE_EXEC_NODES_PATH, consumes = "*/*;charset=UTF-8")
public class QueryResourceQueueExecNodes {
    static final String V1_QUEUE_EXEC_NODES_PATH = "/queue_execNodes/";
    private final IQueryQueueExecNodesFacade queryLazyCommandFacade;

    @PostMapping("{commandid}")
    public ResponseEntity send(@PathVariable("commandid") String commandID,
                               @Valid @RequestBody QueueExecNodesDto execNodesDto,
                               HttpServletRequest request) {
        return ResponseEntity.ok(queryLazyCommandFacade.send(commandID, execNodesDto, Util.getBaseUrl(request)));
    }

    @GetMapping("waiting")
    public ResponseEntity findWaiting() {
        return ResponseEntity.ok(queryLazyCommandFacade.findWaiting());
    }

    @DeleteMapping("{commandid}")
    public ResponseEntity remove(@PathVariable("commandid") String commandID) {
        return ResponseEntity.ok(queryLazyCommandFacade.remove(commandID));
    }
}
