package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQueryFileSystemFacade;
import co.kr.coresolutions.model.dtos.FileSystemDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = QueryResourceFileSystem.V1_FILE_SYSTEM_PATH)
@Slf4j
public class QueryResourceFileSystem {
    static final String V1_FILE_SYSTEM_PATH = "/filesystem/";
    private final IQueryFileSystemFacade queryFileSystemFacade;

    @PostMapping("files")
    public ResponseEntity<Object> files(@Valid @RequestBody FileSystemDto fileSystemDto) {
        CustomResponseDto customResponseDto = queryFileSystemFacade.getFiles(fileSystemDto);
        return ResponseEntity.status(customResponseDto.getErrorCode() == null ? 200 : HttpStatus.BAD_REQUEST.value()).header("Content-Type", "application/json; charset=utf-8").body(customResponseDto.getErrorCode() == null ? customResponseDto.getDetailedMessage() : customResponseDto);
    }

    @PostMapping("contents")
    public ResponseEntity<Object> contents(@Valid @RequestBody FileSystemDto fileSystemDto) {
        if (fileSystemDto.getFilename() == null || fileSystemDto.getFilename().isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(CustomResponseDto.builder().ErrorCode(HttpStatus.BAD_REQUEST.value()).ErrorMessage("filename should not be empty").build());
        }
        CustomResponseDto customResponseDto = queryFileSystemFacade.getContents(fileSystemDto);
        return ResponseEntity.status(customResponseDto.getErrorCode() == null ? 200 : HttpStatus.BAD_REQUEST.value()).header("Content-Type", "application/json; charset=utf-8").body(customResponseDto.getErrorCode() == null ? customResponseDto.getDetailedMessage() : customResponseDto);
    }

    @PostMapping("write")
    public ResponseEntity<Object> write(@Valid @RequestBody FileSystemDto fileSystemDto) {
        if (fileSystemDto.getFilename() == null || fileSystemDto.getContents() == null || fileSystemDto.getFilename().isEmpty() || fileSystemDto.getContents().isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(ResponseDto.builder().errorCode(HttpStatus.BAD_REQUEST.value()).message("filename/contents/overwrite should not be empty or null").build());
        }
        CustomResponseDto customResponseDto = queryFileSystemFacade.write(fileSystemDto);
        return ResponseEntity.status(customResponseDto.getErrorCode() == null ? 200 : HttpStatus.BAD_REQUEST.value()).header("Content-Type", "application/json; charset=utf-8").body(customResponseDto);
    }

    @DeleteMapping("write")
    public ResponseEntity<Object> delete(@Valid @RequestBody FileSystemDto fileSystemDto) {
        if (fileSystemDto.getFilename() == null || fileSystemDto.getFilename().isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(ResponseDto.builder().errorCode(HttpStatus.BAD_REQUEST.value()).message("filename should not be empty or null").build());
        }
        CustomResponseDto customResponseDto = queryFileSystemFacade.delete(fileSystemDto);
        return ResponseEntity.status(customResponseDto.getErrorCode() == null ? 200 : HttpStatus.BAD_REQUEST.value()).header("Content-Type", "application/json; charset=utf-8").body(customResponseDto);
    }

}
