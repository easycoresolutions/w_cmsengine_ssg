package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQuerySchemaTableFacade;
import co.kr.coresolutions.model.dtos.SchemaDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = QueryResourceSchemaTable.V1_SCHEMA_TABLE_PATH)
public class QueryResourceSchemaTable {
    static final String V1_SCHEMA_TABLE_PATH = "/schematable/";
    private final IQuerySchemaTableFacade querySchemaTableFacade;

    @PostMapping(value = "{connectionid}/{schema_id}", consumes = "application/json; charset=utf-8")
    public ResponseEntity saveSchemaTable(@Valid @RequestBody SchemaDto schemaDto,
                                          @PathVariable(value = "connectionid") String connectionID,
                                          @PathVariable(value = "schema_id") String schemaID) {
        return ResponseEntity.ok(querySchemaTableFacade.saveSchema(schemaDto, connectionID, schemaID));
    }

    @DeleteMapping("{connectionid}/{schema_id}")
    public ResponseEntity deleteRow(@PathVariable(value = "connectionid") String connectionID,
                                    @PathVariable(value = "schema_id") String schemaID) {
        return ResponseEntity.ok(querySchemaTableFacade.deleteRow(connectionID, schemaID));
    }

    @GetMapping("{connectionid}/{schema_id}")
    public ResponseEntity getSchema(@PathVariable(value = "connectionid") String connectionID,
                                    @PathVariable(value = "schema_id") String schemaID) {
        CustomResponseDto customResponseDto = querySchemaTableFacade.getSchema(connectionID, schemaID);
        return ResponseEntity.status(HttpStatus.OK)
                .body(customResponseDto.getSuccess() ? customResponseDto.getMessage() : customResponseDto);
    }
}
