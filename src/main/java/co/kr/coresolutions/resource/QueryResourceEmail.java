package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQueryEmailFacade;
import co.kr.coresolutions.model.dtos.EmailDto;
import co.kr.coresolutions.model.dtos.EmailSpawnDto;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = QueryResourceEmail.V1_EMAIL_PATH)
@Slf4j
public class QueryResourceEmail {
    static final String V1_EMAIL_PATH = "/send/email/";
    private final IQueryEmailFacade queryEmailFacade;

    @PostMapping
    @ApiOperation(value = "send email to specified address")
    @SneakyThrows
    public ResponseEntity<ResponseDto> sendSimpleEmail(@Valid @RequestBody EmailDto emailDto) {
        ResponseDto responseDto = queryEmailFacade.sendSimpleEmail(emailDto);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).body(responseDto);
    }

    @PostMapping(value = "spawnoutput")
    public ResponseEntity<ResponseDto> sendSpawnEmail(@Valid @RequestBody EmailSpawnDto emailSpawnDto) {
        return ResponseEntity.ok(queryEmailFacade.sendSpawnEmail(emailSpawnDto));
    }
}
