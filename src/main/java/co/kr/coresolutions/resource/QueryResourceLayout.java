package co.kr.coresolutions.resource;

import co.kr.coresolutions.model.QueryLayout;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceLayout;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
@Scope(value = "request")
@RequiredArgsConstructor
public class QueryResourceLayout {
    private final QueryService queryService;
    private final QueryServiceLayout queryServiceLayout;

    @RequestMapping(value = "/deletequerylayout/{ownerid}/{querylayoutid}", method = RequestMethod.DELETE, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> deleteLayout(@PathVariable(value = "ownerid") String owner,
                                               @PathVariable(value = "querylayoutid") String layoutid,
                                               UriComponentsBuilder _ucBuilder) throws Exception {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/deletequerylayout/" + owner + "/" + layoutid).buildAndExpand().toUri());

        boolean resultDelete = queryServiceLayout.deleteLayout(owner, layoutid);

        if (!resultDelete) {
            queryService.addLogging("DELETE", layoutid + " doesn't exist.\n");
            return new ResponseEntity<Object>(layoutid + " doesn't exist.", __headers, HttpStatus.CONFLICT);
        }

        String log = "deleteLayout: " + layoutid;
        queryService.addLogging("DELETE", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(layoutid + " Deleted successfully", __headers, HttpStatus.OK);
    }


    @RequestMapping(value = {"/getquerylayouts/{owner}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getLayouts(@PathVariable String owner,
                                             UriComponentsBuilder _ucBuilder) throws IOException {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/getquerylayouts/" + owner).buildAndExpand().toUri());

        String resultgetLayouts = queryServiceLayout.getLayouts(owner);

        if (resultgetLayouts.equalsIgnoreCase("fail")) {
            queryService.addLogging("GET", owner + " doesn't exist.\n");
            return new ResponseEntity<Object>(owner + " doesn't exist.", __headers, HttpStatus.GONE);
        }
        __headers.add("Content-Type", "application/json; charset=utf-8");
        String log = " getquerylayouts called " + owner + "\n";
        queryService.addLogging("GET", log);

        return new ResponseEntity<Object>(new String(resultgetLayouts.getBytes(), StandardCharsets.UTF_8), __headers, HttpStatus.OK);

    }

    @RequestMapping(value = {"/getquerylayout/{ownerid}/{querylayoutid}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getLayout(@PathVariable(value = "ownerid") String owner,
                                            @PathVariable(value = "querylayoutid") String layoutid,
                                            UriComponentsBuilder _ucBuilder) throws IOException {
//        System.out.println("getquerylayout called!! ");
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/getquerylayout/" + owner + "/" + layoutid).buildAndExpand().toUri());

        String resultgetLayout = queryServiceLayout.getLayout(owner, layoutid);
        if (resultgetLayout.equalsIgnoreCase("faillayoutid")) {
            queryService.addLogging("GET", layoutid + " doesn't exist.\n");
            return new ResponseEntity<Object>(layoutid + " doesn't exist.", __headers, HttpStatus.CONFLICT);
        } else if (resultgetLayout.equalsIgnoreCase("failowner")) {
            queryService.addLogging("GET", owner + " directory doesn't exist.\n");
            return new ResponseEntity<Object>(owner + " doesn't exist.", __headers, HttpStatus.GONE);
        }
        String log = " getLayout called " + owner + " , " + layoutid + "\n";
        queryService.addLogging("GET", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(new String(resultgetLayout.getBytes(), StandardCharsets.UTF_8), __headers, HttpStatus.OK);

    }


    @RequestMapping(value = "/savequerylayout/{owner}/{querylayoutid}", method = RequestMethod.POST, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> saveLayout(@Valid @RequestBody QueryLayout queryLayout,
                                             @PathVariable(value = "owner") String owner,
                                             @PathVariable(value = "querylayoutid") String querylayoutid,
                                             UriComponentsBuilder _ucBuilder, BindingResult result) throws Exception {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/savequerylayout/" + owner + "/" + querylayoutid).buildAndExpand().toUri());

        if (result.hasErrors()) {
            queryService.addLogging("POST", "savequerylayout : Some errors , Please check ur json content!" + "\n");
            return new ResponseEntity<Object>("savequerylayout : Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }

        String resultsaveLayout = queryServiceLayout.saveLayout(owner, querylayoutid, queryLayout);
        if (resultsaveLayout.equalsIgnoreCase("fail")) {
            queryService.addLogging("POST", "can't create the layout " + querylayoutid + " for the owner " + owner + "\n");
            return new ResponseEntity<Object>("can't create the layout " + querylayoutid + " for the owner " + owner, __headers, HttpStatus.BANDWIDTH_LIMIT_EXCEEDED);
        }

        String log = querylayoutid + " layout uploaded successfully\n";
        queryService.addLogging("POST", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(querylayoutid + " layout uploaded successfully", __headers, HttpStatus.OK);

    }

}
