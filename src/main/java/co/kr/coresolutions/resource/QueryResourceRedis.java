package co.kr.coresolutions.resource;

import co.kr.coresolutions.model.Message;
import co.kr.coresolutions.model.RedisSql;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceJSON;
import co.kr.coresolutions.service.QueryServiceRedis;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import redis.clients.jedis.Jedis;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Scope(value = "request")
@RequiredArgsConstructor
public class QueryResourceRedis {
    private final QueryService queryService;
    private final QueryServiceRedis queryServiceRedis;
    private final QueryServiceJSON queryServiceJSON;

    @PostMapping(value = "/redis/{connectionid}/string", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> saveString(@RequestBody ObjectNode jsonBody,
                                             @PathVariable("connectionid") String _connectionID,
                                             UriComponentsBuilder _ucBuilder
            , BindingResult bindingResult) {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/redis/" + _connectionID + "/string").buildAndExpand().toUri());
        __headers.add("Content-Type", "application/json; charset=utf-8");

        if (bindingResult.hasErrors() || !jsonBody.has("KEY") || !jsonBody.has("VALUE")) {
            queryService.addLogging("POST", "Update : Some errors , Please check ur json content!" + "\n");
            return new ResponseEntity<Object>("Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }

        Jedis jedis = queryServiceRedis.connectToRedis(_connectionID);

        if (jedis == null) {
            _message.setMessage("can't connect to Redis with Connection file " + _connectionID);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
        }

        String result = queryServiceRedis.saveString(jedis, jsonBody);

        String log = "ConnectionID is: " + _connectionID + " , Redis Command is: Save Cache" +
                "\n\tResult : " + result +
                "\n\tQuery is: String\n\n";
        queryService.addLogging("POST", log);
        queryServiceRedis.disconnect(jedis);

        return new ResponseEntity<Object>("Cache String Saved successfully", __headers, HttpStatus.OK);
    }


    @PostMapping(value = "/redis/{connectionid}/list", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> saveList(@RequestBody ObjectNode jsonBody,
                                           @PathVariable("connectionid") String _connectionID,
                                           UriComponentsBuilder _ucBuilder
            , BindingResult bindingResult) {

        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/redis/" + _connectionID + "/list").buildAndExpand().toUri());
        __headers.add("Content-Type", "application/json; charset=utf-8");

        if (bindingResult.hasErrors() || !jsonBody.has("KEY") || !jsonBody.has("LIST")) {
            queryService.addLogging("POST", "Update : Some errors , Please check ur json content!" + "\n");
            return new ResponseEntity<Object>("Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }

        Jedis jedis = queryServiceRedis.connectToRedis(_connectionID);

        if (jedis == null) {
            _message.setMessage("can't connect to Redis with Connection file " + _connectionID);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
        }

        String result = queryServiceRedis.saveList(jedis, jsonBody);
        if (result.length() > 0) {
            _message.setMessage(result);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.CONFLICT);
        }

        String log = "ConnectionID is: " + _connectionID + " , Redis Command is: Save Cache" +
                "\n\tQuery is: List\n\n";
        queryService.addLogging("POST", log);
        queryServiceRedis.disconnect(jedis);

        return new ResponseEntity<Object>("Cache List Saved successfully", __headers, HttpStatus.OK);
    }

    @PostMapping(value = "/redis/{connectionid}/hash", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> saveHash(@RequestBody ObjectNode jsonBody,
                                           @PathVariable("connectionid") String _connectionID,
                                           UriComponentsBuilder _ucBuilder,
                                           BindingResult bindingResult) {

        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/redis/" + _connectionID + "/hash").buildAndExpand().toUri());
        __headers.add("Content-Type", "application/json; charset=utf-8");

        if (bindingResult.hasErrors() || !jsonBody.has("KEY") || !jsonBody.has("HASH")) {
            queryService.addLogging("POST", "Update : Some errors , Please check ur json content!" + "\n");
            return new ResponseEntity<Object>("Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }

        Jedis jedis = queryServiceRedis.connectToRedis(_connectionID);

        if (jedis == null) {
            _message.setMessage("can't connect to Redis with Connection file " + _connectionID);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
        }

        String result = queryServiceRedis.saveHash(jedis, jsonBody);

        String log = "ConnectionID is: " + _connectionID + " , Redis Command is: Save Cache" +
                "\n\tResult : " + result +
                "\n\tQuery is: Hash\n\n";
        queryService.addLogging("POST", log);
        queryServiceRedis.disconnect(jedis);

        return new ResponseEntity<Object>("Cache Hash Saved successfully", __headers, HttpStatus.OK);
    }


    @PostMapping(value = "/redis/{connectionid}/file_hash", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> saveFileHash(@RequestBody ObjectNode jsonBody,
                                               @PathVariable("connectionid") String _connectionID,
                                               UriComponentsBuilder _ucBuilder,
                                               BindingResult bindingResult) throws IOException {

        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/redis/" + _connectionID + "/file_hash").buildAndExpand().toUri());
        __headers.add("Content-Type", "application/json; charset=utf-8");

        if (bindingResult.hasErrors() || !jsonBody.has("filename") || !jsonBody.has("format")
                || !jsonBody.has("KEY_NAME")) {
            queryService.addLogging("POST", "Update : Some errors , Please check ur json content!" + "\n");
            return new ResponseEntity<Object>("Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }
        String format = jsonBody.get("format").asText();
        boolean isFileExists = queryService.isFileExists(jsonBody.get("filename").asText() + "." + (format.equalsIgnoreCase("csv") ? format : "txt"));
        if (!isFileExists || !(Arrays.asList(new String[]{"csv", "tab"}).contains(format))) {
            _message.setMessage("filename (" + jsonBody.get("filename").asText() + ") doesn't exist.");
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }

        Jedis jedis = queryServiceRedis.connectToRedis(_connectionID);

        if (jedis == null) {
            _message.setMessage("can't connect to Redis with Connection file " + _connectionID);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
        }

        String result = queryServiceRedis.saveFileHash(jedis, jsonBody);
        if (result.startsWith("error")) {
            _message.setMessage(result);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.BAD_REQUEST);
        }

        String log = "ConnectionID is: " + _connectionID + " , Redis Command is: Save Cache" +
                "\n\tQuery is: File Hash\n\n";
        queryService.addLogging("POST", log);
        queryServiceRedis.disconnect(jedis);

        return new ResponseEntity<Object>("Cache File Hash Saved successfully", __headers, HttpStatus.OK);
    }


    @PostMapping(value = "/redis/{connectionid}/file_string", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> saveFileString(@RequestBody ObjectNode jsonBody,
                                                 @PathVariable("connectionid") String _connectionID,
                                                 UriComponentsBuilder _ucBuilder,
                                                 BindingResult bindingResult) throws IOException {

        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/redis/" + _connectionID + "/file_string").buildAndExpand().toUri());
        __headers.add("Content-Type", "application/json; charset=utf-8");

        if (bindingResult.hasErrors() || !jsonBody.has("filename") || !jsonBody.has("format")) {
            queryService.addLogging("POST", "Update : Some errors , Please check ur json content!" + "\n");
            return new ResponseEntity<Object>("Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }

        String format = jsonBody.get("format").asText();
        boolean isFileExists = queryService.isFileExists(jsonBody.get("filename").asText() + "." + (format.equalsIgnoreCase("csv") ? format : "txt"));
        if (!isFileExists || !(Arrays.asList(new String[]{"csv", "tab"}).contains(format))) {
            _message.setMessage("filename (" + jsonBody.get("filename").asText() + ") doesn't exist.");
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }

        Jedis jedis = queryServiceRedis.connectToRedis(_connectionID);

        if (jedis == null) {
            _message.setMessage("can't connect to Redis with Connection file " + _connectionID);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
        }

        String result = queryServiceRedis.saveFileString(jedis, jsonBody);
        if (result.startsWith("error")) {
            _message.setMessage(result);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.BAD_REQUEST);
        }

        String log = "ConnectionID is: " + _connectionID + " , Redis Command is: Save Cache" +
                "\n\tQuery is: File String\n\n";
        queryService.addLogging("POST", log);
        queryServiceRedis.disconnect(jedis);

        return new ResponseEntity<Object>("Cache File String Saved successfully", __headers, HttpStatus.OK);
    }


    @PostMapping(value = "/redis/{connectionid}/gethash", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> getHash(@RequestBody ObjectNode jsonBody,
                                          @PathVariable("connectionid") String _connectionID,
                                          UriComponentsBuilder _ucBuilder,
                                          BindingResult bindingResult) {

        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/redis/" + _connectionID + "/gethash").buildAndExpand().toUri());
        __headers.add("Content-Type", "application/json; charset=utf-8");

        if (bindingResult.hasErrors() || !jsonBody.has("KEY") || !jsonBody.has("HASH")) {
            queryService.addLogging("POST", "Update : Some errors , Please check ur json content!" + "\n");
            return new ResponseEntity<Object>("Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }
        if (!jsonBody.get("HASH").isArray() || !jsonBody.get("KEY").isTextual()) {
            queryService.addLogging("POST", "Update : Some errors , Please check ur json content!" + "\n");
            return new ResponseEntity<Object>("Some errors , HASH field must be array, and KEY field must be String", __headers, HttpStatus.BAD_REQUEST);
        }

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }

        Jedis jedis = queryServiceRedis.connectToRedis(_connectionID);
        if (jedis == null) {
            _message.setMessage("can't connect to Redis with Connection file " + _connectionID);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
        }

        JSONArray jsonArray = queryServiceRedis.readFileHash(jedis, jsonBody);
        if (jsonArray.length() == 0) {
            _message.setMessage("empty results");
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.BAD_REQUEST);
        }

        String log = "ConnectionID is: " + _connectionID + " , Redis Command is: get cache\n\n";
        queryService.addLogging("POST", log);
        queryServiceRedis.disconnect(jedis);

        return new ResponseEntity<Object>(new String(jsonArray.toString().getBytes(), StandardCharsets.UTF_8), __headers, HttpStatus.OK);
    }

    @GetMapping(value = "/redis/{connectionid}/string/{key}")
    public ResponseEntity<Object> getStringCache(@PathVariable("connectionid") String _connectionID,
                                                 @PathVariable("key") String key,
                                                 UriComponentsBuilder _ucBuilder) {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.add("Content-Type", "text/plain; charset=utf-8");
        __headers.setLocation(_ucBuilder.path("/redis/" + _connectionID + "/string/" + key).buildAndExpand().toUri());

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }

        Jedis jedis = queryServiceRedis.connectToRedis(_connectionID);
        if (jedis == null) {
            _message.setMessage("can't connect to Redis with Connection file " + _connectionID);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
        }

        String result = queryServiceRedis.getStringCache(jedis, key);

        String log = "getStringCache called \n";
        queryService.addLogging("GET", log);
        return new ResponseEntity<Object>(new String(result.getBytes(), StandardCharsets.UTF_8), __headers, HttpStatus.OK);

    }

    @GetMapping(value = "/redis/{connectionid}/list/{key}")
    public ResponseEntity<Object> getListCache(@PathVariable("connectionid") String _connectionID,
                                               @PathVariable("key") String key,
                                               UriComponentsBuilder _ucBuilder) {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.add("Content-Type", "text/plain; charset=utf-8");
        __headers.setLocation(_ucBuilder.path("/redis/" + _connectionID + "/list/" + key).buildAndExpand().toUri());

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }

        Jedis jedis = queryServiceRedis.connectToRedis(_connectionID);
        if (jedis == null) {
            _message.setMessage("can't connect to Redis with Connection file " + _connectionID);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
        }

        List<String> result = (queryServiceRedis.getListCache(jedis, key)).stream().map(s -> {
            return new String(s.getBytes(), StandardCharsets.UTF_8);
        }).collect(Collectors.toList());

        String log = "getListCache called \n";
        queryService.addLogging("GET", log);
        return new ResponseEntity<Object>(result, __headers, HttpStatus.OK);

    }

    @GetMapping(value = "/redis/{connectionid}/hash/{key}")
    public ResponseEntity<Object> getHashCache(@PathVariable("connectionid") String _connectionID,
                                               @PathVariable("key") String key,
                                               UriComponentsBuilder _ucBuilder) {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.add("Content-Type", "application/json; charset=utf-8");
        __headers.setLocation(_ucBuilder.path("/redis/" + _connectionID + "/hash/" + key).buildAndExpand().toUri());

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }

        Jedis jedis = queryServiceRedis.connectToRedis(_connectionID);
        if (jedis == null) {
            _message.setMessage("can't connect to Redis with Connection file " + _connectionID);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
        }

        JSONObject jsonObject = queryServiceRedis.getHash(jedis, key);
        if (jsonObject.length() == 0) {
            _message.setMessage("empty results");
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.BAD_REQUEST);
        }
        String log = "getListCache called \n";
        queryService.addLogging("GET", log);
        return new ResponseEntity<Object>(new String(jsonObject.toString().getBytes(), StandardCharsets.UTF_8), __headers, HttpStatus.OK);

    }


    @PostMapping(value = "/redis/{connectionid}/sql_hash", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> saveSqlHash(@Valid @RequestBody RedisSql redisSql,
                                              @PathVariable("connectionid") String _connectionID,
                                              UriComponentsBuilder _ucBuilder,
                                              BindingResult bindingResult) {

//        System.out.println("call sql_hash !!!");
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/redis/" + _connectionID + "/sql_hash").buildAndExpand().toUri());
        __headers.add("Content-Type", "application/json; charset=utf-8");

        String queryid = "";
        queryid = queryService.makeQueryId(queryid);

        if (bindingResult.hasErrors()) {
            queryService.addLogging("POST", "Update : Some errors , Please check ur json content!" + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>("Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " for redis doesn't exist.");
            queryService.addLogging("POST", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }
        isConnectionFileExist = queryService.isConnectionIdFileExists(redisSql.getConnectionid());
        if (!isConnectionFileExist) {
            _message.setMessage(redisSql.getConnectionid() + " for DBMS doesn't exist.");
            queryService.addLogging("POST", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }


        Jedis jedis = queryServiceRedis.connectToRedis(_connectionID);

        if (jedis == null) {
            _message.setMessage("can't connect to Redis with Connection file " + _connectionID);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
        }

        String queryAfterConversion = queryService.convertGlobalVariable(queryService.getFormattedString(redisSql.getSql()));
        if (queryAfterConversion.contains("NOT RESOLVED")) {
            _message.setMessage(new String(queryAfterConversion.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.PAYMENT_REQUIRED);
        }

        String resultValidity = queryService.checkValidity(queryAfterConversion, redisSql.getConnectionid(), "", queryid, true);
        if (resultValidity.startsWith("Error") || resultValidity.startsWith("error")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", resultValidity + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.ACCEPTED);
        } else if (resultValidity.startsWith("maxRunningTimeOut")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", resultValidity + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.REQUEST_TIMEOUT);
        } else if (resultValidity.startsWith("MaxRows")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", resultValidity + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.PAYLOAD_TOO_LARGE);
        }

        String _response = new String(queryServiceJSON.structureJson(resultValidity, false, queryService.getColumnsDataType(queryAfterConversion, redisSql.getConnectionid()), queryid).getBytes(), StandardCharsets.UTF_8);

        String result = queryServiceRedis.saveResultSQLToHash(jedis, _response, redisSql.getKEY_NAME());
        if (result.length() > 0) {
            _message.setMessage(new String(result.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        }

        String log = "ConnectionID is: " + _connectionID + " , Redis Command is: Save Cache" +
                "\n\tQuery type is SQL HASH\tQuery is: " + queryAfterConversion + "\n\n";
        queryService.addLogging("POST", log);
        queryServiceRedis.disconnect(jedis);
        queryService.setRunStop(queryid);

        return new ResponseEntity<Object>("Cache Sql Hash Saved successfully", __headers, HttpStatus.OK);
    }


    @PostMapping(value = "/redis/{connectionid}/sql_string", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> saveSqlString(@Valid @RequestBody RedisSql redisSql,
                                                @PathVariable("connectionid") String _connectionID,
                                                UriComponentsBuilder _ucBuilder,
                                                BindingResult bindingResult) {

        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/redis/" + _connectionID + "/sql_string").buildAndExpand().toUri());
        __headers.add("Content-Type", "application/json; charset=utf-8");

        String queryid = "";
        queryid = queryService.makeQueryId(queryid);

        if (bindingResult.hasErrors()) {
            queryService.addLogging("POST", "Update : Some errors , Please check ur json content!" + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>("Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " for redis doesn't exist.");
            queryService.addLogging("POST", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }
        isConnectionFileExist = queryService.isConnectionIdFileExists(redisSql.getConnectionid());
        if (!isConnectionFileExist) {
            _message.setMessage(redisSql.getConnectionid() + " for DBMS doesn't exist.");
            queryService.addLogging("POST", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }

        Jedis jedis = queryServiceRedis.connectToRedis(_connectionID);

        if (jedis == null) {
            _message.setMessage("can't connect to Redis with Connection file " + _connectionID);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
        }

        String queryAfterConversion = queryService.convertGlobalVariable(queryService.getFormattedString(redisSql.getSql()));
        if (queryAfterConversion.contains("NOT RESOLVED")) {
            _message.setMessage(new String(queryAfterConversion.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.PAYMENT_REQUIRED);
        }

        String resultValidity = queryService.checkValidity(queryAfterConversion, redisSql.getConnectionid(), "", queryid, true);
        if (resultValidity.startsWith("Error") || resultValidity.startsWith("error")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", resultValidity + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.ACCEPTED);
        } else if (resultValidity.startsWith("maxRunningTimeOut")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", resultValidity + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.REQUEST_TIMEOUT);
        } else if (resultValidity.startsWith("MaxRows")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", resultValidity + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.PAYLOAD_TOO_LARGE);
        }

        String _response = new String(queryServiceJSON.structureJson(resultValidity, false, queryService.getColumnsDataType(queryAfterConversion, redisSql.getConnectionid()), queryid).getBytes(), StandardCharsets.UTF_8);

        String result = queryServiceRedis.saveResultSQLToString(jedis, _response, redisSql.getKEY_NAME());
        if (result.length() > 0) {
            _message.setMessage(new String(result.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        }

        String log = "ConnectionID is: " + _connectionID + " , Redis Command is: Save Cache" +
                "\n\tQuery type is SQL String\tQuery is: " + queryAfterConversion + "\n\n";
        queryService.addLogging("POST", log);
        queryServiceRedis.disconnect(jedis);
        queryService.setRunStop(queryid);

        return new ResponseEntity<Object>("Cache Sql String Saved successfully", __headers, HttpStatus.OK);
    }


}
