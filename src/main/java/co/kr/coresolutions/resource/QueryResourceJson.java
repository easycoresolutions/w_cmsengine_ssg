package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.ResponseCodes;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQueryJsonFacade;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.Message;
import co.kr.coresolutions.model.dtos.Chart4Dto;
import co.kr.coresolutions.model.dtos.QueryDto;
import co.kr.coresolutions.model.dtos.QueryTemplateDto;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceCSV;
import co.kr.coresolutions.service.QueryServiceFiles;
import co.kr.coresolutions.service.QueryServiceJSON;
import co.kr.coresolutions.service.QueryServiceSQLite;
import co.kr.coresolutions.service.QueryServiceSession;
import co.kr.coresolutions.util.Util;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Lists;
import liqp.Template;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@RestController
@Scope(value = "request")
@RequiredArgsConstructor
public class QueryResourceJson {
    private static final String OWNER = "owner";
    private final QueryService queryService;
    private final IQueryJsonFacade queryJsonFacade;
    private final QueryServiceJSON queryServiceJson;
    private final QueryServiceCSV queryServiceCSV;
    private final QueryServiceFiles queryServiceFile;
    private final QueryServiceSQLite queryServiceSQLite;
    private final QueryServiceSession queryServiceSession;
    private final IQuerySocketFacade querySocketFacade;
    private final ObjectMapper objectMapper;

    @PostMapping(value = "multiple/select_json", produces = "application/json;charset=utf-8")
    public ResponseEntity multipleQueries(@Valid @RequestBody List<QueryDto> queryDtoList) {
        return ResponseEntity.ok(queryJsonFacade.postForAll(queryDtoList));
    }

    @PostMapping(value = "multiple/exec_json", produces = "application/json;charset=utf-8")
    public ResponseEntity multipleQueriesExecJson(@Valid @RequestBody List<QueryTemplateDto> queryTemplateDtoList) {
        return ResponseEntity.ok(queryJsonFacade.postForAllExecJson(queryTemplateDtoList));
    }

    @RequestMapping(value = "/update/{connectionid}", method = RequestMethod.POST, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> updateSQL(@RequestBody ObjectNode json,
                                            @PathVariable("connectionid") String _connectionID,
                                            UriComponentsBuilder _ucBuilder, BindingResult result) {
//        System.out.println("Call updateSQL!!");
        String sql = json.get("sql").asText();
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/update/" + _connectionID).buildAndExpand().toUri());
        Message _message = Message.getInstance();

        if (result.hasErrors()) {
            queryService.addLogging("POST", "Update : Some errors , Please check ur json content!" + "\n");
            return new ResponseEntity<>("Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }

        ResponseEntity responseEntity = checkSession(json);
        if (responseEntity != null) {
            return responseEntity;
        }

        String query = queryService.convertGlobalVariable(queryService.getFormattedString(sql));
        if (query.contains("NOT RESOLVED")) {
            _message.setMessage(new String(query.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<>(_message, __headers, HttpStatus.PAYMENT_REQUIRED);
        }

        String resultValidity = queryService.checkValidityUpdateClauses(query, _connectionID);
        if (!resultValidity.startsWith("success")) {
            if (resultValidity.equalsIgnoreCase(_connectionID)) {
                queryService.addLogging("POST", resultValidity + " (information) doesn't exist." + "\n");
                return new ResponseEntity<>(resultValidity + " (information) doesn't exist.", __headers, HttpStatus.UNAUTHORIZED);
            } else {
                queryService.addLogging("POST", resultValidity + "\n");
                return new ResponseEntity<>(new String(resultValidity.getBytes(), StandardCharsets.UTF_8), __headers, HttpStatus.METHOD_NOT_ALLOWED);
            }
        }

        String log = "Update: ConnectionID is: " + _connectionID + "\n\tQuery Sql is: " + query + "\n\n";
        queryService.addLogging("POST", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<>("SQL update successfully. " + resultValidity, __headers, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getowners"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getOwners(UriComponentsBuilder _ucBuilder) {
//        System.out.println("Call getOwners!!");
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/getowners").buildAndExpand().toUri());
        String resultGetOwners = queryService.getOwners();
        if (resultGetOwners.equalsIgnoreCase("fail")) {
            queryService.addLogging("GET", "some issues from server\n");
            return new ResponseEntity<Object>("some issues from server", __headers, HttpStatus.GONE);
        }
        __headers.add("Content-Type", "application/json; charset=utf-8");
        String log = "getowners called \n";
        queryService.addLogging("GET", log);

        //return new ResponseEntity<Object>(new String(resultGetOwners.getBytes(), "UTF-8"), __headers, HttpStatus.OK);
        return new ResponseEntity<Object>(resultGetOwners, __headers, HttpStatus.OK);
    }

    private ResponseEntity checkSession(ObjectNode json) {
        if (!json.hasNonNull(OWNER) || !json.get(OWNER).isTextual()) {
            return ResponseEntity.status(ResponseCodes.GENERAL_ERROR).body("owner is Required");
        } else {
            if (json.get(OWNER).asText().equalsIgnoreCase("batchjob")) {
                return null;
            }
            ResponseDto responseDto = queryServiceSession.updateSession(json.get(OWNER).asText(), json.get("session_id"));
            Objects.requireNonNull(responseDto);
            if (responseDto.getSuccessCode() != ResponseCodes.OK_RESPONSE) {
                return ResponseEntity.status(responseDto.getErrorCode()).body(responseDto.getMessage());
            }
        }
        return null;
    }

    @RequestMapping(value = "/select_json/{connectionid}", method = RequestMethod.POST, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> postQueryJson(@RequestBody ObjectNode json,
                                                @PathVariable("connectionid") String _connectionID,
                                                UriComponentsBuilder _ucBuilder, BindingResult result) throws Exception {
        Message _message = Message.getInstance();
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/select_json/" + _connectionID).buildAndExpand().toUri());
        String partMessage = "select_json/" + _connectionID + " triggered, ";
        String sql = json.get("sql").asText();
        String userid = "";
        String queryid = "";

        ResponseEntity responseEntity = checkSession(json);
        if (responseEntity != null) {
            return responseEntity;
        }

        if (json.has("queryid")) {
            queryid = json.get("queryid").asText();
        }

        queryid = queryService.makeQueryId(queryid);

        if (json.has("userid")) {
            userid = json.get("userid").asText();
            if (!userid.equals("mss_scheduler")) {
                JSONObject jsonObject = QueryService.sessionMap.get(queryid);
                if (jsonObject != null) {
                    jsonObject.put("userid", userid);
                    QueryService.sessionMap.put(queryid, jsonObject);
                }
            }
        }
        sql = sql.replace("@@USERID@@", userid);
        if (json.has("userid") && !json.has("dbms") && _connectionID.toLowerCase().contains("text")) {
            userid = json.get("userid").asText();
            if (userid.length() > 0) {
                String fileName = (StringUtils.split(sql.substring(sql.toLowerCase().indexOf("from") + 4)))[0] + ".csv";

                boolean isFileExist = queryServiceFile.FileExist(userid, fileName);
                if (!isFileExist) {
                    _message.setMessage("File name " + fileName + " for UserId " + userid + " (information) doesn't exist.");
                    queryService.setRunStop(queryid);
                    queryService.addLogging("POST", partMessage + _message.getMessage());
                    querySocketFacade.send(SocketBodyDto.builder().userId(userid).key("SELECT_JSON").message(partMessage + _message.getMessage()).build());
                    return new ResponseEntity<>(_message, __headers, HttpStatus.NOT_FOUND);
                }
                boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
                if (!isConnectionFileExist) {
                    _message.setMessage(_connectionID + " (information) doesn't exist.");
                    queryService.addLogging("POST", partMessage + _message.getMessage());
                    querySocketFacade.send(SocketBodyDto.builder().userId(userid).key("SELECT_JSON").message(partMessage + _message.getMessage()).build());
                    queryService.setRunStop(queryid);
                    return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
                }
                String resultQueryCsvJdbc = queryService.FetchwithCsvJdbc(_connectionID, sql, queryid, userid);

                if (resultQueryCsvJdbc.startsWith("Error") || resultQueryCsvJdbc.startsWith("error")) {
                    _message.setMessage(new String(resultQueryCsvJdbc.getBytes(), StandardCharsets.UTF_8));
                    queryService.addLogging("POST", partMessage + _message.getMessage());
                    querySocketFacade.send(SocketBodyDto.builder().userId(userid).key("SELECT_JSON").message(partMessage + _message.getMessage()).build());
                    queryService.setRunStop(queryid);
                    return new ResponseEntity<Object>(_message, __headers, HttpStatus.PAYMENT_REQUIRED);
                }
                String log = "UserID is : " + userid + " , Query Format is: JSON" +
                        "\n\tQuery Sql is: " + sql + "\n\n";
                queryService.addLogging("POST", log);
                __headers.add("Content-Type", "application/json; charset=utf-8");

                System.out.println("send before time : " + Util.getCurrentTime());
                //String _response = new String(queryServiceJson.structureJson(resultQueryCsvJdbc, false).getBytes(), "UTF-8");
                String _response = queryServiceJson.structureJson(resultQueryCsvJdbc, false, new HashMap<>(), queryid);
//                System.out.println("ResponseEntity postQueryJson - textdriver !!");
                System.out.println("send start time : " + Util.getCurrentTime());
                queryService.setRunStop(queryid);
                queryService.addLogging("POST", partMessage + ", success");
                querySocketFacade.send(SocketBodyDto.builder().userId(userid).key("SELECT_JSON").message(partMessage + ", success").build());
                return new ResponseEntity<>(_response, __headers, HttpStatus.OK);
            }
        }

        if (result.hasErrors()) {
            _message.setMessage("Some errors , Please check ur json content!");
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userid).key("SELECT_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.BAD_REQUEST);
        }

        String query = queryService.convertGlobalVariable(queryService.convertGlobalDateTime(queryService.getFormattedString(sql)));
        if (query.contains("NOT RESOLVED")) {
            _message.setMessage(new String(query.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userid).key("SELECT_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.PAYMENT_REQUIRED);
        }

        boolean sqlite = false;

        String resultValidity = "";
        if (json.has("userid") && json.has("dbms")) {
            String dbms = json.get("dbms").asText();
            if (dbms.equalsIgnoreCase("sqlite")) {
                String userID = json.get("userid").asText();
                String dbFileName = queryServiceSQLite.getDBFileRepo(userID);
                if (dbFileName.equals(QueryServiceSQLite.fileRepo + " doesn't exist!") || dbFileName.equals("There is no active dbfile defined!") ||
                        dbFileName.startsWith("error : {")) {
                    queryService.addLogging("POST", partMessage + "fail due: " + dbFileName);
                    querySocketFacade.send(SocketBodyDto.builder().userId(userid).key("SELECT_JSON").message(partMessage + "fail due: " + dbFileName).build());
                    queryService.setRunStop(queryid);
                    return new ResponseEntity<>("fail due: " + dbFileName, __headers, HttpStatus.BAD_REQUEST);
                }


                boolean isDBFileName = queryServiceSQLite.isDBFileNameExists(userID, dbFileName);
                if (!isDBFileName) {
                    _message.setMessage("db-file " + dbFileName + " doesn't exist.");
                    queryService.addLogging("POST", partMessage + _message.getMessage());
                    querySocketFacade.send(SocketBodyDto.builder().userId(userid).key("SELECT_JSON").message(partMessage + _message.getMessage()).build());
                    queryService.setRunStop(queryid);
                    return new ResponseEntity<>(_message.getMessage(), __headers, HttpStatus.BAD_REQUEST);
                }
                sqlite = true;
                synchronized (QueryServiceSQLite.DBFILE) {
                    synchronized (QueryServiceSQLite.SQLITE_USERID) {
                        QueryServiceSQLite.SQLITE_USERID = userID;
                        QueryServiceSQLite.DBFILE = dbFileName + ".db";
                    }
                }
                resultValidity = queryService.checkValidity(query, _connectionID, queryServiceSQLite.getDBFile(userID, dbFileName), queryid, false);
                synchronized (QueryServiceSQLite.DBFILE) {
                    synchronized (QueryServiceSQLite.SQLITE_USERID) {
                        QueryServiceSQLite.SQLITE_USERID = "mss";
                        QueryServiceSQLite.DBFILE = "mydbfile.db";
                    }
                }
            }
        }

        if (!sqlite) {
            resultValidity = queryService.checkValidity(query, _connectionID, "", queryid, true);
        }

        if (resultValidity.equalsIgnoreCase(_connectionID)) {
            _message.setMessage(resultValidity + " (information) doesn't exist.");
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userid).key("SELECT_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAUTHORIZED);
        } else if (resultValidity.startsWith("Error") || resultValidity.startsWith("error")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userid).key("SELECT_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.PAYMENT_REQUIRED);
        } else if (resultValidity.startsWith("maxRunningTimeOut")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userid).key("SELECT_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.REQUEST_TIMEOUT);
        } else if (resultValidity.startsWith("MaxRows")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userid).key("SELECT_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.PAYLOAD_TOO_LARGE);
        }

        String log = "ConnectionID is: " + _connectionID + " , Query Format is: JSON"
                + "\n\tQuery Sql is: " + query + "\n\n";
        String _response = "";
        __headers.add("Content-Type", "application/json; charset=utf-8");

        if (!userid.equals("mss_scheduler")) {
            _response = queryServiceJson.structureJson(resultValidity, false, queryService.getQueryDataTypes(_connectionID, query, false), queryid);
        } else {
            _response = resultValidity;
        }

        if (_response.startsWith("Error") || _response.startsWith("error")) {
            _message.setMessage(new String(_response.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userid).key("SELECT_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.PAYMENT_REQUIRED);
        }

        queryService.setRunStop(queryid);
//        System.out.println("ResponseEntity postQueryJson!!");
        System.out.println(queryid + "/send start time : " + Util.getCurrentTime());
        queryService.addLogging("POST", partMessage + log);
        querySocketFacade.send(SocketBodyDto.builder().userId(userid).key("SELECT_JSON").message(partMessage + log).build());
        return new ResponseEntity<>(_response, __headers, HttpStatus.OK);
    }

    @GetMapping(value = {"/exec_json/{connectionid}/{query_id}", "/exec_json2/{connectionid}/{query_id}"}, produces = "application/json; charset=utf-8")
    public ResponseEntity<Object> getJsonQuery(HttpServletRequest request,
                                               @PathVariable("connectionid") String _connectionID,
                                               @PathVariable("query_id") String _query_id) throws IOException {
        String path = request.getServletPath();
        boolean isExecJson2 = false;
        String partMessage;
        if (path.startsWith("/exec_json2")) {
            partMessage = "exec_json2/" + _connectionID + "/" + _query_id + " triggered, ";
            isExecJson2 = true;
        } else {
            partMessage = "exec_json/" + _connectionID + "/" + _query_id + " triggered, ";
        }
        Message _message = Message.getInstance();

        String queryid = queryService.makeQueryId("");

        boolean isQueryFileExists = queryServiceFile.isQueryFileExist(_query_id);

        if (!isQueryFileExists) {
            _message.setMessage(_query_id + " (information) doesn't exist.");
            queryService.addLogging("GET", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId("").key("EXEC_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(_message);
        }

        String queryFromFile = queryServiceFile.fetchQueryFromFile(_query_id);

        String query = queryService.convertGlobalVariable(queryService.convertGlobalDateTime(queryService.getFormattedString(queryFromFile)));
        if (query.contains("NOT RESOLVED")) {
            _message.setMessage(new String(query.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("GET", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId("").key("EXEC_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.PAYMENT_REQUIRED).body(_message);
        }

        String resultValidity = queryService.checkValidity(query, _connectionID, "", queryid, true);
        if (resultValidity.equalsIgnoreCase(_connectionID)) {
            _message.setMessage(resultValidity + " (information) doesn't exist.");
            queryService.addLogging("GET", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId("").key("EXEC_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(_message);
        } else if (resultValidity.startsWith("Error") || resultValidity.startsWith("error")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("GET", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId("").key("EXEC_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.PAYMENT_REQUIRED).body(_message);
        } else if (resultValidity.startsWith("maxRunningTimeOut")) {
            _message.setMessage(resultValidity);
            queryService.addLogging("GET", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId("").key("EXEC_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.REQUEST_TIMEOUT).body(_message);
        } else if (resultValidity.startsWith("MaxRows")) {
            _message.setMessage(resultValidity);
            queryService.addLogging("GET", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId("").key("EXEC_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.PAYLOAD_TOO_LARGE).body(_message);
        }
        String log = "ConnectionID is: " + _connectionID + " , Query Format is: json" + ", Query is: " + query + "\n";
        queryService.addLogging("GET", partMessage + log);
        querySocketFacade.send(SocketBodyDto.builder().userId("").key("EXEC_JSON").message(partMessage + log).build());
        String _response = isExecJson2 ? resultValidity : new String
                (queryServiceJson.structureJson(resultValidity, false,
                        queryService.getQueryDataTypes(_connectionID, query, false), queryid).getBytes(), StandardCharsets.UTF_8);
        queryService.setRunStop(queryid);
        return ResponseEntity.status(HttpStatus.OK).body(_response);
    }

    //add paging

    @PostMapping(value = {"/exec_json/{connectionid}/{queryFileName}", "/exec_json2/{connectionid}/{queryFileName}"}, produces = "application/json; charset=utf-8")
    public ResponseEntity<Object> PostJsonQuery(HttpServletRequest request,
                                                @PathVariable("connectionid") String _connectionID,
                                                @PathVariable("queryFileName") String queryFileName,
                                                @RequestBody ObjectNode json
            , BindingResult result) throws IOException {
        String path = request.getServletPath();
        boolean isExecJson2 = false;
        String partMessage;
        if (path.startsWith("/exec_json2")) {
            partMessage = "exec_json2/" + _connectionID + "/" + queryFileName + " triggered, ";
            isExecJson2 = true;
        } else {
            partMessage = "exec_json/" + _connectionID + "/" + queryFileName + " triggered, ";
        }

        Message _message = Message.getInstance();

        String queryid = "";

        if (json.has("queryid")) {
            queryid = json.get("queryid").asText();
        }

        queryid = queryService.makeQueryId(queryid);

        String userId = "";
        if (json.has("userid")) {
            userId = json.get("userid").asText();
            JSONObject jsonObject = QueryService.sessionMap.get(queryid);
            if (jsonObject != null) {
                jsonObject.put("userid", userId);
                QueryService.sessionMap.put(queryid, jsonObject);
            }

        }

        if (result.hasErrors()) {
            _message.setMessage("Some errors , Please check ur json content!");
            queryService.addLogging("POST", "Some errors , Please check ur json content!" + "\n");
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(_message);
        }
        String dir = json.has("dir") && json.get("dir").isTextual() ? json.get("dir").asText() : "";

        boolean isQueryFileExists = queryServiceFile.isQueryFileExist((!dir.isEmpty() ? (dir + File.separator) : "") + queryFileName);

        if (!isQueryFileExists) {
            _message.setMessage(queryFileName + " (information) doesn't exist.");
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("EXEC_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(_message);
        }

        String query = queryServiceFile.fetchQueryFromFile((!dir.isEmpty() ? (dir + File.separator) : "") + queryFileName);
        String queryafterkeyreplace = queryService.getFormattedQuery(query, json);

        if (!userId.isEmpty()) {
            String fileName = (StringUtils.split(queryafterkeyreplace.substring(queryafterkeyreplace.indexOf("from") + 4)))[0] + ".csv";
            boolean isFileExist = queryServiceFile.FileExist(userId, fileName);

            if (!isFileExist) {
                _message.setMessage("File name " + fileName + " for UserId " + userId + " (information) doesn't exist.");
                queryService.addLogging("POST", partMessage + _message.getMessage());
                querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("EXEC_JSON").message(partMessage + _message.getMessage()).build());
                queryService.setRunStop(queryid);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(_message);
            }
            boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
            if (!isConnectionFileExist) {
                _message.setMessage(_connectionID + " (information) doesn't exist.");
                queryService.addLogging("POST", partMessage + _message.getMessage());
                querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("EXEC_JSON").message(partMessage + _message.getMessage()).build());
                queryService.setRunStop(queryid);
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(_message);
            }
            String resultQueryCsvJdbc = queryService.FetchwithCsvJdbc(_connectionID, queryafterkeyreplace, queryid, userId);
            String log = "ConnectionID is: " + _connectionID + " , Query Format is: json  " +
                    "\n\tQuery Sql after key Replace is: " + queryafterkeyreplace + "\n\n";
            queryService.addLogging("POST", partMessage + log);
            querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("EXEC_JSON").message(partMessage + log).build());
            String _response = isExecJson2 ? resultQueryCsvJdbc : new String(queryServiceJson.structureJson(resultQueryCsvJdbc, false, new HashMap<>(), queryid).getBytes(), StandardCharsets.UTF_8);
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.OK).body(_response);
        }

        String queryAfterConversion = queryService.convertGlobalDateTime(queryService.convertGlobalVariable(queryafterkeyreplace));
        if (queryAfterConversion.contains("NOT RESOLVED")) {
            _message.setMessage(new String(queryAfterConversion.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("EXEC_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.PAYMENT_REQUIRED).body(_message);
        }

        String resultValidity = queryService.checkValidity(queryAfterConversion, _connectionID, "", queryid, true);
        if (resultValidity.equalsIgnoreCase(_connectionID)) {
            _message.setMessage(resultValidity + " (information) doesn't exist.");
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("EXEC_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(_message);
        } else if (resultValidity.startsWith("Error") || resultValidity.startsWith("error")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("EXEC_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.PAYMENT_REQUIRED).body(_message);
        } else if (resultValidity.startsWith("maxRunningTimeOut")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("EXEC_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.REQUEST_TIMEOUT).body(_message);
        } else if (resultValidity.startsWith("MaxRows")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("EXEC_JSON").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.PAYLOAD_TOO_LARGE).body(_message);
        }

        String log = "ConnectionID is: " + _connectionID + " , Query Format is: json "
                + "\n\tQuery Sql after key Replace and conversion is: " + queryAfterConversion + "\n\n";
        queryService.addLogging("POST", partMessage + log);
        querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("EXEC_JSON").message(partMessage + log).build());
        String _response = isExecJson2 ? resultValidity : new String(queryServiceJson.structureJson(resultValidity, false,
                queryService.getQueryDataTypes(_connectionID, queryAfterConversion, false), queryid).getBytes(), StandardCharsets.UTF_8);
        queryService.setRunStop(queryid);
        return ResponseEntity.status(HttpStatus.OK).body(_response);
    }


    @RequestMapping(value = "/{owner_id}/select_text", method = RequestMethod.POST, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> postQueryJsonFromCSV(@RequestBody ObjectNode json,
                                                       @PathVariable("owner_id") String _ownerId,
                                                       UriComponentsBuilder _ucBuilder, BindingResult result) throws Exception {
//        System.out.println("Call postQueryJsonFromCSV!!");
        Message _message = Message.getInstance();
        String sql = json.get("sql").asText().toLowerCase();
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path(_ownerId + "/select_text").buildAndExpand().toUri());

        String queryid = "";
        if (json.has("queryid")) {
            queryid = json.get("queryid").asText();
        }

        queryid = queryService.makeQueryId(queryid);

        JSONObject jsonObject = QueryService.sessionMap.get(queryid);
        if (jsonObject != null) {
            jsonObject.put("userid", _ownerId);
            QueryService.sessionMap.put(queryid, jsonObject);
        }

        if (result.hasErrors() || !sql.contains("from")) {
            _message.setMessage("Some errors , Please check ur json content!");
            queryService.addLogging("POST", "Some errors , Please check ur json content!" + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.BAD_REQUEST);
        }

        String fileName = (StringUtils.split(sql.substring(sql.toLowerCase().indexOf("from") + 4)))[0] + ".csv";
        boolean isFileExist = queryServiceFile.FileExist(_ownerId, fileName);
        if (!isFileExist) {
            _message.setMessage("File name " + fileName + " for UserId " + _ownerId + " (information) doesn't exist.");
            queryService.addLogging("POST", "File name " + sql + " for UserId " + _ownerId + " (information) doesn't exist." + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.NOT_FOUND);
        }
        String log = "UserID is : " + _ownerId + " , Query Format is: JSON" +
                "\n\tQuery Sql is: " + sql + "\n\n";
        queryService.addLogging("POST", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        String resultQueryCsvJdbc = queryService.FetchwithCsvJdbc("TEXT", sql, queryid, _ownerId);
//        System.out.println("ResponseEntity postQueryJsonFromCSV!!");
        String _response = new String(queryServiceJson.structureJson(resultQueryCsvJdbc, false, new HashMap<>(), queryid).getBytes(), StandardCharsets.UTF_8);
        queryService.setRunStop(queryid);
        return new ResponseEntity<Object>(_response, __headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/select_json_stop/{connectionid}", method = RequestMethod.POST, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> postQueryJsonStop(@RequestBody ObjectNode json,
                                                    @PathVariable("connectionid") String _connectionID,
                                                    UriComponentsBuilder _ucBuilder) {
        Message _message = Message.getInstance();
        String queryid = "";

        String _response = "";
        HttpHeaders __headers = new HttpHeaders();
        if (json.has("queryid")) {
            queryid = json.get("queryid").asText();
            if (queryid == null || queryid.equals("")) {
                _message.setMessage("queryid is empty!!!");
//                System.out.println("queryid is empty!!");
                __headers.add("Content-Type", "application/json; charset=utf-8");
                return new ResponseEntity<Object>(_message, __headers, HttpStatus.BAD_REQUEST);
            } else if (!queryService.ExistQueryId(queryid)) {
                _message.setMessage("[" + queryid + "] is not runnging!!!");
//                System.out.println("[" + queryid + "] is not runnging!!");
                __headers.add("Content-Type", "application/json; charset=utf-8");
                return new ResponseEntity<Object>(_message, __headers, HttpStatus.BAD_REQUEST);
            }
        } else {
            _message.setMessage("No [queryid] element!!!");
//            System.out.println("No [queryid] element!!");
            __headers.add("Content-Type", "application/json; charset=utf-8");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.BAD_REQUEST);
        }

//        System.out.println("stop queryid : [" + queryid + "]");
        __headers.setLocation(_ucBuilder.path("/select_json_stop" + _connectionID).buildAndExpand().toUri());

        String log = "stop queryid is: " + queryid + "\n\n";
        queryService.addLogging("POST", log);
        if (_connectionID.toLowerCase().contains("clickhouse") || _connectionID.toLowerCase().contains("sqlite")) {
//            System.out.println("query Stop CSV!!");
            _response = queryService.executeQueryStop(queryid);
            __headers.add("Content-Type", "application/json; charset=utf-8");
            return new ResponseEntity<Object>(_response, __headers, HttpStatus.OK);
        }
        _response = queryService.executeQueryStop(queryid);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(_response, __headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/exec_query_list", method = RequestMethod.GET)
    public ResponseEntity<Object> getExecQueryList(UriComponentsBuilder _ucBuilder) {
//        System.out.println("Call getExecQueryList!!");
        Message _message = Message.getInstance();

        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/select_json_list").buildAndExpand().toUri());

        String _response = "";
        _response = queryService.getExecQueryList();

        if (_response.equals("")) {
            _message.setMessage("no exec list");
            queryService.addLogging("GET", _message + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.OK);
        }

        String log = "get queryList \n\n";
        queryService.addLogging("GET", log);

        //_response = queryService.getExecuteQueryList();
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(_response, __headers, HttpStatus.OK);
    }
}
