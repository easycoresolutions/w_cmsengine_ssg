package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryPollingFacade;
import co.kr.coresolutions.model.dtos.PollingDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Slf4j
public class QueryResourcePolling {
    private final IQueryPollingFacade queryPoolingFacade;

    @PostMapping("dbpolling")
    public ResponseEntity<CustomResponseDto> pooling(@Valid @RequestBody PollingDto pollingDto) {
        return ResponseEntity.ok(queryPoolingFacade.pooling(pollingDto));
    }
}
