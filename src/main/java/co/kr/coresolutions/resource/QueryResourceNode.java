package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryNodeFacade;
import co.kr.coresolutions.model.dtos.NodeDto;
import co.kr.coresolutions.util.Util;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Slf4j
public class QueryResourceNode {
    private final IQueryNodeFacade queryNodeFacade;

    @PostMapping("execNodes/{campId}/{stepId}")
    public ResponseEntity<CustomResponseDto> execNodes(@PathVariable("campId") String campId,
                                                       @PathVariable("stepId") String stepId,
                                                       @Valid @RequestBody NodeDto nodeDto, HttpServletRequest request) {
        CustomResponseDto customResponseDto = queryNodeFacade.execNodes(campId, stepId, nodeDto, Util.getBaseUrl(request));
        customResponseDto.setCount(null);
        return ResponseEntity.ok(customResponseDto);
    }
}
