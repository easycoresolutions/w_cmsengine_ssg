package co.kr.coresolutions.resource;

import co.kr.coresolutions.model.Message;
import co.kr.coresolutions.model.RModel;
import co.kr.coresolutions.model.RModelActivated;
import co.kr.coresolutions.model.RModelData;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceRServe;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@Scope(value = "request")
@RequiredArgsConstructor
public class QueryResourceRServe {
    private final QueryService queryService;
    private final QueryServiceRServe queryServiceRServe;

    /********************************EXECUTE SHEET****************************************************************************/
//    @Deprecated
//    @PostMapping(value = "/R/{connectionid}/openconnection", consumes = "*/*;charset=UTF-8")
//    public ResponseEntity<Object> openConnection(@RequestBody JSONObject jsonBody,
//                                                 @PathVariable("connectionid") String _connectionID,
//                                                 UriComponentsBuilder _ucBuilder, BindingResult bindingResult) throws IOException {
//        HttpHeaders __headers = new HttpHeaders();
//        Message _message = Message.getInstance();
//        __headers.setLocation(_ucBuilder.path("/R/" + _connectionID + "/openconnection").buildAndExpand().toUri());
//        __headers.add("Content-Type", "application/json; charset=utf-8");
//        if (!jsonBody.containsKey("userid") || bindingResult.hasErrors()) {
//            _message.setMessage("openConnection : Some errors , Please check ur json content!" + "\n");
//            queryService.addLogging("POST", _message.getMessage());
//            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.BAD_REQUEST);
//        }
//        //todo uncomment
//        /*if(jsonBody.get("userid").toString().length()==0){
//            _message.setMessage("openConnection : Some errors , userid must be not empty!"+"\n");
//            queryService.addLogging("POST", _message.getMessage());
//            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.FAILED_DEPENDENCY);
//        }*/
//
//        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
//        if (!isConnectionFileExist) {
//            _message.setMessage(_connectionID + " (information) doesn't exist.");
//            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
//            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
//        }
//
//        String result = queryServiceRServe.openConnection(_connectionID, jsonBody.get("userid").toString());
//        if (result.startsWith("error")) {
//            _message.setMessage(new String(result.getBytes(), StandardCharsets.UTF_8));
//            queryService.addLogging("POST", result + "\n");
//            return new ResponseEntity<Object>(_message, __headers, HttpStatus.NOT_IMPLEMENTED);
//        }
//        String log = "openConnection with R server successfully";
//        queryService.addLogging("POST", log + "\n");
//        return new ResponseEntity<Object>(log, __headers, HttpStatus.OK);
//    }
//
//
//    @PostMapping(value = "/R/{connectionid}/closeconnection", consumes = "*/*;charset=UTF-8")
//    @Deprecated
//    public ResponseEntity<Object> closeConnection(@RequestBody JSONObject jsonBody,
//                                                  @PathVariable("connectionid") String _connectionID,
//                                                  UriComponentsBuilder _ucBuilder, BindingResult bindingResult) throws IOException {
//        HttpHeaders __headers = new HttpHeaders();
//        Message _message = Message.getInstance();
//        __headers.setLocation(_ucBuilder.path("/R/" + _connectionID + "/closeconnection").buildAndExpand().toUri());
//        __headers.add("Content-Type", "application/json; charset=utf-8");
//        if (!jsonBody.containsKey("userid") || bindingResult.hasErrors()) {
//            _message.setMessage("openConnection : Some errors , Please check ur json content!" + "\n");
//            queryService.addLogging("POST", _message.getMessage());
//            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.BAD_REQUEST);
//        }
//        //todo uncomment
//        /*
//        if(jsonBody.get("userid").toString().length()==0){
//            _message.setMessage("closeConnection : Some errors , userid must be not empty!"+"\n");
//            queryService.addLogging("POST", _message.getMessage());
//            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.FAILED_DEPENDENCY);
//        }*/
//        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
//        if (!isConnectionFileExist) {
//            _message.setMessage(_connectionID + " (information) doesn't exist.");
//            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
//            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
//        }
//        String result = queryServiceRServe.closeConnection(jsonBody.get("userid").toString());
//        if (result.startsWith("error")) {
//            _message.setMessage(new String(result.getBytes(), StandardCharsets.UTF_8));
//            queryService.addLogging("POST", result + "\n");
//            return new ResponseEntity<Object>(_message, __headers, HttpStatus.NOT_IMPLEMENTED);
//        }
//        String log = "closeConnection with R server successfully";
//        queryService.addLogging("POST", log + "\n");
//        return new ResponseEntity<Object>(log, __headers, HttpStatus.OK);
//    }



    @PostMapping(value = "/R/{connectionid}/evaluate", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> evaluate(@RequestBody JsonNode jsonBody,
                                           @PathVariable("connectionid") String _connectionID,
                                           UriComponentsBuilder _ucBuilder, BindingResult bindingResult) {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/R/" + _connectionID + "/evaluate").buildAndExpand().toUri());
        __headers.add("Content-Type", "text/plain; charset=utf-8");
        if (!jsonBody.has("userid") || !jsonBody.has("script") || bindingResult.hasErrors()) {
            _message.setMessage("evaluate : Some errors , Please check ur json content!" + "\n");
            queryService.addLogging("POST", _message.getMessage());
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.BAD_REQUEST);
        }

        String userid = jsonBody.get("userid").asText();
        //todo uncomment
        /*if(userid.length()==0){
            _message.setMessage("evaluate : Some errors , userid must be not empty!"+"\n");
            queryService.addLogging("POST", _message.getMessage());
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.FAILED_DEPENDENCY);
        }*/

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        }
        String result = new String(queryServiceRServe.evaluate(userid, jsonBody.get("script").asText()).getBytes(), StandardCharsets.UTF_8);
        if (result.startsWith("error")) {
            _message.setMessage(result);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.NOT_IMPLEMENTED);
        }
        String log = "evaluate Script for userid " + userid + " to R server successfully";
        queryService.addLogging("POST", log + "\n");
        return new ResponseEntity<Object>(result, __headers, HttpStatus.OK);
    }

    @PostMapping(value = "/R/{connectionid}/import", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> importFile(@RequestBody JsonNode jsonBody,
                                             @PathVariable("connectionid") String _connectionID,
                                             UriComponentsBuilder _ucBuilder, BindingResult bindingResult) {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/R/" + _connectionID + "/import").buildAndExpand().toUri());
        __headers.add("Content-Type", "text/plain; charset=utf-8");
        if (!jsonBody.has("userid") || !jsonBody.has("file") || bindingResult.hasErrors()) {
            _message.setMessage("import : Some errors , Please check ur json content!" + "\n");
            queryService.addLogging("POST", _message.getMessage());
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.BAD_REQUEST);
        }
        String userid = jsonBody.get("userid").asText();
        //todo uncomment
        /*if (userid.length() == 0) {
            _message.setMessage("import : Some errors , userid must be not empty!" + "\n");
            queryService.addLogging("POST", _message.getMessage());
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.FAILED_DEPENDENCY);
        }*/

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        }
        String result = new String(queryServiceRServe.importFile(userid, jsonBody.get("file").asText()).getBytes(), StandardCharsets.UTF_8);
        if (result.startsWith("error")) {
            _message.setMessage(result);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.NOT_IMPLEMENTED);
        }
        String log = "import Script for userid " + userid + " and execute via Rserve successfully";
        queryService.addLogging("POST", log + "\n");
        return new ResponseEntity<Object>(result, __headers, HttpStatus.OK);

    }

    @PostMapping(value = "/R/{connectionid}/getimage", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> getimage(@RequestBody ObjectNode jsonBody,
                                           @PathVariable("connectionid") String _connectionID,
                                           UriComponentsBuilder _ucBuilder, BindingResult bindingResult) {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/R/" + _connectionID + "/getimage").buildAndExpand().toUri());
        __headers.add("Content-Type", "text/plain; charset=utf-8");
        if (!jsonBody.has("userid") || !jsonBody.has("images") || bindingResult.hasErrors()) {
            _message.setMessage("getimage : Some errors , Please check ur json content!" + "\n");
            queryService.addLogging("POST", _message.getMessage());
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.BAD_REQUEST);
        }
        String userid = jsonBody.get("userid").asText();
        //todo uncomment
        /*if (userid.length() == 0) {
            _message.setMessage("import : Some errors , userid must be not empty!" + "\n");
            queryService.addLogging("POST", _message.getMessage());
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.FAILED_DEPENDENCY);
        }*/

        if (!jsonBody.get("images").isArray()) {
            _message.setMessage("getimage : Some errors , images values must be an array" + "\n");
            queryService.addLogging("POST", _message.getMessage());
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.BAD_REQUEST);
        }

        Set<String> images = new HashSet<>();
        jsonBody.get("images").elements().forEachRemaining(jsonNode -> {
            images.add(jsonNode.asText());
        });

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        }

        List<String> result = queryServiceRServe.getImages(userid, images);

        if (result.size() > 0)
            if (result.get(0).startsWith("error")) {
                _message.setMessage(result.get(0));
                queryService.addLogging("POST", _message.getMessage() + "\n");
                return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.NOT_IMPLEMENTED);
            }

        String log = "getimages Script for userid " + userid + " execute via Rserve successfully";
        queryService.addLogging("POST", log + "\n");
        return new ResponseEntity<Object>(result, __headers, HttpStatus.OK);

    }


    /******************************************MODEL SHEET******************************************************/
    @PostMapping(value = "/R/{connectionid}/modelupload/{modelid}", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> modelupload(@Valid @RequestBody RModel rModel,
                                              @PathVariable("connectionid") String _connectionID,
                                              @PathVariable("modelid") String modelid,
                                              UriComponentsBuilder _ucBuilder, BindingResult bindingResult) {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/R/" + _connectionID + "/modelupload").buildAndExpand().toUri());
        __headers.add("Content-Type", "text/plain; charset=utf-8");


        if (bindingResult.hasErrors()) {
            _message.setMessage("model upload : Some errors , Please check ur json content!" + "\n");
            queryService.addLogging("POST", _message.getMessage());
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.BAD_REQUEST);
        }
        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.NOT_IMPLEMENTED);
        }
        String result = new String(queryServiceRServe.uploadFile(_connectionID, rModel, modelid).getBytes(), StandardCharsets.UTF_8);
        if (result.startsWith("error")) {
            _message.setMessage(result);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.NOT_IMPLEMENTED);
        }

        String log = "modelupload successfully";
        queryService.addLogging("POST", log + "\n");
        return new ResponseEntity<Object>(result, __headers, HttpStatus.OK);
    }

    @GetMapping(value = "/R/{connectionid}/models")
    public ResponseEntity<Object> getModelsIndexes(@PathVariable("connectionid") String _connectionID,
                                                   UriComponentsBuilder _ucBuilder) {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.add("Content-Type", "application/json; charset=utf-8");
        __headers.setLocation(_ucBuilder.path("/R/" + _connectionID + "/models").buildAndExpand().toUri());

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
        }
        String resultgetModels = queryServiceRServe.getModels();

        if (resultgetModels.startsWith("error")) {
            queryService.addLogging("GET", resultgetModels + "\n");
            return new ResponseEntity<Object>(resultgetModels, __headers, HttpStatus.NOT_IMPLEMENTED);
        }
        String log = " getModels called with connectionID " + _connectionID + "\n";
        queryService.addLogging("GET", log);
        return new ResponseEntity<Object>(new String(resultgetModels.getBytes(), StandardCharsets.UTF_8), __headers, HttpStatus.OK);
    }


    @DeleteMapping(value = "/R/{connectionid}/{userid}/{modelid}", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> deleteModel(@PathVariable(value = "connectionid") String connectionid,
                                              @PathVariable(value = "modelid") String modelid,
                                              @PathVariable(value = "userid") String userid,
                                              UriComponentsBuilder _ucBuilder) throws Exception {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/R/" + connectionid + "/" + modelid).buildAndExpand().toUri());
        __headers.add("Content-Type", "text/plain; charset=utf-8");
        Message _message = Message.getInstance();

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(connectionid);
        if (!isConnectionFileExist) {
            _message.setMessage(connectionid + " (information) doesn't exist.");
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
        }

        boolean resultDelete = queryServiceRServe.deleteModel(userid, modelid);

        if (!resultDelete) {
            queryService.addLogging("DELETE", modelid + " doesn't exist.\n");
            return new ResponseEntity<Object>(modelid + " doesn't exist.", __headers, HttpStatus.NOT_IMPLEMENTED);
        }

        String log = "deleteModel: " + modelid;
        queryService.addLogging("DELETE", log);
        return new ResponseEntity<Object>(modelid + " Deleted successfully", __headers, HttpStatus.OK);
    }


    @PostMapping(value = "/R/{connectionid}/activate", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> activate(@Valid @RequestBody RModelActivated rModelActivated,
                                           @PathVariable("connectionid") String _connectionID,
                                           UriComponentsBuilder _ucBuilder, BindingResult bindingResult) {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/R/" + _connectionID + "/activate").buildAndExpand().toUri());
        __headers.add("Content-Type", "text/plain; charset=utf-8");

        if (bindingResult.hasErrors()) {
            _message.setMessage("activate : Some errors , Please check ur json content!" + "\n");
            queryService.addLogging("POST", _message.getMessage());
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.BAD_REQUEST);
        }

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.NOT_IMPLEMENTED);
        }

        String result = new String(queryServiceRServe.activate(rModelActivated).getBytes(), StandardCharsets.UTF_8);
        if (result.startsWith("error")) {
            _message.setMessage(result);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.NOT_IMPLEMENTED);
        }
        String log = "activate executed successfully";
        queryService.addLogging("POST", log + "\n");
        return new ResponseEntity<Object>(result, __headers, HttpStatus.OK);
    }


    @DeleteMapping(value = "/R/{connectionid}/deactivate/{modelid}", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> deActivate(@PathVariable(value = "connectionid") String connectionid,
                                             @PathVariable(value = "modelid") String modelid,
                                             UriComponentsBuilder _ucBuilder) {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/R/" + connectionid + "/deactivate" + modelid).buildAndExpand().toUri());
        __headers.add("Content-Type", "text/plain; charset=utf-8");
        Message _message = Message.getInstance();

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(connectionid);
        if (!isConnectionFileExist) {
            _message.setMessage(connectionid + " (information) doesn't exist.");
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
        }

        boolean resultDelete = queryServiceRServe.deActivate(modelid);

        if (!resultDelete) {
            queryService.addLogging("DELETE", modelid + " doesn't Found.\n");
            return new ResponseEntity<Object>(modelid + " doesn't Found.", __headers, HttpStatus.NOT_IMPLEMENTED);
        }

        String log = "deActivate: " + modelid;
        queryService.addLogging("DELETE", log);
        return new ResponseEntity<Object>(modelid + " Deactivated successfully", __headers, HttpStatus.OK);
    }


    @GetMapping(value = "/R/{connectionid}/activates")
    public ResponseEntity<Object> Activates(@PathVariable("connectionid") String _connectionID,
                                            UriComponentsBuilder _ucBuilder) {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.add("Content-Type", "application/json; charset=utf-8");
        __headers.setLocation(_ucBuilder.path("/R/" + _connectionID + "/Activates").buildAndExpand().toUri());

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAUTHORIZED);
        }
        String resultActivates = new String(queryServiceRServe.getActivates().getBytes(), StandardCharsets.UTF_8);

        if (resultActivates.startsWith("error")) {
            queryService.addLogging("GET", resultActivates + "\n");
            return new ResponseEntity<Object>(resultActivates, __headers, HttpStatus.NOT_IMPLEMENTED);
        }
        String log = " activates called with connectionID " + _connectionID + "\n";
        queryService.addLogging("GET", log);
        return new ResponseEntity<Object>(resultActivates, __headers, HttpStatus.OK);
    }

    @PostMapping("/Rclose/{userid}")
    public ResponseEntity Rclose(@PathVariable("userid") String userID) {
        return ResponseEntity.ok(queryServiceRServe.closeConnection(userID));
    }

    /******************************************API SHEET********************************************************************/

    @PostMapping(value = "/Rconnect/{connectionid}/{userid}", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> Rconnect(@PathVariable("userid") String userid,
                                           @PathVariable("connectionid") String _connectionID,
                                           UriComponentsBuilder _ucBuilder) {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/Rconnect/" + _connectionID + "/" + userid).buildAndExpand().toUri());
        __headers.add("Content-Type", "application/json; charset=utf-8");

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        }

        String result = queryServiceRServe.openConnection(_connectionID, userid);
        if (result.startsWith("error")) {
            _message.setMessage(new String(result.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", result + "\n");
            return new ResponseEntity<Object>(_message, __headers, HttpStatus.NOT_IMPLEMENTED);
        }
        String log = "connect with R server successfully";
        queryService.addLogging("POST", log + "\n");
        return new ResponseEntity<Object>(log, __headers, HttpStatus.OK);
    }

    @PostMapping(value = "/Rscript/{connectionid}/{userid}", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> Rscript(@PathVariable("userid") String userid,
                                          @PathVariable("connectionid") String _connectionID,
                                          @RequestBody ObjectNode json,
                                          UriComponentsBuilder _ucBuilder, BindingResult bindingResult) {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/Rscript/" + _connectionID + "/" + userid).buildAndExpand().toUri());
        __headers.add("Content-Type", "text/plain; charset=utf-8");
        if (!json.has("script") || bindingResult.hasErrors()) {
            _message.setMessage("Rscript : Some errors , Please check ur json content!" + "\n");
            queryService.addLogging("POST", _message.getMessage());
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.BAD_REQUEST);
        }
        if (json.get("script").toString().length() == 0) {
            _message.setMessage("Rscript : Some errors , script must be not empty!" + "\n");
            queryService.addLogging("POST", _message.getMessage());
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.FAILED_DEPENDENCY);
        }

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        }

        String result = new String(queryServiceRServe.evaluate(userid, json.get("script").asText()).getBytes(), StandardCharsets.UTF_8);

        if (result.startsWith("error")) {
            _message.setMessage(result);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.NOT_IMPLEMENTED);
        }
        String log = "Rscript for userid " + userid + " evaluated with R server successfully";
        queryService.addLogging("POST", log + "\n");
        return new ResponseEntity<Object>(result, __headers, HttpStatus.OK);
    }

    /******************************************************************************************************/


    @PostMapping(value = "/R/{connectionid}/getdata/{userid}", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> getData(@Valid @RequestBody RModelData rModelData,
                                          @PathVariable("connectionid") String _connectionID,
                                          @PathVariable("userid") String userid,
                                          UriComponentsBuilder _ucBuilder, BindingResult bindingResult) {
        HttpHeaders __headers = new HttpHeaders();
        Message _message = Message.getInstance();
        __headers.setLocation(_ucBuilder.path("/R/" + _connectionID + "/getdata/" + userid).buildAndExpand().toUri());
        __headers.add("Content-Type", "application/json; charset=utf-8");

        if (bindingResult.hasErrors()) {
            _message.setMessage("getData : Some errors , Please check ur json content!" + "\n");
            queryService.addLogging("POST", _message.getMessage());
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.BAD_REQUEST);
        }

        boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
        if (!isConnectionFileExist) {
            _message.setMessage(_connectionID + " (information) doesn't exist.");
            queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.NOT_IMPLEMENTED);
        }

        String result = new String(queryServiceRServe.getData(rModelData, userid).getBytes(), StandardCharsets.UTF_8);
        if (result.startsWith("error")) {
            _message.setMessage(result);
            queryService.addLogging("POST", _message.getMessage() + "\n");
            return new ResponseEntity<Object>(_message.getMessage(), __headers, HttpStatus.NOT_IMPLEMENTED);
        }
        String log = "getData called successfully";
        queryService.addLogging("POST", log + "\n");
        return new ResponseEntity<Object>(result, __headers, HttpStatus.OK);
    }


}
