package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.ResponseCodes;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.Message;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceCSV;
import co.kr.coresolutions.service.QueryServiceFiles;
import co.kr.coresolutions.service.QueryServiceSQLite;
import co.kr.coresolutions.service.QueryServiceSession;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import static org.springframework.http.ResponseEntity.status;

@RestController
@Scope(value = "request")
@RequiredArgsConstructor
public class QueryResourceCsv {
    private static final String OWNER = "owner";
    private final QueryService queryService;
    private final QueryServiceCSV queryServiceCsv;
    private final QueryServiceFiles queryServiceFile;
    private final QueryServiceSQLite queryServiceSQLite;
    private final QueryServiceSession queryServiceSession;
    private final IQuerySocketFacade querySocketFacade;

    private ResponseEntity checkSession(ObjectNode json) {
        if (!json.hasNonNull(OWNER) || !json.get(OWNER).isTextual()) {
            return status(ResponseCodes.GENERAL_ERROR).body("owner is Required");
        } else {
            if (json.get(OWNER).asText().equalsIgnoreCase("batchjob")) {
                return null;
            }
            ResponseDto responseDto = queryServiceSession.updateSession(json.get(OWNER).asText(), json.get("session_id"));
            Objects.requireNonNull(responseDto);
            if (responseDto.getSuccessCode() != ResponseCodes.OK_RESPONSE) {
                return status(responseDto.getErrorCode()).body(responseDto.getMessage());
            }
        }
        return null;
    }

    @PostMapping(value = "/select_csv/{connectionid}", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> postQueryCsv(@RequestBody ObjectNode json,
                                               @PathVariable("connectionid") String _connectionID,
                                               UriComponentsBuilder _ucBuilder, BindingResult result) throws Exception {
        Message _message = Message.getInstance();

        String sql = json.get("sql").asText();
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/select_csv/" + _connectionID).buildAndExpand().toUri());
        String partMessage = "select_csv/" + _connectionID + " triggered, ";

//        System.out.println("Call PostQueryCsv !!!");
        String queryid = "";

        if (json.has("queryid")) {
            queryid = json.get("queryid").asText();
        }

        ResponseEntity responseEntity = checkSession(json);
        if (responseEntity != null) {
            return responseEntity;
        }

        queryid = queryService.makeQueryId(queryid);
        String userId = "";
        if (json.has("userid")) {
            userId = json.get("userid").asText();
            JSONObject jsonObject = QueryService.sessionMap.get(queryid);
            if (jsonObject != null) {
                jsonObject.put("userid", userId);
                QueryService.sessionMap.put(queryid, jsonObject);
            }
        }
        if (!userId.isEmpty() && !json.has("dbms")) {
            String fileName = (StringUtils.split(sql.substring(sql.toLowerCase().indexOf("from") + 4)))[0] + ".csv";

            boolean isFileExist = queryServiceFile.FileExist(userId, fileName);
            if (!isFileExist) {
                _message.setMessage("File name " + fileName + " for UserId " + userId + " (information) doesn't exist.");
                queryService.addLogging("POST", partMessage + _message.getMessage());
                querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("SELECT_CSV").message(partMessage + _message.getMessage()).build());
                queryService.setRunStop(queryid);
                return new ResponseEntity<>(_message, __headers, HttpStatus.NOT_FOUND);
            }
            boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
            if (!isConnectionFileExist) {
                _message.setMessage(_connectionID + " (information) doesn't exist.");
                queryService.addLogging("POST", partMessage + _message.getMessage());
                querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("SELECT_CSV").message(partMessage + _message.getMessage()).build());
                queryService.setRunStop(queryid);
                return new ResponseEntity<>(_message, __headers, HttpStatus.UNAUTHORIZED);
            }

            String resultQueryCsvJdbc = queryService.FetchwithCsvJdbc(_connectionID, sql, queryid, userId);
            String log = "ConnectionID is: " + _connectionID + " , Query Format is: CSV" + "\n\tQuery Sql is: " + sql + "\n\n";
            queryService.addLogging("POST", partMessage + log);
            querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("SELECT_CSV").message(partMessage + log).build());
            String _response;
            __headers.add("Content-Type", "text/plain; charset=utf-8");
            _response = new String(queryServiceCsv.structureCSV(resultQueryCsvJdbc).getBytes(), StandardCharsets.UTF_8);
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_response, __headers, HttpStatus.OK);
        }


        if (result.hasErrors()) {
            _message.setMessage("Some errors , Please check ur json content!");
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("SELECT_CSV").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.BAD_REQUEST);
        }

        String queryAfterConversion = queryService.convertGlobalVariable(queryService.convertGlobalDateTime(queryService.getFormattedString(sql)));

        if (queryAfterConversion.contains("NOT RESOLVED")) {
            _message.setMessage(new String(queryAfterConversion.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("SELECT_CSV").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.PAYMENT_REQUIRED);
        }
        String resultValidity = "";
        boolean sqlite = false;
        if (json.has("userid") && json.has("dbms")) {
            String dbms = json.get("dbms").asText();
            if (dbms.equalsIgnoreCase("sqlite")) {
                String userID = json.get("userid").asText();
                String dbFileName = queryServiceSQLite.getDBFileRepo(userID);
                if (dbFileName.equals(QueryServiceSQLite.fileRepo + " doesn't exist!") || dbFileName.equals("There is no active dbfile defined!") ||
                        dbFileName.startsWith("error : {")) {
                    queryService.addLogging("POST", partMessage + "fail due: " + dbFileName);
                    querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("SELECT_CSV").message(partMessage + "fail due: " + dbFileName).build());
                    queryService.setRunStop(queryid);
                    return new ResponseEntity<>("fail due: " + dbFileName, __headers, HttpStatus.BAD_REQUEST);
                }

                boolean isDBFileName = queryServiceSQLite.isDBFileNameExists(userID, dbFileName);
                if (!isDBFileName) {
                    _message.setMessage("db-file " + dbFileName + " doesn't exist.");
                    queryService.addLogging("POST", partMessage + _message.getMessage());
                    querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("SELECT_CSV").message(partMessage + _message.getMessage()).build());
                    queryService.setRunStop(queryid);
                    return new ResponseEntity<>(_message.getMessage(), __headers, HttpStatus.BAD_REQUEST);
                }

                sqlite = true;
                synchronized (QueryServiceSQLite.DBFILE) {
                    synchronized (QueryServiceSQLite.SQLITE_USERID) {
                        QueryServiceSQLite.SQLITE_USERID = userID;
                        QueryServiceSQLite.DBFILE = dbFileName + ".db";
                    }
                }
                resultValidity = queryService.checkValidity(queryAfterConversion, _connectionID,
                        userID + "/" + dbFileName + ".db", queryid, true);
                synchronized (QueryServiceSQLite.DBFILE) {
                    synchronized (QueryServiceSQLite.SQLITE_USERID) {
                        QueryServiceSQLite.SQLITE_USERID = "mss";
                        QueryServiceSQLite.DBFILE = "mydbfile.db";
                    }
                }
            }
        }

        if (!sqlite) {
            resultValidity = queryService.checkValidity(queryAfterConversion, _connectionID, "", queryid, true);
        }

        if (resultValidity.equalsIgnoreCase(_connectionID)) {
            _message.setMessage(resultValidity + " (information) doesn't exist.");
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("SELECT_CSV").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.UNAUTHORIZED);
        } else if (resultValidity.startsWith("Error") || resultValidity.startsWith("error")) {
            _message.setMessage((new String(resultValidity.getBytes(), StandardCharsets.UTF_8)));
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("SELECT_CSV").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.PAYMENT_REQUIRED);
        } else if (resultValidity.startsWith("maxRunningTimeOut")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("SELECT_CSV").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.REQUEST_TIMEOUT);
        } else if (resultValidity.startsWith("MaxRows")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", partMessage + _message.getMessage());
            querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("SELECT_CSV").message(partMessage + _message.getMessage()).build());
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.PAYLOAD_TOO_LARGE);
        }

        String log = "ConnectionID is: " + _connectionID + " , Query Format is: CSV" +
                "\n\tQuery Sql is: " + queryAfterConversion + "\n\n";
        queryService.addLogging("POST", partMessage + log);
        querySocketFacade.send(SocketBodyDto.builder().userId(userId).key("SELECT_CSV").message(partMessage + log).build());
        String _response;
        __headers.add("Content-Type", "text/plain; charset=utf-8");
        _response = new String(queryServiceCsv.structureCSV(resultValidity).getBytes(), StandardCharsets.UTF_8);
        queryService.setRunStop(queryid);
        return new ResponseEntity<>(_response, __headers, HttpStatus.OK);
    }


    @RequestMapping(value = {"/exec_csv/{connectionid}/{query_id}", "/exec_csv2/{connectionid}/{query_id}"}, method = RequestMethod.POST)
    public ResponseEntity<Object> PostJsonQuery(@PathVariable("connectionid") String _connectionID,
                                                @PathVariable("query_id") String _query_id,
                                                @RequestBody ObjectNode json
            , BindingResult result) throws IOException {
//        System.out.println("Call PostJsonQuery !!!");
        Message _message = Message.getInstance();
        HttpHeaders __headers = new HttpHeaders();
        __headers.add("Content-Type", "text/plain; charset=utf-8");

        String userid = "";
        String queryid = "";

        if (json.has("queryid")) {
            queryid = json.get("queryid").asText();
        }

        queryid = queryService.makeQueryId(queryid);

        if (json.has("userid")) {
            JSONObject jsonObject = QueryService.sessionMap.get(queryid);
            if (jsonObject != null) {
                jsonObject.put("userid", json.get("userid").asText());
                QueryService.sessionMap.put(queryid, jsonObject);
            }

        }

        if (result.hasErrors()) {
            _message.setMessage("Some errors , Please check ur json content!");
            queryService.addLogging("POST", "Some errors , Please check ur json content!" + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.BAD_REQUEST);
        }

        boolean isQueryFileExists = queryServiceFile.isQueryFileExist(_query_id);

        if (!isQueryFileExists) {
            _message.setMessage(_query_id + " (information) doesn't exist.");
            queryService.addLogging("POST", _query_id + " (information) doesn't exist." + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }

        String query = queryServiceFile.fetchQueryFromFile(_query_id);
        String queryafterKeyReplace = queryService.getFormattedQuery(query, json);

        if (json.has("userid")) {
            userid = json.get("userid").asText();
            if (userid.length() > 0) {
                String fileName = (StringUtils.split(queryafterKeyReplace.substring(queryafterKeyReplace.toLowerCase().indexOf("from") + 4)))[0] + ".csv";

                boolean isFileExist = queryServiceFile.FileExist(userid, fileName);
                if (!isFileExist) {
                    _message.setMessage("File name " + fileName + " for UserId " + userid + " (information) doesn't exist.");
                    queryService.addLogging("POST", "File name " + queryafterKeyReplace + " for UserId " + userid + " (information) doesn't exist." + "\n");
                    queryService.setRunStop(queryid);
                    return new ResponseEntity<>(_message, __headers, HttpStatus.NOT_FOUND);
                }
                boolean isConnectionFileExist = queryService.isConnectionIdFileExists(_connectionID);
                if (!isConnectionFileExist) {
                    _message.setMessage(_connectionID + " (information) doesn't exist.");
                    queryService.addLogging("POST", _connectionID + " (information) doesn't exist." + "\n");
                    queryService.setRunStop(queryid);
                    return new ResponseEntity<>(_message, __headers, HttpStatus.UNAUTHORIZED);
                }
                String resultQueryCsvJdbc = queryService.FetchwithCsvJdbc(_connectionID, queryafterKeyReplace, queryid, userid);


                String log = "ConnectionID is: " + _connectionID + " , Query Format is: CSV  " +
                        "\n\tQuery Sql after key Replace is: " + queryafterKeyReplace + "\n\n";
                queryService.addLogging("POST", log);
                String _response = new String(queryServiceCsv.structureCSV(resultQueryCsvJdbc).getBytes(), StandardCharsets.UTF_8);
                queryService.setRunStop(queryid);
                return new ResponseEntity<>(_response, __headers, HttpStatus.OK);
            }
        }

        String queryAfterConversion = queryService.convertGlobalDateTime(queryService.convertGlobalVariable(queryafterKeyReplace));
        if (queryAfterConversion.contains("NOT RESOLVED")) {
            _message.setMessage(new String(queryAfterConversion.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.PAYMENT_REQUIRED);
        }

        String resultValidity = queryService.checkValidity(queryAfterConversion, _connectionID, "", queryid, true);
        if (resultValidity.equalsIgnoreCase(_connectionID)) {
            _message.setMessage(resultValidity + " (information) doesn't exist.");
            queryService.addLogging("POST", resultValidity + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.BAD_REQUEST);
        } else if (resultValidity.startsWith("Error") || resultValidity.startsWith("error")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", resultValidity + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.PAYMENT_REQUIRED);
        } else if (resultValidity.startsWith("maxRunningTimeOut")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", resultValidity + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.REQUEST_TIMEOUT);
        } else if (resultValidity.startsWith("MaxRows")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", resultValidity + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.PAYLOAD_TOO_LARGE);
        }

        String log = "ConnectionID is: " + _connectionID + " , Query Format is: json  " +
                "\n\tQuery Sql after key Replace and conversion is: " + queryAfterConversion + "\n\n";
        queryService.addLogging("POST", log);
        String _response = new String(queryServiceCsv.structureCSV(resultValidity).getBytes(), StandardCharsets.UTF_8);
        queryService.setRunStop(queryid);
        return new ResponseEntity<>(_response, __headers, HttpStatus.OK);
    }


    @RequestMapping(value = {"/exec_csv/{connectionid}/{query_id}", "/exec_csv2/{connectionid}/{query_id}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getCsvQuery(@PathVariable("connectionid") String _connectionID,
                                              @PathVariable("query_id") String _query_id) throws IOException {

        Message _message = Message.getInstance();
        HttpHeaders __headers = new HttpHeaders();
        boolean isQueryFileExists = queryServiceFile.isQueryFileExist(_query_id);

        String queryid = "";

        queryid = queryService.makeQueryId(queryid);

        if (!isQueryFileExists) {
            _message.setMessage(_query_id + " (information) doesn't exist.");
            queryService.addLogging("GET", _query_id + " (information) doesn't exist." + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.UNAUTHORIZED);
        }
        String query = queryServiceFile.fetchQueryFromFile(_query_id);
        String queryAfterConversion = queryService.convertGlobalVariable(queryService.convertGlobalDateTime(queryService.getFormattedString(query)));
        if (queryAfterConversion.contains("NOT RESOLVED")) {
            _message.setMessage(new String(queryAfterConversion.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.PAYMENT_REQUIRED);
        }
        String resultValidity = queryService.checkValidity(queryAfterConversion, _connectionID, "", queryid, true);

        if (resultValidity.equalsIgnoreCase(_connectionID)) {
            _message.setMessage(resultValidity + " (information) doesn't exist.");
            queryService.addLogging("GET", resultValidity + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.BAD_REQUEST);
        } else if (resultValidity.startsWith("Error") || resultValidity.startsWith("error")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("GET", resultValidity + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.PAYMENT_REQUIRED);
        } else if (resultValidity.startsWith("maxRunningTimeOut")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", resultValidity + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.REQUEST_TIMEOUT);
        } else if (resultValidity.startsWith("MaxRows")) {
            _message.setMessage(new String(resultValidity.getBytes(), StandardCharsets.UTF_8));
            queryService.addLogging("POST", resultValidity + "\n");
            queryService.setRunStop(queryid);
            return new ResponseEntity<>(_message, __headers, HttpStatus.PAYLOAD_TOO_LARGE);
        }

        String log = "ConnectionID is: " + _connectionID + " , Query Format is: csv" + ", Query is: " + queryAfterConversion + "\n";
        queryService.addLogging("GET", log);
        String _response = new String(queryServiceCsv.structureCSV(resultValidity).getBytes(), StandardCharsets.UTF_8);
        __headers.add("Content-Type", "text/plain; charset=utf-8");
        queryService.setRunStop(queryid);
        return new ResponseEntity<>(_response, __headers, HttpStatus.OK);
    }

}
