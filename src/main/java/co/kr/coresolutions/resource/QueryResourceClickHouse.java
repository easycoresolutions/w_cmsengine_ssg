package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryClickHouseFacade;
import co.kr.coresolutions.model.TableLoadClickHouse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class QueryResourceClickHouse {
    private final IQueryClickHouseFacade queryClickHouseFacade;

    @PostMapping("clickhouse_import")
    public ResponseEntity tableLoad(@Valid @RequestBody TableLoadClickHouse tableLoadClickHouse) {
        CustomResponseDto customResponseDto = (CustomResponseDto) queryClickHouseFacade.tableLoad(tableLoadClickHouse);
        return ResponseEntity.status(!customResponseDto.getSuccess() ? HttpStatus.BAD_REQUEST : HttpStatus.OK)
                .header("Content-Type", "application/json; charset=utf-8").body(customResponseDto);
    }

    @GetMapping("clickhouse_process/{connectionid}")
    public ResponseEntity monitorProcess(@PathVariable(value = "connectionid") String connectionID) {
        return ResponseEntity.ok(queryClickHouseFacade.monitorProcess(connectionID));
    }

    @DeleteMapping("clickhouse_process/{connectionid}/{queryid}")
    public ResponseEntity deleteProcess(@PathVariable(value = "connectionid") String connectionID,
                                        @PathVariable(value = "queryid") String queryID) {
        return ResponseEntity.ok(queryClickHouseFacade.deleteProcess(connectionID, queryID));
    }

    @GetMapping("clickhouse_import/monitor")
    public ResponseEntity monitorImport() {
        return ResponseEntity.ok(queryClickHouseFacade.monitorImport());
    }

    @DeleteMapping("clickhouse_import/monitor/{queryid}")
    public ResponseEntity deleteImport(@PathVariable(value = "queryid") String queryID) {
        return ResponseEntity.ok(queryClickHouseFacade.deleteImport(queryID));
    }

    @GetMapping("clickhouse_tables/{connectionid}/{database_name}")
    public ResponseEntity monitorTables(@PathVariable(value = "connectionid") String connectionID,
                                        @PathVariable(value = "database_name") String dbName) {
        return ResponseEntity.ok(queryClickHouseFacade.monitorTables(connectionID, dbName));
    }

}
