package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQuerySurveyResultFacade;
import co.kr.coresolutions.model.dtos.SurveyResultDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class QueryResourceSurveyResult {
    private final IQuerySurveyResultFacade querySurveyResultFacade;

    @PostMapping(value = "/savesurveyresult/{userid}/{projectid}", consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> saveSurveyResult(@Valid @RequestBody SurveyResultDto surveyResultDto,
                                                   @PathVariable(value = "userid") String userID,
                                                   @PathVariable(value = "projectid") String projectID) {
        ResponseDto responseDto = querySurveyResultFacade.saveSurveyResult(surveyResultDto, userID, projectID);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).header("Content-Type", "application/json; charset=utf-8").body(responseDto.getMessage());
    }

    @DeleteMapping("/deletesurveyresult/{userid}/{projectid}")
    public ResponseEntity<Object> deleteSurveyResult(@PathVariable(value = "userid") String userID,
                                                     @PathVariable(value = "projectid") String projectID) {
        ResponseDto responseDto = querySurveyResultFacade.deleteSurveyResult(userID, projectID);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).header("Content-Type", "application/json; charset=utf-8").body(responseDto.getMessage());
    }

    @GetMapping("/getsurveyresult/{userid}/{projectid}")
    public ResponseEntity<Object> getSurveyResult(@PathVariable(value = "userid") String userID,
                                                  @PathVariable(value = "projectid") String projectID) {
        ResponseDto responseDto = querySurveyResultFacade.getSurveyResult(userID, projectID);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).header("Content-Type", "application/json; charset=utf-8").body(responseDto.getMessage());
    }

    @GetMapping("/getsurveyresults/{userid}")
    public ResponseEntity<Object> getSurveyResults(@PathVariable(value = "userid") String userID) {
        ResponseDto responseDto = querySurveyResultFacade.getSurveyResults(userID);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).header("Content-Type", "application/json; charset=utf-8").body(responseDto.getMessage());
    }
}
