package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.ResponseCodes;
import co.kr.coresolutions.commons.ResponseDto;
import co.kr.coresolutions.facades.IQueryCommandFacade;
import co.kr.coresolutions.facades.IQuerySocketFacade;
import co.kr.coresolutions.model.Command;
import co.kr.coresolutions.model.Message;
import co.kr.coresolutions.model.dtos.CommandDto;
import co.kr.coresolutions.model.dtos.FileDto;
import co.kr.coresolutions.model.dtos.SocketBodyDto;
import co.kr.coresolutions.service.Constants;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceJSON;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(consumes = "*/*;charset=UTF-8")
@Slf4j
public class QueryResourceCommand {
    private final IQueryCommandFacade queryCommandFacade;
    private final QueryServiceJSON queryServiceJSON;
    private final QueryService queryService;
    private final ObjectMapper objectMapper;
    private final IQuerySocketFacade querySocketFacade;


    @PostMapping(value = {"command/fileupload", "command2nd/fileupload", "command3rd/fileupload"})
    @ApiOperation(value = "upload file for later execution of command")
    @SneakyThrows
    public ResponseEntity<ResponseDto> fileUpload(@Valid @RequestBody FileDto fileDto) {

//        System.out.println("call command fileUpload !!!");
        ResponseDto responseDto = queryCommandFacade.fileUpload(fileDto);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).body(responseDto);
    }

    @DeleteMapping(value = {"command/filedelete/{userId}/{fileNameExt}", "command2nd/filedelete/{userId}/{fileNameExt}", "command3rd/filedelete/{userId}/{fileNameExt}"})
    @ApiOperation(value = "delete file from command dir")
    @SneakyThrows
    public ResponseEntity<ResponseDto> fileDelete(@PathVariable("userId") String userId,
                                                  @PathVariable("fileNameExt") String fileNameExt) {
//        System.out.println("call command filedelete !!! ");
        ResponseDto responseDto = queryCommandFacade.fileDelete(userId, fileNameExt);
        return ResponseEntity.status(responseDto.getSuccessCode() == HttpStatus.OK.value() ? responseDto.getSuccessCode() : responseDto.getErrorCode()).build();
    }

    @PostMapping(value = {"command/{userId}", "command2nd/{userId}", "command3rd/{userId}"})
    @ApiOperation(value = "execute command from file resides in commandDir/userId")
    @SneakyThrows
    public ResponseEntity execute(@Valid @RequestBody CommandDto commandDto,
                                  @PathVariable("userId") String userId, HttpServletResponse response) {

//        System.out.println("Call Command UserId !!!");
        ResponseDto responseDto = queryCommandFacade.execute(commandDto, userId, response);
        return ResponseEntity.status(responseDto.getErrorCode() == 0 ? responseDto.getSuccessCode() : responseDto.getErrorCode()).body(responseDto.getMessage());
    }

    @GetMapping(value = {"command/filelist/{userId}", "command2nd/filelist/{userId}", "command3rd/filelist/{userId}"})
    @ApiOperation(value = "get all files from commandDir/userId")
    @SneakyThrows
    public ResponseEntity findAll(@PathVariable("userId") String userId) {

//        System.out.println("call command filelist !!! ");
        String response = queryCommandFacade.findAll(userId);
        return ResponseEntity.status(response.length() == 0 ? ResponseCodes.NOT_FOUND_ERROR : ResponseCodes.OK_RESPONSE)
                .body(response);
    }

    @PostMapping(value = {"command", "command2nd", "command3rd"}, produces = "application/json; charset=utf-8")
    public ResponseEntity postCommand(@Valid @RequestBody Command command, HttpServletResponse response) throws Exception {
        
        Message message = Message.getInstance();
        boolean isCommandFileExists = queryService.isCommandFileExists(command.getCommand(), false);
        String owner;
        if (command.getOwner() != null && !command.getOwner().isEmpty()) {
            owner = command.getOwner();
        } else if (command.getUserId() != null && !command.getUserId().isEmpty()) {
            owner = command.getUserId();
        } else {
            try {
                owner = queryCommandFacade.getOwner();
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("please provide userId or owner or token");
            }
        }

        if (!isCommandFileExists) {
            message.setMessage(Constants.commandDirName + " file " + command.getCommand() + " (information) doesn't exist.");
            queryService.addLogging("POST", message.getMessage() + "\n");
            querySocketFacade.send(SocketBodyDto.builder().userId(owner).key("COMMAND").message(Constants.commandDirName + "\noutput message:\n--------------\n\t\t" + message.getMessage() + "\nBODY:\n--------------\n\t" + "\t/" + Constants.commandDirName + "/\n\tPOST\n\t\"" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(command) + "\"\n").build());
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(message);
        }

        String output = command.getOutput();
        if (!(output.equalsIgnoreCase("csv")
                || output.equalsIgnoreCase("json")
                || output.equalsIgnoreCase("text"))) {
            message.setMessage("OUTPUT name is not valid.\n" +
                    "OUTPUT property can have one of \"json\", \"csv\" and \"text\"");
            queryService.addLogging("POST", message.getMessage() + "\n");
            querySocketFacade
                    .send(SocketBodyDto.builder().key("COMMAND").message(Constants.commandDirName + "\noutput message:\n--------------\n\t\t" + message.getMessage() + "\nBODY:\n--------------\n\t" +
                            "\t/" + Constants.commandDirName + "/\n\tPOST\n\t\"" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(command) + "\"\n").build());
            return ResponseEntity.status(HttpStatus.NON_AUTHORITATIVE_INFORMATION).body(message);
        }

        String commandAfterKeyReplace = queryService.getFormattedCommand(command.getCommand(), command.getParameter(), false);

        if (commandAfterKeyReplace.startsWith("Error") || commandAfterKeyReplace.length() == 0) {
            message.setMessage(commandAfterKeyReplace);
            queryService.addLogging("POST", message.getMessage() + "\n");
            querySocketFacade
                    .send(SocketBodyDto.builder().key("COMMAND").message(Constants.commandDirName + "\noutput message:\n--------------\n\t\t" + message.getMessage() + "\nBODY:\n--------------\n\t" +
                            "\t/" + Constants.commandDirName + "/\n\tPOST\n\t\"" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(command) + "\"\n").build());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
        }
        String commandID;
        if (command.getCommandId() == null || command.getCommandId().isEmpty()) {
            commandID = owner + new SimpleDateFormat("yyyyMMdd:HHmmss").format(new Date());
        } else {
            commandID = command.getCommandId();
        }
        String startDateTime = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        Command.CommandDetails commandDetails = Command.CommandDetails.builder().commadName(command.getCommand()).CommandID(commandID).owner(owner).startdatetime(startDateTime).build();
        queryService.addLogging("POST", Constants.commandDirName + " + BODY  endpoint is started (" + command.toString() + ")\n");
        querySocketFacade
                .send(SocketBodyDto.builder().key("COMMAND").message("POST /" + Constants.commandDirName + " + BODY  endpoint is started (" + command.toString() + ")\n").build());
        
        String resultExecute;
        if(null != command.getRunMode() && !"".equals(command.getRunMode())) {
            resultExecute = queryService.executeCommand("command_" + command.getCommand(), commandAfterKeyReplace, commandID, owner, response, command.getRunMode());
        } else {
            resultExecute = queryService.executeCommand("command_" + command.getCommand(), commandAfterKeyReplace, commandID, owner, response);
        }
        
        queryService.addLogging("POST", Constants.commandDirName + "\noutput message:\n--------------\n\t\t" + (resultExecute.isEmpty() ? "Empty Result" : resultExecute) + "\nBODY:\n--------------\n\t" +
                "\t/" + Constants.commandDirName + "/\n\tPOST\n\t\"" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(command) + "\"\n");
        querySocketFacade.send(SocketBodyDto.builder().userId(owner).key("COMMAND").message(Constants.commandDirName + "\noutput message:\n--------------\n\t\t" + (resultExecute.isEmpty() ? "Empty Result" : resultExecute) + "\nBODY:\n--------------\n\t\t/" + Constants.commandDirName + "/\n\tPOST\n\t\"" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(command) + "\"\n").build());

        if (resultExecute.length() == 0) {
            message.setMessage("Empty result returned by the process from Command\t" + commandDetails.toString());
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(message);
        }

        if (resultExecute.startsWith("Error") || resultExecute.startsWith("Fatal") || resultExecute.startsWith("fail")) {
            message.setMessage(new String(resultExecute.getBytes(), StandardCharsets.UTF_8));
            return ResponseEntity.status(HttpStatus.OK).body(resultExecute);
        }

        if (output.equalsIgnoreCase("text") || output.equalsIgnoreCase("csv")) {
            return ResponseEntity.status(HttpStatus.OK).body(resultExecute);
        }

        String responseStr = "";
        if (output.equalsIgnoreCase("json")) {
            try {
                responseStr = queryServiceJSON.structureJsonForCommandStructure(queryServiceJSON.structureJsonFromCSV(resultExecute), command.getCHAR(), command.getNUM());
            } catch (Exception e) {
                message.setMessage("can't format the output to json format from command\t" + commandDetails.toString());
                queryService.addLogging("POST", Constants.commandDirName + "\noutput message:\n--------------\n\t\t" + message.getMessage() + "\nBODY:\n--------------\n\t" +
                        "\t/" + Constants.commandDirName + "/\n\tPOST\n\t\"" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(command) + "\"\n");
                querySocketFacade.send(SocketBodyDto.builder().userId(owner).key("COMMAND").message(Constants.commandDirName + "\noutput message:\n--------------\n\t\t" + message.getMessage() + "\nBODY:\n--------------\n\t\t/" + Constants.commandDirName + "/\n\tPOST\n\t\"" + objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL).writeValueAsString(command) + "\"\n").build());
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(responseStr);
    }

    @GetMapping(value = {"command/monitor", "command2nd/monitor", "command3rd/monitor"})
    public ResponseEntity<List<Command.CommandDetails>> monitor() {
        return ResponseEntity.ok(queryCommandFacade.monitor());
    }

    @GetMapping(value = {"command/waiting", "command2nd/waiting", "command3rd/waiting"})
    public ResponseEntity waiting() {
        return ResponseEntity.ok(queryCommandFacade.monitorWaiting());
    }

    @DeleteMapping(value = {"command/monitor/{commandid}", "command2nd/monitor/{commandid}", "command3rd/monitor/{commandid}"})
    public ResponseEntity<ResponseDto> deleteCommand(@PathVariable("commandid") String commandID) {
        return ResponseEntity.ok(queryCommandFacade.deleteCommand(commandID));
    }

    @GetMapping(value = {"command/json/{commandFileName}", "command2nd/json/{commandFileName}", "command3rd/json/{commandFileName}"})
    public ResponseEntity<Object> getCommandJson(@PathVariable(value = "commandFileName") String commandFileName) {

        Message _message = Message.getInstance();

        boolean isCommandFileExists = queryService.isCommandFileExists(commandFileName, false);

        String queryid = "";

        queryid = queryService.makeQueryId(queryid);

        if (!isCommandFileExists) {
            _message.setMessage(Constants.commandDirName + " file " + commandFileName + " (information) doesn't exist.");
            queryService.addLogging("GET", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(_message);
        }

        String resultExecute = queryService.executeCommandFile(commandFileName);
        if (resultExecute.length() == 0) {
            _message.setMessage("Os not supported");
            queryService.addLogging("GET", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(_message);
        }

        if (resultExecute.startsWith("Error") || resultExecute.startsWith("Fatal")) {
            _message.setMessage(resultExecute);
            queryService.addLogging("GET", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(_message);
        }

        String log = "getCommandJson called\n";
        queryService.addLogging("GET", log);
        String _response = "";
        try {
            _response = queryServiceJSON.structureJson(queryServiceJSON.structureJsonFromCSV(resultExecute), false, new HashMap<>(), queryid);
        } catch (Exception e) {
            _message.setMessage("Not valid output csv");
            queryService.addLogging("GET", _message.getMessage() + "\n");
            queryService.setRunStop(queryid);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(_message);
        }
        queryService.setRunStop(queryid);
        return ResponseEntity.status(HttpStatus.OK).body(_response);
    }

    @GetMapping(value = {"command/csv/{commandFileName}", "command2nd/csv/{commandFileName}", "command3rd/csv/{commandFileName}"})
    public ResponseEntity<Object> getCommandCSV(@PathVariable(value = "commandFileName") String commandFileName) {
        Message _message = Message.getInstance();

        boolean isCommandFileExists = queryService.isCommandFileExists(commandFileName, false);

        if (!isCommandFileExists) {
            _message.setMessage(Constants.commandDirName + " file " + commandFileName + " (information) doesn't exist.");
            queryService.addLogging("GET", _message.getMessage() + "\n");
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(_message);
        }

        String resultExecute = queryService.executeCommandFile(commandFileName);

        //System.out.println("resultExecute length : " + resultExecute.length());
        if (resultExecute.length() == 0) {
            _message.setMessage("Os not supported");
            queryService.addLogging("GET", _message.getMessage() + "\n");
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(_message);
        }

        if (resultExecute.startsWith("Error")) {
            _message.setMessage(resultExecute);
            queryService.addLogging("GET", _message.getMessage() + "\n");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(_message);
        }

        String log = "getCommandJson called\n";
        queryService.addLogging("GET", log);

        return ResponseEntity.status(HttpStatus.OK).body(new String(resultExecute.getBytes(), StandardCharsets.UTF_8));
    }
}
