package co.kr.coresolutions.resource;

import co.kr.coresolutions.facades.IQueryJsonPropertyFacade;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = QueryResourceJsonProperty.V1_JP_PATH)
@Slf4j
public class QueryResourceJsonProperty {
    static final String V1_JP_PATH = "/jsonproperty";
    private final IQueryJsonPropertyFacade queryJsonPropertyFacade;

    @PostMapping("/{user}/{subsystem}/{keyname}")
    public ResponseEntity post(@PathVariable("user") String user, @PathVariable("subsystem") String subsystem,
                               @PathVariable("keyname") String keyName,
                               @RequestBody JsonNode objectNode) {
        return ResponseEntity.ok(queryJsonPropertyFacade.post(user, subsystem, keyName, objectNode));
    }

    @DeleteMapping("/{user}/{subsystem}/{keyname}/{seqNum}")
    public ResponseEntity delete(@PathVariable("user") String user, @PathVariable("subsystem") String subsystem,
                                 @PathVariable("keyname") String keyName, @PathVariable("seqNum") int seqNum) {
        return ResponseEntity.ok(queryJsonPropertyFacade.delete(user, subsystem, keyName, seqNum));
    }

    @GetMapping
    public ResponseEntity get() {
        return ResponseEntity.ok(queryJsonPropertyFacade.get());
    }

    @GetMapping("/{userid}/{subsys}/{keyname}")
    public ResponseEntity findBy(@PathVariable("userid") String user, @PathVariable("subsys") String subsystem,
                                 @PathVariable("keyname") String keyName) {
        return ResponseEntity.ok(queryJsonPropertyFacade.findBy(user, subsystem, keyName));
    }
}
