package co.kr.coresolutions.resource;

import co.kr.coresolutions.model.Project;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceProjects;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@Scope(value = "request")
@RequiredArgsConstructor
public class QueryResourceProjects {
    private final QueryService queryService;
    private final QueryServiceProjects queryServiceProject;

    @RequestMapping(value = "/deleteproject/{ownerid}/{projectid}", method = RequestMethod.DELETE, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> deleteProject(@PathVariable(value = "ownerid") String owner,
                                                @PathVariable(value = "projectid") String projectID,
                                                UriComponentsBuilder _ucBuilder) throws Exception {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/deleteproject/" + owner + "/" + projectID).buildAndExpand().toUri());

        boolean resultDelete = queryServiceProject.deleteProject(owner, projectID);

        if (!resultDelete) {
            queryService.addLogging("DELETE", "deleteproject : " + projectID + " doesn't exist.\n");
            return new ResponseEntity<Object>(projectID + " doesn't exist.", __headers, HttpStatus.CONFLICT);
        }

        String log = "deleteproject : " + projectID;
        queryService.addLogging("DELETE", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(projectID + " Deleted successfully", __headers, HttpStatus.OK);
    }


    @RequestMapping(value = {"/getprojects/{owner}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getProjects(@PathVariable String owner,
                                              UriComponentsBuilder _ucBuilder) throws IOException {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/getprojects/" + owner).buildAndExpand().toUri());

        String resultgetProjects = queryServiceProject.getProjects(owner);

        if (resultgetProjects.equalsIgnoreCase("fail")) {
            queryService.addLogging("GET", "getprojects : " + owner + " doesn't exist.\n");
            return new ResponseEntity<Object>(owner + " doesn't exist.", __headers, HttpStatus.GONE);
        }
        __headers.add("Content-Type", "application/json; charset=utf-8");
        String log = " getProjects called " + owner + "\n";
        queryService.addLogging("GET", log);

        //return new ResponseEntity<Object>(new String(resultgetProjects.getBytes(), "UTF-8"), __headers, HttpStatus.OK);
        return new ResponseEntity<Object>(resultgetProjects, __headers, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getproject/{owner}/{projectid}"}, method = RequestMethod.GET)
    public ResponseEntity<Object> getProject(@PathVariable(value = "owner") String owner,
                                             @PathVariable(value = "projectid") String projectID,
                                             UriComponentsBuilder _ucBuilder) throws IOException {
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/getproject/" + owner + "/" + projectID).buildAndExpand().toUri());

        String resultgetProject = queryServiceProject.getProject(owner, projectID);
        if (resultgetProject.equalsIgnoreCase("failprojectid")) {
            queryService.addLogging("GET", projectID + " doesn't exist.\n");
            return new ResponseEntity<Object>(projectID + " doesn't exist.", __headers, HttpStatus.CONFLICT);
        } else if (resultgetProject.equalsIgnoreCase("failowner")) {
            queryService.addLogging("GET", "getproject : " + owner + " doesn't exist.\n");
            return new ResponseEntity<Object>(owner + " doesn't exist.", __headers, HttpStatus.GONE);
        }
        String log = " getProject called " + owner + " , " + projectID + "\n";
        queryService.addLogging("GET", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        //return new ResponseEntity<Object>(new String(resultgetProject.getBytes(), "UTF-8"), __headers, HttpStatus.OK);
        return new ResponseEntity<Object>(resultgetProject, __headers, HttpStatus.OK);

    }


    @RequestMapping(value = "/saveproject/{owner}/{projectid}", method = RequestMethod.POST, consumes = "*/*;charset=UTF-8")
    public ResponseEntity<Object> saveProject(@Valid @RequestBody Project _project,
                                              @PathVariable(value = "owner") String owner,
                                              @PathVariable(value = "projectid") String projectid,
                                              UriComponentsBuilder _ucBuilder, BindingResult result) throws Exception {
//        System.out.println("saveProject Call!! " + _project.getProj_Name() + " / " + _project.getProj_Desc());
        HttpHeaders __headers = new HttpHeaders();
        __headers.setLocation(_ucBuilder.path("/saveproject/" + owner + "/" + projectid).buildAndExpand().toUri());

        if (result.hasErrors()) {
            queryService.addLogging("POST", "saveproject : Some errors , Please check ur json content!" + "\n");
            return new ResponseEntity<Object>("saveproject : Some errors , Please check ur json content!", __headers, HttpStatus.BAD_REQUEST);
        }
        String resultsaveProject = queryServiceProject.saveProject(owner, projectid, _project);
        if (resultsaveProject.equalsIgnoreCase("fail")) {
            queryService.addLogging("POST", "saveproject : can't create the project " + projectid + " for the owner " + owner + "\n");
            return new ResponseEntity<Object>("can't create the project " + projectid + " for the owner " + owner, __headers, HttpStatus.BANDWIDTH_LIMIT_EXCEEDED);
        }

        String log = projectid + " project uploaded successfully\n";
        queryService.addLogging("POST", log);
        __headers.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<Object>(projectid + " project uploaded successfully", __headers, HttpStatus.OK);
    }

}
