package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryChartFacade;
import co.kr.coresolutions.model.dtos.Chart2Dto;
import co.kr.coresolutions.model.dtos.Chart3Dto;
import co.kr.coresolutions.model.dtos.ChartDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(consumes = "*/*; charset=utf-8")
public class QueryResourceChart {
    private final IQueryChartFacade queryChartFacade;

    @PostMapping("select_chart/{connectionid}")
    public ResponseEntity getChart(@PathVariable("connectionid") String connectionid,
                                   @Valid @RequestBody ChartDto chartDto) {
        CustomResponseDto customResponseDto = (CustomResponseDto) queryChartFacade.getChart(connectionid, chartDto);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }

    @PostMapping("exec_chart/{connectionid}")
    public ResponseEntity execChart(@PathVariable("connectionid") String connectionid,
                                    @Valid @RequestBody Chart2Dto chartDto) {
        CustomResponseDto customResponseDto = (CustomResponseDto) queryChartFacade.execChart(connectionid, chartDto, false);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }

    @PostMapping("exec_data/{connectionid}")
    public ResponseEntity execData(@PathVariable("connectionid") String connectionid,
                                   @Valid @RequestBody Chart2Dto chartDto) {
        CustomResponseDto customResponseDto = (CustomResponseDto) queryChartFacade.execChart(connectionid, chartDto, true);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }

    @PostMapping("query_json/{connectionid}")
    public ResponseEntity formatOutputJson(@PathVariable("connectionid") String connectionid,
                                           @Valid @RequestBody Chart3Dto chartDto) {
        CustomResponseDto customResponseDto = (CustomResponseDto) queryChartFacade.execChart(connectionid, chartDto, false);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }

    @PostMapping("refresh/date_time_definition")
    public ResponseEntity refreshDateTime() {
        CustomResponseDto customResponseDto = (CustomResponseDto) queryChartFacade.refreshDateTime();
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }

}
