package co.kr.coresolutions.resource;

import co.kr.coresolutions.commons.CustomResponseDto;
import co.kr.coresolutions.facades.IQueryProjectFacade;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = QueryResourceProject.V1_PROJECT_PATH)
@Slf4j
public class QueryResourceProject {
    static final String V1_PROJECT_PATH = "/project/";
    private final IQueryProjectFacade queryProjectFacade;

    @PostMapping(value = "{connectionid}/{tablename}/{projectid}")
    public ResponseEntity<CustomResponseDto> saveProject(@RequestBody JsonNode jsonObject,
                                                         @PathVariable(value = "connectionid") String connectionID,
                                                         @PathVariable(value = "tablename") String tableName,
                                                         @PathVariable(value = "projectid") String projectID) {
        CustomResponseDto customResponseDto = queryProjectFacade.saveProject(jsonObject, connectionID, tableName, projectID);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }

    @DeleteMapping("{connectionid}/{tablename}/{projectid}")
    public ResponseEntity<CustomResponseDto> deleteRow(@RequestBody JsonNode jsonObject,
                                                       @PathVariable(value = "connectionid") String connectionID,
                                                       @PathVariable(value = "tablename") String tableName,
                                                       @PathVariable(value = "projectid") String projectID) {
        CustomResponseDto customResponseDto = queryProjectFacade.deleteRow(jsonObject, connectionID, tableName, projectID);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto);
    }

    @GetMapping("{connectionid}/{tablename}/{projectid}")
    public ResponseEntity getRow(@PathVariable(value = "connectionid") String connectionID,
                                 @PathVariable(value = "tablename") String tableName,
                                 @PathVariable(value = "projectid") String projectID) {
        CustomResponseDto customResponseDto = queryProjectFacade.getRow(connectionID, tableName, projectID);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK)
                .header("Content-Type", "application/json; charset=utf-8").body(customResponseDto.getErrorCode() != null ? customResponseDto : customResponseDto.getDetailedMessage());
    }

    @GetMapping("projects/{connectionid}/{tablename}/{userid}")
    public ResponseEntity getRows(@PathVariable(value = "connectionid") String connectionID,
                                  @PathVariable(value = "tablename") String tableName,
                                  @PathVariable(value = "userid") String userid) {
        CustomResponseDto customResponseDto = queryProjectFacade.getRows(connectionID, tableName, userid);
        return ResponseEntity.status(customResponseDto.getErrorCode() != null ? HttpStatus.BAD_REQUEST : HttpStatus.OK)
                .header("Content-Type", "application/json; charset=utf-8").body(customResponseDto.getErrorCode() != null ? customResponseDto : customResponseDto.getDetailedMessage());
    }

}
