package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class QueryLayout {
    @NotBlank
    private String querylayoutID;
    @NotBlank
    private String querylayoutName;
    @NotBlank
    private String querylayoutOwner;
    @NotBlank
    private String querylayoutCreated;
    @NotBlank
    private String querylayoutModified;
    @NotBlank
    private String querylayoutDesc;
    @NotBlank
    private String querylayoutUserid;
    @NotBlank
    private String querylayoutCategory;
    @NotBlank
    private String querylayoutVersion;
    private ObjectNode querylayoutjson;
}
