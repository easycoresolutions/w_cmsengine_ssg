package co.kr.coresolutions.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.Data;

import javax.validation.Valid;

@Data
public class ETLTemplate {
    @JsonProperty
    private String description;

    @Valid
    private ETLQuery query;

    @Data
    @JacksonXmlRootElement
    public static class ETLQuery {
        @JacksonXmlProperty(localName = "connection-id", isAttribute = true)
        private String connectionID;

        @Valid
        @JsonProperty
        private ETLScript script;

        @JacksonXmlText
        private String fetchSql;

        @Data
        @JacksonXmlRootElement
        public static class ETLScript {
            @JacksonXmlProperty(localName = "connection-id", isAttribute = true)
            private String connectionID;

            @JacksonXmlProperty(localName = "commit_count", isAttribute = true)
            private Integer count;

            @JacksonXmlText
            private String insertSql;
        }
    }
}
