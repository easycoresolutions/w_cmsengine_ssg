package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GAConnection {
    @JsonProperty(value = "UA-key")
    private String uaKey;
    @JsonProperty(value = "apiEmail")
    private String apiEmail;
    @JsonProperty(value = "json-key", required = true)
    @NotEmpty
    private String jsonKey;
    @JsonProperty(value = "AccountID")
    private String accountID;
    @JsonProperty(value = "webPropertyId")
    private String webPropertyId;
    @JsonProperty(value = "profileId", required = true)
    @NotEmpty
    private String profileId;
}
