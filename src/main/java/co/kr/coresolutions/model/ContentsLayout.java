package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ContentsLayout {
    @NotBlank
    private String contentslayoutID;
    @NotBlank
    private String contentslayoutName;
    @NotBlank
    private String contentslayoutOwner;
    @NotBlank
    private String contentslayoutCreated;
    @NotBlank
    private String contentslayoutModified;
    @NotBlank
    private String contentslayoutDesc;
    @NotBlank
    private String contentslayoutUserid;
    @NotBlank
    private String contentslayoutCategory;
    @NotBlank
    private String contentslayoutVersion;
    private ObjectNode contentslayoutjson;
}
