package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Project {
    @NotBlank
    @JsonProperty("Proj_ID")
    private String Proj_ID;
    @NotBlank
    @JsonProperty("Proj_Name")
    private String Proj_Name;
    @NotBlank
    @JsonProperty("Proj_Owner")
    private String Proj_Owner;
    @NotBlank
    @JsonProperty("Proj_Created")
    private String Proj_Created;
    @NotBlank
    @JsonProperty("Proj_Modified")
    private String Proj_Modified;
    @NotBlank
    @JsonProperty("Proj_Desc")
    private String Proj_Desc;
    @NotBlank
    @JsonProperty("Proj_Userid")
    private String Proj_Userid;
    @NotBlank
    @JsonProperty("Proj_Category")
    private String Proj_Category;
    @NotBlank
    @JsonProperty("Proj_Version")
    private String Proj_Version;
    @JsonProperty("Proj_json")
    private ObjectNode Proj_json;
}