package co.kr.coresolutions.model;

import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Data
public class DateConverter {
    private final Date date;
    private final Calendar calendar;
    private final String SECONDS = "0";
    private final String DAYSOFWEEK = "?";
    private String minutes;
    private String hours;
    private String daysOfMonth;
    private String months;
    private String years;

    public DateConverter(Date date) {
        this.date = date;
        calendar = Calendar.getInstance();
        this.generateCronExpression();
    }

    private void generateCronExpression() {
        calendar.setTime(date);

        this.hours = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));

        this.minutes = String.valueOf(calendar.get(Calendar.MINUTE));

        this.daysOfMonth = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));

        this.months = new SimpleDateFormat("MM").format(calendar.getTime());

        this.years = String.valueOf(calendar.get(Calendar.YEAR));
    }
}


