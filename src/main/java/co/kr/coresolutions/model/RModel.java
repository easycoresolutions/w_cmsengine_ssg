package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RModel {
    @NotBlank
    private String userid;
    @NotBlank
    private String file;
    @NotBlank
    private String modelname;
    @NotBlank
    private String ext;
    @NotBlank
    private String activationscript;
    @NotBlank
    private String source;
    @NotBlank
    private String uploaded;
    @NotBlank
    private String modified;
    @NotNull
    private ObjectNode params;
}

