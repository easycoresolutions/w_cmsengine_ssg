package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Builder
public class SqliteFileRepo {
    @JsonProperty(value = "dbfile", required = true)
    private String dbFile;
    @JsonProperty(required = true)
    private String desc;
    @JsonProperty(required = true)
    @Builder.Default
    private String date = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    @JsonProperty(required = true)
    @Builder.Default
    private boolean active = false;
}
