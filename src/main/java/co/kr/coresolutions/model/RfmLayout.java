package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RfmLayout {
    @NotBlank
    private String rfmlayoutID;
    @NotBlank
    private String rfmlayoutName;
    @NotBlank
    private String rfmlayoutOwner;
    @NotBlank
    private String rfmlayoutCreated;
    @NotBlank
    private String rfmlayoutModified;
    @NotBlank
    private String rfmlayoutDesc;
    @NotBlank
    private String rfmlayoutUserid;
    @NotBlank
    private String rfmlayoutCategory;
    @NotBlank
    private String rfmlayoutVersion;
    private ObjectNode rfmlayoutjson;
}
