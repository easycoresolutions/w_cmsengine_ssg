package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Transfer {
    @NotBlank
    private String sourceId;
    @NotBlank
    private String sql;
    @NotNull
    private Boolean deleteOption;
    @NotBlank
    private String targetId;
    @NotBlank
    private String targetTable;
    @NotNull
    private Boolean audit_enabled;
    @NotNull
    private org.json.JSONObject output_scheme;
    private String audit_connid;
    private String audit_table;
}
