package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HtmlLayout {
    @NotBlank
    private String htmllayoutID;
    @NotBlank
    private String htmllayoutName;
    @NotBlank
    private String htmllayoutOwner;
    @NotBlank
    private String htmllayoutCreated;
    @NotBlank
    private String htmllayoutModified;
    @NotBlank
    private String htmllayoutDesc;
    @NotBlank
    private String htmllayoutUserid;
    @NotBlank
    private String htmllayoutCategory;
    @NotBlank
    private String htmllayoutVersion;
    private ObjectNode htmllayoutjson;
}
