package co.kr.coresolutions.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    private static volatile Message _message = null;
    private String message;

    public static Message getInstance() {
        //double checked locked pattern
        if (_message == null) {
            synchronized (Message.class) {
                if (_message == null) {
                    return new Message();
                }
            }
        }
        return _message;
    }
}

