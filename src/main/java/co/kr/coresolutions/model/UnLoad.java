package co.kr.coresolutions.model;

import co.kr.coresolutions.enums.Format;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@AllArgsConstructor
@NoArgsConstructor
public class UnLoad {
    @JsonProperty(required = true)
    private String sql;
    @JsonProperty(required = true)
    private String filename;
    @JsonProperty(required = true)
    private Format format;
    @JsonProperty
    private JsonNode outfields;
    @JsonProperty(required = true)
    private String owner;
    @JsonProperty(required = true)
    private Boolean header;
    private UnloadDetails unloadDetails;
    @JsonProperty(value = "db_file")
    private String dbFile;

    @Data
    @Builder
    @NoArgsConstructor
    public static class UnloadDetails implements Serializable {
        String unloadID;
        String fileName;
        String startdatetime;
        String owner;

        @JsonCreator
        public UnloadDetails(@JsonProperty("unloadID") String unloadID, @JsonProperty("fileName") String fileName,
                             @JsonProperty("startdatetime") String startDT, @JsonProperty("owner") String owner) {
            this.unloadID = unloadID;
            this.fileName = fileName;
            this.startdatetime = startDT;
            this.owner = owner;
        }
    }

}

