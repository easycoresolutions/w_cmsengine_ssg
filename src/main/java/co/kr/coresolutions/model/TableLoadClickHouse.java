package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TableLoadClickHouse {
    @JsonProperty(value = "userid", required = true)
    private String userID;
    @JsonProperty(value = "connectionid", required = true)
    private String connectionID;
    @JsonProperty(value = "database", required = true)
    private String database;
    @JsonProperty(value = "table_name", required = true)
    private String tableName;
    @JsonProperty(value = "input_directory", required = true)
    private String inputDirectory;
    @JsonProperty(value = "file_name", required = true)
    private String fileName;
    @JsonProperty(required = true)
    private Boolean header;
    @JsonProperty
    private String method;
    private ClickHouseDetails clickHouseDetails;

    @Data
    @Builder
    @NoArgsConstructor
    public static class ClickHouseDetails implements Serializable {
        String queryID;
        String fileName;
        String startdatetime;
        String owner;

        @JsonCreator
        public ClickHouseDetails(@JsonProperty("queryID") String queryID, @JsonProperty("fileName") String fileName,
                                 @JsonProperty("startdatetime") String startDT, @JsonProperty("owner") String owner) {
            this.queryID = queryID;
            this.fileName = fileName;
            this.startdatetime = startDT;
            this.owner = owner;
        }
    }
}

