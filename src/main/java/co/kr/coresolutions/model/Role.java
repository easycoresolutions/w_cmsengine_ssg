package co.kr.coresolutions.model;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    ROLE_ADMIN, ROLE_USER;

    public String getAuthority() {
        return name();
    }

    public Integer getCodeAuthority() {
        return name().equals(ROLE_ADMIN.name()) ? 1 : 0;
    }

}
