package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Load {
    @NotBlank
    private String table;
    @NotBlank
    private String filename;
    @NotNull
    private boolean appendmode;
    @NotNull
    private List<String> key;
    @NotBlank
    private String format;

}

