package co.kr.coresolutions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MessageBodySocket {
    @JsonProperty(required = true)
    private String script;
    @JsonProperty(required = true)
    private String owner;
    @JsonProperty(value = "idRequest")
    @Builder.Default
    private String idRequest = UUID.randomUUID().toString();
}
