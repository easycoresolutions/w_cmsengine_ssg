package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SessionDto {

    @JsonProperty(value = "ownerid", required = true)
    private String ownerId;

    @JsonProperty(value = "session_id_value")
    private String sessionId;

    @JsonProperty(value = "opened_time")
    private String openedTime;

    @JsonProperty(value = "last_updated_time")
    private String lastUpdatedTime;
}
