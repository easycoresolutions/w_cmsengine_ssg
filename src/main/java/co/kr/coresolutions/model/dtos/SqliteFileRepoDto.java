package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Builder
public class SqliteFileRepoDto {
    @JsonProperty(value = "db_file", required = true)
    private String dbFile;
    @JsonProperty(value = "userid", required = true)
    private String userID;
    @JsonProperty(value = "desc", required = true)
    private String desc;
}
