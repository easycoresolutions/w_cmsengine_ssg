package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DBPollingDto {
    @JsonProperty(value = "DP_ID", required = true)
    private String dpID;
    @JsonProperty(value = "DP_OUTTYPE", required = true)
    private String dpOutType;
    @JsonProperty(value = "DP_TARGET", required = true)
    private String dpTarget;
    @JsonProperty(value = "DP_QUERY", required = true)
    private String dpQuery;
    @JsonProperty(value = "DP_IN_CONNID", required = true)
    private String dpInConnId;
    @JsonProperty(value = "DP_TABLENAME", defaultValue = "")
    private String dpTableName;
    @JsonProperty(value = "DP_OUTMODE", defaultValue = "")
    private String dpOutMode;
    @JsonProperty(value = "DP_TABLE_KEY", defaultValue = "")
    private String dpTableKey;
    @JsonProperty(value = "DP_SEQ_FIELD", required = true)
    private String dpSqlField;
    @JsonProperty(value = "DP_LAST_SEQ", required = true)
    private String dpLastSeq;
    @JsonProperty(value = "OUT_CONNID", required = true)
    private String outConnId;
    @JsonProperty(value = "DP_MAX_RECORDS", required = true)
    private Integer dpMaxRecords;
    @JsonProperty(value = "DP_OUT_SEQ", required = true)
    private Integer dpOutSeq;
    @JsonProperty(value = "DP_FILE_SEP", defaultValue = "")
    private String dpFileSEP;
    @JsonProperty(value = "DP_FILE_REPEAT", defaultValue = "")
    private String dpFileRepeat;
    @JsonProperty(value = "DML_SQL", required = true)
    private String dmlSql;
}
