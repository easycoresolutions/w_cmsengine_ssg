package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SpawnOutputDto {
    @JsonProperty(value = "userid", required = true)
    private String userID;
    @JsonProperty(required = true)
    private String dir;
    @Valid
    private SpawnOutputDto.OutputInfo outputInfo;

    @Data
    public static class OutputInfo {
        String filename;
        String desc;
        String title;
        long seq;

        @JsonCreator
        public OutputInfo(@JsonProperty(value = "filename", required = true) String filename,
                          @JsonProperty(value = "desc", required = true) String desc,
                          @JsonProperty(value = "title", required = true) String title,
                          @JsonProperty(value = "seq", required = true) long seq) {
            this.filename = filename;
            this.desc = desc;
            this.title = title;
            this.seq = seq;
        }
    }


}
