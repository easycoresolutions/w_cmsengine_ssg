package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import static co.kr.coresolutions.commons.Monitor.ID;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Chart4Dto extends ID {
    @JsonProperty
    private ObjectNode prompt;
    @JsonProperty(value = "db_file")
    private String dbFile;
    @JsonProperty(required = true)
    private JsonNode template;
    @JsonProperty(required = true, value = "userid")
    private String userId;
    @JsonProperty(required = true)
    private String owner;
    @JsonProperty(required = true)
    private String sql;
    @JsonProperty
    private String dbms;
}
