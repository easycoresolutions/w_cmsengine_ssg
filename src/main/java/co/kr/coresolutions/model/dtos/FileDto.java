package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;


@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileDto {

    @NotBlank(message = "file content should not be empty")
    private String fileName;

    @NotBlank(message = "file content should not be empty")
    private String content;

    @NotBlank(message = "userId should not be empty")
    private String userId;

    @NotBlank(message = "ext should not be empty")
    private String ext;
}
