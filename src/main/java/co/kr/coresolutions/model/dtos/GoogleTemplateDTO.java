package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GoogleTemplateDTO {
    @JsonProperty(required = true)
    private String sheetId;
    @JsonProperty(required = true)
    private String sheetRange;
    @JsonProperty(required = true)
    private String sheetKey;
    @JsonProperty
    private String owner;
}
