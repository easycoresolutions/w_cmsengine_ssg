package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import static co.kr.coresolutions.commons.Monitor.ID;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CommandTemplateLiquid extends ID {
    @JsonProperty(required = true)
    private ObjectNode prompt;
    @JsonProperty(required = true)
    private JsonNode template;
    @JsonProperty(value = "commandfile", required = true)
    private String commandFile;
    @JsonProperty(value = "userid", required = true)
    private String userID;
}
