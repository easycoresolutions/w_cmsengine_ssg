package co.kr.coresolutions.model.dtos;

import co.kr.coresolutions.enums.EmailDeliveryType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class EmailSpawnDto {

    @JsonProperty(value = "connectionid", required = true)
    private String connectionId;

    @NotNull
    private List<String> to;

    @NotEmpty
    private String from;

    @NotNull
    private List<String> cc;

    @JsonProperty(required = true)
    private EmailDeliveryType deliveryType;

    @NotEmpty
    private String data;

    @NotEmpty
    private String userid;

    @JsonProperty(required = true)
    private String dir;

    @NotNull
    private String descFile;

    /*  {
	connectionid :"cafe24",
	to: ["msseo@coresolutions.co.kr"],
	from: "easy@coresolutions.co.kr",
	cc: [],
	data : "<h1> Hello ! </h1>",
	dir : "dir1",
	descFile : "myyououtput",
	"userid" : "admin",
 	"deliveryType" : "attached"
*/
}

