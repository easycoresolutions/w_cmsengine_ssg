package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContainerProjectDto {
    @JsonProperty(value = "containerprojectID", required = true)
    @NotBlank
    private String containerProjectID;
    @JsonProperty(value = "containerprojectName", required = true)
    private String containerProjectName;
    @JsonProperty(value = "containerprojectOwner", required = true)
    private String containerProjectOwner;
    @JsonProperty(value = "containerprojectCreated", required = true)
    private String containerProjectCreated;
    @JsonProperty(value = "containerprojectModified", required = true)
    private String containerProjectModified;
    @JsonProperty(value = "containerprojectDesc", required = true)
    private String containerProjectDesc;
    @JsonProperty(value = "containerprojectUserid", required = true)
    private String containerProjectUserid;
    @JsonProperty(value = "containerprojectCategory", required = true)
    private String containerProjectCategory;
    @JsonProperty(value = "containerprojectVersion", required = true)
    private String containerProjectVersion;
    @JsonProperty(value = "containerprojectjson")
    private ObjectNode containerProjectJson;
}
