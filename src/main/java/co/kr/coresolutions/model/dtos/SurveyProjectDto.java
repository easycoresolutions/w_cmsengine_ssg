package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SurveyProjectDto {
    @JsonProperty(value = "surveyprojectID", required = true)
    @NotBlank
    private String surveyProjectID;
    @JsonProperty(value = "surveyprojectName", required = true)
    private String surveyProjectName;
    @JsonProperty(value = "surveyprojectOwner", required = true)
    private String surveyProjectOwner;
    @JsonProperty(value = "surveyprojectCreated", required = true)
    private String surveyProjectCreated;
    @JsonProperty(value = "surveyprojectModified", required = true)
    private String surveyProjectModified;
    @JsonProperty(value = "surveyprojectDesc", required = true)
    private String surveyProjectDesc;
    @JsonProperty(value = "surveyprojectUserid", required = true)
    private String surveyProjectUserid;
    @JsonProperty(value = "surveyprojectCategory", required = true)
    private String surveyProjectCategory;
    @JsonProperty(value = "surveyprojectVersion", required = true)
    private String surveyProjectVersion;
    @JsonProperty(value = "surveyprojectjson")
    private ObjectNode surveyProjectJson;
}
