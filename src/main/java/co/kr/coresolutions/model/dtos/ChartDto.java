package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ChartDto {
    @JsonProperty(required = true)
    private String sql;
    @JsonProperty(required = true, value = "userid")
    private String userId;
    @JsonProperty(required = true)
    private String owner;
    @JsonProperty(required = true)
    private String chartFile;
    @JsonProperty(required = true)
    private JsonNode parameter;
    @JsonProperty(required = true, value = "data_parameter")
    @Valid
    private DataParameter dataParameter;

    @Data
    public static class DataParameter {
        String dataType;
        List<String> XAxis;
        List<String> YAxis;
        String Category;

        @JsonCreator
        public DataParameter(@JsonProperty("dataType") String dataType, @JsonProperty("XAXIS") List<String> XAxis,
                             @JsonProperty("YAXIS") List<String> YAxis, @JsonProperty("CATEGORY") String Category) {
            this.dataType = dataType;
            this.XAxis = XAxis;
            this.YAxis = YAxis;
            this.Category = Category;
        }
    }
}
