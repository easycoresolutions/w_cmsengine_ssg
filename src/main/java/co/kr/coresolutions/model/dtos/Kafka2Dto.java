package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Kafka2Dto {
    @JsonProperty(value = "connectionid", required = true)
    private String connectionID;
    @JsonProperty(required = true)
    private String sql;
    @JsonProperty(required = true)
    private String owner;
}
