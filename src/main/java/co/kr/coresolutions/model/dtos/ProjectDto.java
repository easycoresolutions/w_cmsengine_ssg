package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.JSONObject;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ProjectDto extends Chart2Dto {
    @JsonProperty(required = true)
    private String projectID;
    @JsonProperty(value = "projectname")
    private String projectName;
    @JsonProperty(value = "projectcategory")
    private String projectCategory;
    @JsonProperty(value = "projecttype")
    private String projectType;
    @JsonProperty(value = "projectowner")
    private String projectOwner;
    @JsonProperty(value = "projectcreated")
    private String projectCreated;
    @JsonProperty(value = "projectmodified")
    private String projectModified;
    @JsonProperty(value = "projectdesc")
    private String projectDesc;
    @JsonProperty(required = true, value = "projectVersion")
    private String projectVersion;
    @JsonProperty(required = true, value = "projectjson")
    private JSONObject projectJson;
}
