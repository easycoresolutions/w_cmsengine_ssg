package co.kr.coresolutions.model.dtos;

import co.kr.coresolutions.enums.OutputType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LazyCommandDto {
    @JsonProperty(value = "queuename", required = true)
    private String queueName;
    @JsonProperty(value = "userid", required = true)
    private String userId;
    @JsonProperty(required = true)
    private String owner;
    @JsonProperty(value = "exec_datetime", required = true)
    private String execDateTime;
    @JsonProperty(value = "Command", required = true)
    private String command;

    @JsonProperty(value = "Output", required = true)
    private OutputType output;

    @JsonProperty(value = "CHAR")
    private List<String> chars;

    @JsonProperty(value = "Alert", defaultValue = "false")
    private boolean alert;

    private String requestedDateTime;
    private ObjectNode parameter;
}
