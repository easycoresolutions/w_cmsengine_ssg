package co.kr.coresolutions.model.dtos;

import co.kr.coresolutions.enums.DelayType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DelayDto {
    @JsonProperty(required = true)
    private DelayType delayType;
    @JsonProperty(required = true)
    private String delayDay;
    @JsonProperty(required = true)
    private String delayTime;
}
