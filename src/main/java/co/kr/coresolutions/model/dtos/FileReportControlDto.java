package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileReportControlDto {
    @JsonProperty(value = "GETDATA")
    private JsonNode getData;

    @JsonProperty(value = "CHECK_EXIST1")
    private JsonNode checkExists1;

    @JsonProperty(value = "CREATE_REPORT")
    private JsonNode createReport;

    @JsonProperty(value = "CHECK_EXIST2")
    private JsonNode checkExists2;

    @JsonProperty(value = "SEND_EMAIL")
    private JsonNode sendEmail;

    @JsonProperty(value = "CHECK_EXIST3")
    private JsonNode checkExists3;
}
