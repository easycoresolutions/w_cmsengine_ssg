package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class LiquidTemplateQueryDTO {
    @JsonProperty(value = "connectionid", required = true)
    private String connectionID;
    @JsonProperty(required = true)
    private String sql;
    @JsonProperty(required = true)
    private List<String> ids;
    @JsonProperty(required = true)
    private String separator;
    @JsonProperty
    private String owner;
}
