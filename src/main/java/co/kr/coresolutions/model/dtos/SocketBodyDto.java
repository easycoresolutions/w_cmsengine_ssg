package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SocketBodyDto {
    @JsonProperty(required = true, value = "UserId")
    private String userId;
    @JsonProperty(required = true, value = "Message")
    private String message;
    @JsonProperty(required = true, value = "Key")
    private String key;
}
