package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SchemaDto {
    @JsonProperty(required = true, value = "schema_id")
    private String schemaID;
    @JsonProperty(required = true, value = "schema_name")
    private String schemaName;
    @JsonProperty(required = true, value = "schema_seq")
    private Integer schemaSeq;
    @JsonProperty(value = "sechema_desc", required = true)
    private String schemaDesc;
    @JsonProperty(required = true)
    private String owner;
    @JsonProperty(required = true)
    private String shared;
    @JsonProperty(required = true, value = "del_f")
    private String delF;
    @JsonProperty(required = true)
    @Valid
    @Size(min = 1)
    private List<SchemaAuth> auth;
    @JsonProperty(required = true, value = "schema-json")
    private JsonNode schemaJson;

    @Data
    public static class SchemaAuth {
        String authID;

        @JsonCreator
        public SchemaAuth(@JsonProperty("auth_id") String authID) {
            this.authID = authID;
        }
    }
}
