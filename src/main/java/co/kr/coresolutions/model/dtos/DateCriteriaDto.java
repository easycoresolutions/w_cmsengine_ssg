package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DateCriteriaDto {
    @JsonProperty
    private Boolean validateHoliday;
    @JsonProperty
    @Valid
    private Month month;
    @JsonProperty
    @Valid
    private Week week;

    @Data
    public static class Month {
        public Boolean jan;
        public Boolean feb;
        public Boolean mar;
        public Boolean apr;
        public Boolean may;
        public Boolean jun;
        public Boolean jul;
        public Boolean aug;
        public Boolean sep;
        public Boolean oct;
        public Boolean nov;
        public Boolean dec;

        @JsonCreator
        public Month(@JsonProperty("jan") Boolean jan, @JsonProperty("feb") Boolean feb, @JsonProperty("mar") Boolean mar, @JsonProperty("apr") Boolean apr,
                     @JsonProperty("may") Boolean may, @JsonProperty("jun") Boolean jun, @JsonProperty("jul") Boolean jul, @JsonProperty("aug") Boolean aug,
                     @JsonProperty("sep") Boolean sep, @JsonProperty("oct") Boolean oct, @JsonProperty("nov") Boolean nov, @JsonProperty("dec") Boolean dec) {
            this.jan = jan;
            this.feb = feb;
            this.mar = mar;
            this.apr = apr;
            this.may = may;
            this.jun = jun;
            this.jul = jul;
            this.aug = aug;
            this.sep = sep;
            this.oct = oct;
            this.nov = nov;
            this.dec = dec;
        }
    }

    @Data
    public static class Week {
        public Boolean sun;
        public Boolean mon;
        public Boolean tue;
        public Boolean wed;
        public Boolean thu;
        public Boolean fri;
        public Boolean sat;

        @JsonCreator
        public Week(@JsonProperty("sun") Boolean sun, @JsonProperty("mon") Boolean mon, @JsonProperty("tue") Boolean tue, @JsonProperty("wed") Boolean wed,
                    @JsonProperty("thu") Boolean thu, @JsonProperty("fri") Boolean fri, @JsonProperty("sat") Boolean sat) {
            this.sun = sun;
            this.mon = mon;
            this.tue = tue;
            this.wed = wed;
            this.thu = thu;
            this.fri = fri;
            this.sat = sat;
        }
    }

}
