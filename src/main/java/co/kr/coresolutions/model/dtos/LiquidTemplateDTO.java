package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class LiquidTemplateDTO {
    @JsonProperty(required = true)
    private ObjectNode replace;
    @JsonProperty(value = "connectionid", required = true)
    private String connectionID;
    @JsonProperty(required = true)
    private String profile;
    @JsonProperty(required = true)
    private String recommendations;
    @JsonProperty(required = true)
    private List<String> ids;
    @JsonProperty(required = true)
    private String separator;
    @JsonProperty
    private String owner;
}
