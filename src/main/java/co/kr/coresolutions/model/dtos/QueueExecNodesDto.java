package co.kr.coresolutions.model.dtos;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class QueueExecNodesDto extends CommandTemplate {
    @JsonProperty(required = true)
    private String command;
    @Valid
    @JsonProperty(required = true)
    private NodeDto execnodes;
}
