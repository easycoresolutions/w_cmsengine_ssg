package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;

import static co.kr.coresolutions.commons.Monitor.ID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Chart2Dto extends ID {
    @JsonProperty(required = true)
    private ObjectNode prompt;
    @JsonProperty(required = true, value = "userid")
    private String userId;
    @JsonProperty(required = true)
    private String owner;
    @JsonProperty(required = true)
    private String chartFile;
    @JsonProperty
    private String queryFile;
    @JsonProperty
    private String dir;
    @JsonProperty
    private String dbms;
    @JsonProperty(required = true)
    private JsonNode parameter;
    @JsonProperty(required = true, value = "data_parameter")
    @Valid
    private ChartDto.DataParameter dataParameter;
}
