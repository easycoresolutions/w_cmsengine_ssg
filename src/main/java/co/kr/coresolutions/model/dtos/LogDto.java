package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LogDto {
    @JsonProperty(required = true)
    private String message;
    @JsonProperty(value = "userid", required = true)
    private String userID;
    @JsonProperty(value = "message-key", required = true)
    private String messageKey;
}
