package co.kr.coresolutions.model.dtos;

import co.kr.coresolutions.enums.OutputType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CommandDto {

    @NotEmpty
    @JsonProperty(value = "Command")
    private String command;

    @NotNull
    @JsonProperty(value = "Output")
    private OutputType output;

    private String CommandId;

    @JsonProperty(value = "CHAR")
    private List<String> chars;

    @JsonProperty(value = "Alert", defaultValue = "false")
    private boolean alert;

    private ObjectNode parameter;
}

