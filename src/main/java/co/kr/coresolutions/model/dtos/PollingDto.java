package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PollingDto {
    @JsonProperty(value = "DP_ID", required = true)
    private String dpID;
    @JsonProperty(value = "CONNECTION_ID", required = true)
    private String connectionID;
    @JsonProperty(value = "user", required = true)
    private String user;
    @JsonProperty(value = "DP_OUT_SEQ")
    private Integer dpOutSeq;
    @JsonProperty(value = "replace_tablename")
    private JsonNode replaceTableName;
}
