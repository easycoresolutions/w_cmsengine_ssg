package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SurveyResultDto {
    @JsonProperty(value = "surveyresultID", required = true)
    @NotBlank
    private String surveyProjectID;
    @JsonProperty(value = "surveyresultName", required = true)
    private String surveyProjectName;
    @JsonProperty(value = "surveyresultOwner", required = true)
    private String surveyProjectOwner;
    @JsonProperty(value = "surveyresultCreated", required = true)
    private String surveyProjectCreated;
    @JsonProperty(value = "surveyresultModified", required = true)
    private String surveyProjectModified;
    @JsonProperty(value = "surveyresultDesc", required = true)
    private String surveyProjectDesc;
    @JsonProperty(value = "surveyresultUserid", required = true)
    private String surveyProjectUserid;
    @JsonProperty(value = "surveyresultCategory", required = true)
    private String surveyProjectCategory;
    @JsonProperty(value = "surveyresultVersion", required = true)
    private String surveyProjectVersion;
    @JsonProperty(value = "surveyresultjson")
    private ObjectNode surveyProjectJson;
}
