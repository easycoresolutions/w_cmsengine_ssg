package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;


@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileControlDto {

    private String file;

    @NotBlank(message = "owner should not be empty")
    private String owner;

    @JsonProperty(value = "control-filename")
    private String controlFilename;

    @JsonProperty(value = "report-dir", required = true)
    private String reportDir;
}
