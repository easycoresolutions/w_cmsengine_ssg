package co.kr.coresolutions.model.dtos;

import co.kr.coresolutions.commons.ConditionalOneExist;
import co.kr.coresolutions.enums.QueuePushLevel;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ConditionalOneExist(firstSelected = "script", secondSelected = "file")
public class SocketDto {
    @JsonProperty(required = true)
    private String owner;
    @JsonProperty(required = true, value = "userid")
    private String userID;
    @JsonProperty
    private String script;
    @JsonProperty
    private String file;
    private String format;
    private boolean outputEnabled;
    @Valid
    private OutputInfo outputInfo;
    @JsonProperty(value = "queryid")
    private String queryID;
    @JsonProperty(value = "queuePushed")
    private QueuePushLevel queuePushLevel;
    @JsonProperty(value = "idRequest")
    private String idRequest;

    @Data
    public static class OutputInfo {
        String dir;
        String filename;
        String ext;
        String desc;
        String title;
        long seq;

        @JsonCreator
        public OutputInfo(@JsonProperty(value = "dir", required = true) String dir,
                          @JsonProperty(value = "filename", required = true) String filename,
                          @JsonProperty(value = "ext", required = true) String ext,
                          @JsonProperty(value = "desc", required = true) String desc,
                          @JsonProperty(value = "title", required = true) String title,
                          @JsonProperty(value = "seq", required = true) long seq) {
            this.dir = dir;
            this.filename = filename;
            this.ext = ext;
            this.desc = desc;
            this.title = title;
            this.seq = seq;
        }
    }


}
