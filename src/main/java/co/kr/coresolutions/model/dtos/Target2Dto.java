package co.kr.coresolutions.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class Target2Dto {
    @JsonProperty(required = true)
    @NotEmpty(message = "file should not be empty")
    private String file;
    @JsonProperty(value = "userid", required = true)
    @NotEmpty(message = "userid should not be empty")
    private String userID;
    @JsonProperty
    private String owner;
    @JsonProperty(required = true)
    @NotEmpty(message = "connection should not be empty")
    private String connection;
    @JsonProperty(required = true)
    @Valid
    private TargetDto.Parameter parameters;
}
