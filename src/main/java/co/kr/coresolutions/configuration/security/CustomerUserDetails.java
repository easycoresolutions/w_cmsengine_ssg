package co.kr.coresolutions.configuration.security;

import co.kr.coresolutions.jpa.domain.Role;
import co.kr.coresolutions.jpa.domain.User;
import co.kr.coresolutions.service.QueryService;
import co.kr.coresolutions.service.QueryServiceSession;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
@RequestScope
public class CustomerUserDetails implements UserDetailsService {
    private final QueryService queryService;
    private final QueryServiceSession queryServiceSession;
    private final ObjectMapper objectMapper;
    private final PasswordEncoder passwordEncoder;
    private final HttpServletRequest httpServletRequest;

    @Override
    public UserDetails loadUserByUsername(String username) {

        try {
            JsonNode jsonNodeConfig = queryServiceSession.readFile();
            final String[] resultSelect = {""};
            final String[] connectionID = {""};
                final UserDetails[] userDetails = new UserDetails[1];
            final boolean[] authorizationOption = {true};
            Optional.ofNullable(jsonNodeConfig)
                        .ifPresent(jsonNode -> {
                            if (jsonNode.hasNonNull("authorizationOption") && jsonNode.get("authorizationOption").isBoolean()) {
                                authorizationOption[0] = jsonNode.get("authorizationOption").asBoolean();
                                if (!authorizationOption[0]) {
                                    userDetails[0] = org.springframework.security.core.userdetails.User
                                            .withUsername(username)
                                            .password(passwordEncoder.encode("++Core2018."))
                                            .roles(Role.ROLE_USER.getName())
                                            .build();
                                }
                            }
                            if (!httpServletRequest.getServletPath().startsWith("/auth/") && authorizationOption[0]) {
                                if (jsonNode.hasNonNull("auth_ignored") && jsonNode.get("auth_ignored").isArray()) {
                                    List<String> list = StreamSupport
                                            .stream(jsonNode.get("auth_ignored").spliterator(), false).map(JsonNode::asText).collect(Collectors.toList());
                                    if (list.contains(username)) {
                                        userDetails[0] = org.springframework.security.core.userdetails.User
                                                .withUsername(username)
                                                .password(passwordEncoder.encode("++Core2018."))
                                                .roles(Role.ROLE_USER.getName())
                                                .build();
                                    }
                                }
                            }
                        });

                if (userDetails[0] != null) {
                    return userDetails[0];
                }

            Optional.ofNullable(jsonNodeConfig)
                    .ifPresent(jsonNode -> {
                        if (jsonNode.hasNonNull("LogonConnectionId") && jsonNode.get("LogonConnectionId").isTextual()) {
                            connectionID[0] = jsonNode.get("LogonConnectionId").asText();
                                resultSelect[0] = queryService
                                        .checkValidity("SELECT * FROM " + queryService.getInfoConnection(connectionID[0]).getSCHEME() + "T_EMP WHERE EMP_ID = '" + username + "'",
                                                connectionID[0], "", "", false);
                        }
                    });

            if (!(resultSelect[0].equalsIgnoreCase(connectionID[0]) || resultSelect[0].startsWith("Error") || resultSelect[0].startsWith("error"))) {
                List<User> users = Arrays.asList(objectMapper.readValue(resultSelect[0], User[].class));
                User user = users.stream().findFirst().orElseThrow(() -> new UsernameNotFoundException("Emp not found with username\t" + username));
                return org.springframework.security.core.userdetails.User
                        .withUsername(username)
                        .password(user.getUserpw())
                        .roles(/*user.getAccessLevel().equals("0") ?*/ /*Role.ROLE_USER.getName() : */Role.ROLE_ADMIN.getName())
                        .build();
            }
        } catch (IOException e) {
            throw new UsernameNotFoundException("Emp not found with username\t" + username);
        }
        throw new UsernameNotFoundException("Emp not found with username\t" + username);
    }

}
